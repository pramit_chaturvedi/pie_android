package com.ybs.countrypicker;

public class CountryDataModel {
    private String name,code,dialCode;
    private int flagDrawableResId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDialCode() {
        return dialCode;
    }

    public void setDialCode(String dialCode) {
        this.dialCode = dialCode;
    }

    public int getFlagDrawableResId() {
        return flagDrawableResId;
    }

    public void setFlagDrawableResId(int flagDrawableResId) {
        this.flagDrawableResId = flagDrawableResId;
    }
}
