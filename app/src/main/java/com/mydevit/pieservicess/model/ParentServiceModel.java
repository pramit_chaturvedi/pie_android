package com.mydevit.pieservicess.model;

import java.util.List;

public class ParentServiceModel{
    private String name;
    private List<ServiceModel> serviceModelList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ServiceModel> getServiceModelList() {
        return serviceModelList;
    }

    public void setServiceModelList(List<ServiceModel> serviceModelList) {
        this.serviceModelList = serviceModelList;
    }
}
