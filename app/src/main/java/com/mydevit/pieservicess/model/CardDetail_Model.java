package com.mydevit.pieservicess.model;

public class CardDetail_Model {
    private String name;
    private String mailid;
    private String cardNo;
    private String expiryMonth;
    private String expiryYear;
    private String cvvString;


    public String getExpiryMonth() {
        return expiryMonth;
    }

    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    public String getExpiryYear() {
        return expiryYear;
    }

    public void setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMailid() {
        return mailid;
    }

    public void setMailid(String mailid) {
        this.mailid = mailid;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }


    public String getCvvString() {
        return cvvString;
    }

    public void setCvvString(String cvvString) {
        this.cvvString = cvvString;
    }
}
