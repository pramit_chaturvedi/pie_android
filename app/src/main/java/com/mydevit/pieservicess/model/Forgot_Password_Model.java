package com.mydevit.pieservicess.model;

public class Forgot_Password_Model {
    private String codeGorFromEmail,password,confirmedPassword;

    public String getCodeGorFromEmail() {
        return codeGorFromEmail;
    }

    public void setCodeGorFromEmail(String codeGorFromEmail) {
        this.codeGorFromEmail = codeGorFromEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmedPassword() {
        return confirmedPassword;
    }

    public void setConfirmedPassword(String confirmedPassword) {
        this.confirmedPassword = confirmedPassword;
    }
}
