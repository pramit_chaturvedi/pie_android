package com.mydevit.pieservicess.model;

import com.mydevit.pieservicess.service.ViewScheduleModel;

import java.util.List;

public class ServiceModel {
    List<ViewScheduleModel.DataBean.ServiceDataBean> stringArrayList;
    private String timeSlot;
    private String personal_Id;
    private String client_Id;

    public String getClient_Id() {
        return client_Id;
    }

    public void setClient_Id(String client_Id) {
        this.client_Id = client_Id;
    }

    private String service_duration;
    private String appointment_id;


    public String getAppointment_id() {
        return appointment_id;
    }

    public void setAppointment_id(String appointment_id) {
        this.appointment_id = appointment_id;
    }

    public String getService_duration() {
        return service_duration;
    }

    public void setService_duration(String service_duration) {
        this.service_duration = service_duration;
    }

    public String getPersonal_Id() {
        return personal_Id;
    }

    public void setPersonal_Id(String personal_Id) {
        this.personal_Id = personal_Id;
    }

    public List<ViewScheduleModel.DataBean.ServiceDataBean> getStringArrayList() {
        return stringArrayList;
    }

    public void setStringArrayList(List<ViewScheduleModel.DataBean.ServiceDataBean> stringArrayList) {
        this.stringArrayList = stringArrayList;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }
}
