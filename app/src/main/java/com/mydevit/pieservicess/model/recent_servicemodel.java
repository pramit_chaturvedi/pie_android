package com.mydevit.pieservicess.model;

import java.util.List;

public class recent_servicemodel {

    /**
     * status : success
     * status_code : 200
     * message : Business Activity Show Successfully
     * data : [{"id":"13","name":"Matrix Styles edit","image_url":"https://storage.googleapis.com/piesvc_gcp/public/front/images/user_placeholder.jpg","log_name":"APP Template logo removed","log_description":"Business removed the app template logo","created_at":"2019-06-28 06:31:19"},{"id":"13","name":"Matrix Styles edit","image_url":"https://storage.googleapis.com/piesvc_gcp/public/front/images/user_placeholder.jpg","log_name":"Web Template updated","log_description":"Business updated the web template","created_at":"2019-09-06 07:30:14"},{"id":"13","name":"Matrix Styles edit","image_url":"https://storage.googleapis.com/piesvc_gcp/public/front/images/user_placeholder.jpg","log_name":"Web Template updated","log_description":"Business updated the web template","created_at":"2019-09-06 07:32:15"},{"id":"13","name":"Matrix Styles edit","image_url":"https://storage.googleapis.com/piesvc_gcp/public/front/images/user_placeholder.jpg","log_name":"POS Launched","log_description":"Business launched POS for location \"Style Location\"","created_at":"2019-09-06 10:39:40"},{"id":"13","name":"Matrix Styles edit","image_url":"https://storage.googleapis.com/piesvc_gcp/public/front/images/user_placeholder.jpg","log_name":"Business added payment mode in POS","log_description":"Business added payment setting mode in POS for location \"Style Location\"","created_at":"2019-09-06 10:40:08"},{"id":"7","name":"Martha","image_url":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg","log_name":"Personnels tab POS","log_description":"Personnel clicked personnels tab in POS for location \"New Tempa\"","created_at":"2019-12-04 12:22:45"},{"id":"15","name":"Charlie","image_url":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/cfd6fbda410e8ad157d1dbb319df1a25.jpg","log_name":"POS Launched","log_description":"Business launched POS for location \"New Tempa\"","created_at":"2019-12-02 10:43:42"},{"id":"20","name":"Jevel Santhana","image_url":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/1e42ebad59e070c146387f8a8dce40f4.jpg","log_name":"Web Template saved","log_description":"Business added the web template","created_at":"2019-10-25 06:41:07"}]
     */

    private String status;
    private String status_code;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 13
         * name : Matrix Styles edit
         * image_url : https://storage.googleapis.com/piesvc_gcp/public/front/images/user_placeholder.jpg
         * log_name : APP Template logo removed
         * log_description : Business removed the app template logo
         * created_at : 2019-06-28 06:31:19
         */

        private String id;
        private String name;
        private String image_url;
        private String log_name;
        private String log_description;
        private String created_at;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public String getLog_name() {
            return log_name;
        }

        public void setLog_name(String log_name) {
            this.log_name = log_name;
        }

        public String getLog_description() {
            return log_description;
        }

        public void setLog_description(String log_description) {
            this.log_description = log_description;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }
    }
}
