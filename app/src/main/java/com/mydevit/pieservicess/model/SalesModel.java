package com.mydevit.pieservicess.model;

import java.util.List;

import lecho.lib.hellocharts.model.SliceValue;

public class SalesModel {
    boolean isSelected;
    String nestSalary,newSustomer;
    List<SliceValue> pieData;

    public List<SliceValue> getPieData() {
        return pieData;
    }

    public void setPieData(List<SliceValue> pieData) {
        this.pieData = pieData;
    }

    public String getNestSalary() {
        return nestSalary;
    }

    public void setNestSalary(String nestSalary) {
        this.nestSalary = nestSalary;
    }

    public String getNewSustomer() {
        return newSustomer;
    }

    public void setNewSustomer(String newSustomer) {
        this.newSustomer = newSustomer;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
