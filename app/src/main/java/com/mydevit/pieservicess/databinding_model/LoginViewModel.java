package com.mydevit.pieservicess.databinding_model;

import android.content.Context;
import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.utils.AlphaHolder;

import java.util.regex.Pattern;


public class LoginViewModel extends ViewModel {

    public MutableLiveData<String> errorPassword = new MutableLiveData<>();
    public MutableLiveData<String> errorEmail = new MutableLiveData<>();

    public MutableLiveData<String> email = new MutableLiveData<>();
    public MutableLiveData<String> password = new MutableLiveData<>();
    public MutableLiveData<Integer> busy;
    private MutableLiveData<LoginUser> userMutableLiveData;


    public LoginViewModel() {

    }

    public static boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public MutableLiveData<Integer> getBusy() {

        if (busy == null) {
            busy = new MutableLiveData<>();
            busy.setValue(8);
        }

        return busy;
    }

    public LiveData<LoginUser> getUser() {
        if (userMutableLiveData == null) {
            userMutableLiveData = new MutableLiveData<>();
        }
       return userMutableLiveData;
    }

    public void onLoginClicked(Context context) {
        //getBusy().setValue(0); //View.VISIBLE
        LoginUser user = new LoginUser(email.getValue(), password.getValue());
        if (TextUtils.isEmpty(user.getmEmail())) {
            AlphaHolder.customToast(context, context.getResources().getString(R.string.emailmustrequire));
        } else if (TextUtils.isEmpty(user.getmPassword())) {
            AlphaHolder.customToast(context, context.getResources().getString(R.string.passwordmustrequire));
        } else if (!isValidEmailId(user.getmEmail())) {
            AlphaHolder.customToast(context, context.getResources().getString(R.string.emailmustvalid));
        } else {
            userMutableLiveData.setValue(user);
        }
    }
}
