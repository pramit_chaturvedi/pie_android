package com.mydevit.pieservicess.databinding_model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class Bill_Model extends ViewModel {

    public MutableLiveData<String> promocodeString = new MutableLiveData<>();
    public MutableLiveData<Bill> billMutableLiveData = new MutableLiveData<>();

    public LiveData<Bill> getPromoCode() {
        billMutableLiveData = new MutableLiveData<>();
        return billMutableLiveData;
    }

    public void applyClicked() {
        Bill bill = new Bill(promocodeString.getValue());
        billMutableLiveData.setValue(bill);
    }

    public class Bill {
        String promoCode;

        public Bill(String promoCode) {
            this.promoCode = promoCode;
        }

        public String getPromoCode() {
            return promoCode;
        }
    }
}
