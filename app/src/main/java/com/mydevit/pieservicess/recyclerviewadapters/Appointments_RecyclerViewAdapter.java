package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.AppointmentsLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.model.SelectedAppointment_Model;
import com.mydevit.pieservicess.service.appointment_personal_model;

import java.util.List;

public class Appointments_RecyclerViewAdapter extends RecyclerView.Adapter<Appointments_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<appointment_personal_model.DataBean.MainAppointmentData> appointmentsDataArrayList;
    List<SelectedAppointment_Model> selectedAppointment_List;
    ClickListener clickListener;

    public Appointments_RecyclerViewAdapter(Context context, List<appointment_personal_model.DataBean.MainAppointmentData> appointmentsDataArrayList, List<SelectedAppointment_Model> selectedAppointment_List, ClickListener clickListener) {
        this.context = context;
        this.appointmentsDataArrayList = appointmentsDataArrayList;
        this.clickListener = clickListener;
        this.selectedAppointment_List = selectedAppointment_List;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AppointmentsLayoutBinding appointmentsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.appointments_layout, parent, false);
        return new CustomViewHolder(appointmentsLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((appointment_personal_model.DataBean.MainAppointmentData) getItem(position), (SelectedAppointment_Model) getItemChecked(position));

    }

    @Override
    public int getItemCount() {
        return appointmentsDataArrayList.size();
    }

    public Object getItem(int position) {
        return appointmentsDataArrayList.get(position);
    }

    public Object getItemChecked(int position) {
        return selectedAppointment_List.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        AppointmentsLayoutBinding appointmentsLayoutBinding;

        public CustomViewHolder(AppointmentsLayoutBinding appointmentsLayoutBinding) {
            super(appointmentsLayoutBinding.getRoot());
            this.appointmentsLayoutBinding = appointmentsLayoutBinding;
        }

        public void notifyViewBinding(appointment_personal_model.DataBean.MainAppointmentData dataBean,SelectedAppointment_Model selectedAppointment_model) {
            appointmentsLayoutBinding.setVariable(BR.personalAppointmentModel, dataBean);
            appointmentsLayoutBinding.setVariable(BR.clickListener, clickListener);
            appointmentsLayoutBinding.setVariable(BR.isChecked, selectedAppointment_model);
            appointmentsLayoutBinding.setVariable(BR.position, this.getPosition());
        }
    }
}
