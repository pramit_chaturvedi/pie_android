package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ScheduleTimeChildSlotLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.service.ConfigureAvailabiltyModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ScheduleTimeSlot_Child_RecyclerViewAdapter extends RecyclerView.Adapter<ScheduleTimeSlot_Child_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean.DayTimingsBean> personnelTimingsBeanList;
    ClickListenerPC clickListener;
    int parentPositionTop;
    SharedPreferences mPrefs;
    ArrayList<String> timeSlotList;


    public ScheduleTimeSlot_Child_RecyclerViewAdapter(Context context, int parentPosition, List<ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean.DayTimingsBean> personnelTimingsBeanList, ClickListenerPC clickListener, ArrayList<String> timeSlotList) {
        this.context = context;
        this.personnelTimingsBeanList = personnelTimingsBeanList;
        this.clickListener = clickListener;
        this.parentPositionTop = parentPosition;
        this.timeSlotList = timeSlotList;
        // AlphaHolder.customToast(context, timeSlotList.get(0));
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ScheduleTimeChildSlotLayoutBinding appointmentsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.schedule_time_child_slot_layout, parent, false);
        return new CustomViewHolder(appointmentsLayoutBinding);

    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean.DayTimingsBean) getItem(position));

    }

    @Override
    public int getItemCount() {
        return personnelTimingsBeanList.size();
    }

    public Object getItem(int position) {
        return personnelTimingsBeanList.get(position);
    }

    public List<String> getCompaniesList(String key) {
        if (mPrefs != null) {
            Gson gson = new Gson();
            List<String> companyList;

            String string = mPrefs.getString(key, null);
            Type type = new TypeToken<List<String>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return null;
    }

    public void virtualKeyboard(AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setFocusable(false);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListenerPC {
        ScheduleTimeChildSlotLayoutBinding appointmentsLayoutBinding;

        public CustomViewHolder(ScheduleTimeChildSlotLayoutBinding appointmentsLayoutBinding) {
            super(appointmentsLayoutBinding.getRoot());
            this.appointmentsLayoutBinding = appointmentsLayoutBinding;
        }

        public void notifyViewBinding(ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean.DayTimingsBean dataBean) {
            appointmentsLayoutBinding.setVariable(BR.scheudleDataChildSlot, dataBean);
            appointmentsLayoutBinding.setVariable(BR.clickListener, this);
            appointmentsLayoutBinding.setVariable(BR.parentPosition, parentPositionTop);
            appointmentsLayoutBinding.setVariable(BR.childPosition, getPosition());

            appointmentsLayoutBinding.startSpinnerId.setAdapter(new ArrayAdapter<String>(context, R.layout.spinner_layout, timeSlotList));
            appointmentsLayoutBinding.endSpinnerId.setAdapter(new ArrayAdapter<String>(context, R.layout.spinner_layout, timeSlotList));
            appointmentsLayoutBinding.endSpinnerId.setThreshold(300);
            appointmentsLayoutBinding.startSpinnerId.setThreshold(300);


            appointmentsLayoutBinding.startSpinnerId.setOnTouchListener(new View.OnTouchListener() {
                int CLICK_ACTION_THRESHOLD = 200;
                float startX;
                float startY;

                @Override
                public boolean onTouch(View view, MotionEvent event) {


                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            startX = event.getX();
                            startY = event.getY();
                            break;
                        case MotionEvent.ACTION_UP:
                            float endX = event.getX();
                            float endY = event.getY();
                            if (isAClick(startX, endX, startY, endY)) {

                                appointmentsLayoutBinding.startSpinnerId.showDropDown();
                                virtualKeyboard(appointmentsLayoutBinding.startSpinnerId);

                            }
                            break;
                    }
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }

                private boolean isAClick(float startX, float endX, float startY, float endY) {
                    float differenceX = Math.abs(startX - endX);
                    float differenceY = Math.abs(startY - endY);
                    return !(differenceX > CLICK_ACTION_THRESHOLD/* =5 */ || differenceY > CLICK_ACTION_THRESHOLD);
                }
            });

            appointmentsLayoutBinding.endSpinnerId.setOnTouchListener(new View.OnTouchListener() {

                int CLICK_ACTION_THRESHOLD = 200;
                float startX;
                float startY;

                @Override
                public boolean onTouch(View view, MotionEvent event) {


                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            startX = event.getX();
                            startY = event.getY();
                            break;
                        case MotionEvent.ACTION_UP:
                            float endX = event.getX();
                            float endY = event.getY();
                            if (isAClick(startX, endX, startY, endY)) {

                                appointmentsLayoutBinding.endSpinnerId.showDropDown();
                                virtualKeyboard(appointmentsLayoutBinding.endSpinnerId);

                            }
                            break;
                    }
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }

                private boolean isAClick(float startX, float endX, float startY, float endY) {
                    float differenceX = Math.abs(startX - endX);
                    float differenceY = Math.abs(startY - endY);
                    return !(differenceX > CLICK_ACTION_THRESHOLD/* =5 */ || differenceY > CLICK_ACTION_THRESHOLD);
                }
            });
            appointmentsLayoutBinding.startSpinnerId.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    personnelTimingsBeanList.get(getPosition()).setOpen_time(appointmentsLayoutBinding.startSpinnerId.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


            appointmentsLayoutBinding.endSpinnerId.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //     Toast.makeText(context, "" + getPosition(), Toast.LENGTH_SHORT).show();
                    personnelTimingsBeanList.get(getPosition()).setClose_time(appointmentsLayoutBinding.endSpinnerId.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });




            /*appointmentsLayoutBinding.startSpinnerId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    emailReminderTimeBeanList.get(getPosition()).setOpenTimePosition(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            appointmentsLayoutBinding.endSpinnerId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    emailReminderTimeBeanList.get(getPosition()).setCloseTimePosition(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });*/
        }

        @Override
        public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
            clickListener.onClickPC(parentPositionTop, childPosition, object, tag_FromWhere);
        }
    }

}
