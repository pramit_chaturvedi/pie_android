package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.PersonnelLayoutBinding;
import com.mydevit.pieservicess.service.personal_fragment_data_model;

import java.util.List;

public class PersonalsRecyclerViewAdapter extends RecyclerView.Adapter<PersonalsRecyclerViewAdapter.CustomViewHolder> {

    Context context;

    List<personal_fragment_data_model.DataBean> dataArrayList;
    RequestsRecyclerViewAdapter requestsRecyclerViewAdapter;
    ClickListener clickListener;


    public PersonalsRecyclerViewAdapter(List<personal_fragment_data_model.DataBean> personnalData_ArrayList, Context context, ClickListener clickListener) {
        this.dataArrayList = personnalData_ArrayList;
        this.context = context;
        this.clickListener = clickListener;

    }

    @BindingAdapter({"bind:loadUrl"})
    public static void loadUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url).apply(new RequestOptions().centerCrop())
                .skipMemoryCache(false)
                .placeholder(R.mipmap.user_refer)
                .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PersonnelLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.personnel_layout, parent, false);
        return new CustomViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyDataBinding((personal_fragment_data_model.DataBean) getItem(position));
    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public Object getItem(int position) {
        return dataArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        PersonnelLayoutBinding personnelLayoutBinding;
        RecyclerView.ViewHolder viewHolder = this;

        public CustomViewHolder(PersonnelLayoutBinding personnelLayoutBinding) {
            super(personnelLayoutBinding.getRoot());
            this.personnelLayoutBinding = personnelLayoutBinding;
        }

        public void notifyDataBinding(personal_fragment_data_model.DataBean dataBean) {
            personnelLayoutBinding.setVariable(BR.personalData, dataBean);
            personnelLayoutBinding.setVariable(BR.imageUrll, dataBean.getPersonnel_image());
            personnelLayoutBinding.executePendingBindings();

            personnelLayoutBinding.mainLinLayoutId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onClick(viewHolder.getPosition(), dataBean, "MAIN");
                }
            });
        }
    }
}
