package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.PaymentSettingLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.Payment_Setting_Model;

import java.util.List;

public class PaymentSetting_RecyclerViewAdapter extends RecyclerView.Adapter<PaymentSetting_RecyclerViewAdapter.CustomViewHolder> {

    public static List<Payment_Setting_Model.DataBean.PaymentModeBean> selectedAppointment_List;
    public static ClickListener clickListener;
    Context context;
    boolean isEnabled;

    public PaymentSetting_RecyclerViewAdapter(Context context, List<Payment_Setting_Model.DataBean.PaymentModeBean> appointmentsDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.selectedAppointment_List = appointmentsDataArrayList;
        this.clickListener = clickListener;
    }

    @BindingAdapter("bind:isCheckedData")
    public static void isTriggeredData(CheckBox radioButton, String isChecked) {
        if ("Y".equals(isChecked))
            radioButton.setChecked(true);
        else
            radioButton.setChecked(false);
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PaymentSettingLayoutBinding appointmentsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.payment_setting_layout, parent, false);
        appointmentsLayoutBinding.getRoot().setEnabled(isEnabled);
        appointmentsLayoutBinding.executePendingBindings();
        return new CustomViewHolder(appointmentsLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((Payment_Setting_Model.DataBean.PaymentModeBean) getItem(position));

    }

    @Override
    public int getItemCount() {
        return selectedAppointment_List.size();
    }

    public Object getItem(int position) {
        return selectedAppointment_List.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        PaymentSettingLayoutBinding appointmentsLayoutBinding;

        public CustomViewHolder(PaymentSettingLayoutBinding appointmentsLayoutBinding) {
            super(appointmentsLayoutBinding.getRoot());
            this.appointmentsLayoutBinding = appointmentsLayoutBinding;
        }

        public void notifyViewBinding(Payment_Setting_Model.DataBean.PaymentModeBean dataBean) {
            appointmentsLayoutBinding.setVariable(BR.data, dataBean);
            appointmentsLayoutBinding.executePendingBindings();
            appointmentsLayoutBinding.setPosition(getPosition());
            appointmentsLayoutBinding.setIsEnabled(isEnabled);
            appointmentsLayoutBinding.setClickListener(this);
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            if ("paymentsettingischecked".equals(tag_FromWhere)) {
                if (selectedAppointment_List.get(position).getPayment_mode_status().equals("Y")) {
                    appointmentsLayoutBinding.isCheckedRadioBtnId.setChecked(false);
                    selectedAppointment_List.get(position).setPayment_mode_status("N");
                } else {
                    appointmentsLayoutBinding.isCheckedRadioBtnId.setChecked(true);
                    selectedAppointment_List.get(position).setPayment_mode_status("Y");
                }
            }
            if ("radioclicked".equals(tag_FromWhere)) {
                clickListener.onClick(position, object, context.getResources().getString(R.string.radioclicked));
                if (selectedAppointment_List.get(position).getPayment_mode_status().equals("Y")) {
                    appointmentsLayoutBinding.isCheckedRadioBtnId.setChecked(false);
                    selectedAppointment_List.get(position).setPayment_mode_status("N");
                } else {
                    appointmentsLayoutBinding.isCheckedRadioBtnId.setChecked(true);
                    selectedAppointment_List.get(position).setPayment_mode_status("Y");
                }
            }


        }
    }
}
