package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ClientAppoitnementLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.ClientAppointment_ServiceModel;

import java.util.List;

public class ClientsAppointments_RecyclerViewAdapter extends RecyclerView.Adapter<ClientsAppointments_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<ClientAppointment_ServiceModel.DataBean> appointmentsDataArrayList;
    ClickListener clickListener;

    public ClientsAppointments_RecyclerViewAdapter(Context context, List<ClientAppointment_ServiceModel.DataBean> appointmentsDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.clickListener = clickListener;
        this.appointmentsDataArrayList = appointmentsDataArrayList;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ClientAppoitnementLayoutBinding clientAppoitnementLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.client_appoitnement_layout, parent, false);
        return new CustomViewHolder(clientAppoitnementLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((ClientAppointment_ServiceModel.DataBean) getItem(position));

    }

    @Override
    public int getItemCount() {
        return appointmentsDataArrayList.size();
    }

    public Object getItem(int position) {
        return appointmentsDataArrayList.get(position);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        ClientAppoitnementLayoutBinding clientAppoitnementLayoutBinding;

        public CustomViewHolder(ClientAppoitnementLayoutBinding clientAppoitnementLayoutBinding) {
            super(clientAppoitnementLayoutBinding.getRoot());
            this.clientAppoitnementLayoutBinding = clientAppoitnementLayoutBinding;
        }

        public void notifyViewBinding(ClientAppointment_ServiceModel.DataBean dataBean) {
            clientAppoitnementLayoutBinding.setVariable(BR.personalAppointmentModel, dataBean);
            clientAppoitnementLayoutBinding.setVariable(BR.clickListener, this);
            clientAppoitnementLayoutBinding.setVariable(BR.position, this.getPosition());
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            clickListener.onClick(position, object, tag_FromWhere);
        }
    }
}
