package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.graphics.ColorUtils;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.TodayScheduleDesignBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.today_schedule;

import java.util.List;


public class TodaysMenu_RecyclerViewAdapter extends RecyclerView.Adapter<TodaysMenu_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<today_schedule.DataBean.PersonnelTimingsBean> appointmentsDataArrayList;
    ClickListener clickListener;

    public TodaysMenu_RecyclerViewAdapter(Context context, List<today_schedule.DataBean.PersonnelTimingsBean> appointmentsDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.appointmentsDataArrayList = appointmentsDataArrayList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TodayScheduleDesignBinding todayHeaderLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.today_schedule_design, parent, false);
        return new CustomViewHolder(todayHeaderLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        today_schedule.DataBean.PersonnelTimingsBean dataBean = (today_schedule.DataBean.PersonnelTimingsBean) getItem(position);
        /*if (!"".equals(dataBean.getPersonnel_color()) && null != dataBean.getPersonnel_color()) {

            int color = Color.parseColor(dataBean.getPersonnel_color());
            int[][] states = new int[][]{
                    new int[]{android.R.attr.state_enabled}};
            int[] colors = new int[]{getAlphaTxtColor(coloPor)};

            ColorStateList colorStateList = new ColorStateList(states, colors);
            holder.todayHeaderLayoutBinding.backgroundRoundImageViewId.setSupportBackgroundTintList(colorStateList);
        }*/
        if (!dataBean.getPersonnel_name().equals("") && dataBean.getPersonnel_name() != null)
            holder.todayHeaderLayoutBinding.nameTextViewId.setText(dataBean.getPersonnel_name().substring(0, 1));

        holder.notifyDataUtil(dataBean);
    }

    private int getAlphaTxtColor(int color) {
        return ColorUtils.setAlphaComponent(color, 128);
    }

    @Override
    public int getItemCount() {
        return appointmentsDataArrayList.size();
    }

    public Object getItem(int position) {
        return appointmentsDataArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        public TodayScheduleDesignBinding todayHeaderLayoutBinding;

        public CustomViewHolder(TodayScheduleDesignBinding todayHeaderLayoutBinding) {
            super(todayHeaderLayoutBinding.getRoot());
            this.todayHeaderLayoutBinding = todayHeaderLayoutBinding;
        }

        public void notifyDataUtil(Object dataBean) {
            todayHeaderLayoutBinding.setVariable(BR.dataPersonnel, dataBean);
            todayHeaderLayoutBinding.executePendingBindings();
        }

    }
}
