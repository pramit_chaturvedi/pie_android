package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.R;

import java.util.ArrayList;

import butterknife.ButterKnife;

public class CurrentCodeRecyclerViewAdapter extends RecyclerView.Adapter<CurrentCodeRecyclerViewAdapter.CustomViewHolder> {

    ArrayList recentActivity_trueArrayList;
    Context context;

    public CurrentCodeRecyclerViewAdapter(ArrayList recentActivity_trueArrayList, Context context) {
        this.recentActivity_trueArrayList = recentActivity_trueArrayList;
        this.context = context;

    }
    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.current_codes, null);
        view.setLayoutParams(new ViewGroup.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
       /* Glide.with(context)
                .load(R.drawable.user)
                .override(200, 200)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(false)
                .centerCrop()
                .transform(new CenterInside(), new RoundedCorners(200))
                .into(holder.userImageView);*/
    }

    @Override
    public int getItemCount() {
        return recentActivity_trueArrayList.size();
    }

    public Object getItem(int position) {
        return recentActivity_trueArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

      /*  @BindView(R.id.userImageViewId)
        ImageView userImageView;
*/

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
