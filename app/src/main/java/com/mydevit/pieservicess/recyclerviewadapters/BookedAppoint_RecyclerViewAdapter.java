package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.BookedAppointDataBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.ScheduleNewServiceModel;

import java.util.List;

public class BookedAppoint_RecyclerViewAdapter extends RecyclerView.Adapter<BookedAppoint_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<ScheduleNewServiceModel.DataBean.PersonnelDataBean.BookedAptmtBean> bookedAppointmentSlotArrayList;
    ClickListener clickListener;

    public BookedAppoint_RecyclerViewAdapter(Context context, List<ScheduleNewServiceModel.DataBean.PersonnelDataBean.BookedAptmtBean> bookedAppointmentSlotArrayList, ClickListener clickListener) {
        this.context = context;
        this.bookedAppointmentSlotArrayList = bookedAppointmentSlotArrayList;
        this.clickListener = clickListener;
    }
    @BindingAdapter("bind:manipulateMargin")
    public static void manipulateMargin(LinearLayout linearLayout, int position) {
        if (position == 0) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(40, 0, 0, 0);
            linearLayout.setLayoutParams(layoutParams);
        } else {

        }
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BookedAppointDataBinding appointmentsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.booked_appoint_data, parent, false);
        return new CustomViewHolder(appointmentsLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((ScheduleNewServiceModel.DataBean.PersonnelDataBean.BookedAptmtBean) getItem(position));

    }

    @Override
    public int getItemCount() {
        return bookedAppointmentSlotArrayList.size();
    }

    public Object getItem(int position) {
        return bookedAppointmentSlotArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        BookedAppointDataBinding bookedAppointDataBinding;

        public CustomViewHolder(BookedAppointDataBinding bookedAppointDataBinding) {
            super(bookedAppointDataBinding.getRoot());
            this.bookedAppointDataBinding = bookedAppointDataBinding;
        }

        public void notifyViewBinding(ScheduleNewServiceModel.DataBean.PersonnelDataBean.BookedAptmtBean dataBean) {
            bookedAppointDataBinding.setVariable(BR.slotData, dataBean);
            bookedAppointDataBinding.setVariable(BR.clickListener, clickListener);
            bookedAppointDataBinding.setVariable(BR.position, this.getPosition());
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            clickListener.onClick(position, object, tag_FromWhere);
        }
    }
}
