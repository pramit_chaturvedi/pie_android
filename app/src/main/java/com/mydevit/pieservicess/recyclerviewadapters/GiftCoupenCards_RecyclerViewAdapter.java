package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.CurrentCodesBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.Payment_Setting_Model;

import java.util.List;

public class GiftCoupenCards_RecyclerViewAdapter extends RecyclerView.Adapter<GiftCoupenCards_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<Payment_Setting_Model.DataBean.GiftCardsCouponsBean> selectedAppointment_List;
    ClickListener clickListener;

    public GiftCoupenCards_RecyclerViewAdapter(Context context, List<Payment_Setting_Model.DataBean.GiftCardsCouponsBean> appointmentsDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.selectedAppointment_List = appointmentsDataArrayList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CurrentCodesBinding currentCodesBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.current_codes, parent, false);
        return new CustomViewHolder(currentCodesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((Payment_Setting_Model.DataBean.GiftCardsCouponsBean) getItem(position));

    }

    @Override
    public int getItemCount() {
        return selectedAppointment_List.size();
    }

    public Object getItem(int position) {
        return selectedAppointment_List.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        CurrentCodesBinding currentCodesBinding;

        public CustomViewHolder(CurrentCodesBinding currentCodesBinding) {
            super(currentCodesBinding.getRoot());
            this.currentCodesBinding = currentCodesBinding;
        }

        public void notifyViewBinding(Payment_Setting_Model.DataBean.GiftCardsCouponsBean dataBean) {
            currentCodesBinding.setVariable(BR.coupenData, dataBean);
            currentCodesBinding.setClickListener(this);
            currentCodesBinding.setPosition(getPosition());
            currentCodesBinding.executePendingBindings();
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            clickListener.onClick(position, object, tag_FromWhere);
        }
    }
}
