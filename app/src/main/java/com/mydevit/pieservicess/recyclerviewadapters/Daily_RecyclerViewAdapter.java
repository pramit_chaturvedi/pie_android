package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.DailyLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;

import java.util.ArrayList;

public class Daily_RecyclerViewAdapter extends RecyclerView.Adapter<Daily_RecyclerViewAdapter.CustomViewHolder> {
    Context context;
    ArrayList appointmentsDataArrayList;
    ClickListener clickListener;

    public Daily_RecyclerViewAdapter(Context context, ArrayList appointmentsDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.appointmentsDataArrayList = appointmentsDataArrayList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DailyLayoutBinding dailyLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.daily_layout, parent, false);
        return new CustomViewHolder(dailyLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return appointmentsDataArrayList.size();
    }

    public Object getItem(int position) {
        return appointmentsDataArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        DailyLayoutBinding dailyLayoutBinding;
        public CustomViewHolder(DailyLayoutBinding dailyLayoutBinding) {
            super(dailyLayoutBinding.getRoot());
            this.dailyLayoutBinding = dailyLayoutBinding;

        }
        public void notifyViewBinding(){

        }
    }
}
