package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.BillingRowBinding;
import com.mydevit.pieservicess.databinding.ServiceAppointmentsLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.Cart_Interface;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.CustomerAppointment_DetailModel;

import java.util.List;

public class ServiceClientAppointment_RecyclerViewAdapter extends RecyclerView.Adapter<ServiceClientAppointment_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<CustomerAppointment_DetailModel.DataBean.ServiceDataBean> serviceDataBeanList;
    ClickListener clickListener;
    Cart_Interface cart_interface;

    public ServiceClientAppointment_RecyclerViewAdapter(Context context, List<CustomerAppointment_DetailModel.DataBean.ServiceDataBean> serviceDataBeanList, ClickListener clickListener) {
        this.context = context;
        this.serviceDataBeanList = serviceDataBeanList;
        this.clickListener = clickListener;

    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ServiceAppointmentsLayoutBinding serviceAppointmentsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.service_appointments_layout, parent, false);
        return new CustomViewHolder(serviceAppointmentsLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBionding((CustomerAppointment_DetailModel.DataBean.ServiceDataBean) getItem(position));
    }

    @Override
    public int getItemCount() {
        return serviceDataBeanList.size();
    }

    public Object getItem(int position) {
        return serviceDataBeanList.get(position);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ServiceAppointmentsLayoutBinding serviceAppointmentsLayoutBinding;

        public CustomViewHolder(ServiceAppointmentsLayoutBinding serviceAppointmentsLayoutBinding) {
            super(serviceAppointmentsLayoutBinding.getRoot());
            this.serviceAppointmentsLayoutBinding = serviceAppointmentsLayoutBinding;
        }

        public void notifyViewBionding(CustomerAppointment_DetailModel.DataBean.ServiceDataBean serviceDataBean) {
            serviceAppointmentsLayoutBinding.setServiceAppointmentData(serviceDataBean);
            //serviceAppointmentsLayoutBinding.executePendingBindings();


        }


    }
}
