package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ClientdatainformationlayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.TodayAppoinClientModel;

import java.util.List;

public class ClientDataInformationTab_RecyclerViewAdapter extends RecyclerView.Adapter<ClientDataInformationTab_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<TodayAppoinClientModel.DataBean.PastEventsBean> appointmentsDataArrayList;
    ClickListener clickListener;


    public ClientDataInformationTab_RecyclerViewAdapter(Context context, List<TodayAppoinClientModel.DataBean.PastEventsBean> appointmentsDataArrayListClickListener, ClickListener clickListener) {
        this.context = context;
        this.appointmentsDataArrayList = appointmentsDataArrayListClickListener;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ClientdatainformationlayoutBinding appointmentsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.clientdatainformationlayout, parent, false);
        return new CustomViewHolder(appointmentsLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((TodayAppoinClientModel.DataBean.PastEventsBean) getItem(position));

    }

    @Override
    public int getItemCount() {
        return appointmentsDataArrayList.size();
    }

    public Object getItem(int position) {
        return appointmentsDataArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ClientdatainformationlayoutBinding appointmentsLayoutBinding;

        public CustomViewHolder(ClientdatainformationlayoutBinding appointmentsLayoutBinding) {
            super(appointmentsLayoutBinding.getRoot());
            this.appointmentsLayoutBinding = appointmentsLayoutBinding;
        }

        public void notifyViewBinding(TodayAppoinClientModel.DataBean.PastEventsBean dataBean) {
            appointmentsLayoutBinding.setVariable(BR.mainDataBean, dataBean);
            appointmentsLayoutBinding.setVariable(BR.clickListener, clickListener);
            appointmentsLayoutBinding.setVariable(BR.position, this.getPosition());
        }
    }
}
