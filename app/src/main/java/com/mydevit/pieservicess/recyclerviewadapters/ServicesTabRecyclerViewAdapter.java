package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ServicesLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.Service_DataModel;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import java.util.List;

public class ServicesTabRecyclerViewAdapter extends RecyclerView.Adapter<ServicesTabRecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<Service_DataModel.DataBean.LocationServicesBean> dataArrayList;
    ClickListener clickListener;
    boolean isPersonnelLoggedIn;

    public ServicesTabRecyclerViewAdapter(Context context, List<Service_DataModel.DataBean.LocationServicesBean> dataArrayList, ClickListener clickListener) {
        this.context = context;
        this.dataArrayList = dataArrayList;
        this.clickListener = clickListener;
        this.isPersonnelLoggedIn = AlphaHolder.isLoginWithPersonnel(SharedPreferences_Util.getRoleId(context));

    }

    @BindingAdapter("bind:imageofservice")
    public static void imageofservice(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                .placeholder(R.drawable.user)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(false)
                .into(imageView);

    }

    @NonNull
    @Override
    public ServicesTabRecyclerViewAdapter.CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ServicesLayoutBinding servicesLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.services_layout, parent, false);
        return new CustomViewHolder(servicesLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicesTabRecyclerViewAdapter.CustomViewHolder holder, int position) {

        holder.notifyViewBinding((Service_DataModel.DataBean.LocationServicesBean) getItem(position));
    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public Object getItem(int position) {
        return dataArrayList.get(position);
    }

    /*private void callFunction(int position) {
        for (int i = 0; i < dataArrayList.size(); i++) {
            if (i == position) {
                if (dataArrayList.get(i).getIsSelected().equals("Y"))
                    dataArrayList.get(i).setIsSelected("N");
                else
                    dataArrayList.get(i).setIsSelected("Y");
            }
            if (i == dataArrayList.size() - 1)
                notifyItemChanged(position);
        }
    }*/

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        ServicesLayoutBinding servicesLayoutBinding;

        public CustomViewHolder(ServicesLayoutBinding servicesLayoutBinding) {
            super(servicesLayoutBinding.getRoot());
            this.servicesLayoutBinding = servicesLayoutBinding;
        }

        public void notifyViewBinding(Service_DataModel.DataBean.LocationServicesBean locationPersonnelServicesBean) {
            servicesLayoutBinding.setLocationData(locationPersonnelServicesBean);
            servicesLayoutBinding.setClickListener(this);
            servicesLayoutBinding.setIsPersonnelLoggedIn(isPersonnelLoggedIn);

/*
            if ("".equals(locationPersonnelServicesBean.getPriceWillBeShow())) {
            } else {
                servicesLayoutBinding.setSetLocationPrice('$' + locationPersonnelServicesBean.getPriceWillBeShow());
            }
        */  servicesLayoutBinding.setPosition(getPosition());
            servicesLayoutBinding.executePendingBindings();

           /* servicesLayoutBinding.checkCheclBoxId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callFunction(getPosition());
                }
            });*/
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            if ("serviceclick".equals(tag_FromWhere)) {
                clickListener.onClick(position, object, tag_FromWhere);
            }
            //callFunction(position);
        }

    }
}
