package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.NotificationReminderLayooutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.service.NotificationMainModel;
import com.mydevit.pieservicess.utils.AlphaHolder;

import java.util.List;

public class NotificationReminderTimeSlot_RecyclerViewAdapter extends RecyclerView.Adapter<NotificationReminderTimeSlot_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<NotificationMainModel.DataBean.EmailReminderDataBean> emailReminderTimeBeanList;
    ClickListenerPC clickListener;
    int parentPositionTop;
    SharedPreferences mPrefs;
    List<String> timeSlotList;


    public NotificationReminderTimeSlot_RecyclerViewAdapter(Context context, List<NotificationMainModel.DataBean.EmailReminderDataBean> emailReminderTimeBeanList, ClickListenerPC clickListener) {
        this.context = context;
        this.emailReminderTimeBeanList = emailReminderTimeBeanList;
        this.clickListener = clickListener;

        //  String date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        timeSlotList = AlphaHolder.getSlotList("12:00 AM", "11:55 PM");
        Log.e("TOTALDATA", timeSlotList.toString());
    }


    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        NotificationReminderLayooutBinding notificationReminderLayooutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.notification_reminder_layoout, parent, false);
        return new CustomViewHolder(notificationReminderLayooutBinding);

    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((NotificationMainModel.DataBean.EmailReminderDataBean) getItem(position));

    }

    @Override
    public int getItemCount() {
        return emailReminderTimeBeanList.size();
    }

    public Object getItem(int position) {
        return emailReminderTimeBeanList.get(position);
    }

    public void virtualKeyboard(AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setFocusable(false);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListenerPC {
        NotificationReminderLayooutBinding appointmentsLayoutBinding;

        public CustomViewHolder(NotificationReminderLayooutBinding appointmentsLayoutBinding) {
            super(appointmentsLayoutBinding.getRoot());
            this.appointmentsLayoutBinding = appointmentsLayoutBinding;
        }

        public void notifyViewBinding(NotificationMainModel.DataBean.EmailReminderDataBean dataBean) {
            appointmentsLayoutBinding.setVariable(BR.clickListener, this);

            appointmentsLayoutBinding.notificationReminderAutoTId.setAdapter(new ArrayAdapter<String>(context, R.layout.spinner_layout, timeSlotList));
            appointmentsLayoutBinding.notificationReminderAutoTId.setThreshold(300);
            appointmentsLayoutBinding.notificationReminderAutoTId.setText(dataBean.getReminder_time());

            appointmentsLayoutBinding.notificationReminderAutoTId.setOnTouchListener(new View.OnTouchListener() {

                int CLICK_ACTION_THRESHOLD = 200;
                float startX;
                float startY;

                @Override
                public boolean onTouch(View view, MotionEvent event) {


                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            startX = event.getX();
                            startY = event.getY();
                            break;
                        case MotionEvent.ACTION_UP:
                            float endX = event.getX();
                            float endY = event.getY();
                            if (isAClick(startX, endX, startY, endY)) {

                                appointmentsLayoutBinding.notificationReminderAutoTId.showDropDown();
                                virtualKeyboard(appointmentsLayoutBinding.notificationReminderAutoTId);

                            }
                            break;
                    }
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }

                private boolean isAClick(float startX, float endX, float startY, float endY) {
                    float differenceX = Math.abs(startX - endX);
                    float differenceY = Math.abs(startY - endY);
                    return !(differenceX > CLICK_ACTION_THRESHOLD/* =5 */ || differenceY > CLICK_ACTION_THRESHOLD);
                }
            });
            appointmentsLayoutBinding.notificationReminderAutoTId.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    emailReminderTimeBeanList.get(getPosition()).setReminder_time(appointmentsLayoutBinding.notificationReminderAutoTId.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

        }

        @Override
        public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
            clickListener.onClickPC(parentPositionTop, childPosition, object, tag_FromWhere);
        }
    }
}
