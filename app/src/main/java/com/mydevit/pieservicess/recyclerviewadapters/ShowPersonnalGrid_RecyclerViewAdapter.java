package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.PersonnalGridBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.Service_DataModel;

import java.util.List;

public class ShowPersonnalGrid_RecyclerViewAdapter extends RecyclerView.Adapter<ShowPersonnalGrid_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<Service_DataModel.DataBean.LocationServicesBean.PersoonelsBean> persoonelsBeanList;
    ClickListener clickListener;

    public ShowPersonnalGrid_RecyclerViewAdapter(Context context, List<Service_DataModel.DataBean.LocationServicesBean.PersoonelsBean> persoonelsBeanList, ClickListener clickListener) {
        this.context = context;
        this.persoonelsBeanList = persoonelsBeanList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PersonnalGridBinding appointmentsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.personnal_grid, parent, false);
        return new CustomViewHolder(appointmentsLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((Service_DataModel.DataBean.LocationServicesBean.PersoonelsBean) getItem(position));

    }

    @Override
    public int getItemCount() {
        return persoonelsBeanList.size();
    }

    public Object getItem(int position) {
        return persoonelsBeanList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {

        PersonnalGridBinding appointmentsLayoutBinding;

        public CustomViewHolder(PersonnalGridBinding appointmentsLayoutBinding) {
            super(appointmentsLayoutBinding.getRoot());
            this.appointmentsLayoutBinding = appointmentsLayoutBinding;

        }

        public void notifyViewBinding(Service_DataModel.DataBean.LocationServicesBean.PersoonelsBean dataBean) {
            appointmentsLayoutBinding.setVariable(BR.personnal_grid, dataBean);
            appointmentsLayoutBinding.setVariable(BR.clickListener, this);
            appointmentsLayoutBinding.setVariable(BR.position, this.getPosition());
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            clickListener.onClick(position, object, tag_FromWhere);
        }
    }
}
