package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.WeeklyLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.Daily_report_service_model;

import java.util.List;

public class Weekly_RecyclerViewAdapter extends RecyclerView.Adapter<Weekly_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<Daily_report_service_model.DataBean.OrderDataBean> appointmentsDataArrayList;
    ClickListener clickListener;

    public Weekly_RecyclerViewAdapter(Context context, List<Daily_report_service_model.DataBean.OrderDataBean> appointmentsDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.appointmentsDataArrayList = appointmentsDataArrayList;
        this.clickListener = clickListener;

    }

 /*   @BindingAdapter("bind:loadUrl")
    public static void loadUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                *//*.apply(RequestOptions.bitmapTransform(new CircleCrop()))*//*
                .override(100, 100)
                .into(imageView);
    }*/

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        WeeklyLayoutBinding weeklyLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.weekly_layout, parent, false);
        return new CustomViewHolder(weeklyLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((Daily_report_service_model.DataBean.OrderDataBean) getItem(position));
    }

    @Override
    public int getItemCount() {
        return appointmentsDataArrayList.size();
    }

    public Object getItem(int position) {
        return appointmentsDataArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        WeeklyLayoutBinding weeklyLayoutBinding;

        public CustomViewHolder(WeeklyLayoutBinding weeklyLayoutBinding) {
            super(weeklyLayoutBinding.getRoot());
            this.weeklyLayoutBinding = weeklyLayoutBinding;
        }

        public void notifyViewBinding(Daily_report_service_model.DataBean.OrderDataBean dataBean) {
            weeklyLayoutBinding.setDataManipulation(dataBean);
            weeklyLayoutBinding.executePendingBindings();
        }
    }
}
