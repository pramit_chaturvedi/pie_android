package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.PaymentModeLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.BilllNow_Model;

import java.util.List;

public class PaymentMode_RecyclerViewAdapter extends RecyclerView.Adapter<PaymentMode_RecyclerViewAdapter.CustomViewHolder> implements ClickListener {

    Context context;
    List<BilllNow_Model.DataBean> appointmentsDataArrayList;
    ClickListener clickListener;
    ClickListener clickListenerAAAA;

    public PaymentMode_RecyclerViewAdapter(Context context, List<BilllNow_Model.DataBean> appointmentsDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.appointmentsDataArrayList = appointmentsDataArrayList;
        this.clickListener = clickListener;
        this.clickListenerAAAA = this;
    }

    @BindingAdapter({"bind:isCheckedReturn"})
    public static void isCheckedReturn(RadioButton radioButton, String paymentModeStatus) {
        if (paymentModeStatus.equals("Y")) {
            radioButton.setChecked(true);
        } else {
            radioButton.setChecked(false);
        }
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PaymentModeLayoutBinding paymentModeLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.payment_mode_layout, parent, false);
        return new CustomViewHolder(paymentModeLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((BilllNow_Model.DataBean) getItem(position));
    }

    @Override
    public int getItemCount() {
        return appointmentsDataArrayList.size();
    }

    public Object getItem(int position) {
        return appointmentsDataArrayList.get(position);
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        clickListener.onClick(position, object, tag_FromWhere);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        PaymentModeLayoutBinding paymentModeLayoutBinding;


        public CustomViewHolder(PaymentModeLayoutBinding paymentModeLayoutBinding) {
            super(paymentModeLayoutBinding.getRoot());
            this.paymentModeLayoutBinding = paymentModeLayoutBinding;
        }

        public void notifyViewBinding(BilllNow_Model.DataBean dataBean) {
            paymentModeLayoutBinding.setVariable(BR.paymentModeData, dataBean);
            paymentModeLayoutBinding.setClicklisterner(this);
            paymentModeLayoutBinding.setPosition(getPosition());
        }


        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            if ("paymenttypetag".equals(tag_FromWhere)) {
                clickListener.onClick(position, object, tag_FromWhere);
            }
        }
    }
}
