package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.GratuitySettingLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.Payment_Setting_Model;

import java.util.List;

public class GratuitySetting_RecyclerViewAdapter extends RecyclerView.Adapter<GratuitySetting_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<Payment_Setting_Model.DataBean.GratuityBean.GratuityDataBean> selectedAppointment_List;
    ClickListener clickListener;
    boolean isEnabled;

    public GratuitySetting_RecyclerViewAdapter(Context context, List<Payment_Setting_Model.DataBean.GratuityBean.GratuityDataBean> appointmentsDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.selectedAppointment_List = appointmentsDataArrayList;
        this.clickListener = clickListener;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        GratuitySettingLayoutBinding gratuitySettingLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.gratuity_setting_layout, parent, false);
        return new CustomViewHolder(gratuitySettingLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((Payment_Setting_Model.DataBean.GratuityBean.GratuityDataBean) getItem(position));

    }

    @Override
    public int getItemCount() {
        return selectedAppointment_List.size();
    }

    public Object getItem(int position) {
        return selectedAppointment_List.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        GratuitySettingLayoutBinding gratuitySettingLayoutBinding;

        public CustomViewHolder(GratuitySettingLayoutBinding gratuitySettingLayoutBinding) {
            super(gratuitySettingLayoutBinding.getRoot());
            this.gratuitySettingLayoutBinding = gratuitySettingLayoutBinding;
        }

        public void notifyViewBinding(Payment_Setting_Model.DataBean.GratuityBean.GratuityDataBean dataBean) {
            gratuitySettingLayoutBinding.setVariable(BR.dataGratuity, dataBean);
            if ("C".equals(dataBean.getGratuity_type()) && "Y".equals(dataBean.getGratuity_setting_status())) {
                clickListener.onClick(getPosition(), dataBean, "SHOW_CUSTOM_AMOUNT_EDITTEXT");
            }
            gratuitySettingLayoutBinding.setPosition(getPosition());
            gratuitySettingLayoutBinding.setClickListener(this);
            gratuitySettingLayoutBinding.setIsEnabled(isEnabled());
            gratuitySettingLayoutBinding.executePendingBindings();
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            if ("grauityradioclicked".equals(tag_FromWhere)) {
                for (int i = 0; i < selectedAppointment_List.size(); i++) {
                    if (i == position) {
                        selectedAppointment_List.get(i).setGratuity_setting_status("Y");
                        clickListener.onClick(position, object, "HIDE_CUSTOM_AMOUNT_EDITTEXT");
                    } else
                        selectedAppointment_List.get(i).setGratuity_setting_status("N");

                    if (i == selectedAppointment_List.size() - 1) {
                        if ("C".equals(selectedAppointment_List.get(position).getGratuity_type())) {
                            clickListener.onClick(position, object, "SHOW_CUSTOM_AMOUNT_EDITTEXT");
                        }
                        notifyDataSetChanged();

                    }
                }

            }
        }
    }
}
