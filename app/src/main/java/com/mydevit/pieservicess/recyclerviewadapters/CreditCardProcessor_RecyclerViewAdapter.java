package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.CreditCardProcessorLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.Payment_Setting_Model;

import java.util.List;

public class CreditCardProcessor_RecyclerViewAdapter extends RecyclerView.Adapter<CreditCardProcessor_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<Payment_Setting_Model.DataBean.PaymentProcessorBean> selectedAppointment_List;
    ClickListener clickListener;

    boolean isEnabled;
    ;

    public CreditCardProcessor_RecyclerViewAdapter(Context context, List<Payment_Setting_Model.DataBean.PaymentProcessorBean> appointmentsDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.selectedAppointment_List = appointmentsDataArrayList;
        this.clickListener = clickListener;
    }

    @BindingAdapter("bind:isCheckedData")
    public static void isTriggeredData(RadioButton radioButton, String isChecked) {
        if ("Y".equals(isChecked))
            radioButton.setChecked(true);
        else
            radioButton.setChecked(false);
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CreditCardProcessorLayoutBinding appointmentsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.credit_card_processor_layout, parent, false);
        return new CustomViewHolder(appointmentsLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((Payment_Setting_Model.DataBean.PaymentProcessorBean) getItem(position));

    }

    @Override
    public int getItemCount() {
        return selectedAppointment_List.size();
    }

    public Object getItem(int position) {
        return selectedAppointment_List.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        CreditCardProcessorLayoutBinding creditCardProcessorLayoutBinding;

        public CustomViewHolder(CreditCardProcessorLayoutBinding creditCardProcessorLayoutBinding) {
            super(creditCardProcessorLayoutBinding.getRoot());
            this.creditCardProcessorLayoutBinding = creditCardProcessorLayoutBinding;

        }

        public void notifyViewBinding(Payment_Setting_Model.DataBean.PaymentProcessorBean dataBean) {
            creditCardProcessorLayoutBinding.setVariable(BR.data, dataBean);
            creditCardProcessorLayoutBinding.setClickListener(this);
            creditCardProcessorLayoutBinding.setIsEnabled(isEnabled());
            creditCardProcessorLayoutBinding.setPosition(getPosition());
            creditCardProcessorLayoutBinding.executePendingBindings();
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            if ("creditradioclick".equals(tag_FromWhere)) {
                for (int i = 0; i < selectedAppointment_List.size(); i++) {
                    if (i == position)
                        selectedAppointment_List.get(i).setPayment_mode_status("Y");
                    else
                        selectedAppointment_List.get(i).setPayment_mode_status("N");

                    if (i == selectedAppointment_List.size() - 1) {
                        notifyDataSetChanged();
                    }
                }
            }

        }
    }
}
