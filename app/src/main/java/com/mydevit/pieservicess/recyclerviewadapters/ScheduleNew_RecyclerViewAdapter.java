package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ScheduleNewUserLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.ScheduleNewServiceModel;
import com.mydevit.pieservicess.utils.CircleImageView;

import java.util.List;

public class ScheduleNew_RecyclerViewAdapter extends RecyclerView.Adapter<ScheduleNew_RecyclerViewAdapter.CustomViewHolder> {

    public static int sCorner = 15;
    public static int sMargin = 2;
    public static int sBorder = 10;
    public static String sColor = "#2DB66E";
    Context context;
    List<ScheduleNewServiceModel.DataBean.PersonnelDataBean> scheduleDataArrayList;
    ClickListener clickListener;


    public ScheduleNew_RecyclerViewAdapter(Context context, List<ScheduleNewServiceModel.DataBean.PersonnelDataBean> scheduleDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.scheduleDataArrayList = scheduleDataArrayList;
        this.clickListener = clickListener;
    }

    @BindingAdapter("bind:loadScheduleImage")
    public static void loadImage(CircleImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url).
                skipMemoryCache(false).
                placeholder(R.mipmap.user_refer).
                apply(RequestOptions.bitmapTransform(new CircleCrop())).
                diskCacheStrategy(DiskCacheStrategy.ALL).
                into(imageView);


    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ScheduleNewUserLayoutBinding appointmentsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.schedule_new_user_layout, parent, false);
        return new CustomViewHolder(appointmentsLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((ScheduleNewServiceModel.DataBean.PersonnelDataBean) getItem(position));
    }

    @Override
    public int getItemCount() {
        return scheduleDataArrayList.size();
    }

    public Object getItem(int position) {
        return scheduleDataArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        ScheduleNewUserLayoutBinding scheduleNewUserLayoutBinding;

        public CustomViewHolder(ScheduleNewUserLayoutBinding scheduleNewUserLayoutBinding) {
            super(scheduleNewUserLayoutBinding.getRoot());
            this.scheduleNewUserLayoutBinding = scheduleNewUserLayoutBinding;
        }

        public void notifyViewBinding(ScheduleNewServiceModel.DataBean.PersonnelDataBean dataBean) {
            scheduleNewUserLayoutBinding.setVariable(BR.userData, dataBean);
            scheduleNewUserLayoutBinding.setVariable(BR.position, this.getPosition());
            scheduleNewUserLayoutBinding.setVariable(BR.clickListener, this);

            if ("N".equals(dataBean.getIsSelected())) {
                scheduleNewUserLayoutBinding.userImageViewId.setBorderColor(context.getResources().getColor(R.color.colorPrimary));
                scheduleNewUserLayoutBinding.userImageViewId.setBorderWidth(0);
            } else {
                scheduleNewUserLayoutBinding.userImageViewId.setBorderColor(context.getResources().getColor(R.color.colorPrimary));
                scheduleNewUserLayoutBinding.userImageViewId.setBorderWidth(8);
            }


        }


        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            clickListener.onClick(position, object, tag_FromWhere);
        }
    }
}
