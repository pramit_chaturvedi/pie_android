package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.BillingRowBinding;
import com.mydevit.pieservicess.listenerInterface.Cart_Interface;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.Data_cart_model;

import java.util.List;

public class BillingRow_RecyclerViewAdapter extends RecyclerView.Adapter<BillingRow_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<Data_cart_model.DataBean.CartItemDetailBean> appointmentsDataArrayList;
    ClickListener clickListener;
    Cart_Interface cart_interface;

    public BillingRow_RecyclerViewAdapter(Context context, List<Data_cart_model.DataBean.CartItemDetailBean> appointmentsDataArrayList, ClickListener clickListener, Cart_Interface cart_interface) {
        this.context = context;
        this.appointmentsDataArrayList = appointmentsDataArrayList;
        this.clickListener = clickListener;
        this.cart_interface = cart_interface;

    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BillingRowBinding billingRowBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.billing_row, parent, false);
        return new CustomViewHolder(billingRowBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBionding((Data_cart_model.DataBean.CartItemDetailBean) getItem(position));
    }

    @Override
    public int getItemCount() {
        return appointmentsDataArrayList.size();
    }

    public Object getItem(int position) {
        return appointmentsDataArrayList.get(position);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {
        BillingRowBinding billingRowBinding;

        public CustomViewHolder(BillingRowBinding billingRowBinding) {
            super(billingRowBinding.getRoot());
            this.billingRowBinding = billingRowBinding;
        }

        public void notifyViewBionding(Data_cart_model.DataBean.CartItemDetailBean cartItemDetailBean) {
            billingRowBinding.setRowData(cartItemDetailBean);
            billingRowBinding.executePendingBindings();

            billingRowBinding.minusTextViewId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cart_interface.minusClicked(getPosition(), Integer.parseInt(billingRowBinding.qtyTextViewId.getText().toString()), cartItemDetailBean.getItem_price());
                  /*  int value = Integer.parseInt(billingRowBinding.qtyTextViewId.getText().toString());
                    if (value > 0) {
                        value--;
                        billingRowBinding.qtyTextViewId.setText("" + value);
                    }*/
                }
            });
            billingRowBinding.plusTextViewId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cart_interface.plusClicked(getPosition(), Integer.parseInt(billingRowBinding.qtyTextViewId.getText().toString()), cartItemDetailBean.getItem_price());

                  /*  int value = Integer.parseInt(billingRowBinding.qtyTextViewId.getText().toString());
                    value++;
                    billingRowBinding.qtyTextViewId.setText("" + value);*/

                }
            });
            billingRowBinding.deleteLinLayoutId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onClick(getPosition(), cartItemDetailBean, "DELETE_ITEM");
                }
            });
        }


    }
}
