package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.TimeSlotBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;

import java.util.ArrayList;
import java.util.List;

public class TimeSlot_RecyclerViewAdapter extends RecyclerView.Adapter<TimeSlot_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    ArrayList<Boolean> timeSlotSelectedArrayList;
    List<String> timeSlotArrayList;
    ClickListener clickListener;

    public TimeSlot_RecyclerViewAdapter(Context context, List<String> timeSlotArrayList, ArrayList<Boolean> timeSlotSelectedArrayList, ClickListener clickListener) {
        this.context = context;
        this.clickListener = clickListener;
        this.timeSlotArrayList = timeSlotArrayList;
        this.timeSlotSelectedArrayList = timeSlotSelectedArrayList;
        clickListener.onClick(0,timeSlotArrayList.get(0),"timeslot");
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TimeSlotBinding timeSlotBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.time_slot, parent, false);
        return new CustomViewHolder(timeSlotBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyDataBinding(getItemSlot(position), getItem(position));
    }

    @Override
    public int getItemCount() {
        return timeSlotSelectedArrayList.size();
    }

    public Boolean getItem(int position) {
        return timeSlotSelectedArrayList.get(position);
    }

    public String getItemSlot(int position) {
        return timeSlotArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {

        TimeSlotBinding timeSlotBinding;

        public CustomViewHolder(TimeSlotBinding timeSlotBinding) {
            super(timeSlotBinding.getRoot());
            this.timeSlotBinding = timeSlotBinding;
        }

        public void notifyDataBinding(String s, boolean isSelected) {
            timeSlotBinding.setClickListner(this);
            timeSlotBinding.setPosition(getPosition());
            timeSlotBinding.setTimeSlot(s);
            timeSlotBinding.setIsSelected(isSelected);
            timeSlotBinding.executePendingBindings();
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            if (tag_FromWhere.equals("timeslot")) {
                clickListener.onClick(position,object,tag_FromWhere);
                updateData(position);
            }
        }

        private void updateData(int position) {
            for (int i = 0; i < timeSlotSelectedArrayList.size(); i++) {
                if (i == position) {
                    timeSlotSelectedArrayList.set(i, true);
                } else
                    timeSlotSelectedArrayList.set(i, false);

                if (i == timeSlotSelectedArrayList.size() - 1) {
                    notifyDataSetChanged();
                }
            }
        }
        /*@OnClick({R.id.timeSlotLinLayoutId})
        public void onClick(View view) {
            if (view == timeSlotLinLayout) {
                updateData(this.getPosition());
                clickListener.onClick(this.getPosition(), timeSlotSelectedArrayList.get(this.getPosition()),"TIMESLOT");
            }
        }
        */
    }
}
