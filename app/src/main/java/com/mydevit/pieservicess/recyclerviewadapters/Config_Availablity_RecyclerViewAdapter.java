package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ConfigureAvailabilityBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.ConfigureAvailabiltyModel;

import java.util.List;

public class Config_Availablity_RecyclerViewAdapter extends RecyclerView.Adapter<Config_Availablity_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<ConfigureAvailabiltyModel.DataBean.PersonnelBean> configDataArrayList;
    ClickListener clickListener;

    public Config_Availablity_RecyclerViewAdapter(Context context, List<ConfigureAvailabiltyModel.DataBean.PersonnelBean> configDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.configDataArrayList = configDataArrayList;
        this.clickListener = clickListener;
    }

    @BindingAdapter("bind:loadImageFrom")
    public static void loadImageConfig(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url).
                skipMemoryCache(false).
                placeholder(R.mipmap.user_refer).
                apply(RequestOptions.bitmapTransform(new CircleCrop())).
                diskCacheStrategy(DiskCacheStrategy.ALL).
                into(imageView);
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConfigureAvailabilityBinding appointmentsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.configure_availability, parent, false);
        return new CustomViewHolder(appointmentsLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((ConfigureAvailabiltyModel.DataBean.PersonnelBean) getItem(position));

    }

    @Override
    public int getItemCount() {
        return configDataArrayList.size();
    }

    public Object getItem(int position) {
        return configDataArrayList.get(position);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        ConfigureAvailabilityBinding appointmentsLayoutBinding;

        public CustomViewHolder(ConfigureAvailabilityBinding appointmentsLayoutBinding) {
            super(appointmentsLayoutBinding.getRoot());
            this.appointmentsLayoutBinding = appointmentsLayoutBinding;
        }

        public void notifyViewBinding(ConfigureAvailabiltyModel.DataBean.PersonnelBean dataBean) {
            appointmentsLayoutBinding.setVariable(BR.personalAppointmentModel, dataBean);
            appointmentsLayoutBinding.setVariable(BR.clickListener, this);
            appointmentsLayoutBinding.setVariable(BR.position, getPosition());
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            clickListener.onClick(position, object, tag_FromWhere);
        }
    }
}
