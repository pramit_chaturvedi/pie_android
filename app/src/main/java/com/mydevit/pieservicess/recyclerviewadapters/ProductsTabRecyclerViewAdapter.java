package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ProductsLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.Product_List_Model;

import java.util.ArrayList;
import java.util.List;

public class ProductsTabRecyclerViewAdapter extends RecyclerView.Adapter<ProductsTabRecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<Product_List_Model.DataBean.LocationItemsBean> locationItemsBeanList;

    public ProductsTabRecyclerViewAdapter(Context context, List<Product_List_Model.DataBean.LocationItemsBean> locationItemsBeanList) {
        this.context = context;
        this.locationItemsBeanList = locationItemsBeanList;
    }

    @BindingAdapter("bind:loadImageImage")
    public static void loadImage(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                /*.apply(RequestOptions.bitmapTransform(new CircleCrop()))*/
                .override(100, 100)
                .into(imageView);
    }

    @BindingAdapter("bind:manipulateAdapter")
    public static void manipulateAdapter(Spinner spinner, List<Product_List_Model.DataBean.LocationItemsBean.PriceVariationsBean> priceVariationsBeans) {
        List<String> valueList = new ArrayList<>();
        for (int i = 0; i < priceVariationsBeans.size(); i++) {
            valueList.add(priceVariationsBeans.get(i).getVariation_option_name());
            if (i == priceVariationsBeans.size() - 1) {
                ArrayAdapter arrayAdapter = new ArrayAdapter(spinner.getContext(), android.R.layout.simple_list_item_1, valueList);
                spinner.setAdapter(arrayAdapter);
            }
        }
    }

    @BindingAdapter("bind:haveToHideOrNot")
    public static void haveToHideOrNot(LinearLayout linearLayout, List<Product_List_Model.DataBean.LocationItemsBean.PriceVariationsBean> priceVariationsBeans) {
        List<String> valueList = new ArrayList<>();
        for (int i = 0; i < priceVariationsBeans.size(); i++) {
            if ("".equals(priceVariationsBeans.get(i).getVariation_option_name()) || null == priceVariationsBeans.get(i).getVariation_option_name()) {

            } else {
                valueList.add(priceVariationsBeans.get(i).getVariation_option_name());
            }
            if (i == priceVariationsBeans.size() - 1) {
                if (valueList.size() == 0)
                    linearLayout.setVisibility(View.GONE);
                else
                    linearLayout.setVisibility(View.VISIBLE);
            }
        }
    }


    @NonNull
    @Override
    public ProductsTabRecyclerViewAdapter.CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ProductsLayoutBinding productsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.products_layout, parent, false);
        return new CustomViewHolder(productsLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsTabRecyclerViewAdapter.CustomViewHolder holder, int position) {
        holder.verifyViewBinding(getItem(position));
        holder.productsLayoutBinding.spinnerNameId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                for (int j = 0; j < locationItemsBeanList.get(position).getPrice_variations().size(); j++) {
                    if (j == i) {
                        String price = locationItemsBeanList.get(position).getPrice_variations().get(j).getItem_price();
                        holder.productsLayoutBinding.setPrice(price);

                        locationItemsBeanList.get(position).getPrice_variations().get(j).setIsSelected("Y");
                    } else {
                        locationItemsBeanList.get(position).getPrice_variations().get(j).setIsSelected("N");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        holder.productsLayoutBinding.checkCheclBoxId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = locationItemsBeanList.get(position).getIsSelected();
                if ("".equals(value) || "N".equals(value)) {
                    locationItemsBeanList.get(position).setIsSelected("Y");
                } else {
                    locationItemsBeanList.get(position).setIsSelected("N");
                }
            }
        });
        //holder.checkCheclBox.setChecked(getItem(position));
        /*holder.checkCheclBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        Glide.with(context)
                .load(R.drawable.user)
                .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                .override(80,80)
                .into(holder.userImageView);*/
    }

    /*private void updateDataInArrayList(int position) {
        for (int kk = 0; kk < isCheckedArrayList.size(); kk++) {
            if (kk == position) {
                if (isCheckedArrayList.get(kk))
                    isCheckedArrayList.set(kk, false);
                else
                    isCheckedArrayList.set(kk, true);

            }
            if (kk == isCheckedArrayList.size() - 1) {
                notifyItemChanged(kk);
            }
        }
    }*/

    @Override
    public int getItemCount() {
        return locationItemsBeanList.size();
    }

    public Product_List_Model.DataBean.LocationItemsBean getItem(int position) {
        return locationItemsBeanList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        ProductsLayoutBinding productsLayoutBinding;

        public CustomViewHolder(ProductsLayoutBinding productsLayoutBinding) {
            super(productsLayoutBinding.getRoot());
            this.productsLayoutBinding = productsLayoutBinding;
        }

        private void verifyViewBinding(Product_List_Model.DataBean.LocationItemsBean locationItemsBean) {
            productsLayoutBinding.setLocationDataObject(locationItemsBean);
            productsLayoutBinding.setPosition(getPosition());
            productsLayoutBinding.setClickListener(this);
            productsLayoutBinding.setPrice(locationItemsBean.getPrice_variations().get(0).getItem_price());
            productsLayoutBinding.executePendingBindings();
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            if ("productmainclicked".equals(tag_FromWhere)) {
                if (locationItemsBeanList.get(position).getIsSelected().equals("") | locationItemsBeanList.get(position).getIsSelected().equals("N")) {
                    locationItemsBeanList.get(position).setIsSelected("Y");

                } else {
                    locationItemsBeanList.get(position).setIsSelected("N");
                }
                notifyItemChanged(position);
            }
        }
    }
}
