package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ShowservicelayouttwoBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.model.ServiceModel;
import com.mydevit.pieservicess.service.ViewScheduleModel;

import java.util.List;

public class ShowServiceTwo_RecyclerViewAdapter extends RecyclerView.Adapter<ShowServiceTwo_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<ServiceModel> serviceDataArrayList;
    ClickListenerPC clickListener;
    int parentPositionFrom;

    public ShowServiceTwo_RecyclerViewAdapter(Context context, List<ServiceModel> serviceDataArrayList, ClickListenerPC clickListener, int parentPosition) {
        this.context = context;
        this.serviceDataArrayList = serviceDataArrayList;
        this.clickListener = clickListener;
        this.parentPositionFrom = parentPosition;
    }

    @BindingAdapter("bind:manipulateSerData")
    public static void manipulateData(TextView textView, List<ViewScheduleModel.DataBean.ServiceDataBean> stringArrayList) {
        String serviceNameText = "";
        for (int i = 0; i < stringArrayList.size(); i++) {
            serviceNameText = serviceNameText + stringArrayList.get(i).getService_name() + ", ";
        }
        textView.setText(serviceNameText);
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ShowservicelayouttwoBinding showservicelayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.showservicelayouttwo, parent, false);
        return new CustomViewHolder(showservicelayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((ServiceModel) getItem(position));

    }

    @Override
    public int getItemCount() {
        return serviceDataArrayList.size();
    }

    public Object getItem(int position) {
        return serviceDataArrayList.get(position);
    }

    public Object getItemChecked(int position) {
        return serviceDataArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListenerPC {
        ShowservicelayouttwoBinding showservicelayoutBinding;

        public CustomViewHolder(ShowservicelayouttwoBinding showservicelayoutBinding) {
            super(showservicelayoutBinding.getRoot());
            this.showservicelayoutBinding = showservicelayoutBinding;
        }

        public void notifyViewBinding(ServiceModel dataBean) {
            showservicelayoutBinding.setVariable(BR.serviceData, dataBean);
            showservicelayoutBinding.setVariable(BR.parentPosition, parentPositionFrom);
            showservicelayoutBinding.setVariable(BR.clickListener, clickListener);
            showservicelayoutBinding.setVariable(BR.position, this.getPosition());
        }

        @Override
        public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
            if ("serviceclick".equals(tag_FromWhere)) {
                clickListener.onClickPC(parentPosition, childPosition, object, tag_FromWhere);

            }
        }
    }
}
