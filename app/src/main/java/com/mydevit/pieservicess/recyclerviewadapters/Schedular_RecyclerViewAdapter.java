package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.SchedularLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.SchedulerServiceModel;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import java.util.List;

import butterknife.BindView;

public class Schedular_RecyclerViewAdapter extends RecyclerView.Adapter<Schedular_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<SchedulerServiceModel.DataBean> setDataArrayList;
    ClickListener clickListener;
    boolean isNeddToHidePersonnel;


    public Schedular_RecyclerViewAdapter(Context context, List<SchedulerServiceModel.DataBean> setDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.setDataArrayList = setDataArrayList;
        this.clickListener = clickListener;
        this.isNeddToHidePersonnel = AlphaHolder.isLoginWithPersonnel(SharedPreferences_Util.getRoleId(context));
    }
    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SchedularLayoutBinding schedularLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.schedular_layout, parent, false);
        return new CustomViewHolder(schedularLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyDataBinding(getItem(position));
    }

    @Override
    public int getItemCount() {
        return setDataArrayList.size();
    }

    public Object getItem(int position) {
        return setDataArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {

        @BindView(R.id.editSetUpRelaLayoutId)
        RelativeLayout editSetUpRelaLayout;

        SchedularLayoutBinding schedularLayoutBinding;

        RecyclerView.ViewHolder viewHolder = this;

        public CustomViewHolder(SchedularLayoutBinding schedularLayoutBinding) {
            super(schedularLayoutBinding.getRoot());
            this.schedularLayoutBinding = schedularLayoutBinding;
        }

        public void notifyDataBinding(Object o) {
            schedularLayoutBinding.setVariable(BR.posSetUpData, o);
            schedularLayoutBinding.executePendingBindings();
            schedularLayoutBinding.setClickListener(this);
            schedularLayoutBinding.setPosition(getPosition());
            schedularLayoutBinding.setNeedToHideView(isNeddToHidePersonnel);
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            if ("launchclicked".equals(tag_FromWhere)) {
                clickListener.onClick(position, object, tag_FromWhere);
            }
        }
    }
}
