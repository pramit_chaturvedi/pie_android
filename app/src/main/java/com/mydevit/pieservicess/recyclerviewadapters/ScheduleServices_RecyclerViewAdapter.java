package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ScheduleServiceItemBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.ScheduleNewServiceModel;

import java.util.List;

public class ScheduleServices_RecyclerViewAdapter extends RecyclerView.Adapter<ScheduleServices_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<ScheduleNewServiceModel.DataBean.PersonnelDataBean.PersonnelServiceBean> personnelServiceBeanList;
    ClickListener clickListener;

    public ScheduleServices_RecyclerViewAdapter(Context context, List<ScheduleNewServiceModel.DataBean.PersonnelDataBean.PersonnelServiceBean> personnelServiceBeanList, ClickListener clickListener) {
        this.context = context;
        this.personnelServiceBeanList = personnelServiceBeanList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ScheduleServiceItemBinding appointmentsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.schedule_service_item, parent, false);
        return new CustomViewHolder(appointmentsLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((ScheduleNewServiceModel.DataBean.PersonnelDataBean.PersonnelServiceBean) getItem(position));
    }

    @Override
    public int getItemCount() {
        return personnelServiceBeanList.size();
    }

    public Object getItem(int position) {
        return personnelServiceBeanList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        ScheduleServiceItemBinding appointmentsLayoutBinding;

        public CustomViewHolder(ScheduleServiceItemBinding appointmentsLayoutBinding) {
            super(appointmentsLayoutBinding.getRoot());
            this.appointmentsLayoutBinding = appointmentsLayoutBinding;


        }

        public void notifyViewBinding(ScheduleNewServiceModel.DataBean.PersonnelDataBean.PersonnelServiceBean dataBean) {
            appointmentsLayoutBinding.setVariable(BR.dataOfService, dataBean);
            appointmentsLayoutBinding.setVariable(BR.onClick, clickListener);
            appointmentsLayoutBinding.setVariable(BR.position, this.getPosition());


          /*  appointmentsLayoutBinding.checkBoxCheckboxId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onClick(getPosition(), dataBean, "checkclicked");
                }
            });*/
            appointmentsLayoutBinding.checkBoxCheckboxId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    clickListener.onClick(getPosition(), dataBean, "checkclicked");
                }
            });
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            if ("checkclicked".equals(tag_FromWhere)) {
                clickListener.onClick(position, object, tag_FromWhere);
            }
        }
    }
}
