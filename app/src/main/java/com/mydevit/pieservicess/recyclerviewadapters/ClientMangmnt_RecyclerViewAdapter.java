package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ClientManageLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.ClientListModel;
import com.mydevit.pieservicess.utils.AlphaHolder;

import java.util.List;


public class ClientMangmnt_RecyclerViewAdapter extends RecyclerView.Adapter<ClientMangmnt_RecyclerViewAdapter.CustomViewHolder> {

    List<ClientListModel.DataBean> personnalData_ArrayList;
    Context context;
    ClickListener clickListener;

    public ClientMangmnt_RecyclerViewAdapter(List<ClientListModel.DataBean> personnalData_ArrayList, Context context, ClickListener clickListener) {
        this.personnalData_ArrayList = personnalData_ArrayList;
        this.context = context;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ClientManageLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.client_manage_layout, parent, false);
        return new CustomViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        ClientListModel.DataBean dataBean = (ClientListModel.DataBean) getItem(position);
        if (null != dataBean.getFirst_name() && !"".equals(dataBean.getFirst_name())) {
            holder.customerManageLayoutBinding.textNameTextView.setText(dataBean.getFirst_name().substring(0, 1));

        }
        holder.notifyViewBinding(dataBean);
    }

    @Override
    public int getItemCount() {
        return personnalData_ArrayList.size();
    }

    public Object getItem(int position) {
        return personnalData_ArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        ClientManageLayoutBinding customerManageLayoutBinding;
        RecyclerView.ViewHolder viewHolder = this;

        public CustomViewHolder(ClientManageLayoutBinding customerManageLayoutBinding) {
            super(customerManageLayoutBinding.getRoot());
            this.customerManageLayoutBinding = customerManageLayoutBinding;
        }

        public void notifyViewBinding(Object o) {
            customerManageLayoutBinding.setVariable(BR.customerManagementData, o);
            customerManageLayoutBinding.setVariable(BR.alpha, new AlphaHolder());
            customerManageLayoutBinding.setVariable(BR.position, getPosition());
            customerManageLayoutBinding.setVariable(BR.clickListener, this);
            customerManageLayoutBinding.executePendingBindings();

            /*customerManageLayoutBinding.eyeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "EYE" + viewHolder.getPosition(), Toast.LENGTH_SHORT).show();
                }
            });
            customerManageLayoutBinding.deleteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "DELETE" + viewHolder.getPosition(), Toast.LENGTH_SHORT).show();

                }
            });*/
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            clickListener.onClick(position, object, tag_FromWhere);
        }
    }
}
