package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.graphics.ColorUtils;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.AppointmentPersonnelLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.CustomerAppointment_DetailModel;

import java.util.List;

public class PersonnelClientAppointment_RecyclerViewAdapter extends RecyclerView.Adapter<PersonnelClientAppointment_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<CustomerAppointment_DetailModel.DataBean.PersonnelDataBean> personnelDataBeanList;
    ClickListener clickListener;

    public PersonnelClientAppointment_RecyclerViewAdapter(Context context, List<CustomerAppointment_DetailModel.DataBean.PersonnelDataBean> personnelDataBeanList, ClickListener clickListener) {
        this.context = context;
        this.personnelDataBeanList = personnelDataBeanList;
        this.clickListener = clickListener;
    }

    @BindingAdapter("bind:loadPersonnelDataImage")
    public static void loadImageFromAA(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url).
                skipMemoryCache(false).
                placeholder(R.drawable.user).
                apply(RequestOptions.bitmapTransform(new CircleCrop())).
                diskCacheStrategy(DiskCacheStrategy.ALL).
                into(imageView);
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AppointmentPersonnelLayoutBinding appointmentPersonnelLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.appointment_personnel_layout, parent, false);
        return new CustomViewHolder(appointmentPersonnelLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBionding((CustomerAppointment_DetailModel.DataBean.PersonnelDataBean) getItem(position));
    }

    @Override
    public int getItemCount() {
        return personnelDataBeanList.size();
    }

    public Object getItem(int position) {
        return personnelDataBeanList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        AppointmentPersonnelLayoutBinding appointmentPersonnelLayoutBinding;

        public CustomViewHolder(AppointmentPersonnelLayoutBinding appointmentPersonnelLayoutBinding) {
            super(appointmentPersonnelLayoutBinding.getRoot());
            this.appointmentPersonnelLayoutBinding = appointmentPersonnelLayoutBinding;
        }

        public void notifyViewBionding(CustomerAppointment_DetailModel.DataBean.PersonnelDataBean personnelDataBean) {
            appointmentPersonnelLayoutBinding.setSetPersonnelData(personnelDataBean);
            appointmentPersonnelLayoutBinding.setClickListener(this);


            manipulateCustomer(personnelDataBean, appointmentPersonnelLayoutBinding);
            appointmentPersonnelLayoutBinding.executePendingBindings();

        }

        private void manipulateCustomer(CustomerAppointment_DetailModel.DataBean.PersonnelDataBean personnelDataBean, AppointmentPersonnelLayoutBinding appointmentPersonnelLayoutBinding) {

            int color = Color.parseColor(personnelDataBean.getPersonnel_color());

            int[][] states = new int[][]{
                    new int[]{android.R.attr.state_enabled}};
            int[] colors = new int[]{
                    getAlphaTxtColor(color)};

            ColorStateList colorStateList = new ColorStateList(states, colors);
            appointmentPersonnelLayoutBinding.colorImageView.setSupportBackgroundTintList(colorStateList);

        }

        private int getAlphaTxtColor(int color) {
            return ColorUtils.setAlphaComponent(color, 128);
        }


        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {

        }
    }
}
