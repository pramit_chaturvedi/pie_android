package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.PosSetupLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.pos_set_up_model;

import java.util.List;

import butterknife.BindView;

public class POS_SetUp_RecyclerViewAdapter extends RecyclerView.Adapter<POS_SetUp_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<pos_set_up_model.DataBean> setDataArrayList;
    ClickListener clickListener;


    public POS_SetUp_RecyclerViewAdapter(Context context, List<pos_set_up_model.DataBean> setDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.setDataArrayList = setDataArrayList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PosSetupLayoutBinding posSetupLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.pos_setup_layout, parent, false);
        return new CustomViewHolder(posSetupLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyDataBinding(getItem(position));
    }

    @Override
    public int getItemCount() {
        return setDataArrayList.size();
    }

    public Object getItem(int position) {
        return setDataArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.editSetUpRelaLayoutId)
        RelativeLayout editSetUpRelaLayout;

        PosSetupLayoutBinding posSetupLayoutBinding;

        RecyclerView.ViewHolder viewHolder = this;

        public CustomViewHolder(PosSetupLayoutBinding posSetupLayoutBinding) {
            super(posSetupLayoutBinding.getRoot());
            this.posSetupLayoutBinding = posSetupLayoutBinding;
        }
        public void notifyDataBinding(Object o) {
            posSetupLayoutBinding.setVariable(BR.posSetUpData, o);
            posSetupLayoutBinding.executePendingBindings();

            posSetupLayoutBinding.mainViewLinLayoutId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onClick(viewHolder.getPosition(), o, "CLICK");
                }
            });
        }


    }
}
