package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.TodayHeaderLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.today_schedule;

import java.util.List;

public class TodaysHeader_RecyclerViewAdapter extends RecyclerView.Adapter<TodaysHeader_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<today_schedule.DataBean> appointmentsDataArrayList;
    ClickListener clickListener;

    public TodaysHeader_RecyclerViewAdapter(Context context, List<today_schedule.DataBean> appointmentsDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.appointmentsDataArrayList = appointmentsDataArrayList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TodayHeaderLayoutBinding todayHeaderLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.today_header_layout, parent, false);
        return new CustomViewHolder(todayHeaderLayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        today_schedule.DataBean dataBeanData = (today_schedule.DataBean) getItem(position);
        List<today_schedule.DataBean.PersonnelTimingsBean> personnelTimingsBeans = dataBeanData.getPersonnel_timings();
        holder.notifyDataUtil(dataBeanData, personnelTimingsBeans);
    }

    @Override
    public int getItemCount() {
        return appointmentsDataArrayList.size();
    }

    public Object getItem(int position) {
        return appointmentsDataArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListener {
        public TodayHeaderLayoutBinding todayHeaderLayoutBinding;

        public CustomViewHolder(TodayHeaderLayoutBinding todayHeaderLayoutBinding) {
            super(todayHeaderLayoutBinding.getRoot());
            this.todayHeaderLayoutBinding = todayHeaderLayoutBinding;
            todayHeaderLayoutBinding.setClickListener(this);
            todayHeaderLayoutBinding.setPosition(getPosition());


            todayHeaderLayoutBinding.rightButtonId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onClick(getPosition(), null, "viewnowtag");
                }
            });
        }

        public void notifyDataUtil(Object dataBean, List<today_schedule.DataBean.PersonnelTimingsBean> personnelTimingsBeans) {

            TodaysMenu_RecyclerViewAdapter todaysMenu_recyclerViewAdapter = new TodaysMenu_RecyclerViewAdapter(context, personnelTimingsBeans, this);
            todayHeaderLayoutBinding.setTodayDataAdapter(todaysMenu_recyclerViewAdapter);

            todayHeaderLayoutBinding.setVariable(BR.todayHeaderData, dataBean);
            todayHeaderLayoutBinding.executePendingBindings();
        }

        @Override
        public void onClick(int position, Object object, String tag_FromWhere) {
            if ("viewnowtag".equals(tag_FromWhere)) {


            }
        }
    }
}
