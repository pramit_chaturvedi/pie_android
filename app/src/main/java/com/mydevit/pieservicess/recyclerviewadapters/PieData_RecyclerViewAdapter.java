package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.DataLayoutBinding;
import com.mydevit.pieservicess.model.PieModel;

import java.util.ArrayList;

public class PieData_RecyclerViewAdapter extends RecyclerView.Adapter<PieData_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    ArrayList<PieModel> pieDataArrayList;
    ClickListener clickListener;

    public PieData_RecyclerViewAdapter(Context context, ArrayList<PieModel> pieDataArrayList, ClickListener clickListener) {
        this.context = context;
        this.pieDataArrayList = pieDataArrayList;
        this.clickListener = clickListener;
    }
    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DataLayoutBinding dataLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.data_layout, parent, false);
        return new CustomViewHolder(dataLayoutBinding);
    }
    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        PieModel pieModel = getItem(position);
        holder.dataLayoutBinding.ringImageViewId.setSupportBackgroundTintList(ContextCompat.getColorStateList(context, pieModel.getColor()));
        holder.dataLayoutBinding.nameTextViewId.setText(pieModel.getName());
        holder.notityData(pieModel);
    }

    @Override
    public int getItemCount() {
        return pieDataArrayList.size();
    }
    public PieModel getItem(int position) {
        return pieDataArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        DataLayoutBinding dataLayoutBinding;

        public CustomViewHolder(DataLayoutBinding dataLayoutBinding) {
            super(dataLayoutBinding.getRoot());
            this.dataLayoutBinding = dataLayoutBinding;
        }

        public void notityData(PieModel pieModel) {
            dataLayoutBinding.setVariable(BR.dataModel, pieModel);
            dataLayoutBinding.executePendingBindings();
        }
    }
}
