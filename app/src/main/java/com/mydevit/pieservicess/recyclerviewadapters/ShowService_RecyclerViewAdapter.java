package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ShowservicelayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.model.ParentServiceModel;

import java.util.ArrayList;

public class ShowService_RecyclerViewAdapter extends RecyclerView.Adapter<ShowService_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    ArrayList<ParentServiceModel> serviceModelParentArrayList;
    ClickListenerPC clickListener;

    public ShowService_RecyclerViewAdapter(Context context, ArrayList<ParentServiceModel> serviceModelParentArrayList, ClickListenerPC clickListener) {
        this.context = context;
        this.serviceModelParentArrayList = serviceModelParentArrayList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ShowservicelayoutBinding showservicelayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.showservicelayout, parent, false);
        return new CustomViewHolder(showservicelayoutBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((ParentServiceModel) getItem(position));

    }

    @Override
    public int getItemCount() {
        return serviceModelParentArrayList.size();
    }

    public Object getItem(int position) {
        return serviceModelParentArrayList.get(position);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListenerPC {
        ShowservicelayoutBinding showservicelayoutBinding;
        ShowServiceTwo_RecyclerViewAdapter showServiceTwo_recyclerViewAdapter;


        public CustomViewHolder(ShowservicelayoutBinding showservicelayoutBinding) {
            super(showservicelayoutBinding.getRoot());
            this.showservicelayoutBinding = showservicelayoutBinding;
        }

        public void notifyViewBinding(ParentServiceModel dataBean) {
            showservicelayoutBinding.setVariable(BR.dataService, dataBean);
            showservicelayoutBinding.setVariable(BR.clickListener, clickListener);
            showservicelayoutBinding.setVariable(BR.position, this.getPosition());

            showServiceTwo_recyclerViewAdapter = new ShowServiceTwo_RecyclerViewAdapter(context, dataBean.getServiceModelList(), this,getPosition());
            showservicelayoutBinding.setDataAdapter(showServiceTwo_recyclerViewAdapter);
            showservicelayoutBinding.executePendingBindings();

        }

        @Override
        public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
            clickListener.onClickPC(parentPosition, childPosition, object, tag_FromWhere);

        }
    }
}
