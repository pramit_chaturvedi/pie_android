package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ScheduleTimeSlotLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.service.ConfigureAvailabiltyModel;

import java.util.ArrayList;
import java.util.List;

public class ScheduleTimeSlot_Parent_RecyclerViewAdapter extends RecyclerView.Adapter<ScheduleTimeSlot_Parent_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean> personnelTimingsBeanList;
    ClickListenerPC clickListener;
    ArrayList<String> timeSlotList;

    public ScheduleTimeSlot_Parent_RecyclerViewAdapter(Context context, List<ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean> personnelTimingsBeanList, ClickListenerPC clickListener, ArrayList<String> timeSlotList) {
        this.context = context;
        this.personnelTimingsBeanList = personnelTimingsBeanList;
        this.clickListener = clickListener;
        this.timeSlotList = timeSlotList;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ScheduleTimeSlotLayoutBinding appointmentsLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.schedule_time_slot_layout, parent, false);
        return new CustomViewHolder(appointmentsLayoutBinding);

    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyViewBinding((ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean) getItem(position));

    }

    @Override
    public int getItemCount() {
        return personnelTimingsBeanList.size();
    }

    public Object getItem(int position) {
        return personnelTimingsBeanList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListenerPC {
        ScheduleTimeSlotLayoutBinding appointmentsLayoutBinding;
        ScheduleTimeSlot_Child_RecyclerViewAdapter scheduleTimeSlot_child_recyclerViewAdapter;

        public CustomViewHolder(ScheduleTimeSlotLayoutBinding appointmentsLayoutBinding) {
            super(appointmentsLayoutBinding.getRoot());
            this.appointmentsLayoutBinding = appointmentsLayoutBinding;
        }

        public void notifyViewBinding(ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean dataBean) {
            appointmentsLayoutBinding.setVariable(BR.scheudleDataSlot, dataBean);
            appointmentsLayoutBinding.setVariable(BR.clickListener, this);
            appointmentsLayoutBinding.setVariable(BR.parentPosition, getPosition());
            appointmentsLayoutBinding.setVariable(BR.childPosition, 0);

            appointmentsLayoutBinding.childRecyclerViewId.getItemAnimator().endAnimations();
            scheduleTimeSlot_child_recyclerViewAdapter = new ScheduleTimeSlot_Child_RecyclerViewAdapter(context, getPosition(), dataBean.getDay_timings(), this, timeSlotList);
            appointmentsLayoutBinding.setChildRecyclerView(scheduleTimeSlot_child_recyclerViewAdapter);
            appointmentsLayoutBinding.executePendingBindings();

        }

        @Override
        public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
            clickListener.onClickPC(parentPosition, childPosition, object, tag_FromWhere);
        }
    }
}
