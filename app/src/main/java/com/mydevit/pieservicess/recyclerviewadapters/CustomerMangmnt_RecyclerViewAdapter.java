package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.CustomerManageLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.service.customer_management_model;
import com.mydevit.pieservicess.utils.AlphaHolder;

import java.util.List;


public class CustomerMangmnt_RecyclerViewAdapter extends RecyclerView.Adapter<CustomerMangmnt_RecyclerViewAdapter.CustomViewHolder> {

    List<customer_management_model.DataBean> personnalData_ArrayList;
    Context context;
    ClickListenerPC clickListenerPC;

    public CustomerMangmnt_RecyclerViewAdapter(List<customer_management_model.DataBean> personnalData_ArrayList, Context context, ClickListenerPC clickListenerPC) {
        this.personnalData_ArrayList = personnalData_ArrayList;
        this.context = context;
        this.clickListenerPC = clickListenerPC;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CustomerManageLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.customer_manage_layout, parent, false);

        return new CustomViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        customer_management_model.DataBean dataBean = (customer_management_model.DataBean) getItem(position);
        if (null != dataBean.getFirst_name() && !"".equals(dataBean.getFirst_name())) {
            holder.customerManageLayoutBinding.textNameTextView.setText(dataBean.getFirst_name().substring(0, 1));

        }
        holder.notifyViewBinding(dataBean);
    }

    @Override
    public int getItemCount() {
        return personnalData_ArrayList.size();
    }

    public Object getItem(int position) {
        return personnalData_ArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements ClickListenerPC {
        CustomerManageLayoutBinding customerManageLayoutBinding;
        RecyclerView.ViewHolder viewHolder = this;

        public CustomViewHolder(CustomerManageLayoutBinding customerManageLayoutBinding) {
            super(customerManageLayoutBinding.getRoot());
            this.customerManageLayoutBinding = customerManageLayoutBinding;
        }

        public void notifyViewBinding(Object o) {
            customerManageLayoutBinding.setVariable(BR.customerManagementData, o);
            customerManageLayoutBinding.setVariable(BR.alpha, new AlphaHolder());
            customerManageLayoutBinding.setVariable(BR.clickListener, this);
            customerManageLayoutBinding.setVariable(BR.position, getPosition());
            customerManageLayoutBinding.executePendingBindings();

         /*   customerManageLayoutBinding.eyeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "EYE" + viewHolder.getPosition(), Toast.LENGTH_SHORT).show();
                }
            });*/
            /*customerManageLayoutBinding.deleteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "DELETE" + viewHolder.getPosition(), Toast.LENGTH_SHORT).show();

                }
            });*/
        }

        @Override
        public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
            clickListenerPC.onClickPC(parentPosition, childPosition, object, tag_FromWhere);
        }
    }
}
