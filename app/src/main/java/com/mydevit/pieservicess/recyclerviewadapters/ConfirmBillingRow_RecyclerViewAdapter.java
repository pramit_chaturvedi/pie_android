package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ConfirmBillingRowBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.Payment_submit_model;

import java.util.List;

public class ConfirmBillingRow_RecyclerViewAdapter extends RecyclerView.Adapter<ConfirmBillingRow_RecyclerViewAdapter.CustomViewHolder> {

    Context context;
    List<Payment_submit_model.DataBean.CartItemDetailBean> cartItemDetailBeans;
    ClickListener clickListener;

    public ConfirmBillingRow_RecyclerViewAdapter(Context context, List<Payment_submit_model.DataBean.CartItemDetailBean> cartItemDetailBeans, ClickListener clickListener) {
        this.context = context;
        this.cartItemDetailBeans = cartItemDetailBeans;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConfirmBillingRowBinding confirmBillingRowBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.confirm_billing_row, parent, false);
        return new CustomViewHolder(confirmBillingRowBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        holder.notifyDataBinding((Payment_submit_model.DataBean.CartItemDetailBean) getItem(position));
    }

    @Override
    public int getItemCount() {
        return cartItemDetailBeans.size();
    }

    public Object getItem(int position) {
        return cartItemDetailBeans.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ConfirmBillingRowBinding confirmBillingRowBinding;

        public CustomViewHolder(ConfirmBillingRowBinding confirmBillingRowBinding) {
            super(confirmBillingRowBinding.getRoot());
            this.confirmBillingRowBinding = confirmBillingRowBinding;

        }

        public void notifyDataBinding(Payment_submit_model.DataBean.CartItemDetailBean cartItemDetailBean) {
            confirmBillingRowBinding.setVariable(BR.confirmDataItem, cartItemDetailBean);
            confirmBillingRowBinding.executePendingBindings();
        }
    }
}
