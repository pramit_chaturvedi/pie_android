package com.mydevit.pieservicess.recyclerviewadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterInside;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.RecentActivitiesBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.model.recent_servicemodel;
import com.mydevit.pieservicess.utils.AlphaHolder;

import java.util.List;

public class RequestsRecyclerViewAdapter extends RecyclerView.Adapter<RequestsRecyclerViewAdapter.CustomViewHolder> {
    List<recent_servicemodel.DataBean> dataArrayList;
    Context context;
    ClickListener clickListener;

    public RequestsRecyclerViewAdapter(List<recent_servicemodel.DataBean> dataArrayList, Context context, ClickListener clickListener) {
        this.dataArrayList = dataArrayList;
        this.context = context;
        this.clickListener = clickListener;
    }

//    @BindingAdapter("bind:loadRequestDataImage")
//    public static void loadImage(ImageView imageView, String url) {
//        Glide.with(imageView.getContext())
//                .load(url)
//                .override(200, 200)
//                .placeholder(R.drawable.user)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .skipMemoryCache(false)
//                .centerCrop()
//                .transform(new CenterInside(), new RoundedCorners(400))
//                .into(imageView);
//    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecentActivitiesBinding recentActivityBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.recent_activities, parent, false);
        return new CustomViewHolder(recentActivityBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        recent_servicemodel.DataBean dataBean = (recent_servicemodel.DataBean) getItem(position);
        holder.bindingView(dataBean, clickListener);

        //  holder.recentActivityBinding.setEventListner(clickListener);

    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public Object getItem(int position) {
        return dataArrayList.get(position);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        RecentActivitiesBinding recentActivityBinding;

        public CustomViewHolder(RecentActivitiesBinding recentActivityBinding) {
            super(recentActivityBinding.getRoot());
            this.recentActivityBinding = recentActivityBinding;
        }

        public void bindingView(recent_servicemodel.DataBean object, ClickListener clickListener) {
            recentActivityBinding.setVariable(BR.dataModel, object);
            recentActivityBinding.setFunction(new AlphaHolder());
            recentActivityBinding.executePendingBindings();
            RecyclerView.ViewHolder viewHolder = this;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onClick(viewHolder.getPosition(), object, "YAMMA");
                    notifyItemChanged(0);
                }
            });
        }
    }
}
