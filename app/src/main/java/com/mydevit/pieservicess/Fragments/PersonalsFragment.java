package com.mydevit.pieservicess.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.activities.AppointmentsActivity;
import com.mydevit.pieservicess.databinding.FragmentPersonalsBinding;
import com.mydevit.pieservicess.recyclerviewadapters.PersonalsRecyclerViewAdapter;
import com.mydevit.pieservicess.service.personal_fragment_data_model;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class PersonalsFragment extends Fragment implements ClickListener, ServiceReponseInterface_Duplicate {

    View view;
    Context context;

    @BindView(R.id.personnelRecyclerViewId)
    RecyclerView personnelRecyclerView;

    List<personal_fragment_data_model.DataBean> dataArrayList;
    PersonalsRecyclerViewAdapter personalsRecyclerViewAdapter;
    FragmentPersonalsBinding fragmentPersonalsBinding;

    List<NameValuePair> nameValuePairList;
    String location_id;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentPersonalsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_personals, container, false);
        serviceInitialization();

        return fragmentPersonalsBinding.getRoot();
    }
    private void serviceInitialization() {
        location_id = getArguments().getString("location_id");
        //Toast.makeText(context,location_id,Toast.LENGTH_SHORT).show();

        dataArrayList = new ArrayList<>();
        nameValuePairList = new ArrayList<>();

        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));


        new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/pos_personnel", this, "PERSONALDATA", true).execute();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        personal_fragment_data_model.DataBean dataBean = (personal_fragment_data_model.DataBean) object;
        Intent intent = new Intent(context, AppointmentsActivity.class);
        intent.putExtra("location_id",location_id);
        intent.putExtra("personal_id",dataBean.getPersonnel_id());

        startActivity(intent);
        ((Activity) context).overridePendingTransition(0, 0);
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("PERSONALDATA".equals(type)) {
            personal_fragment_data_model personal_fragment_data_model = new Gson().fromJson(result, com.mydevit.pieservicess.service.personal_fragment_data_model.class);
            if (personal_fragment_data_model.getData().size() > 0) {
                dataArrayList.addAll(personal_fragment_data_model.getData());
                personalsRecyclerViewAdapter = new PersonalsRecyclerViewAdapter(dataArrayList, context, this::onClick);
                fragmentPersonalsBinding.setPersionalAdapter(personalsRecyclerViewAdapter);

            }
        }
    }

    @Override
    public void onFailed(String result) {

    }
}
