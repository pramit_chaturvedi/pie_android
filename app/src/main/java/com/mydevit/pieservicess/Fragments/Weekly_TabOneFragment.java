package com.mydevit.pieservicess.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.FragmentWeeklyTabTwoBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.Weekly_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.Daily_report_service_model;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class Weekly_TabOneFragment extends Fragment implements ClickListener, ServiceReponseInterface_Duplicate {

    View view;

    FragmentWeeklyTabTwoBinding fragmentWeeklyTabTwoBinding;

    ArrayList setDataArrayList;
    Weekly_RecyclerViewAdapter weekly_recyclerViewAdapter;
    Context context;
    List<NameValuePair> nameValuePairList;
    String location_id, personeel_id;
    float grandTotalCash = 0;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentWeeklyTabTwoBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_weekly__tab_two, container, false);
        getDataValue();
        return fragmentWeeklyTabTwoBinding.getRoot();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getView()!=null)
                    Initialize("weekly");
            }
        }, 1000);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void getDataValue() {
        location_id = getArguments().getString("location_id");
        personeel_id = getArguments().getString("personal_id");

    }

    private void Initialize(String type) {

        nameValuePairList = new ArrayList<>();
        setDataArrayList = new ArrayList<>();

        weekly_recyclerViewAdapter = new Weekly_RecyclerViewAdapter(context, setDataArrayList, this);
        fragmentWeeklyTabTwoBinding.setSetAdapter(weekly_recyclerViewAdapter);

        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        if (!"".equals(date)) {
            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
            nameValuePairList.add(new BasicNameValuePair("report_date", date));
            nameValuePairList.add(new BasicNameValuePair("report_interval", type));
            nameValuePairList.add(new BasicNameValuePair("personnel_id", personeel_id));
            nameValuePairList.add(new BasicNameValuePair("location_id", location_id));

            new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/pos_reports", this, "WEEK_REPORT", true).execute();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {

    }

    @Override
    public void onSuccess(String result, String type) {
        if ("WEEK_REPORT".equals(type)) {
            Daily_report_service_model daily_report_service_model = new Gson().fromJson(result, Daily_report_service_model.class);
            setDataArrayList.addAll(daily_report_service_model.getData().getOrder_Data());

            float totalCash = 0;
            for (int i = 0; i < daily_report_service_model.getData().getOrder_Data().size(); i++) {
                String amount = daily_report_service_model.getData().getOrder_Data().get(i).getAmount_Paid();
                if (!"".equals(amount)) {
                    float amountFloat = Float.parseFloat(amount);
                    totalCash = totalCash + amountFloat;
                }
                if (i == daily_report_service_model.getData().getOrder_Data().size() - 1) {
                    grandTotalCash = grandTotalCash + totalCash;
                    daily_report_service_model.getData().setTotal_Cash("" + grandTotalCash);
                }
            }
            weekly_recyclerViewAdapter = new Weekly_RecyclerViewAdapter(context, setDataArrayList, this);
            fragmentWeeklyTabTwoBinding.setSetAdapter(weekly_recyclerViewAdapter);
            fragmentWeeklyTabTwoBinding.executePendingBindings();

        }
    }

    @Override
    public void onFailed(String result) {

    }
}
