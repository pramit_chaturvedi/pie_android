package com.mydevit.pieservicess.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityCalenderBinding;
import com.mydevit.pieservicess.databinding.FragmentDailyTabTwoBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.Weekly_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.Daily_report_service_model;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;


public class Daily_TabOneFragment extends Fragment implements ClickListener, ServiceReponseInterface_Duplicate {

    View view;

    FragmentDailyTabTwoBinding fragmentDailyTabTwoBinding;
    List<Daily_report_service_model.DataBean.OrderDataBean> setDataArrayList;
    Weekly_RecyclerViewAdapter weekly_recyclerViewAdapter;
    Context context;
    List<NameValuePair> nameValuePairList;

    String location_id, personeel_id;
    float grandTotalCash = 0;

    AlertDialog alertDialog = null;
    ActivityCalenderBinding activityCalenderBinding;
    private Handler mWaitHandler;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentDailyTabTwoBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_daily__tab_two, container, false);
        getDataValue();
        return fragmentDailyTabTwoBinding.getRoot();
    }

    private void getDataValue() {
        location_id = getArguments().getString("location_id");
        personeel_id = getArguments().getString("personal_id");

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(getView()!=null)
                    Initialize("daily");
            }
        }, 1000);

    }

    private void Initialize(String type) {

        nameValuePairList = new ArrayList<>();
        setDataArrayList = new ArrayList<>();


        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        if (!"".equals(date)) {
            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
            nameValuePairList.add(new BasicNameValuePair("report_date", date));
            nameValuePairList.add(new BasicNameValuePair("report_interval", type));
            nameValuePairList.add(new BasicNameValuePair("personnel_id", personeel_id));
            nameValuePairList.add(new BasicNameValuePair("location_id", location_id));

            new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/pos_reports", this, "DAILY_REPORT", true).execute();
        }

       /* setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);*/

       /* weeklyRecyclerView.setHasFixedSize(true);
        weeklyRecyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        weekly_recyclerViewAdapter = new Weekly_RecyclerViewAdapter(context, setDataArrayList, this);
        weeklyRecyclerView.setAdapter(weekly_recyclerViewAdapter);*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("calenderclick".equals(tag_FromWhere)) {
            calender();
        }
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("DAILY_REPORT".equals(type)) {

            Daily_report_service_model daily_report_service_model = new Gson().fromJson(result, Daily_report_service_model.class);
            setDataArrayList.addAll(daily_report_service_model.getData().getOrder_Data());

            float totalCash = 0;
            for (int i = 0; i < daily_report_service_model.getData().getOrder_Data().size(); i++) {
                String amount = daily_report_service_model.getData().getOrder_Data().get(i).getAmount_Paid();
                if (!"".equals(amount)) {
                    float amountFloat = Float.parseFloat(amount);
                    totalCash = totalCash + amountFloat;
                }
                if (i == daily_report_service_model.getData().getOrder_Data().size() - 1) {
                    grandTotalCash = grandTotalCash + totalCash;
                    daily_report_service_model.getData().setTotal_Cash("" + grandTotalCash);
                }
            }

            fragmentDailyTabTwoBinding.setTotalSale(daily_report_service_model.getData().getTotal_Sale());
            fragmentDailyTabTwoBinding.setCashData(daily_report_service_model.getData().getTotal_Cash());

            weekly_recyclerViewAdapter = new Weekly_RecyclerViewAdapter(context, setDataArrayList, this);
            fragmentDailyTabTwoBinding.setAdapterData(weekly_recyclerViewAdapter);
            fragmentDailyTabTwoBinding.executePendingBindings();

        }
    }

    public void calender() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        activityCalenderBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_calender, viewGroup, false);
        activityCalenderBinding.setClickListener(this);
        builder.setView(activityCalenderBinding.getRoot());
        InitialteCalender(activityCalenderBinding);

        alertDialog = builder.create();
        alertDialog.show();

        String date = getCurrenData();
       /* getSlotServicesApi(date);
        appointmentSelectedData = date;
        fragmentAppointmentsTabTwoBinding.setDate(date);
        fragmentAppointmentsTabTwoBinding.executePendingBindings();*/

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    private void InitialteCalender(ActivityCalenderBinding viewA) {

        viewA.markDayButton.setOnClickListener(view -> {
            Calendar calendar = Calendar.getInstance();
            Random random = new Random(System.currentTimeMillis());
            int style = random.nextInt(2);
            int daySelected = random.nextInt(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.DAY_OF_MONTH, daySelected);
            switch (style) {
                case 0:
                    viewA.robotoCalendarPicker.markCircleImage1(calendar.getTime());
                    break;
                case 1:
                    viewA.robotoCalendarPicker.markCircleImage2(calendar.getTime());
                    break;
                default:
                    break;
            }
        });
        viewA.clearSelectedDayButton.setOnClickListener(v -> viewA.robotoCalendarPicker.clearSelectedDay());
        // Set listener, in this case, the same activity
        //viewA.robotoCalendarPicker.setRobotoCalendarListener(this);

        viewA.robotoCalendarPicker.setShortWeekDays(false);

        viewA.robotoCalendarPicker.showDateTitle(true);

        viewA.robotoCalendarPicker.setDate(new Date());
    }

    private String getCurrenData() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
    }

    @Override
    public void onFailed(String result) {

    }
}
