package com.mydevit.pieservicess.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.FragmentNotificationMainBinding;
import com.mydevit.pieservicess.databinding.LocationLayoutSpinnerBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.service.SchedulerServiceModel;
import com.mydevit.pieservicess.utils.AlphaHolder;

import java.util.ArrayList;
import java.util.List;

public class NotificationMainFragment extends Fragment implements ClickListener {

    Context context;
    FragmentNotificationMainBinding fragmentNotificationMainBinding;
    NotificationTabOneFragment notificationTabOneFragment;
    NotificationTabTwoFragment notificationTabTwoFragment;

    String location_id;
    LocationLayoutSpinnerBinding locationLayoutSpinnerBinding;
    AlertDialog alertDialog;
    ArrayAdapter arrayAdapter;
    AlphaHolder alphaHolder;
    List<SchedulerServiceModel.DataBean> locationArrayListFromSP;
    boolean isFromFirstEvent = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentNotificationMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification_main, container, false);
        fragmentNotificationMainBinding.setClickListener(this);
        manipulateLocationSpinner();

        return fragmentNotificationMainBinding.getRoot();
    }

    private void manipulateLocationSpinner() {
        fragmentNotificationMainBinding.selectLocationsLinLayoutId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLocationSpinnerData(arrayAdapter, true);
                // getPersonalSpinnerData(position);
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.e("VISIBLE", "YES");

        alphaHolder = new AlphaHolder();
        locationArrayListFromSP = alphaHolder.getScheduledArrayList(SchedularLaunch_TabFragment.schedularMainContext);
        location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();

        if (fragmentNotificationMainBinding != null && fragmentNotificationMainBinding.selectUserLocaitonTextView != null) {
            isFromFirstEvent = false;
            List<String> locationNameList = new ArrayList();
            if (locationArrayListFromSP.size() > 0) {
                for (int i = 0; i < locationArrayListFromSP.size(); i++) {
                    locationNameList.add(locationArrayListFromSP.get(i).getLocation_name());
                    arrayAdapter = new ArrayAdapter(context, R.layout.spinner_layout, locationNameList);
                    if (i == locationArrayListFromSP.size() - 1) {
                        fragmentNotificationMainBinding.selectUserLocaitonTextView.setText(locationNameList.get(AlphaHolder.selectedLocationSpinnerPosition));
                        loadInitialFragment();
                    }
                }
            }
        } else {
            isFromFirstEvent = true;
            location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();
        }
    }

    public void showLocationSpinnerData(ArrayAdapter arrayAdapter, boolean shouldShowDialog) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        locationLayoutSpinnerBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.location_layout_spinner, viewGroup, false);
        locationLayoutSpinnerBinding.setClickListener(this);
        builder.setView(locationLayoutSpinnerBinding.getRoot());

        locationLayoutSpinnerBinding.closeImageViewId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        locationLayoutSpinnerBinding.mainListViewLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int positionClick, long l) {
                //Log.e("GETTINGCALL","LISTVIEW");
                AlphaHolder.selectedLocationSpinnerPosition = positionClick;
                location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();
                fragmentNotificationMainBinding.selectUserLocaitonTextView.setText(locationArrayListFromSP.get(positionClick).getLocation_name());
                loadInitialFragment();
                //getPersonalSpinnerData(positionClick);

                alertDialog.dismiss();

            }
        });

        locationLayoutSpinnerBinding.mainListViewLocation.setAdapter(arrayAdapter);
        alertDialog = builder.create();
        if (shouldShowDialog && alertDialog!=null && !alertDialog.isShowing()){
            alertDialog.show();
        }


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance

        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadInitialFragment();
    }

    private void loadInitialFragment() {
        fragmentNotificationMainBinding.setIsSelectedTabOne(true);
        fragmentNotificationMainBinding.setIsSelectedTabTwo(false);

        notificationTabOneFragment = new NotificationTabOneFragment();
        loadFragmenmtFrom(notificationTabOneFragment);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        Bundle bundle = new Bundle();

        if ("tabone".equals(tag_FromWhere)) {
            fragmentNotificationMainBinding.setIsSelectedTabOne(true);
            fragmentNotificationMainBinding.setIsSelectedTabTwo(false);

            notificationTabOneFragment = new NotificationTabOneFragment();
            bundle.putString("location_id", location_id);
            loadFragmenmtFrom(notificationTabOneFragment);


        }
        if ("tabtwo".equals(tag_FromWhere)) {
            fragmentNotificationMainBinding.setIsSelectedTabOne(false);
            fragmentNotificationMainBinding.setIsSelectedTabTwo(true);

            notificationTabTwoFragment = new NotificationTabTwoFragment();
            bundle.putString("location_id", location_id);
            loadFragmenmtFrom(notificationTabTwoFragment);
        }
    }

    private void loadFragmenmtFrom(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("location_id", location_id);
        fragment.setArguments(bundle);
        transaction.replace(R.id.mainReplaceableViewId, fragment); // fragment container id in first parameter is the  container(Main layout id) of Activity
        transaction.commit();
    }
}
