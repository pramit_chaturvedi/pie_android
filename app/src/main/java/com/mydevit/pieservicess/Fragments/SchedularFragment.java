package com.mydevit.pieservicess.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.FragmentSchedularSetUpBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.navigationDrawer.MenuFragment;
import com.mydevit.pieservicess.navigationDrawer.SlidingActivity;
import com.mydevit.pieservicess.recyclerviewadapters.Schedular_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.SchedulerServiceModel;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;


public class SchedularFragment extends Fragment implements ClickListener, ServiceReponseInterface_Duplicate {

    View view;

    List<SchedulerServiceModel.DataBean> setDataArrayList;
    List<NameValuePair> nameValuePairList;

    Context context;
    Schedular_RecyclerViewAdapter schedular_recyclerViewAdapter;
    FragmentSchedularSetUpBinding fragmentSchedularSetUpBinding;
    SchedulerServiceModel schedule_modelObject = null;
    AlphaHolder alphaHolder;
    MenuFragment menuFragment = null;
    List<SchedulerServiceModel.DataBean> dataBeanList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentSchedularSetUpBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_schedular__set_up_, container, false);
        Initialize();
        return fragmentSchedularSetUpBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (menuFragment == null) {
            menuFragment = new MenuFragment();
        }
        changeActiveNavigationBarColorAt(2);
    }
    private void changeActiveNavigationBarColorAt(int position) {
        menuFragment.updateMenuItems(position);
        ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.scheduler));
        try {
            if (null != MenuFragment.adapter)
                MenuFragment.adapter.notifyDataSetChanged();

        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    private void Initialize() {
        setDataArrayList = new ArrayList();
        nameValuePairList = new ArrayList<>();
        alphaHolder = new AlphaHolder();

        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/pos_setup", this, "SCHEDULE_SETUP", true).execute();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("CLICK".equals(tag_FromWhere)) {

            SchedulerServiceModel.DataBean dataBean = (SchedulerServiceModel.DataBean) object;
            Bundle args = new Bundle();
            args.putString("location_id", dataBean.getLocation_id());
            args.putParcelableArrayList("locationArrayList", (ArrayList<? extends Parcelable>) schedule_modelObject.getData());

            Pos_SetUp_TabsFragment pos_setUp_tabsFragment = new Pos_SetUp_TabsFragment();
            pos_setUp_tabsFragment.setArguments(args);

            transactionFragments(pos_setUp_tabsFragment, R.id.container);
        }
        if ("launchclicked".equals(tag_FromWhere)) {
            ///updateClickOfLocations(position);

            SchedulerServiceModel.DataBean dataBean = (SchedulerServiceModel.DataBean) object;
            ((SlidingActivity) context).titleTextView.setText(getResources().getString(R.string.scheduler));

            //alphaHolder.saveArrayList((ArrayList<SchedulerServiceModel.DataBean.PersonnelsBean>) setDataArrayList.get(position).getPersonnels(), context);

            if (dataBean.getPersonnels().size() > 0) {
                AlphaHolder.selectedLocationSpinnerPosition = position;
                Bundle bundle = new Bundle();
                bundle.putString("location_id", dataBean.getLocation_id());
                SchedularLaunch_TabFragment schedularLaunch_tabFragment = new SchedularLaunch_TabFragment();
                schedularLaunch_tabFragment.setArguments(bundle);
                ((SlidingActivity) context).transactionFragmentsFromOther(schedularLaunch_tabFragment, R.id.container);
            } else {
                AlphaHolder.customToast(context, getResources().getString(R.string.thislocationdonthavepersonnel));
            }
        }
    }
    private void updateClickOfLocations(int position) {
        if (dataBeanList.size() > 0) {
            for (int i = 0; i < dataBeanList.size(); i++) {
                if (i == position) {
                    dataBeanList.get(i).setSelected(true);
                } else {
                    dataBeanList.get(i).setSelected(false);
                }

                if (i == dataBeanList.size() - 1) {
                    alphaHolder.saveScheduledArrayList(dataBeanList, context);
                }
            }

        }

    }

    public void transactionFragments(Fragment fragment, int viewResource) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(viewResource, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }
    @Override
    public void onSuccess(String result, String type) {
        if ("SCHEDULE_SETUP".equals(type)) {
            schedule_modelObject = new Gson().fromJson(result, SchedulerServiceModel.class);
            if (schedule_modelObject != null) {
                setDataArrayList.addAll(schedule_modelObject.getData());
                schedular_recyclerViewAdapter = new Schedular_RecyclerViewAdapter(context, setDataArrayList, this);
                fragmentSchedularSetUpBinding.setPosSetUpAdapter(schedular_recyclerViewAdapter);

                /// here i filtering the data if list dont have personnel then dont add data in list that we are going to save in SharedPreference
                dataBeanList = new ArrayList<>();
                for (int i = 0; i < schedule_modelObject.getData().size(); i++) {
                    if (schedule_modelObject.getData().get(i).getPersonnels().size() > 0) {
                        dataBeanList.add(schedule_modelObject.getData().get(i));
                    }
                    if (i == schedule_modelObject.getData().size() - 1) {
                        dataBeanList.get(0).setSelected(true);
                        alphaHolder.saveScheduledArrayList(dataBeanList, context);
                    }
                }
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }
}
