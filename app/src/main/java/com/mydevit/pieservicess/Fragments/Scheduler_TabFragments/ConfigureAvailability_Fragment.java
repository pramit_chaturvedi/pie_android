package com.mydevit.pieservicess.Fragments.Scheduler_TabFragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TimePicker;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.gson.Gson;
import com.mydevit.pieservicess.Fragments.SchedularLaunch_TabFragment;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.activities.ConfigureCalenderActivity;
import com.mydevit.pieservicess.activities.SchedularCalenderActivity;
import com.mydevit.pieservicess.databinding.FragmentConfigureAvailabilityBinding;
import com.mydevit.pieservicess.databinding.LocationLayoutSpinnerBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.Config_Availablity_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.ConfigureAvailabiltyModel;
import com.mydevit.pieservicess.service.Save_Persionnel_Schedule_ServiceModel;
import com.mydevit.pieservicess.service.SchedulerServiceModel;
import com.mydevit.pieservicess.service.ViewScheduleModel;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.sql.Time;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class ConfigureAvailability_Fragment extends Fragment implements ClickListener, ServiceReponseInterface_Duplicate {

    FragmentConfigureAvailabilityBinding fragmentConfigureAvailabilityBinding;
    Config_Availablity_RecyclerViewAdapter config_availablity_recyclerViewAdapter;
    Context context;
    List<NameValuePair> nameValuePairList;
    String location_id;
    Calendar sCalendar;
    List<String> timeSlotList;
    SharedPreferences mPrefs;
    boolean isFromFirstEvent = false;
    ArrayList<ViewScheduleModel.DataBean> dataBeanArrayList;
    ArrayList<SchedulerServiceModel.DataBean.PersonnelsBean> personnelsBeanArrayList;
    LocationLayoutSpinnerBinding locationLayoutSpinnerBinding;
    AlertDialog alertDialog;

    List<String> stringList, personnalIdList;
    List<String> stringListTwo;
    AlphaHolder alphaHolder;
    List<SchedulerServiceModel.DataBean> locationArrayListFromSP;
    ArrayAdapter arrayAdapter;

    int weekNo = 0;


    @BindingAdapter("bind:manipulateAutoTextView")
    public static void doOperation(Spinner autoCompleteTextView, ArrayAdapter adapter) {
        autoCompleteTextView.setAdapter(adapter);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentConfigureAvailabilityBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_configure_availability_, container, false);
        fragmentConfigureAvailabilityBinding.setClickListener(this);
        //   location_id = getArguments().getString("location_id");
//        fragmentConfigureAvailabilityBinding.selectUserLocaitonTextView.setText(locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_name());
        manipulateLocationSpinner();
        return fragmentConfigureAvailabilityBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (AlphaHolder.calendarList != null && AlphaHolder.calendarList.size() > 0) {
            fragmentConfigureAvailabilityBinding.pickNewWeekEditTextId.setText(AlphaHolder.calendarList.get(0) + " - " + AlphaHolder.calendarList.get(AlphaHolder.calendarList.size() - 1));
        }
        loadDataFromService();
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.e("VISIBLE", "YES");

        alphaHolder = new AlphaHolder();
        locationArrayListFromSP = alphaHolder.getScheduledArrayList(SchedularLaunch_TabFragment.schedularMainContext);
        location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();

        if (fragmentConfigureAvailabilityBinding != null && fragmentConfigureAvailabilityBinding.selectUserLocaitonTextView != null) {
            isFromFirstEvent = false;
            List<String> locationNameList = new ArrayList();
            if (locationArrayListFromSP.size() > 0) {
                for (int i = 0; i < locationArrayListFromSP.size(); i++) {
                    locationNameList.add(locationArrayListFromSP.get(i).getLocation_name());
                    arrayAdapter = new ArrayAdapter(context, R.layout.spinner_layout, locationNameList);
                    if (i == locationArrayListFromSP.size() - 1) {
                        fragmentConfigureAvailabilityBinding.selectUserLocaitonTextView.setText(locationNameList.get(AlphaHolder.selectedLocationSpinnerPosition));
                        loadDataFromService();
                        //location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();
                    }
                }
            }
        } else {
            isFromFirstEvent = true;
            location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();
        }
    }

    private void manipulateLocationSpinner() {
        fragmentConfigureAvailabilityBinding.selectLocationsLinLayoutId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLocationSpinnerData(arrayAdapter, true);
                // getPersonalSpinnerData(position);
            }
        });
    }

    public void showLocationSpinnerData(ArrayAdapter arrayAdapter, boolean shouldShowDialog) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        locationLayoutSpinnerBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.location_layout_spinner, viewGroup, false);
        locationLayoutSpinnerBinding.setClickListener(this);
        builder.setView(locationLayoutSpinnerBinding.getRoot());

        locationLayoutSpinnerBinding.closeImageViewId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        locationLayoutSpinnerBinding.mainListViewLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int positionClick, long l) {
                //Log.e("GETTINGCALL","LISTVIEW");
                AlphaHolder.selectedLocationSpinnerPosition = positionClick;
                location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();
                fragmentConfigureAvailabilityBinding.selectUserLocaitonTextView.setText(locationArrayListFromSP.get(positionClick).getLocation_name());
                loadDataFromService();
                //getPersonalSpinnerData(positionClick);
                alertDialog.dismiss();

            }
        });

        locationLayoutSpinnerBinding.mainListViewLocation.setAdapter(arrayAdapter);
        alertDialog = builder.create();
        if (shouldShowDialog && alertDialog!=null && !alertDialog.isShowing()) {
            alertDialog.show();
        }


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance

        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }
    private void loadDataFromService() {
        personnalIdList = new ArrayList<>();
        nameValuePairList = new ArrayList<>();

        if ("".equals(AlphaHolder.selectedDate)) {
            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            AlphaHolder.selectedDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            weekNo = getWeekNo(AlphaHolder.selectedDate);
        } else {
            weekNo = getWeekNo(AlphaHolder.selectedDate);
            //nameValuePairList.add(new BasicNameValuePair("date", AlphaHolder.selectedDate));
            Log.e("WEEKNO", "" + weekNo);
        }


        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("week_num", "" + weekNo));
        nameValuePairList.add(new BasicNameValuePair("date", "" + AlphaHolder.selectedDate));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/Scheduler/get_location_schedule", this, "CONFIGURE_AVAILABILTY", true).execute();

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("starttimetag".equals(tag_FromWhere)) {
            calenderPicker("start");
        }
        if ("slotclickedmain".equals(tag_FromWhere)) {
            ConfigureAvailabiltyModel.DataBean.PersonnelBean personnelBean = (ConfigureAvailabiltyModel.DataBean.PersonnelBean) object;
            List<ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean> personnelTimingsBeans = personnelBean.getPersonnel_timings();

            AlphaHolder.personnelTimingsList = new ArrayList<>();
            AlphaHolder.personnelTimingsList = personnelTimingsBeans;


            Intent intent = new Intent(context, SchedularCalenderActivity.class);
            intent.putStringArrayListExtra("timeSlotList", (ArrayList<String>) stringList);
            intent.putExtra("location_id", location_id);
            intent.putExtra("personal_id", personnelBean.getPersonnel_id());
            intent.putExtra("week_no", "" + weekNo);
            startActivity(intent);

        }
        if ("endtimetag".equals(tag_FromWhere)) {
            calenderPicker("end");
        }
        if ("Save".equals(tag_FromWhere)) {
            if (fragmentConfigureAvailabilityBinding.startTimeSpinner.getSelectedItem().toString().equals("Start Time")) {
                AlphaHolder.customToast(context, getResources().getString(R.string.starttimemustbeselected));
            } else if (fragmentConfigureAvailabilityBinding.endTimeSpinner.getSelectedItem().toString().equals("End Time")) {
                AlphaHolder.customToast(context, getResources().getString(R.string.endmustbeselected));
            } else {
                String startDateWeek = getStartEndOFWeek(weekNo, Calendar.getInstance().get(Calendar.YEAR));
                String endDateWeek = getEndOFWeek(weekNo, Calendar.getInstance().get(Calendar.YEAR));

                nameValuePairList = new ArrayList<>();
                nameValuePairList.add(new BasicNameValuePair("all_personnel_id", personnalIdList.toString()));
                nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
                nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
                nameValuePairList.add(new BasicNameValuePair("all_week_time_out",fragmentConfigureAvailabilityBinding.endTimeSpinner.getSelectedItem().toString()));
                nameValuePairList.add(new BasicNameValuePair("all_week_time_in",  fragmentConfigureAvailabilityBinding.startTimeSpinner.getSelectedItem().toString()));
                nameValuePairList.add(new BasicNameValuePair("week_start_date", getNextAndPreviousDate(startDateWeek)));
                nameValuePairList.add(new BasicNameValuePair("week_end_date", getNextAndPreviousDate(endDateWeek)));
                nameValuePairList.add(new BasicNameValuePair("week_number", "" + weekNo));
                nameValuePairList.add(new BasicNameValuePair("personnel_schedule_ids", "[]"));

                new Service_Integration_Duplicate(context, nameValuePairList, "business/Scheduler/save_all_personnel_schedule", this, "SAVE_ALL_PERSIONAL_SCHEDULE", true).execute();

            }


        }
        if ("configuredate".equals(tag_FromWhere)) {
            Intent intent = new Intent(context, ConfigureCalenderActivity.class);
            startActivity(intent);
            ((Activity) context).overridePendingTransition(0, 0);
        }
    }

    public String getNextAndPreviousDate(String sDate) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(sDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, +1);
            String yesterdayAsString = dateFormat.format(calendar.getTime());

            return yesterdayAsString;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    String getStartEndOFWeek(int enterWeek, int enterYear) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        calendar.set(Calendar.YEAR, enterYear);


        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // PST`
        Date startDate = calendar.getTime();
        String startDateInStr = formatter.format(startDate);
        System.out.println("...date..." + startDateInStr);

        return startDateInStr;
    }

    String getEndOFWeek(int enterWeek, int enterYear) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        calendar.set(Calendar.YEAR, enterYear);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // PST`
        calendar.add(Calendar.DATE, 6);
        Date enddate = calendar.getTime();
        String endDaString = formatter.format(enddate);
        System.out.println("...date..." + endDaString);

        return endDaString;
    }

    private void calenderPicker(String tagFrom) {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);


        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        if ("start".equals(tagFrom))
                            fragmentConfigureAvailabilityBinding.setStartTime(getTime(hourOfDay, minute));
                        else
                            fragmentConfigureAvailabilityBinding.setEndTime(getTime(hourOfDay, minute));

                        fragmentConfigureAvailabilityBinding.executePendingBindings();

                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public int getWeekNo(String dateString) {
        String format = "yyyy-MM-dd";
        int week = 0;

        SimpleDateFormat df = new SimpleDateFormat(format);
        Date date = null;
        try {

            date = df.parse(dateString);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            week = cal.get(Calendar.WEEK_OF_YEAR);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return week;
    }

    private String getTime(int hr, int min) {
        Time tme = new Time(hr, min, 0);//seconds by default set to zero
        Format formatter;
        formatter = new SimpleDateFormat("h:mm a");
        return formatter.format(tme);
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("CONFIGURE_AVAILABILTY".equals(type)) {
            ConfigureAvailabiltyModel configureAvailabiltyModel = new Gson().fromJson(result, ConfigureAvailabiltyModel.class);
            fragmentConfigureAvailabilityBinding.dateRecyclerId.setLayoutManager(new GridLayoutManager(context, 2));
            Config_Availablity_RecyclerViewAdapter Config_Availablity_RecyclerViewAdapter = new Config_Availablity_RecyclerViewAdapter(context, configureAvailabiltyModel.getData().getPersonnel(), this);
            fragmentConfigureAvailabilityBinding.setSetConfigData(Config_Availablity_RecyclerViewAdapter);
            stringList = AlphaHolder.getSlotList(configureAvailabiltyModel.getData().getConfigure_availability().getStart_time(), configureAvailabiltyModel.getData().getConfigure_availability().getEnd_time());
            stringListTwo = AlphaHolder.getSlotList(configureAvailabiltyModel.getData().getConfigure_availability().getStart_time(), configureAvailabiltyModel.getData().getConfigure_availability().getEnd_time());
            getPersonalIdsList(configureAvailabiltyModel);

            if (stringList.size() > 0) {
                stringList.add(0, getResources().getString(R.string.starttimeslot));
                ArrayAdapter arrayAdapter = new ArrayAdapter(context, R.layout.spinner_layout, stringList);
                fragmentConfigureAvailabilityBinding.setStartTimeSlotAAdapter(arrayAdapter);

                stringListTwo.add(0, getResources().getString(R.string.endingtimeslot));
                ArrayAdapter arrayAdapterA = new ArrayAdapter(context, R.layout.spinner_layout, stringListTwo);
                fragmentConfigureAvailabilityBinding.setEndTimeSlotAAdapter(arrayAdapterA);
                fragmentConfigureAvailabilityBinding.executePendingBindings();
            }
            //this section for handle slot time view

            if (isViewAbleToVisible(configureAvailabiltyModel)) {
                fragmentConfigureAvailabilityBinding.mainLinLayoutId.setVisibility(View.GONE);
            } else {
                fragmentConfigureAvailabilityBinding.mainLinLayoutId.setVisibility(View.VISIBLE);
            }

//            manipulateLocationSpinnerData();
            fragmentConfigureAvailabilityBinding.executePendingBindings();
        }
        if ("SAVE_ALL_PERSIONAL_SCHEDULE".equals(type)) {
            Save_Persionnel_Schedule_ServiceModel save_persionnel_schedule_serviceModel = new Gson().fromJson(result, Save_Persionnel_Schedule_ServiceModel.class);
            String message = save_persionnel_schedule_serviceModel.getMessage();
            if ("Schedule successfully added for all personnels".equals(message)) {
                stringList = AlphaHolder.getSlotList(fragmentConfigureAvailabilityBinding.startTimeSpinner.getSelectedItem().toString(), fragmentConfigureAvailabilityBinding.endTimeSpinner.getSelectedItem().toString());
                loadDataFromService();
            } else {

            }

        }
    }

    private void manipulateLocationSpinnerData() {
        if (fragmentConfigureAvailabilityBinding != null && fragmentConfigureAvailabilityBinding.selectLocationsLinLayoutId != null) {
            List locationNameList = new ArrayList();
            if (locationArrayListFromSP.size() > 0) {
                for (int i = 0; i < locationArrayListFromSP.size(); i++) {
                    locationNameList.add(locationArrayListFromSP.get(i).getLocation_name());
                    if (i == locationArrayListFromSP.size() - 1) {
                        arrayAdapter = new ArrayAdapter(context, R.layout.spinner_layout, locationNameList);
                        showLocationSpinnerData(arrayAdapter, false);
                        fragmentConfigureAvailabilityBinding.selectUserLocaitonTextView.setText(locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_name());
                    }
                }
            }
        }
    }

    private boolean isViewAbleToVisible(ConfigureAvailabiltyModel configureAvailabiltyModel) {
        for (int j = 0; j < configureAvailabiltyModel.getData().getPersonnel().size(); j++) {
            if (configureAvailabiltyModel.getData().getPersonnel().get(j).getPersonnel_timings().size() > 0) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    private void getPersonalIdsList(ConfigureAvailabiltyModel configureAvailabiltyModel) {
        for (int i = 0; i < configureAvailabiltyModel.getData().getPersonnel().size(); i++) {
            ConfigureAvailabiltyModel.DataBean.PersonnelBean personnelBean = configureAvailabiltyModel.getData().getPersonnel().get(i);
            personnalIdList.add(personnelBean.getPersonnel_id());
        }
    }

    @Override
    public void onFailed(String result) {

    }
}
