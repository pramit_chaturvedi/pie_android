package com.mydevit.pieservicess.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.activities.AppointmentsActivity;
import com.mydevit.pieservicess.activities.BillActivity;
import com.mydevit.pieservicess.databinding.FragmentAppointmentsTabOneBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.model.SelectedAppointment_Model;
import com.mydevit.pieservicess.recyclerviewadapters.Appointments_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.appointment_personal_model;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class Appointments_TabOneFragment extends Fragment implements ClickListener, ServiceReponseInterface_Duplicate, View.OnClickListener {

    View view;
    @BindView(R.id.appointmentsRecyclerViewId)
    RecyclerView posSetUpRecyclerView;
    public static boolean isPersonnelLoggedIn = false;


    public static String location_id, personeel_id, selectUserId = "";


    List<appointment_personal_model.DataBean.MainAppointmentData> setDataArrayList;
    List<SelectedAppointment_Model> selectedArrayList;
    Appointments_RecyclerViewAdapter appointments_recyclerViewAdapter;
    Context context;
    List<NameValuePair> nameValuePairList;
    ArrayList appointmentsArrayList;

    FragmentAppointmentsTabOneBinding fragmentAppointmentsTabOneBinding;
    appointment_personal_model appointment_personal_model;
    float totalPrice = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentAppointmentsTabOneBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_appointments__tab_one, container, false);
        fragmentAppointmentsTabOneBinding.setOnClick(this);
        getDataFrom();
        getPersonnelIfPersonnelLogin();

        return fragmentAppointmentsTabOneBinding.getRoot();
    }
    private void getDataFrom() {
        location_id = getArguments().getString("location_id");
        personeel_id = getArguments().getString("personal_id");
    }
    @Override
    public void onResume() {
        super.onResume();
        Initialize();
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //id customer already selected then selectedservice model will have the data so
        if (fragmentAppointmentsTabOneBinding != null) {
            selectUserId = AlphaHolder.selectedCustomerModel.getCustomer_id();
            getPersonnelIfPersonnelLogin();
            Initialize();
        }
    }
    private void getPersonnelIfPersonnelLogin() {
        isPersonnelLoggedIn = AlphaHolder.isLoginWithPersonnel(SharedPreferences_Util.getRoleId(context));
        if (isPersonnelLoggedIn) {
            personeel_id = SharedPreferences_Util.getLoggedInPersonnelId(context);
        }
    }
    private void Initialize() {
        selectedArrayList = new ArrayList<>();

        setDataArrayList = new ArrayList<>();

        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        nameValuePairList.add(new BasicNameValuePair("personnel_id", personeel_id));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/pos_location_appointment", this, "PERSONAL_APPOINTMENT", true).execute();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        selectedArrayList.get(position).setSelected(selectedArrayList.get(position).getSelected() ? false : true);
        appointments_recyclerViewAdapter.notifyItemChanged(position);
        ((DefaultItemAnimator) fragmentAppointmentsTabOneBinding.appointmentsRecyclerViewId.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("PERSONAL_APPOINTMENT".equals(type)) {
            appointment_personal_model = new Gson().fromJson(result, com.mydevit.pieservicess.service.appointment_personal_model.class);
            if (appointment_personal_model != null) {
                if (appointment_personal_model.getData().getCart_data() != null) {
                    AppointmentsActivity.cartId = appointment_personal_model.getData().getCart_data().getCart_id();
                }
                if (appointment_personal_model.getData() != null) {
                    for (int i = 0; i < appointment_personal_model.getData().getLoc_aptmt().size(); i++) {

                        SelectedAppointment_Model selectedAppointment_model = new SelectedAppointment_Model();
                        selectedAppointment_model.setId(appointment_personal_model.getData().getLoc_aptmt().get(i).getAppointment_id());
                        selectedAppointment_model.setClient_id(appointment_personal_model.getData().getLoc_aptmt().get(i).getClient_id());
                        selectedAppointment_model.setSelected(false);

                        selectedArrayList.add(selectedAppointment_model);

                        if (i == appointment_personal_model.getData().getLoc_aptmt().size() - 1) {
                            setDataArrayList.addAll(appointment_personal_model.getData().getLoc_aptmt());
                            appointments_recyclerViewAdapter = new Appointments_RecyclerViewAdapter(context, setDataArrayList, selectedArrayList, this);
                            fragmentAppointmentsTabOneBinding.setAppointmentAdapter(appointments_recyclerViewAdapter);
                        }
                    }

                }
            }
        }
        if ("APPOINTMENT_CHECKOUT".equals(type)) {
            try {
                String staus = new JSONObject(result).getString("status");
                if ("success".equals(staus)) {

                    Intent intent = new Intent(context, BillActivity.class);
                    intent.putExtra("location_id", location_id);
                    intent.putExtra("personal_id", personeel_id);
                    startActivity(intent);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onClick(View view) {
        for (int i = 0; i < selectedArrayList.size(); i++) {
            SelectedAppointment_Model selectedAppointment_model = selectedArrayList.get(i);
            if (selectedAppointment_model.getSelected()) {
                List<com.mydevit.pieservicess.service.appointment_personal_model.DataBean.MainAppointmentData.AppointmentServiceBean> appointment_service = appointment_personal_model.getData().getLoc_aptmt().get(i).getAppointment_service();
                if (null != appointment_service || appointment_service.size() > 0) {
                    for (int k = 0; k < appointment_service.size(); k++) {
                        com.mydevit.pieservicess.service.appointment_personal_model.DataBean.MainAppointmentData.AppointmentServiceBean serviceBean = appointment_service.get(k);
                        totalPrice = totalPrice + Float.parseFloat(serviceBean.getService_price());
                    }
                }
            }
            if (i == selectedArrayList.size() - 1) {
                Log.e("price", "" + totalPrice);
                nameValuePairList = new ArrayList<>();
                appointmentsArrayList = new ArrayList();

                for (int j = 0; j < selectedArrayList.size(); j++) {
                    if (selectedArrayList.get(j).getSelected())
                        appointmentsArrayList.add(selectedArrayList.get(j).getId());

                    if (j == selectedArrayList.size() - 1) {
                        if (appointmentsArrayList.size() > 0) {

                            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
                            nameValuePairList.add(new BasicNameValuePair("appointment_id", appointmentsArrayList.toString()));
                            nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
                            nameValuePairList.add(new BasicNameValuePair("service_price", "" + totalPrice));
                            nameValuePairList.add(new BasicNameValuePair("personnel_id", personeel_id));
                            nameValuePairList.add(new BasicNameValuePair("client_id", selectedArrayList.get(selectedArrayList.size() - 1).getClient_id()));

                            new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/checkout_appointment_cart", this, "APPOINTMENT_CHECKOUT", true).execute();

                        } else {
                            AlphaHolder.customToast(context, getResources().getString(R.string.selectappointmentfirst));

                        }
                    }
                }
            }
        }
    }
}
