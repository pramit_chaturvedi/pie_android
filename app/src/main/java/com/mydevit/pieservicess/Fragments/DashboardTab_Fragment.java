package com.mydevit.pieservicess.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

public class DashboardTab_Fragment extends Fragment implements TabLayout.OnTabSelectedListener {
    private final String[] PAGE_TITLESFORROLEIDONE = new String[]{
            "Recent Activity",
            "Today's Schedule",
            "Sales Activity"
    };
    private final String[] PAGE_TITLES = new String[]{
            "Recent Activity",
            "Today's Schedule"
    };
    Activity activity;
    Context context;
    View view;
    private Fragment[] PAGES = null;
    private ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dashboard_tab_, container, false);
        TabLayout(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (Activity) context;
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void TabLayout(View view) {

        if (SharedPreferences_Util.getRoleId(getActivity()).equalsIgnoreCase("1")) {
            PAGES = new Fragment[]{
                    new RecentActivity_Fragment(),
                    new TodaySchedule_Fragment(),
                    new SalesActivityFragment()
            };
        } else {
            PAGES = new Fragment[]{
                    new RecentActivity_Fragment(),
                    new TodaySchedule_Fragment()
            };
        }


        mViewPager = view.findViewById(R.id.pager);
        mViewPager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));
        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
//        Toast.makeText(getActivity(),"Reselect",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        //   Toast.makeText(getActivity(),"Reselect",Toast.LENGTH_LONG).show();
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return PAGES[position];
        }

        @Override
        public int getCount() {
            return PAGES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (SharedPreferences_Util.getRoleId(getActivity()).equalsIgnoreCase("1"))
                return PAGE_TITLESFORROLEIDONE[position];
            else
                return PAGE_TITLES[position];

        }

    }

}
