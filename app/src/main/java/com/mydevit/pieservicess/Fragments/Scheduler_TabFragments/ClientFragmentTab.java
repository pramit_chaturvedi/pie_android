package com.mydevit.pieservicess.Fragments.Scheduler_TabFragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.mydevit.pieservicess.Fragments.SchedularLaunch_TabFragment;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.activities.ClientDataInformationTabActivity;
import com.mydevit.pieservicess.databinding.FragmentClientFragmentTabBinding;
import com.mydevit.pieservicess.databinding.LocationLayoutSpinnerBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.ClientMangmnt_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.ClientListModel;
import com.mydevit.pieservicess.service.SchedulerServiceModel;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class ClientFragmentTab extends Fragment implements ServiceReponseInterface_Duplicate, ClickListener {

    FragmentClientFragmentTabBinding fragmentClientFragmentTabBinding;
    List<NameValuePair> nameValuePairList;
    Context context;
    String location_id;
    ClientMangmnt_RecyclerViewAdapter clientMangmnt_recyclerViewAdapter;
    List<ClientListModel.DataBean> dataBeanList;
    ArrayAdapter arrayAdapter;
    AlphaHolder alphaHolder;
    boolean isFromFirstEvent = false;
    AlertDialog alertDialog;

    List<SchedulerServiceModel.DataBean> locationArrayListFromSP;
    LocationLayoutSpinnerBinding locationLayoutSpinnerBinding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentClientFragmentTabBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_client_fragment_tab, container, false);
        manipulateLocationSpinner();
        return fragmentClientFragmentTabBinding.getRoot();
    }

    private void getDataFromServiuce() {
        dataBeanList = new ArrayList<>();
        // location_id = getArguments().getString("location_id");
        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/Scheduler/client_list", this, "CLIENT_LIST", true).execute();


    }

    private void manipulateLocationSpinner() {
        fragmentClientFragmentTabBinding.selectLocationsLinLayoutId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLocationSpinnerData(arrayAdapter, true);
                // getPersonalSpinnerData(position);
            }
        });
    }

    public void showLocationSpinnerData(ArrayAdapter arrayAdapter, boolean shouldShowDialog) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        locationLayoutSpinnerBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.location_layout_spinner, viewGroup, false);
        locationLayoutSpinnerBinding.setClickListener(this);
        builder.setView(locationLayoutSpinnerBinding.getRoot());

        locationLayoutSpinnerBinding.closeImageViewId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        locationLayoutSpinnerBinding.mainListViewLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int positionClick, long l) {
                //Log.e("GETTINGCALL","LISTVIEW");
                AlphaHolder.selectedLocationSpinnerPosition = positionClick;
                location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();
                fragmentClientFragmentTabBinding.selectUserLocaitonTextView.setText(locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_name());
                getDataFromServiuce();
                //getPersonalSpinnerData(positionClick);

                alertDialog.dismiss();

            }
        });
        locationLayoutSpinnerBinding.mainListViewLocation.setAdapter(arrayAdapter);
        alertDialog = builder.create();
        if (shouldShowDialog && alertDialog != null && !alertDialog.isShowing()) {
            alertDialog.show();
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance

        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    @Override
    public void onResume() {
        super.onResume();
        getDataFromServiuce();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.e("VISIBLE", "YES");

        alphaHolder = new AlphaHolder();
        locationArrayListFromSP = alphaHolder.getScheduledArrayList(SchedularLaunch_TabFragment.schedularMainContext);
        location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();

        if (fragmentClientFragmentTabBinding != null && fragmentClientFragmentTabBinding.selectUserLocaitonTextView != null) {
            isFromFirstEvent = false;
            List<String> locationNameList = new ArrayList();
            if (locationArrayListFromSP.size() > 0) {
                for (int i = 0; i < locationArrayListFromSP.size(); i++) {
                    locationNameList.add(locationArrayListFromSP.get(i).getLocation_name());
                    arrayAdapter = new ArrayAdapter(context, R.layout.spinner_layout, locationNameList);
                    if (i == locationArrayListFromSP.size() - 1) {
                        fragmentClientFragmentTabBinding.selectUserLocaitonTextView.setText(locationNameList.get(AlphaHolder.selectedLocationSpinnerPosition));
                        // location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();
                        getDataFromServiuce();
                    }
                }
            }
        } else {
            isFromFirstEvent = true;
            location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public void onSuccess(String result, String type) {
        if ("CLIENT_LIST".equals(type)) {
            ClientListModel clientListModel = new Gson().fromJson(result, ClientListModel.class);
            if (clientListModel != null) {
                dataBeanList.addAll(clientListModel.getData());
                clientMangmnt_recyclerViewAdapter = new ClientMangmnt_RecyclerViewAdapter(dataBeanList, context, this);
                fragmentClientFragmentTabBinding.setCustomerManagment(clientMangmnt_recyclerViewAdapter);
                fragmentClientFragmentTabBinding.selectUserLocaitonTextView.setText(locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_name());

            }
        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        ClientListModel.DataBean dataBean = (ClientListModel.DataBean) object;
        if ("eye".equals(tag_FromWhere)) {

            Intent intent = new Intent(context, ClientDataInformationTabActivity.class);
            intent.putExtra("customer_id", dataBean.getCustomer_id());
            intent.putExtra("location_id", location_id);
            intent.putExtra("clientname", dataBean.getFirst_name() + " " + dataBean.getLast_name());
            startActivity(intent);
            ((Activity) context).overridePendingTransition(0, 0);

        }
        if ("delete".equals(tag_FromWhere)) {

        }
    }
}
