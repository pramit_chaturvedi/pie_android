package com.mydevit.pieservicess.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.FragmentNotificationTabTwoBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.NotificationReminderTimeSlotTwo_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.NotificationMainModel;
import com.mydevit.pieservicess.service.NotificationTabTwoModel;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;


public class NotificationTabTwoFragment extends Fragment implements ClickListenerPC, ServiceReponseInterface_Duplicate {

    Context context;
    FragmentNotificationTabTwoBinding tabTwoBinding;

    List<NameValuePair> nameValuePairList;
    String location_id;
    NotificationMainModel notificationMainModel;
    NotificationReminderTimeSlotTwo_RecyclerViewAdapter notificationReminderTimeSlot_recyclerViewAdapter;

    List<String> dayBeforeSpinnerList;
    int selectedEmailSpinnerId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        tabTwoBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification_tab_two, container, false);
        tabTwoBinding.setClickListener(this);
        manipulateSpinner();

        return tabTwoBinding.getRoot();
    }

    private void manipulateSpinner() {
        dayBeforeSpinnerList = new ArrayList<>();
        dayBeforeSpinnerList.add("One day before appointement");
        dayBeforeSpinnerList.add("Two day before appointement");
        dayBeforeSpinnerList.add("Three day before appointement");
        dayBeforeSpinnerList.add("One Week");
        dayBeforeSpinnerList.add("Two Weeks");


        ArrayAdapter arrayAdapter = new ArrayAdapter(context, R.layout.spinner_layout, dayBeforeSpinnerList);
        tabTwoBinding.setSetDataToSpinner(arrayAdapter);

        tabTwoBinding.sendTextSpinnerId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                selectedEmailSpinnerId = position;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        getDataFromService();

    }

    private void getDataFromService() {

        location_id = getArguments().getString("location_id");
        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/Scheduler/get_email_notify", this, "NOTIFICATION_MAIN_DATA_TAB_TWO", true).execute();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
        if ("addnewreminder".equals(tag_FromWhere)) {
            if (notificationMainModel.getData().getEmail_reminder_data().size() < 1) {
                NotificationMainModel.DataBean.SmsReminderDataBean smsReminderDataBean = new NotificationMainModel.DataBean.SmsReminderDataBean();
                notificationMainModel.getData().getSms_reminder_data().add(smsReminderDataBean);
                tabTwoBinding.getDataBeanAdapter().notifyDataSetChanged();
            } else {
                AlphaHolder.customToast(getActivity(), "You can add only 1 slot for sms notification");
            }


        }
        if ("checkclicked".equals(tag_FromWhere)) {
            String status = notificationMainModel.getData().getSms_notify_data().getSms_status();
            if ("".equals(status) || "N".equals(status))
                notificationMainModel.getData().getSms_notify_data().setSms_status("Y");
            else
                notificationMainModel.getData().getSms_notify_data().setSms_status("N");
            tabTwoBinding.invalidateAll();
        }
        if ("Save".equals(tag_FromWhere)) {

            List<String> getReminderList = getRemiderData(notificationMainModel.getData().getSms_reminder_data());
            JSONArray jsonArray = new JSONArray(getReminderList);
            String reminderDataForSend = jsonArray.toString();

            nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
            nameValuePairList.add(new BasicNameValuePair("sms_status", notificationMainModel.getData().getSms_notify_data().getSms_status()));
            nameValuePairList.add(new BasicNameValuePair("sms_trigger", String.valueOf(selectedEmailSpinnerId + 1)));
            nameValuePairList.add(new BasicNameValuePair("sms_reminder_time", reminderDataForSend));
            nameValuePairList.add(new BasicNameValuePair("sms_notify_id", notificationMainModel.getData().getSms_notify_data().getNotify_id()));
            nameValuePairList.add(new BasicNameValuePair("location_id", location_id));

            new Service_Integration_Duplicate(context, nameValuePairList, "business/Scheduler/save_sms_schedule_notification_reminder", this, "NOTIFICATION_TAB_SMS", true).execute();

        }
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("NOTIFICATION_MAIN_DATA_TAB_TWO".equals(type)) {
            notificationMainModel = new Gson().fromJson(result, NotificationMainModel.class);

            notificationReminderTimeSlot_recyclerViewAdapter = new NotificationReminderTimeSlotTwo_RecyclerViewAdapter(context, notificationMainModel.getData().getSms_reminder_data(), this);
            tabTwoBinding.setDataBeanAdapter(notificationReminderTimeSlot_recyclerViewAdapter);
            tabTwoBinding.setDataSms(notificationMainModel.getData().getSms_notify_data());

            if (notificationMainModel.getData().getSms_notify_data() != null && !"".equals(notificationMainModel.getData().getSms_notify_data())) {
                String triggerValue = manipulatePosition(notificationMainModel.getData().getSms_notify_data().getSms_trigger());
                tabTwoBinding.sendTextSpinnerId.setSelection(Integer.parseInt(triggerValue) - 1);
            }
            //tabTwoBinding.executePendingBindings();
        }
        if ("NOTIFICATION_TAB_SMS".equals(type)) {
            NotificationTabTwoModel notificationTabTwoModel = new Gson().fromJson(result, NotificationTabTwoModel.class);
            if (notificationTabTwoModel != null) {
                if ("SMS Notification Updated Successfully".equals(notificationTabTwoModel.getMessage())) {
                    manipulateSpinner();
                } else {
                    AlphaHolder.customToast(context, notificationTabTwoModel.getMessage());
                }
            }
        }
    }

    private String manipulatePosition(String parseInt) {
        if (null == parseInt || "".equals(parseInt))
            return "0";
        else
            return parseInt;
    }

    @Override
    public void onFailed(String result) {

    }

    private List<String> getRemiderData(List<NotificationMainModel.DataBean.SmsReminderDataBean> smsReminderDataBeans) {
        List<String> stringList = new ArrayList<>();
        for (int j = 0; j < smsReminderDataBeans.size(); j++) {
            stringList.add(smsReminderDataBeans.get(j).getReminder_time());
            if (j == smsReminderDataBeans.size() - 1) {
                return stringList;
            }
        }
        return stringList;
    }
}
