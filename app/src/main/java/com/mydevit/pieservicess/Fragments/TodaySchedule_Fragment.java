package com.mydevit.pieservicess.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.FragmentTodayScheduleBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.navigationDrawer.MenuFragment;
import com.mydevit.pieservicess.navigationDrawer.SlidingActivity;
import com.mydevit.pieservicess.recyclerviewadapters.TodaysHeader_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.SchedulerServiceModel;
import com.mydevit.pieservicess.service.today_schedule;
import com.mydevit.pieservicess.stickyheader_recyclerview.StickyListHeadersListView;
import com.mydevit.pieservicess.stickyheader_recyclerview.TestBaseAdapter;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class TodaySchedule_Fragment extends Fragment implements
        AdapterView.OnItemClickListener, StickyListHeadersListView.OnHeaderClickListener,
        StickyListHeadersListView.OnStickyHeaderOffsetChangedListener,
        StickyListHeadersListView.OnStickyHeaderChangedListener, ServiceReponseInterface_Duplicate, ClickListener {

    MenuFragment menuFragment;
    List<NameValuePair> nameValuePairList;
    FragmentTodayScheduleBinding fragmentTodayScheduleBinding;
    Context context;
    today_schedule today_schedule;
    AlphaHolder alphaHolder;
    List<SchedulerServiceModel.DataBean> locationArrayListFromSP;
    private TestBaseAdapter mAdapter;
    private ArrayList<String> tempArrayList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        menuFragment = new MenuFragment();
        updateSideMenuColor(0);
        ServiceIntegration();
    }

    private void updateSideMenuColor(int i) {
        menuFragment.updateMenuItems(i);
        if (null != MenuFragment.adapter)
            MenuFragment.adapter.notifyDataSetChanged();
        ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.dashboard));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentTodayScheduleBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_today_schedule_, container, false);
        fragmentTodayScheduleBinding.setClickListener(this::onClick);
        alphaHolder = new AlphaHolder();
        locationArrayListFromSP = alphaHolder.getScheduledArrayList(context);
        return fragmentTodayScheduleBinding.getRoot();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void ServiceIntegration() {

        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        new Service_Integration_Duplicate(context, nameValuePairList, "business/dashboard/today_schedule", this, "TODAY_SCHEDULE", true).execute();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onHeaderClick(StickyListHeadersListView l, View header, int itemPosition, long headerId, boolean currentlySticky) {

    }

    @Override
    public void onStickyHeaderOffsetChanged(StickyListHeadersListView l, View header, int offset) {

    }

    @Override
    public void onStickyHeaderChanged(StickyListHeadersListView l, View header, int itemPosition, long headerId) {

    }

    @Override
    public void onSuccess(String result, String type) {
        if ("TODAY_SCHEDULE".equals(type)) {
            today_schedule = new Gson().fromJson(result, com.mydevit.pieservicess.service.today_schedule.class);
            if (today_schedule!=null && today_schedule.getData() != null) {
                TodaysHeader_RecyclerViewAdapter todaysHeader_recyclerViewAdapter = new TodaysHeader_RecyclerViewAdapter(context, today_schedule.getData(), this);
                fragmentTodayScheduleBinding.setTodayAdapter(todaysHeader_recyclerViewAdapter);
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("viewnowtag".equals(tag_FromWhere)) {
            if (today_schedule.getData().get(position).getPersonnel_timings() != null) {
                alphaHolder.saveArrayListToday(today_schedule.getData().get(position).getPersonnel_timings(), context);
            }
            boolean isHavePersonnel = manipualtePositioinAccoringToLocationId(position);
            if (isHavePersonnel) {
                updateSideMenuColor(2);
                //String locationId = today_schedule.getData().get(position).getLocation_id();
                Bundle bundle = new Bundle();
                // bundle.putString("location_id", locationId);
                // bundle.putString("FROMTODAY", "YES");
                SchedularLaunch_TabFragment schedularLaunch_tabFragment = new SchedularLaunch_TabFragment();
                schedularLaunch_tabFragment.setArguments(bundle);
                ((SlidingActivity) getActivity()).transactionFragmentsFromOther(schedularLaunch_tabFragment, R.id.container);
            } else {
                AlphaHolder.customToast(context, getResources().getString(R.string.thislocationdonthavepersonnel));
            }

            //transactionFragments(schedularLaunch_tabFragment, R.id.container);
        }
        if ("bookanappointmenttag".equals(tag_FromWhere)) {
            menuFragment.updateMenuItems(2);
            ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.scheduler));
            transactionFragmentsForBookNewAppointment(new SchedularFragment(), R.id.container);

        }
    }

    private boolean manipualtePositioinAccoringToLocationId(int position) {
        if (locationArrayListFromSP.size() > 0) {
            String locationId = today_schedule.getData().get(position).getLocation_id();
            for (int j = 0; j < locationArrayListFromSP.size(); j++) {
                String locationIsMatch = locationArrayListFromSP.get(j).getLocation_id();
                if (locationId.equals(locationIsMatch) && locationArrayListFromSP.get(j).getPersonnels().size() > 0) {
                    AlphaHolder.selectedLocationSpinnerPosition = j;
                    return true;

                }
            }
        }
        return false;
    }

    public void transactionFragments(Fragment fragment, int viewResource) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(viewResource, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void transactionFragmentsForBookNewAppointment(Fragment fragment, int viewResource) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(viewResource, fragment);
        ft.addToBackStack(null);
        ft.commit();
        try {
            if (null != MenuFragment.adapter)
                MenuFragment.adapter.notifyDataSetChanged();

        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

    }
}
