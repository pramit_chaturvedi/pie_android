package com.mydevit.pieservicess.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mydevit.pieservicess.R;

public class ReportTab_Fragment extends Fragment implements TabLayout.OnTabSelectedListener {

    private final String[] PAGE_TITLES = new String[]{
            "Daily",
            "Weekly"
    };
    Activity activity;
    Context context;
    View view;
    String location_id, personeel_id;
    private Fragment[] PAGES = null;
    private ViewPager mViewPager;
    public static String tabOneString,tabTwoString;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_report_tab_, container, false);
        getData();
        TabLayout(view);
        return view;
    }
    private void getData() {
        location_id = getArguments().getString("location_id");
        personeel_id = getArguments().getString("personal_id");
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (Activity) context;
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void TabLayout(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("location_id", location_id);
        bundle.putString("personal_id", personeel_id);

        Daily_TabOneFragment daily_tabOneFragment = new Daily_TabOneFragment();
        Weekly_TabOneFragment weekly_tabOneFragment = new Weekly_TabOneFragment();

        daily_tabOneFragment.setArguments(bundle);
        weekly_tabOneFragment.setArguments(bundle);

        PAGES = new Fragment[]{
                daily_tabOneFragment,
                weekly_tabOneFragment,
        };

        mViewPager = view.findViewById(R.id.pager);
        mViewPager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));
        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        /*if(tab.getPosition()==0){
            tabOneString = "SELECTED";
            tabTwoString = "UN_SELECTED";
        }if(tab.getPosition()==1){
            tabOneString = "UN_SELECTED";
            tabTwoString = "SELECTED";
        }*/
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
//        Toast.makeText(getActivity(),"Reselect",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        //   Toast.makeText(getActivity(),"Reselect",Toast.LENGTH_LONG).show();
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return PAGES[position];
        }

        @Override
        public int getCount() {
            return PAGES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return PAGE_TITLES[position];
        }

    }

}
