package com.mydevit.pieservicess.Fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.FragmentSaleschangedActivityBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.model.PieModel;
import com.mydevit.pieservicess.model.SalesModel;
import com.mydevit.pieservicess.navigationDrawer.MenuFragment;
import com.mydevit.pieservicess.navigationDrawer.SlidingActivity;
import com.mydevit.pieservicess.recyclerviewadapters.PieData_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.salesmodel;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;


public class SalesActivityFragment extends Fragment implements ServiceReponseInterface_Duplicate, ClickListener {


   /* @BindView(R.id.chart)
    PieChartView pieChartView;*/

    SalesModel salesModel;
    List<SliceValue> pieData;
    PieChartData pieChartData = null;
    FragmentSaleschangedActivityBinding binding;
    List<NameValuePair> nameValuePairList;
    List<Integer> colorList;
    List<Integer> ringColorList;
    ArrayList<PieModel> pieDataList;
    Context context;
    MenuFragment menuFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //View mainTabView = inflater.inflate(R.layout.fragment_sales_activity, container, false);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_saleschanged_activity, container, false);
        binding.setClickListener(this::onClick);
        menuFragment = new MenuFragment();
        listener();

        return binding.getRoot();
    }

    private void listener() {
        binding.swipeLayoutId.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                PIE_SERVICES(false);
                binding.swipeLayoutId.setRefreshing(false);
            }

        });

    }

    private void PIE_SERVICES(boolean load) {
        colorList = new ArrayList();
        ringColorList = new ArrayList<>();

        colorList.add(Color.parseColor("#AA69AA"));
        colorList.add(Color.parseColor("#AADEEA"));
        colorList.add(Color.parseColor("#C9ff56"));
        colorList.add(Color.parseColor("#34ff11"));
        colorList.add(Color.parseColor("#95ff09"));
        colorList.add(Color.parseColor("#24ff35"));
        colorList.add(Color.parseColor("#66ff89"));
        colorList.add(Color.parseColor("#09ff45"));
        colorList.add(Color.parseColor("#11ff67"));
        colorList.add(Color.parseColor("#84ff90"));
        colorList.add(Color.parseColor("#71ff31"));
        colorList.add(Color.parseColor("#45ff88"));
        colorList.add(Color.parseColor("#78ff67"));

        ringColorList.add(R.color.ring1);
        ringColorList.add(R.color.ring2);
        ringColorList.add(R.color.ring3);
        ringColorList.add(R.color.ring4);
        ringColorList.add(R.color.ring5);
        ringColorList.add(R.color.ring6);
        ringColorList.add(R.color.ring7);
        ringColorList.add(R.color.ring8);
        ringColorList.add(R.color.ring9);
        ringColorList.add(R.color.ring10);
        ringColorList.add(R.color.ring11);
        ringColorList.add(R.color.ring12);
        ringColorList.add(R.color.ring13);

       /* LayerDrawable shape = (LayerDrawable) ContextCompat.getDrawable(context,R.drawable.round_border);
        GradientDrawable gradientDrawable = (GradientDrawable) shape.findDrawableByLayerId(R.id.shape);
        // if you want to change the color of background of textview dynamically
        //gradientDrawable.setColor(ContextCompat.getColor(demo.this,R.color.colorAccent));
        // This is mangage the storke width and it's color by this shape.setStroke(strokeWidth,color);
        gradientDrawable.setStroke(10, ContextCompat.getColor(context,R.color.black));

       // binding.mainLinLayourId.setBackground(shape);
      */
        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        new Service_Integration_Duplicate(context,  nameValuePairList, "business/dashboard/sales_activity", this, "SALES_ACTIVITY", load).execute();
    }


    @Override
    public void onSuccess(String result, String type) {
        if ("SALES_ACTIVITY".equals(type)) {
            salesmodel salesModel = new Gson().fromJson(result, salesmodel.class);
            //Toast.makeText(activity,salesModel.getMessage(),Toast.LENGTH_SHORT).show();

            if(salesModel!=null && salesModel.getData()!=null && salesModel.getData().getMonthy_sales_data()!=null){
                pieData = new ArrayList<>();
                List<com.mydevit.pieservicess.service.salesmodel.DataBean.PieSalesDataBean> pieSalesDataBeans = salesModel.getData().getPie_sales_data();
                for (int i = 0; i < salesModel.getData().getPie_sales_data().size(); i++) {
                    String price = pieSalesDataBeans.get(i).getSale_price();
                    float floats = Float.parseFloat(price);
                    pieData.add(new SliceValue(Math.round(floats), colorList.get(i)).setLabel("Q1: $10"));
                }
                SalesModel salesModelView = new SalesModel();
                salesModelView.setPieData(pieData);
                salesModelView.setNestSalary("" + salesModel.getData().getMonthy_sales_data());
                salesModelView.setNewSustomer("" + salesModel.getData().getMonthly_customers_data());
                salesModelView.setSelected(false);

                pieChartData = new PieChartData(pieData);
                pieChartData.setHasLabels(false).setValueLabelTextSize(14);
                //pieChartData.setHasCenterCircle(true).setCenterText1("Sales in million").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#0097A7"));
                binding.chart.setPieChartData(pieChartData);

                binding.setSales(salesModelView);
                binding.executePendingBindings();

                pieDataList = new ArrayList<>();
                List<salesmodel.DataBean.PieSalesDataBean> getPie_sales_data = salesModel.getData().getPie_sales_data();
                if (getPie_sales_data != null) {
                    for (int i = 0; i < getPie_sales_data.size(); i++) {
                        PieModel pieModel = new PieModel();
                        pieModel.setColor(ringColorList.get(i));
                        pieModel.setName(getPie_sales_data.get(i).getPersonnel());
                        pieDataList.add(pieModel);

                        if (i == getPie_sales_data.size() - 1) {
                            PieData_RecyclerViewAdapter pieData_recyclerViewAdapter = new PieData_RecyclerViewAdapter(context, pieDataList, this);
                            binding.setPieAdapter(pieData_recyclerViewAdapter);
                        }
                    }

                }
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("bookanappointmenttag".equals(tag_FromWhere)) {
            menuFragment.updateMenuItems(2);
            ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.scheduler));
            transactionFragments(new SchedularFragment(), R.id.container);

        }
    }

    public void transactionFragments(Fragment fragment, int viewResource) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(viewResource, fragment);
        ft.addToBackStack(null);
        ft.commit();
        try {
            if (null != MenuFragment.adapter)
                MenuFragment.adapter.notifyDataSetChanged();

        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        menuFragment.updateMenuItems(0);
        ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.dashboard));

        PIE_SERVICES(true);


    }
}