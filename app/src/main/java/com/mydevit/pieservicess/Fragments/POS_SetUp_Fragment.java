package com.mydevit.pieservicess.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.activities.AppointmentsActivity;
import com.mydevit.pieservicess.databinding.FragmentPosSetUpBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.model.SelectedCustomerModel;
import com.mydevit.pieservicess.navigationDrawer.MenuFragment;
import com.mydevit.pieservicess.recyclerviewadapters.POS_SetUp_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.pos_set_up_model;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class POS_SetUp_Fragment extends Fragment implements ClickListener, ServiceReponseInterface_Duplicate {

    View view;
    @BindView(R.id.appointmentsRecyclerViewId)
    RecyclerView posSetUpRecyclerView;

    List<pos_set_up_model.DataBean> setDataArrayList;
    List<NameValuePair> nameValuePairList;

    Context context;
    POS_SetUp_RecyclerViewAdapter pos_setUp_recyclerViewAdapter;
    FragmentPosSetUpBinding fragmentPosSetUpBinding;
    pos_set_up_model pos_set_up_modelObject = null;
    MenuFragment menuFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentPosSetUpBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_pos__set_up_, container, false);
        Initialize();
        return fragmentPosSetUpBinding.getRoot();
    }

    private void Initialize() {
        menuFragment = new MenuFragment();
        setDataArrayList = new ArrayList();
        nameValuePairList = new ArrayList<>();

        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/pos_setup", this, "POS_SETUP", true).execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        menuFragment.updateMenuItems(3);
        try {
            if (null != MenuFragment.adapter)
                MenuFragment.adapter.notifyDataSetChanged();

        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("CLICK".equals(tag_FromWhere)) {

            pos_set_up_model.DataBean dataBean = (pos_set_up_model.DataBean) object;
            /*pos_set_up_model.DataBean dataBean = (pos_set_up_model.DataBean) object;
            Bundle args = new Bundle();
            args.putString("location_id", dataBean.getLocation_id());
            args.putParcelableArrayList("locationArrayList", (ArrayList<? extends Parcelable>) pos_set_up_modelObject.getData());

            Pos_SetUp_TabsFragment pos_setUp_tabsFragment = new Pos_SetUp_TabsFragment();
            pos_setUp_tabsFragment.setArguments(args);

            transactionFragments(pos_setUp_tabsFragment, R.id.container);*/

            ////Rest selection if clicked on new position
            AlphaHolder.selectedCustomerModel = new SelectedCustomerModel();
            Intent intent = new Intent(context, AppointmentsActivity.class);
            intent.putExtra("location_id", dataBean.getLocation_id());
            startActivity(intent);
            ((Activity) context).overridePendingTransition(0, 0);
        }
    }

    public void transactionFragments(Fragment fragment, int viewResource) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(viewResource, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("POS_SETUP".equals(type)) {
            pos_set_up_modelObject = new Gson().fromJson(result, com.mydevit.pieservicess.service.pos_set_up_model.class);
            if (pos_set_up_modelObject != null) {
                setDataArrayList.addAll(pos_set_up_modelObject.getData());
                pos_setUp_recyclerViewAdapter = new POS_SetUp_RecyclerViewAdapter(context, setDataArrayList, this);
                fragmentPosSetUpBinding.setPosSetUpAdapter(pos_setUp_recyclerViewAdapter);

            }
        }
    }

    @Override
    public void onFailed(String result) {

    }
}
