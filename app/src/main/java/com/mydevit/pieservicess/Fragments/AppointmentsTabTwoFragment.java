package com.mydevit.pieservicess.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.activities.AddCustomerDialogActivity;
import com.mydevit.pieservicess.activities.AppointmentsActivity;
import com.mydevit.pieservicess.databinding.ActivityCalenderBinding;
import com.mydevit.pieservicess.databinding.FragmentAppointmentsTabTwoBinding;
import com.mydevit.pieservicess.databinding.PersonnalDialogDesignBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.model.SelectedAppointment_Model;
import com.mydevit.pieservicess.model.SelectedCustomerModel;
import com.mydevit.pieservicess.recyclerviewadapters.ServicesTabRecyclerViewAdapter;
import com.mydevit.pieservicess.recyclerviewadapters.ShowPersonnalGrid_RecyclerViewAdapter;
import com.mydevit.pieservicess.recyclerviewadapters.TimeSlot_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.CartLaunched_ServiceModel;
import com.mydevit.pieservicess.service.Service_DataModel;
import com.mydevit.pieservicess.service.signin_model;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.CustomCalendarView;
import com.mydevit.pieservicess.utils.GridSpacingItemDecoration;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;


public class AppointmentsTabTwoFragment extends Fragment implements CustomCalendarView.RobotoCalendarListener, ClickListener, ServiceReponseInterface_Duplicate {

    public String selectedSlotString;
    View view;
    Context context;

    ArrayList dataArrayList;
    List<Service_DataModel.DataBean.LocationServicesBean> locationPersonnelServicesBeanList;
    ArrayList<SelectedAppointment_Model> isCheckedArrayList;
    AlertDialog alertDialog = null;
    ServicesTabRecyclerViewAdapter servicesTabRecyclerViewAdapter;
    TimeSlot_RecyclerViewAdapter timeSlot_recyclerViewAdapter;
    List<NameValuePair> nameValuePairList;
    public static String location_id, personeel_id;
    FragmentAppointmentsTabTwoBinding fragmentAppointmentsTabTwoBinding;
    View dialogView;
    String selectedDate;
    ActivityCalenderBinding activityCalenderBinding;
    ArrayList<Boolean> timeSlotSelectedArrayList;
    String appointmentSelectedData;
    PersonnalDialogDesignBinding personnalDialogDesignBinding;
    ShowPersonnalGrid_RecyclerViewAdapter showPersonnalGrid_recyclerViewAdapter;
    int spanCount = 2; // 3 columns
    int spacing = 10; // 50px
    int clickedServicePosition = 0;
    boolean includeEdge = true;
    Service_DataModel.DataBean.LocationServicesBean locationServiceBean;
    Service_DataModel service_dataModel;
    String cart_id = "";
    Parcelable arrayListState;
    SharedPreferences_Util sharedPreferences_util;
    boolean isNeedToHide = false;

    ///logoutDialog
    private List<String> personnel_slot;
    private List<String> userList;
    private List<Service_DataModel.DataBean.CustomersBean> customerBeanList;
    private String selectUserId = "", checkInTime, checkOutString;


    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentAppointmentsTabTwoBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_appointments_tab_two, container, false);
        fragmentAppointmentsTabTwoBinding.setClickListener(this);
        hidePersonnelSpinnerOnPersonnelLogin(); // here on service click we do not show any personnel dialog because of personnel login roleid=3, so we replace personnel id with id that we are getting on login screen;


        fragmentAppointmentsTabTwoBinding.autocompleteEditTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Log.e("FIRSTTIME", "COMING");
                TextView rl = (TextView) view;
                fragmentAppointmentsTabTwoBinding.autocompleteEditTextView.setText(rl.getText().toString());
                for (int i = 0; i < customerBeanList.size(); i++) {
                    if (rl.getText().toString().equals(customerBeanList.get(i).getLabel())) {
                        selectUserId = customerBeanList.get(i).getId();

                        ////if after select customer and add data to cart we have update all frgament with same customer id and name
                        //so i am using this aharedpreference.

                        AlphaHolder.selectedCustomerModel = new SelectedCustomerModel();
                        AlphaHolder.selectedCustomerModel.setCustomer_id(selectUserId);
                        AlphaHolder.selectedCustomerModel.setCustomerName(customerBeanList.get(i).getLabel());


                    }
                }

            }
        });
        return fragmentAppointmentsTabTwoBinding.getRoot();
    }

    private void hidePersonnelSpinnerOnPersonnelLogin() {
        sharedPreferences_util = new SharedPreferences_Util(context);
        isNeedToHide = AlphaHolder.isLoginWithPersonnel(SharedPreferences_Util.getRoleId(context));
        signin_model signin_model = sharedPreferences_util.getLoginModel();//// if login with personnel then personnel spinner will be hide and personnel id will be replaced with with login personnel id;
        personeel_id = signin_model.getData().getId();
        fragmentAppointmentsTabTwoBinding.serviceFragmentRecycViewId.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));

    }

    @Override
    public void onResume() {
        super.onResume();
        initialization();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        //id customer already selected then selectedservice model will have the data so
        if (AlphaHolder.selectedCustomerModel != null) {
            selectUserId = AlphaHolder.selectedCustomerModel.getCustomer_id();
            if (fragmentAppointmentsTabTwoBinding != null && fragmentAppointmentsTabTwoBinding.autocompleteEditTextView != null) {
                selectUserId = AlphaHolder.selectedCustomerModel.getCustomer_id();
                fragmentAppointmentsTabTwoBinding.autocompleteEditTextView.setText(AlphaHolder.selectedCustomerModel.getCustomerName());
                fragmentAppointmentsTabTwoBinding.autocompleteEditTextView.dismissDropDown();
            }
        }

        /*if (clickDateDateFormat == null) {
          //  fragmentViewScheduleBinding.compactcalendarView.setCurrentDate();
        } else {
            fragmentViewScheduleBinding.compactcalendarView.setCurrentDate(clickDateDateFormat);
        }*/


    }

    private void initialization() {
        appointmentSelectedData = getCurrenData();
        customerBeanList = new ArrayList<>();
        userList = new ArrayList<>();
        location_id = getArguments().getString("location_id");
        //personeel_id = getArguments().getString("personal_id");


        arrayListState = fragmentAppointmentsTabTwoBinding.serviceFragmentRecycViewId.getLayoutManager().onSaveInstanceState();
        personnel_slot = new ArrayList<>();
        locationPersonnelServicesBeanList = new ArrayList<>();
        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        nameValuePairList.add(new BasicNameValuePair("personnel_id", personeel_id));
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/pos_launching", this, "SERVICE_DATA", true).execute();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    public void showDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(context).inflate(R.layout.addcustomer_layout, viewGroup, false);
        builder.setView(dialogView);

        ImageView closeIMageView = dialogView.findViewById(R.id.closeIMageViewId);
        closeIMageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.show();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.Ser
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    public void calender() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        activityCalenderBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_calender, viewGroup, false);
        activityCalenderBinding.setClickListener(this);
        builder.setView(activityCalenderBinding.getRoot());
        InitialteCalender(activityCalenderBinding);

        alertDialog = builder.create();
        alertDialog.show();

        getSlotServicesApi(appointmentSelectedData);
        fragmentAppointmentsTabTwoBinding.setDate(appointmentSelectedData);
        fragmentAppointmentsTabTwoBinding.executePendingBindings();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    private String getCurrenData() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

    }

    public void getSlotServicesApi(String date) {
        personnel_slot = new ArrayList<>();
        nameValuePairList = new ArrayList<>();

        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        nameValuePairList.add(new BasicNameValuePair("personnel_id", personeel_id));
        nameValuePairList.add(new BasicNameValuePair("choosen_date", date));
        nameValuePairList.add(new BasicNameValuePair("serviceduration", location_id));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/fetch_personnel_open_slots", this, "TIME_SLOT", true).execute();
    }

    //for showcalender
    private void initializationSlot(View view) {
      /*  dataArrayList = new ArrayList();
        dataArrayList.add(true);
        for (int j = 0; j < 20; j++) {
            dataArrayList.add(false);
        }
        RecyclerView slotRecyclerView = mainTabView.findViewById(R.id.slotRecyclerViewId);
        slotRecyclerView.setHasFixedSize(true);
        slotRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
        timeSlot_recyclerViewAdapter = new TimeSlot_RecyclerViewAdapter(context, dataArrayList, this);
        slotRecyclerView.setAdapter(timeSlot_recyclerViewAdapter);*/
    }

    private void InitialteCalender(ActivityCalenderBinding viewA) {
        viewA.markDayButton.setOnClickListener(view -> {
            Calendar calendar = Calendar.getInstance();
            Random random = new Random(System.currentTimeMillis());
            int style = random.nextInt(2);
            int daySelected = random.nextInt(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.DAY_OF_MONTH, daySelected);
            switch (style) {
                case 0:
                    viewA.robotoCalendarPicker.markCircleImage1(calendar.getTime());
                    break;
                case 1:
                    viewA.robotoCalendarPicker.markCircleImage2(calendar.getTime());
                    break;
                default:
                    break;
            }
        });
        viewA.clearSelectedDayButton.setOnClickListener(v -> viewA.robotoCalendarPicker.clearSelectedDay());
        // Set listener, in this case, the same activity
        viewA.robotoCalendarPicker.setRobotoCalendarListener(this);
        viewA.robotoCalendarPicker.setShortWeekDays(false);

        viewA.robotoCalendarPicker.showDateTitle(true);
        viewA.robotoCalendarPicker.setDate(new Date());
    }

    @Override
    public void onDayClick(String date, ArrayList<String> stringArrayList) {

        DateFormat inputFormat = new SimpleDateFormat("dd-MMM-yyyy");
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dateis = inputFormat.parse(date);
            String outputDateStr = outputFormat.format(dateis);
            // Toast.makeText(context, outputDateStr, Toast.LENGTH_SHORT).show();
            appointmentSelectedData = outputDateStr;
            getSlotServicesApi(appointmentSelectedData);
            fragmentAppointmentsTabTwoBinding.setDate(appointmentSelectedData);
            fragmentAppointmentsTabTwoBinding.executePendingBindings();

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDayLongClick(Date date, ArrayList<String> stringArrayList) {

    }

    @Override
    public void onRightButtonClick() {
        // Toast.makeText(context, "onRightButtonClick!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLeftButtonClick() {
        // Toast.makeText(context, "onLeftButtonClick!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("timeslot".equals(tag_FromWhere)) {
            selectedSlotString = (String) object;
            String[] parts = selectedSlotString.split("-");
            checkInTime = parts[0];
            checkOutString = parts[1];
        }
        if ("serviceclick".equals(tag_FromWhere)) {
            clickedServicePosition = position;
            locationServiceBean = (Service_DataModel.DataBean.LocationServicesBean) object;

            String dataIs = fragmentAppointmentsTabTwoBinding.autocompleteEditTextView.getText().toString();
            if ("".equals(selectUserId) || "".equals(dataIs)) {
                AlphaHolder.customToast(context, getResources().getString(R.string.selectcustomerfirst));
            } else {
                if (isNeedToHide) {
                    sendDataToServer(locationServiceBean.getService_price(), personeel_id);
                } else {
                    if (locationServiceBean.getPersoonels().size() > 0)
                        showDilaog(locationServiceBean.getPersoonels());
                    else
                        AlphaHolder.customToast(context, getResources().getString(R.string.thisservicedonthavepersonnel));
                }

            }
        }
        if ("servicepersonnelclick".equals(tag_FromWhere)) {
            Service_DataModel.DataBean.LocationServicesBean.PersoonelsBean persoonelsBean = (Service_DataModel.DataBean.LocationServicesBean.PersoonelsBean) object;
            sendDataToServer(persoonelsBean.getPersonnel_service_price(), persoonelsBean.getPersonnel_id());


            /*locationPersonnelServicesBeanList.get(clickedServicePosition).setPriceWillBeShow(locationServiceBean.getPersonnel_service_price());
            servicesTabRecyclerViewAdapter.notifyDataSetChanged();
            alertDialog.dismiss();*/

            //AlphaHolder.customToast(context, getResources().getString(R.string.selectclientfirst));
        }
        if ("CART_LAUNCHED".equals(tag_FromWhere)) {

        }
        if ("close".equals(tag_FromWhere)) {
            alertDialog.dismiss();
        }
        if ("addcustomerdialog".equals(tag_FromWhere)) {
            Intent intent = new Intent(context, AddCustomerDialogActivity.class);
            if (service_dataModel.getData().getCart_data() != null) {
                intent.putExtra("cartid", service_dataModel.getData().getCart_data().getCart_id());
            } else {
                intent.putExtra("cartid", "");
            }
            startActivity(intent);

        }
        /*if ("Book Now".equals(tag_FromWhere)) {

            if ("".equals(fragmentAppointmentsTabTwoBinding.autocompleteEditTextView.getText().toString())) {
                AlphaHolder.customToast(context, getResources().getString(R.string.selectclientfirst));
            } else if ("".equals(fragmentAppointmentsTabTwoBinding.dateEditTextViewId.getText().toString())) {
                AlphaHolder.customToast(context, getResources().getString(R.string.selectdateandtimeslot));
            } else {
                float totalServicePrice = 0;
                float totalDuration = 0;
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();

                List<String> stringList = new ArrayList<>();
                if (null != locationPersonnelServicesBeanList) {
                    for (int k = 0; k < locationPersonnelServicesBeanList.size(); k++) {
                        if ("Y".equals(locationPersonnelServicesBeanList.get(k).getIsSelected())) {

                            float servicePrice = Float.parseFloat(locationPersonnelServicesBeanList.get(k).getService_price());
                            float duration = Float.parseFloat(locationPersonnelServicesBeanList.get(k).getDuration());

                            try {
                                jsonObject.put("price", servicePrice);
                                jsonObject.put("service_id", locationPersonnelServicesBeanList.get(k).getService_id());
                                jsonArray.put(jsonObject);

                                totalServicePrice = totalServicePrice + servicePrice;
                                totalDuration = totalDuration + duration;


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                        if (k == locationPersonnelServicesBeanList.size() - 1) {
                            if (jsonArray.length() <= 0) {
                                AlphaHolder.customToast(context, getResources().getString(R.string.selectoneservice));
                            } else {
                                nameValuePairList = new ArrayList<>();
                                nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
                                nameValuePairList.add(new BasicNameValuePair("selected_service", jsonArray.toString()));
                                nameValuePairList.add(new BasicNameValuePair("personnel_id", personeel_id));
                                nameValuePairList.add(new BasicNameValuePair("client_id", selectUserId));
                                nameValuePairList.add(new BasicNameValuePair("choose_appointment_date", appointmentSelectedData));
                                nameValuePairList.add(new BasicNameValuePair("slottimein", checkInTime));
                                nameValuePairList.add(new BasicNameValuePair("slottimeout", checkOutString));
                                nameValuePairList.add(new BasicNameValuePair("total_service_price", "" + totalServicePrice));
                                nameValuePairList.add(new BasicNameValuePair("total_duration", "" + totalDuration));
                                nameValuePairList.add(new BasicNameValuePair("appointment_note", ""));
                                nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
                                new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/service_book", this, "BOOK_SERVICE", true).execute();
                            }
                        }
                    }
                }
            }
            *//* *//*


        }*/
        /*if ("calenderclick".equals(tag_FromWhere)) {
            if (isAnyHaveActivate()) calender();
            else
                AlphaHolder.customToast(context, getResources().getString(R.string.atleastselectoneservice));
        }
        if ("SELECT".equals(tag_FromWhere)) {
            alertDialog.dismiss();
        }*/
    }

    private void sendDataToServer(String personnelServicePrice, String personnelId) {
        if (service_dataModel.getData().getCart_data() != null) {
            cart_id = service_dataModel.getData().getCart_data().getCart_id();
        }
        nameValuePairList = new ArrayList<>();

        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        nameValuePairList.add(new BasicNameValuePair("service_id", locationServiceBean.getService_id()));
        nameValuePairList.add(new BasicNameValuePair("service_price", personnelServicePrice));
        nameValuePairList.add(new BasicNameValuePair("service_name", locationServiceBean.getService_name()));
        nameValuePairList.add(new BasicNameValuePair("client_id", selectUserId));
        nameValuePairList.add(new BasicNameValuePair("cart_id", cart_id));
        nameValuePairList.add(new BasicNameValuePair("cart_item_id", ""));
        nameValuePairList.add(new BasicNameValuePair("selected_personnel_id", personnelId));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/launch_save_cart", this, "CART_LAUNCHED", true).execute();

    }

    /* public boolean isAnyHaveActivate() {
         for (int i = 0; i < locationPersonnelServicesBeanList.size(); i++) {
             if ("Y".equals(locationPersonnelServicesBeanList.get(i).getIsSelected()))
                 return true;
         }
         return false;
     }*/
    public void showDilaog(List<Service_DataModel.DataBean.LocationServicesBean.PersoonelsBean> persoonelsBeanList) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);

        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);

        personnalDialogDesignBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.personnal_dialog_design, viewGroup, false);
        personnalDialogDesignBinding.setClickListener(this);
        builder.setView(personnalDialogDesignBinding.getRoot());

        personnalDialogDesignBinding.personnelRecyclerViewId.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        personnalDialogDesignBinding.personnelRecyclerViewId.setLayoutManager(new GridLayoutManager(context, 2));
        ((SimpleItemAnimator) personnalDialogDesignBinding.personnelRecyclerViewId.getItemAnimator()).setSupportsChangeAnimations(false);


        showPersonnalGrid_recyclerViewAdapter = new ShowPersonnalGrid_RecyclerViewAdapter(context, persoonelsBeanList, this);
        personnalDialogDesignBinding.setPersonnaldataadapter(showPersonnalGrid_recyclerViewAdapter);


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        alertDialog = builder.create();
        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        // Apply the newly created layout parameters to the alert dialog window

        alertDialog.getWindow().setAttributes(layoutParams);
        alertDialog.show();
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("SERVICE_DATA".equals(type)) {
            service_dataModel = new Gson().fromJson(result, Service_DataModel.class);
            if (service_dataModel != null && service_dataModel.getData().getLocation_services() != null) {
                locationPersonnelServicesBeanList.addAll(service_dataModel.getData().getLocation_services());

                fragmentAppointmentsTabTwoBinding.serviceFragmentRecycViewId.setLayoutManager(new GridLayoutManager(context, 2));
                ((SimpleItemAnimator) fragmentAppointmentsTabTwoBinding.serviceFragmentRecycViewId.getItemAnimator()).setSupportsChangeAnimations(false);

                servicesTabRecyclerViewAdapter = new ServicesTabRecyclerViewAdapter(context, locationPersonnelServicesBeanList, this::onClick);
                fragmentAppointmentsTabTwoBinding.setSetAdapterData(servicesTabRecyclerViewAdapter);
                fragmentAppointmentsTabTwoBinding.executePendingBindings();
                fragmentAppointmentsTabTwoBinding.serviceFragmentRecycViewId.getLayoutManager().onRestoreInstanceState(arrayListState);

            }
            if (service_dataModel.getData().getCart_items() != null && service_dataModel.getData().getCart_items().size() > 0) {
                AppointmentsActivity.activityAppointmentsBinding.setTotalCart("" + service_dataModel.getData().getCart_items().size());
            } else {
                AppointmentsActivity.activityAppointmentsBinding.setTotalCart("0");
            }
            if (service_dataModel.getData().getCart_data() != null) {
                AppointmentsActivity.cartId = service_dataModel.getData().getCart_data().getCart_id();
            }
            customerBeanList.addAll(service_dataModel.getData().getCustomers());
            for (int k = 0; k < service_dataModel.getData().getCustomers().size(); k++) {
                userList.add(service_dataModel.getData().getCustomers().get(k).getLabel());
                if (k == service_dataModel.getData().getCustomers().size() - 1) {

                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, userList);
                    fragmentAppointmentsTabTwoBinding.autocompleteEditTextView.setAdapter(arrayAdapter);
                    fragmentAppointmentsTabTwoBinding.autocompleteEditTextView.setThreshold(1);
                    fragmentAppointmentsTabTwoBinding.executePendingBindings();


                    //id customer already selected then selectedservice model will have the data so
                    /*selectedCustomerModel = AlphaHolder.getCustomerModel(AppointmentsActivity.appointmentActivityContext);
                    if (selectedCustomerModel != null) {
                        selectedCustomerModel = AlphaHolder.getCustomerModel(AppointmentsActivity.appointmentActivityContext);
                        selectUserId = selectedCustomerModel.getCustomer_id();
                        fragmentAppointmentsTabTwoBinding.autocompleteEditTextView.setText(selectedCustomerModel.getCustomerName());
                        fragmentAppointmentsTabTwoBinding.autocompleteEditTextView.dismissDropDown();
                    }*/

                }
            }
            /*if (service_dataModel.getData().getLocation_personnel_services().size() > 0) {
                for (int i = 0; i < service_dataModel.getData().getLocation_personnel_services().size(); i++) {
                    service_dataModel.getData().getLocation_personnel_services().get(i).setIsSelected("N");

                    if (i == service_dataModel.getData().getLocation_personnel_services().size() - 1) {
                        locationPersonnelServicesBeanList.addAll(service_dataModel.getData().getLocation_personnel_services());
                        servicesTabRecyclerViewAdapter = new ServicesTabRecyclerViewAdapter(context, locationPersonnelServicesBeanList);
                        fragmentAppointmentsTabTwoBinding.setSetAdapterData(servicesTabRecyclerViewAdapter);
                        fragmentAppointmentsTabTwoBinding.executePendingBindings();
                        ((SimpleItemAnimator) fragmentAppointmentsTabTwoBinding.serviceFragmentRecycViewId.getItemAnimator()).setSupportsChangeAnimations(false);
                    }
                }
            }
            customerBeanList.addAll(service_dataModel.getData().getCustomer());
            for (int k = 0; k < service_dataModel.getData().getCustomer().size(); k++) {
                userList.add(service_dataModel.getData().getCustomer().get(k).getLabel());
                if (k == service_dataModel.getData().getCustomer().size() - 1) {
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, userList);
                    fragmentAppointmentsTabTwoBinding.autocompleteEditTextView.setAdapter(arrayAdapter);
                    fragmentAppointmentsTabTwoBinding.autocompleteEditTextView.setThreshold(1);
                    fragmentAppointmentsTabTwoBinding.executePendingBindings();
                }
            }*/
        }
        if ("CART_LAUNCHED".equals(type)) {
            CartLaunched_ServiceModel cartLaunched_serviceModel = new Gson().fromJson(result, CartLaunched_ServiceModel.class);
            if (cartLaunched_serviceModel.getMessage().equals("Service Successfully Added To Cart")) {

                AppointmentsActivity.activityAppointmentsBinding.setTotalCart("" + cartLaunched_serviceModel.getData().getCart_items().size());
                initialization();
                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            }
        }
        /*if ("TIME_SLOT".equals(type)) {
            timeSlotSelectedArrayList = new ArrayList<>();
            Slot_Time_Model slot_time_model = new Gson().fromJson(result, Slot_Time_Model.class);
            if (slot_time_model.getData().get(0).size() > 0) {
                for (int i = 0; i < slot_time_model.getData().get(0).size(); i++) {

                    String message = slot_time_model.getData().get(0).get(i).getMessage();

                    if ("Time Off".equals(message) || "Not working, Today Off".equals(message)) {
                        AlphaHolder.customToast(context, getResources().getString(R.string.notimeslotavailable));
                    } else {
                        personnel_slot.addAll(slot_time_model.getData().get(0).get(i).getPersonnel_slot());
                        if (i == slot_time_model.getData().get(0).size() - 1) {
                            timeSlotSelectedArrayList = new ArrayList<>();
                            for (int j = 0; j < personnel_slot.size(); j++) {
                                if (j == 0) {
                                    timeSlotSelectedArrayList.add(true);
                                } else {
                                    timeSlotSelectedArrayList.add(false);
                                }
                                if (j == personnel_slot.size() - 1) {
                                    TimeSlot_RecyclerViewAdapter timeSlot_recyclerViewAdapter = new TimeSlot_RecyclerViewAdapter(context, personnel_slot, timeSlotSelectedArrayList, this);
                                    activityCalenderBinding.slotRecyclerViewId.setLayoutManager(new GridLayoutManager(context, 2));
                                    activityCalenderBinding.setDateSlotAdapter(timeSlot_recyclerViewAdapter);
                                    activityCalenderBinding.executePendingBindings();
                                }
                            }
                        }
                    }
                }
            }
        }
        if ("BOOK_SERVICE".equals(type)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String message = jsonObject.getString("message");
                if ("Client Booking Added".equals(message)) {
                    ((Activity) context).finish();
                    Intent intent = new Intent(context, AppointmentsActivity.class);
                    intent.putExtra("location_id", location_id);
                    intent.putExtra("personal_id", personeel_id);
                    startActivity(intent);
                    ((Activity) context).overridePendingTransition(0, 0);
                } else {
                    AlphaHolder.customToast(context, message);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }*/
    }

    @Override
    public void onFailed(String result) {

    }
}
