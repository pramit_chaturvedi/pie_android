package com.mydevit.pieservicess.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.activities.GiftCarsd_And_CoupenActivity;
import com.mydevit.pieservicess.databinding.FragmentOptionPosUpBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.CreditCardProcessor_RecyclerViewAdapter;
import com.mydevit.pieservicess.recyclerviewadapters.CurrentCodeRecyclerViewAdapter;
import com.mydevit.pieservicess.recyclerviewadapters.GiftCoupenCards_RecyclerViewAdapter;
import com.mydevit.pieservicess.recyclerviewadapters.GratuitySetting_RecyclerViewAdapter;
import com.mydevit.pieservicess.recyclerviewadapters.PaymentSetting_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.Payment_Setting_Model;
import com.mydevit.pieservicess.service.pos_set_up_model;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OptionPosUp_Fragment extends Fragment implements ServiceReponseInterface_Duplicate, ClickListener {

    public static FragmentOptionPosUpBinding fragmentOptionPosUpBinding;

    /* @BindView(R.id.currentCodeTextViewId)
     RecyclerView currentCodeTextView;

     @BindView(R.id.locationSpinnerId)
     Spinner locationSpinner;*/
    View view;
    ArrayList setDataArrayList;
    CurrentCodeRecyclerViewAdapter currentCodeRecyclerViewAdapter;
    ArrayList<pos_set_up_model.DataBean> arraylist;
    List<String> locationList;
    String location_id;
    Context context;
    List<NameValuePair> nameValuePairList;

    Payment_Setting_Model payment_setting_model;
    PaymentSetting_RecyclerViewAdapter paymentSetting_recyclerViewAdapter;
    CreditCardProcessor_RecyclerViewAdapter creditCardProcessor_recyclerViewAdapter;
    GratuitySetting_RecyclerViewAdapter gratuitySetting_recyclerViewAdapter;
    boolean isCreditCardSelected = false;

    List<String> paymentModeIdList;
    String saltKey, publisableKey, paymentProcessotId, graduityCreditData;


    @BindingAdapter("bind:manipulateWithdrawal")
    public static void manipulateData(Spinner spinner, String value) {
        List<String> strings = new ArrayList<>();
        strings.add("Yes");
        strings.add("No");

        ArrayAdapter arrayAdapter = new ArrayAdapter(spinner.getContext(), android.R.layout.simple_list_item_1, strings);
        spinner.setAdapter(arrayAdapter);

        if ("Y".equals(value))
            spinner.setSelection(0);
        else
            spinner.setSelection(1);
    }

    @BindingAdapter("bind:manipulateLinearLayout")
    public static void manipulateData(LinearLayout linearLayout, boolean status) {
        if (status)
            linearLayout.setVisibility(View.VISIBLE);
        else
            linearLayout.setVisibility(View.GONE);
    }

    @BindingAdapter("bind:manipulateEditText")
    public static void manipulateData(EditText editText, boolean status) {
        if (status)
            editText.setVisibility(View.VISIBLE);
        else
            editText.setVisibility(View.GONE);
    }

    @BindingAdapter("bind:isSpinnerEnabled")
    public static void manipulateData(Spinner spinner, boolean status) {
        spinner.setEnabled(status);
    }

    @BindingAdapter("bind:showEditButton")
    public static void showEditButton(LinearLayout linearLayout, boolean status) {
        if (status) {
            linearLayout.setVisibility(View.VISIBLE);
            fragmentOptionPosUpBinding.setShowEditCloseIconView(false);
        } else {
            linearLayout.setVisibility(View.GONE);
            fragmentOptionPosUpBinding.setShowEditCloseIconView(true);
        }
        fragmentOptionPosUpBinding.executePendingBindings();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentOptionPosUpBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_option_pos_up_, container, false);

        fragmentOptionPosUpBinding.setClickListener(this);
        fragmentOptionPosUpBinding.grauityEditTextId.setEnabled(false);

        fragmentOptionPosUpBinding.setShowEditCloseIconView(false);
        fragmentOptionPosUpBinding.setShowGrauityEditCloseIconView(false);

        fragmentOptionPosUpBinding.setShowGratuityEdit(true);
        fragmentOptionPosUpBinding.setIsEnabledGratuityEditableFields(false);


        fragmentOptionPosUpBinding.setIsEnabledEditableFields(false);
        fragmentOptionPosUpBinding.setShowedit(true);

        getLocationNameData();
        getPaymentSetting();
        getSelectedWithdrawalSpinnerData();
        return fragmentOptionPosUpBinding.getRoot();
    }

    private void getSelectedWithdrawalSpinnerData() {
        fragmentOptionPosUpBinding.gratuityWithdrawalSpinnerId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                graduityCreditData = fragmentOptionPosUpBinding.gratuityWithdrawalSpinnerId.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        fragmentOptionPosUpBinding.locationSpinnerId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                location_id = arraylist.get(position).getLocation_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getPaymentSetting() {
        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/pos_settings", this, "POS_SETTING", true).execute();
    }


   /* private void radioBtn() {
        final RadioGroup rg = (RadioGroup) mainTabView.findViewById(R.id.rgid);
        final RadioButton rb_coldfusion = (RadioButton) mainTabView.findViewById(R.id.rone);
        final RadioButton rb_flex = (RadioButton) mainTabView.findViewById(R.id.ronea);
        final RadioButton rb_flash = (RadioButton) mainTabView.findViewById(R.id.roneb);


        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                *//*if (rb_coldfusion.isChecked()) {
                    rb_coldfusion.setTextColor(Color.BLUE);
                    rb_flex.setTextColor(Color.BLACK);
                    rb_flash.setTextColor(Color.BLACK);
                }
                if (rb_flex.isChecked()){
                    rb_coldfusion.setTextColor(Color.BLACK);
                    rb_flex.setTextColor(Color.BLUE);
                    rb_flash.setTextColor(Color.BLACK);
                }
                if (rb_flash.isChecked()){
                    rb_coldfusion.setTextColor(Color.BLACK);
                    rb_flex.setTextColor(Color.BLACK);
                    rb_flash.setTextColor(Color.BLUE);
                }*//*
            }
        });
    }*/

    /*private void Initialize() {
        setDataArrayList = new ArrayList();
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);

        currentCodeTextView.setHasFixedSize(true);
        currentCodeTextView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        currentCodeRecyclerViewAdapter = new CurrentCodeRecyclerViewAdapter(setDataArrayList, context);
        currentCodeTextView.setAdapter(currentCodeRecyclerViewAdapter);
    }*/

    private void getLocationNameData() {
        nameValuePairList = new ArrayList<>();
        locationList = new ArrayList<>();
        location_id = this.getArguments().getString("location_id");
        arraylist = this.getArguments().getParcelableArrayList("locationArrayList");
        for (int i = 0; i < arraylist.size(); i++) {
            locationList.add(arraylist.get(i).getLocation_name());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onSuccess(String result, String type) {
        if ("POS_SETTING".equals(type)) {
            payment_setting_model = new Gson().fromJson(result, Payment_Setting_Model.class);
            if (payment_setting_model != null) {

                ///for show searchable location data
                ArrayAdapter arrayAdapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, locationList);
                fragmentOptionPosUpBinding.setLocationData(arrayAdapter);
                fragmentOptionPosUpBinding.executePendingBindings();

                //for show the payment setting option
                paymentSetting_recyclerViewAdapter = new PaymentSetting_RecyclerViewAdapter(context, payment_setting_model.getData().getPayment_mode(), this);
                paymentSetting_recyclerViewAdapter.setEnabled(false);
                fragmentOptionPosUpBinding.setPaymentSetting(paymentSetting_recyclerViewAdapter);


                //for show the payment processor option
                creditCardProcessor_recyclerViewAdapter = new CreditCardProcessor_RecyclerViewAdapter(context, payment_setting_model.getData().getPayment_processor(), this);
                creditCardProcessor_recyclerViewAdapter.setEnabled(false);
                fragmentOptionPosUpBinding.setPaymentProcessor(creditCardProcessor_recyclerViewAdapter);

                //for show the Tax Service
                fragmentOptionPosUpBinding.setTaxService(payment_setting_model.getData().getPayment_tax_service());

                //for show the Tax Service
                fragmentOptionPosUpBinding.setTaxProducts(payment_setting_model.getData().getPayment_tax_product());

                //for show the Gratuity Setting
                gratuitySetting_recyclerViewAdapter = new GratuitySetting_RecyclerViewAdapter(context, payment_setting_model.getData().getGratuity().getGratuity_data(), this);
                fragmentOptionPosUpBinding.setGratuitySetting(gratuitySetting_recyclerViewAdapter);

                //set withdrawal data
                fragmentOptionPosUpBinding.setWithdrawal(payment_setting_model.getData().getGratuity_Cash_Withdrawl());

                //set withdrawal data
                GiftCoupenCards_RecyclerViewAdapter giftCoupenCards_recyclerViewAdapter = new GiftCoupenCards_RecyclerViewAdapter(context, payment_setting_model.getData().getGift_Cards_Coupons(), this);
                fragmentOptionPosUpBinding.setCoupenCardData(giftCoupenCards_recyclerViewAdapter);
            }
            fragmentOptionPosUpBinding.executePendingBindings();
        }
        if ("SUBMIT_OPTION_DATA".equals(type)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String message = jsonObject.getString("message");
                if ("Business Payment Setting has been saved successfully".equals(message)) {

                    fragmentOptionPosUpBinding.setShowEditCloseIconView(false);
                    fragmentOptionPosUpBinding.setShowedit(true);

                    paymentSetting_recyclerViewAdapter.setEnabled(false);
                    creditCardProcessor_recyclerViewAdapter.setEnabled(false);
                    fragmentOptionPosUpBinding.setIsEnabledEditableFields(false);
                    fragmentOptionPosUpBinding.executePendingBindings();


                    getPaymentSetting();

                } else {
                    AlphaHolder.customToast(context, message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if ("ADD_GRATUITY_SETTINGS".equals(type)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String message = jsonObject.getString("message");

                if ("Settings has been saved Successfully".equals(message)) {

                    fragmentOptionPosUpBinding.grauityEditTextId.setEnabled(false);
                    fragmentOptionPosUpBinding.setIsEnabledGratuityEditableFields(false);
                    fragmentOptionPosUpBinding.setShowGrauityEditCloseIconView(false);
                    gratuitySetting_recyclerViewAdapter.setEnabled(false);
                    fragmentOptionPosUpBinding.setShowGratuityEdit(true);
                    fragmentOptionPosUpBinding.executePendingBindings();

                    getPaymentSetting();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("addgift".equals(tag_FromWhere)) {
            Intent intent = new Intent(context, GiftCarsd_And_CoupenActivity.class);
            intent.putExtra("location_id", location_id);
            intent.putExtra("from", "AddNew");

            startActivity(intent);
            ((Activity) context).overridePendingTransition(0, 0);
        }
        if ("SHOW_CUSTOM_AMOUNT_EDITTEXT".equals(tag_FromWhere)) {
            Payment_Setting_Model.DataBean.GratuityBean.GratuityDataBean dataBean = (Payment_Setting_Model.DataBean.GratuityBean.GratuityDataBean) object;
            fragmentOptionPosUpBinding.grauityEditTextId.setText(dataBean.getGratuity_amount());
            fragmentOptionPosUpBinding.grauityEditTextId.setVisibility(View.VISIBLE);
        }
        if ("HIDE_CUSTOM_AMOUNT_EDITTEXT".equals(tag_FromWhere)) {
            fragmentOptionPosUpBinding.grauityEditTextId.setVisibility(View.GONE);
        }
        if ("editdata".equals(tag_FromWhere)) {
            Payment_Setting_Model.DataBean.GiftCardsCouponsBean giftCardsCouponsBean = (Payment_Setting_Model.DataBean.GiftCardsCouponsBean) object;
            Intent intent = new Intent(context, GiftCarsd_And_CoupenActivity.class);
            intent.putExtra("from", tag_FromWhere);
            intent.putExtra("location_id", location_id);
            intent.putExtra("discount_offer_id", giftCardsCouponsBean.getDiscount_offer_id());
            intent.putExtra("discount_category", giftCardsCouponsBean.getDiscount_category());
            intent.putExtra("discount_value", giftCardsCouponsBean.getDiscount_value());
            intent.putExtra("discount_limit", giftCardsCouponsBean.getDiscount_limit());
            intent.putExtra("discount_code", giftCardsCouponsBean.getDiscount_code());
            intent.putExtra("discount_type", giftCardsCouponsBean.getDiscount_type());
            intent.putExtra("discount_status", giftCardsCouponsBean.getDiscount_status());

            startActivity(intent);
            ((Activity) context).overridePendingTransition(0, 0);
        }
        if ("edit".equals(tag_FromWhere)) {
            fragmentOptionPosUpBinding.setShowEditCloseIconView(true);
            fragmentOptionPosUpBinding.setShowedit(false);

            paymentSetting_recyclerViewAdapter.setEnabled(true);
            fragmentOptionPosUpBinding.setIsEnabledEditableFields(true);
            creditCardProcessor_recyclerViewAdapter.setEnabled(true);

            fragmentOptionPosUpBinding.executePendingBindings();


        }
        if ("Save".equals(tag_FromWhere)) {

            paymentModeIdList = new ArrayList<>();

            nameValuePairList = new ArrayList<>();

            for (int i = 0; i < payment_setting_model.getData().getPayment_mode().size(); i++) {

                String paymentStatus = payment_setting_model.getData().getPayment_mode().get(i).getPayment_mode_status();
                String paymentId = payment_setting_model.getData().getPayment_mode().get(i).getPayment_mode_id();
                if ("Y".equals(paymentStatus)) {
                    paymentModeIdList.add(paymentId);
                }
                if (i == payment_setting_model.getData().getPayment_mode().size() - 1) {
                    if (paymentModeIdList.size() == 0) {
                        AlphaHolder.customToast(context, getResources().getString(R.string.selectalteastonepaymentmethod));
                    }
                    //for check credit card is selected or not and for handle the credit card
                    for (int j = 0; j < paymentModeIdList.size(); j++) {
                        if ("2".equals(paymentModeIdList.get(j))) {
                            isCreditCardSelected = true;
                            //AlphaHolder.customToast(context, "SELECTED");
                            ///credit card is selected so need to get credit card selected option
                            for (int k = 0; k < payment_setting_model.getData().getPayment_processor().size(); k++) {
                                String paymentProcessorStatus = payment_setting_model.getData().getPayment_processor().get(k).getPayment_mode_status();
                                if ("Y".equals(paymentProcessorStatus)) {
                                    publisableKey = payment_setting_model.getData().getPayment_processor().get(k).getPublishable_key();
                                    saltKey = payment_setting_model.getData().getPayment_processor().get(k).getSecret_key();
                                    paymentProcessotId = payment_setting_model.getData().getPayment_processor().get(k).getPayment_processor_id();
                                }
                            }

                        }
                    }


                    nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
                    nameValuePairList.add(new BasicNameValuePair("payment_tax_services", payment_setting_model.getData().getPayment_tax_service().getPayment_tax_services()));
                    nameValuePairList.add(new BasicNameValuePair("salt_key", saltKey));
                    nameValuePairList.add(new BasicNameValuePair("publishable_key", publisableKey));
                    nameValuePairList.add(new BasicNameValuePair("payment_processor", paymentProcessotId));
                    nameValuePairList.add(new BasicNameValuePair("payment_tax_service_value", payment_setting_model.getData().getPayment_tax_service().getPayment_tax_service_value()));
                    nameValuePairList.add(new BasicNameValuePair("payment_tax_products", payment_setting_model.getData().getPayment_tax_product().getPayment_tax_product()));
                    nameValuePairList.add(new BasicNameValuePair("payment_tax_product_value", payment_setting_model.getData().getPayment_tax_product().getPayment_tax_product_value()));
                    nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
                    nameValuePairList.add(new BasicNameValuePair("payment_mode", paymentModeIdList.toString()));

                    new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/add_payment_setting", this, "SUBMIT_OPTION_DATA", true).execute();


                }
            }
        }
        if ("Cancel".equals(tag_FromWhere)) {
            fragmentOptionPosUpBinding.setShowEditCloseIconView(false);

            paymentSetting_recyclerViewAdapter.setEnabled(false);
            creditCardProcessor_recyclerViewAdapter.setEnabled(false);
            fragmentOptionPosUpBinding.setIsEnabledEditableFields(false);


            fragmentOptionPosUpBinding.setShowedit(true);
            fragmentOptionPosUpBinding.executePendingBindings();

        }
        if ("showgrauityeditdelete".equals(tag_FromWhere)) {
            fragmentOptionPosUpBinding.setShowGrauityEditCloseIconView(true);
            gratuitySetting_recyclerViewAdapter.setEnabled(true);
            fragmentOptionPosUpBinding.setIsEnabledGratuityEditableFields(false);
            fragmentOptionPosUpBinding.setShowGratuityEdit(true);
            fragmentOptionPosUpBinding.executePendingBindings();

        }
        if ("gratuitycancel".equals(tag_FromWhere)) {
            fragmentOptionPosUpBinding.grauityEditTextId.setEnabled(false);
            fragmentOptionPosUpBinding.setShowGrauityEditCloseIconView(false);
            fragmentOptionPosUpBinding.setIsEnabledGratuityEditableFields(false);
            gratuitySetting_recyclerViewAdapter.setEnabled(false);
            fragmentOptionPosUpBinding.setShowGratuityEdit(true);
            fragmentOptionPosUpBinding.executePendingBindings();

        }
        if ("gratuitysave".equals(tag_FromWhere)) {


            String gratuityValue = "";
            String gratuity_custom_amount = "";

            for (int i = 0; i < payment_setting_model.getData().getGratuity().getGratuity_data().size(); i++) {
                String gratuityStsus = payment_setting_model.getData().getGratuity().getGratuity_data().get(i).getGratuity_setting_status();
                String type = payment_setting_model.getData().getGratuity().getGratuity_data().get(i).getGratuity_setting_status();
                if ("Y".equals(gratuityStsus)) {
                    gratuityValue = payment_setting_model.getData().getGratuity().getGratuity_data().get(i).getGratuity_setting_id();

                }
                if ("Y".equals(gratuityStsus) && "C".equals(payment_setting_model.getData().getGratuity().getGratuity_data().get(i).getGratuity_type())) {
                    gratuity_custom_amount = payment_setting_model.getData().getGratuity().getGratuity_data().get(i).getGratuity_amount();
                }

                if (i == payment_setting_model.getData().getGratuity().getGratuity_data().size() - 1) {
                    if (gratuity_custom_amount.equals("")) {
                        gratuity_custom_amount = "0";
                    }
                }
            }


            nameValuePairList = new ArrayList<>();

            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
            nameValuePairList.add(new BasicNameValuePair("location", location_id));
            nameValuePairList.add(new BasicNameValuePair("gratuity", gratuityValue));
            nameValuePairList.add(new BasicNameValuePair("gratuity_id", payment_setting_model.getData().getGratuity().getGratuity_id()));
            nameValuePairList.add(new BasicNameValuePair("gratuity_credit", graduityCreditData)); /*withdrawal yes/no*/
            nameValuePairList.add(new BasicNameValuePair("gratuity_custom_amount", gratuity_custom_amount));

            new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/add_gratuity_setting", this, "ADD_GRATUITY_SETTINGS", false).execute();
        }
        if ("gratuityeditshow".equals(tag_FromWhere)) {
            fragmentOptionPosUpBinding.grauityEditTextId.setEnabled(true);
            fragmentOptionPosUpBinding.setShowGrauityEditCloseIconView(true);
            fragmentOptionPosUpBinding.setIsEnabledGratuityEditableFields(true);

            gratuitySetting_recyclerViewAdapter.setEnabled(true);
            fragmentOptionPosUpBinding.setShowGratuityEdit(false);
            fragmentOptionPosUpBinding.executePendingBindings();

        }
        if ("PAYMENT_SETTING_RADIO_CLICK".equals(tag_FromWhere)) {
            /*for (int i = 0; i < payment_setting_model.getData().getPayment_mode().size(); i++) {
                String paymentMode = payment_setting_model.getData().getPayment_mode().get(position).getPayment_mode_status();
                if ("N".equals(paymentMode) || "".equals(paymentMode))
                    payment_setting_model.getData().getPayment_mode().get(position).setPayment_mode_status("Y");
                else
                    payment_setting_model.getData().getPayment_mode().get(position).setPayment_mode_status("N");

                if (i == payment_setting_model.getData().getPayment_mode().size() - 1) {
                    //fragmentOptionPosUpBinding.getPaymentSetting().notifyItemChanged(position);
                    //fragmentOptionPosUpBinding.executePendingBindings();
                }
            }
*/
        }
    }
}
