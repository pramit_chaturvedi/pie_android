package com.mydevit.pieservicess.Fragments.AppointmentDataInformationTabs;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.FragmentTodayAppointmentsBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.ClientDataInformationTab_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.TodayAppoinClientModel;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class TodayAppointmentsFragment extends Fragment implements ServiceReponseInterface_Duplicate, ClickListener {

    Context context;
    FragmentTodayAppointmentsBinding fragmentTodayAppointmentsBinding;

    List<NameValuePair> nameValuePairList;
    String location_id, client_id;

    ClientDataInformationTab_RecyclerViewAdapter clientDataInformationTab_recyclerViewAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentTodayAppointmentsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_today_appointments, container, false);
        getDataFromService();
        return fragmentTodayAppointmentsBinding.getRoot();
    }

    private void getDataFromService() {
        location_id = getArguments().getString("location_id");
        client_id = getArguments().getString("customer_id");

        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        nameValuePairList.add(new BasicNameValuePair("client_id", client_id));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/Scheduler/client_appointment", this, "TODAY_APPOINTMENT_CLIENT", true).execute();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onSuccess(String result, String type) {
        if ("TODAY_APPOINTMENT_CLIENT".equals(type)) {
            TodayAppoinClientModel todayAppoinClientModel = new Gson().fromJson(result, TodayAppoinClientModel.class);
            if (null != todayAppoinClientModel) {
                clientDataInformationTab_recyclerViewAdapter = new ClientDataInformationTab_RecyclerViewAdapter(context, todayAppoinClientModel.getData().getToday_appointments(), this);
                fragmentTodayAppointmentsBinding.setDataOfRecyclerView(clientDataInformationTab_recyclerViewAdapter);
                fragmentTodayAppointmentsBinding.executePendingBindings();
            }


        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {

    }
}
