package com.mydevit.pieservicess.Fragments.Scheduler_TabFragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityCalenderDateBinding;
import com.mydevit.pieservicess.databinding.BookedAppointmentDescriptionBinding;
import com.mydevit.pieservicess.databinding.FragmentScheduleDateNewBinding;
import com.mydevit.pieservicess.databinding.ServiceRecyclerviewLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.navigationDrawer.SlidingActivity;
import com.mydevit.pieservicess.recyclerviewadapters.BookedAppoint_RecyclerViewAdapter;
import com.mydevit.pieservicess.recyclerviewadapters.ScheduleNew_RecyclerViewAdapter;
import com.mydevit.pieservicess.recyclerviewadapters.ScheduleServices_RecyclerViewAdapter;
import com.mydevit.pieservicess.recyclerviewadapters.TimeSlot_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.ScheduleNewServiceModel;
import com.mydevit.pieservicess.service.Slot_Time_Model;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.CustomCalendarView;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;


public class ScheduleNewDateFragment extends Fragment implements ServiceReponseInterface_Duplicate, ClickListener, CustomCalendarView.RobotoCalendarListener {

    View view;
    FragmentScheduleDateNewBinding fragmentScheduleNewBinding;
    Context context;
    List<NameValuePair> nameValuePairList;
    String location_id, personal_id, serviceDurationString;
    ScheduleNewServiceModel scheduleNewServiceModel;
    ScheduleNewServiceModel scheduleNewServiceModelTemp;
    ActivityCalenderDateBinding activityCalenderDateBinding;
    BookedAppointmentDescriptionBinding bookedAppointmentDescriptionBinding;

    ServiceRecyclerviewLayoutBinding serviceRecyclerviewLayoutBinding;
    ArrayList<Boolean> timeSlotSelectedArrayList;
    List<ScheduleNewServiceModel.DataBean.PersonnelDataBean.PersonnelServiceBean> locationPersonnelServicesBeanList;
    AlertDialog alertDialog = null;
    String appointmentSelectedData;
    String selectUserId;
    int clickedPosition = 0;
    String selectedData = "", selectedSlotString;
    String checkInString = "", checkOutString;
    String barberName;
    String isFromDateClick = "";
    TimeSlot_RecyclerViewAdapter timeSlot_recyclerViewAdapter;
    int totalDuration;
    private List<String> personnel_slot;
    private List<ScheduleNewServiceModel.DataBean.CustomerDataBean> customerBeanList;
    private List<String> userList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentScheduleNewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_schedule_date_new, container, false);
        fragmentScheduleNewBinding.setClickListener(this);
        personnel_slot = new ArrayList<>();
        getDataFromService();


        userList = new ArrayList<>();
        customerBeanList = new ArrayList<>();
        fragmentScheduleNewBinding.autocompleteEditTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                TextView rl = (TextView) view;
                fragmentScheduleNewBinding.autocompleteEditTextView.setText(rl.getText().toString());
                for (int i = 0; i < customerBeanList.size(); i++) {
                    if (rl.getText().toString().equals(customerBeanList.get(i).getLabel())) {
                        selectUserId = customerBeanList.get(i).getId();
                    }
                }
            }
        });
        return fragmentScheduleNewBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.addschedule));
    }

    private void getDataFromService() {

        location_id = getArguments().getString("location_id");
        isFromDateClick = getArguments().getString("isFromDateClick");


        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/Scheduler/schedule_personnel_service", this, "SCHEDULE_NEW", true).execute();

        if ("YES".equals(isFromDateClick)) {
            appointmentSelectedData = getArguments().getString("clickedDate");
            fragmentScheduleNewBinding.setDate(appointmentSelectedData);
            fragmentScheduleNewBinding.calenderImageViewId.setVisibility(View.GONE);
            fragmentScheduleNewBinding.bookedLinLayoutId.setVisibility(View.GONE);

        } else {
            appointmentSelectedData = getCurrenData();
            fragmentScheduleNewBinding.setDate(appointmentSelectedData);
            fragmentScheduleNewBinding.calenderImageViewId.setVisibility(View.VISIBLE);
            fragmentScheduleNewBinding.bookedLinLayoutId.setVisibility(View.VISIBLE);

            //getSlotServicesApi(appointmentSelectedData);
        }
    }

    private String getCurrenData() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("SCHEDULE_NEW".equals(type)) {
            scheduleNewServiceModel = new Gson().fromJson(result, ScheduleNewServiceModel.class);
            if (scheduleNewServiceModel.getData().getPersonnel_data().size() > 0) {
                barberName = scheduleNewServiceModel.getData().getPersonnel_data().get(0).getPersonnel_name();
            }
            manipualteDataWithPosition(0);
            customerBeanList.addAll(scheduleNewServiceModel.getData().getCustomer_data());

            for (int k = 0; k < scheduleNewServiceModel.getData().getCustomer_data().size(); k++) {
                userList.add(scheduleNewServiceModel.getData().getCustomer_data().get(k).getLabel());
                if (k == scheduleNewServiceModel.getData().getCustomer_data().size() - 1) {
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, userList);
                    fragmentScheduleNewBinding.autocompleteEditTextView.setAdapter(arrayAdapter);
                    fragmentScheduleNewBinding.autocompleteEditTextView.setThreshold(1);
                    fragmentScheduleNewBinding.executePendingBindings();
                }
            }
        }
        if ("TIME_SLOT".equals(type)) {
            personnel_slot = new ArrayList<>();
            timeSlotSelectedArrayList = new ArrayList<>();
            Slot_Time_Model slot_time_model = new Gson().fromJson(result, Slot_Time_Model.class);
            if (slot_time_model.getData().get(0).get(0).getMessage() == null || "".equals(slot_time_model.getData().get(0).get(0).getMessage())) {
                if (slot_time_model.getData().get(0).size() > 0) {
                    if (slot_time_model.getData().get(0) != null) {
                        for (int i = 0; i < slot_time_model.getData().get(0).size(); i++) {
                            //String message = slot_time_model.getData().get(0).get(i).getMessage();

                            fragmentScheduleNewBinding.timeSlotNewTextViewId.setVisibility(View.GONE);
                            fragmentScheduleNewBinding.timeSlotNewRVId.setVisibility(View.VISIBLE);

                            if (slot_time_model.getData().get(0).get(i).getPersonnel_slot() != null) {
                                personnel_slot.addAll(slot_time_model.getData().get(0).get(i).getPersonnel_slot());
                                if (i == slot_time_model.getData().get(0).size() - 1) {
                                    timeSlotSelectedArrayList = new ArrayList<>();
                                    for (int j = 0; j < personnel_slot.size(); j++) {
                                        if (j == 0) {
                                            timeSlotSelectedArrayList.add(true);
                                        } else {
                                            timeSlotSelectedArrayList.add(false);
                                        }
                                        if (j == personnel_slot.size() - 1) {
                                            fragmentScheduleNewBinding.setDate(appointmentSelectedData + ", " + personnel_slot.get(0));
                                            timeSlot_recyclerViewAdapter = new TimeSlot_RecyclerViewAdapter(context, personnel_slot, timeSlotSelectedArrayList, this);
                                            fragmentScheduleNewBinding.timeSlotNewRVId.setLayoutManager(new GridLayoutManager(context, 2));
                                            fragmentScheduleNewBinding.setDateSlotAdapter(timeSlot_recyclerViewAdapter);
                                            fragmentScheduleNewBinding.executePendingBindings();
                                        }
                                    }

                                }

                            /*if ("Time Off".equals(message) || "Not working, Today Off".equals(message) || "No slots Available for this week".equals(message)) {
                                AlphaHolder.customToast(context, getResources().getString(R.string.notimeslotavailable));
                                activityCalenderDateBinding.timeSlotTextViewId.setVisibility(View.VISIBLE);
                                activityCalenderDateBinding.slotRecyclerViewId.setVisibility(View.GONE);
                                slot_time_model = new Slot_Time_Model();
                                personnel_slot = new ArrayList<>();
                            } else {
                                activityCalenderDateBinding.timeSlotTextViewId.setVisibility(View.GONE);
                                activityCalenderDateBinding.slotRecyclerViewId.setVisibility(View.VISIBLE);

                                if (slot_time_model.getData().get(0).get(i).getPersonnel_slot() != null) {
                                    personnel_slot.addAll(slot_time_model.getData().get(0).get(i).getPersonnel_slot());
                                    if (i == slot_time_model.getData().get(0).size() - 1) {
                                        timeSlotSelectedArrayList = new ArrayList<>();
                                        for (int j = 0; j < personnel_slot.size(); j++) {
                                            if (j == 0) {
                                                timeSlotSelectedArrayList.add(true);
                                            } else {
                                                timeSlotSelectedArrayList.add(false);
                                            }
                                            if (j == personnel_slot.size() - 1) {
                                                TimeSlot_RecyclerViewAdapter timeSlot_recyclerViewAdapter = new TimeSlot_RecyclerViewAdapter(context, personnel_slot, timeSlotSelectedArrayList, this);
                                                activityCalenderDateBinding.slotRecyclerViewId.setLayoutManager(new GridLayoutManager(context, 2));
                                                activityCalenderDateBinding.setDateSlotAdapter(timeSlot_recyclerViewAdapter);
                                                activityCalenderDateBinding.executePendingBindings();
                                            }
                                        }
                                    }
                                }*/

                            }
                        }
                    }
                }
            } else {
                AlphaHolder.customToast(context, getResources().getString(R.string.notimeslotavailable));
                fragmentScheduleNewBinding.timeSlotNewTextViewId.setVisibility(View.VISIBLE);
                fragmentScheduleNewBinding.timeSlotNewRVId.setVisibility(View.GONE);
                slot_time_model = new Slot_Time_Model();
                personnel_slot = new ArrayList<>();
                //timeSlot_recyclerViewAdapter.notifyDataSetChanged();
            }


        }
        if ("BOOK_SERVICE".equals(type)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String message = jsonObject.getString("message");
                if ("Client Booking Added".equals(message)) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    fm.popBackStack();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void manipualteDataWithPosition(int position) {
        //load Personnal Data
        fragmentScheduleNewBinding.userPersonnalDataId.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        if (scheduleNewServiceModel.getData().getPersonnel_data().size() > 0) {
            scheduleNewServiceModel.getData().getPersonnel_data().get(position).setIsSelected("Y");
            personal_id = scheduleNewServiceModel.getData().getPersonnel_data().get(position).getPersonnel_id();
        }
        ScheduleNew_RecyclerViewAdapter scheduleNew_recyclerViewAdapter = new ScheduleNew_RecyclerViewAdapter(context, scheduleNewServiceModel.getData().getPersonnel_data(), this);
        fragmentScheduleNewBinding.setLoadDataUser(scheduleNew_recyclerViewAdapter);
        if (scheduleNewServiceModel.getData().getPersonnel_data().size() > 0) {
            locationPersonnelServicesBeanList = scheduleNewServiceModel.getData().getPersonnel_data().get(position).getPersonnel_service();

            List<String> stringList = getTextFromService(scheduleNewServiceModel.getData().getPersonnel_data().get(position).getPersonnel_service());
            ArrayAdapter arrayAdapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, stringList);
            fragmentScheduleNewBinding.setServices(arrayAdapter);

            //load services
            fragmentScheduleNewBinding.bookedApppintmentsRVId.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            BookedAppoint_RecyclerViewAdapter bookedAppoint_recyclerViewAdapter = new BookedAppoint_RecyclerViewAdapter(context, scheduleNewServiceModel.getData().getPersonnel_data().get(position).getBooked_aptmt(), this);
            fragmentScheduleNewBinding.setBookedAppointmentAdapter(bookedAppoint_recyclerViewAdapter);
        }

        //load services

    }

    private List<String> getTextFromService(List<ScheduleNewServiceModel.DataBean.PersonnelDataBean.PersonnelServiceBean> personnel_service) {
        List<String> stringList = new ArrayList<>();
        for (int i = 0; i < personnel_service.size(); i++) {
            stringList.add(personnel_service.get(i).getService_name());
        }
        return stringList;
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("timeslot".equals(tag_FromWhere)) {
            selectedSlotString = (String) object;
            String[] parts = selectedSlotString.split("-");
            checkInString = parts[0];
            checkOutString = parts[1];
            fragmentScheduleNewBinding.setDate(appointmentSelectedData + ", " + checkInString + " - " + checkOutString);

        }
        if ("close".equals(tag_FromWhere)) {
            alertDialog.dismiss();
        }
        if ("SELECT".equals(tag_FromWhere)) {
            alertDialog.dismiss();
        }
        if ("schedulenewuser".equals(tag_FromWhere)) {
            for (int i = 0; i < scheduleNewServiceModel.getData().getPersonnel_data().size(); i++) {
                if (i == position) {
                    barberName = scheduleNewServiceModel.getData().getPersonnel_data().get(i).getPersonnel_name();
                    personal_id = scheduleNewServiceModel.getData().getPersonnel_data().get(i).getPersonnel_id();
                    scheduleNewServiceModel.getData().getPersonnel_data().get(i).setIsSelected("Y");
                } else {
                    scheduleNewServiceModel.getData().getPersonnel_data().get(i).setIsSelected("N");
                }
            }
            clickedPosition = position;
            fragmentScheduleNewBinding.getLoadDataUser().notifyDataSetChanged();
            selectedData = "";
            fragmentScheduleNewBinding.selectServiceTextViewId.setText(getResources().getString(R.string.selectservice));

            manipualteDataWithPosition(position);
        }
        if ("save".equals(tag_FromWhere)) {

        }
        if ("serviceclicked".equals(tag_FromWhere)) {
            //  scheduleNewServiceModelTemp = scheduleNewServiceModel;
            for (int i = 0; i < scheduleNewServiceModel.getData().getPersonnel_data().get(clickedPosition).getPersonnel_service().size(); i++) {
                scheduleNewServiceModel.getData().getPersonnel_data().get(clickedPosition).getPersonnel_service().get(i).setIsSelected("N");
                if (i == scheduleNewServiceModel.getData().getPersonnel_data().get(clickedPosition).getPersonnel_service().size() - 1) {
                    showServiceSpinner();
                }
            }

        }
        if ("bookappointmentclick".equals(tag_FromWhere)) {
            ScheduleNewServiceModel.DataBean.PersonnelDataBean.BookedAptmtBean bookedAptmtBean = (ScheduleNewServiceModel.DataBean.PersonnelDataBean.BookedAptmtBean) object;
            showBookedAppointmentDetail(bookedAptmtBean);
        }
        if ("checkclicked".equals(tag_FromWhere)) {
            if ("Y".equals(scheduleNewServiceModel.getData().getPersonnel_data().get(clickedPosition).getPersonnel_service().get(position).getIsSelected()))
                scheduleNewServiceModel.getData().getPersonnel_data().get(clickedPosition).getPersonnel_service().get(position).setIsSelected("N");
            else {
                scheduleNewServiceModel.getData().getPersonnel_data().get(clickedPosition).getPersonnel_service().get(position).setIsSelected("Y");
            }
        }
        if ("configuredate".equals(tag_FromWhere)) {
            calender();
        }
        if ("Book Now".equals(tag_FromWhere)) {
            if ("".equals(fragmentScheduleNewBinding.autocompleteEditTextView.getText().toString())) {
                AlphaHolder.customToast(context, getResources().getString(R.string.selectclientfirst));
            } else if ("".equals(fragmentScheduleNewBinding.pickNewWeekEditTextId.getText().toString())) {
                AlphaHolder.customToast(context, getResources().getString(R.string.selectdateandtimeslot));
            } else {
                float totalServicePrice = 0;
                float totalDuration = 0;
                JSONArray jsonArray = new JSONArray();


                List<String> stringList = new ArrayList<>();
                if (null != locationPersonnelServicesBeanList) {
                    for (int k = 0; k < locationPersonnelServicesBeanList.size(); k++) {
                        if ("Y".equals(locationPersonnelServicesBeanList.get(k).getIsSelected())) {

                            float servicePrice = Float.parseFloat(locationPersonnelServicesBeanList.get(k).getService_price());
                            float duration = Float.parseFloat(locationPersonnelServicesBeanList.get(k).getService_duration());
                            String serviceId = locationPersonnelServicesBeanList.get(k).getService_id();

                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("price", servicePrice);
                                jsonObject.put("service_id", serviceId);
                                jsonArray.put(jsonObject);

                                totalServicePrice = totalServicePrice + servicePrice;
                                totalDuration = totalDuration + duration;


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        if (k == locationPersonnelServicesBeanList.size() - 1) {
                            if (jsonArray.length() <= 0) {
                                AlphaHolder.customToast(context, getResources().getString(R.string.selectoneservice));
                            } else if ("".equals(checkInString)) {
                                AlphaHolder.customToast(context, getResources().getString(R.string.selecttimeslot));
                            } else {
                                nameValuePairList = new ArrayList<>();
                                nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
                                nameValuePairList.add(new BasicNameValuePair("selected_service", jsonArray.toString()));
                                nameValuePairList.add(new BasicNameValuePair("personnel_id", personal_id));
                                nameValuePairList.add(new BasicNameValuePair("client_id", selectUserId));
                                nameValuePairList.add(new BasicNameValuePair("choose_appointment_date", appointmentSelectedData));
                                nameValuePairList.add(new BasicNameValuePair("slottimein", checkInString));
                                nameValuePairList.add(new BasicNameValuePair("slottimeout", checkOutString));
                                nameValuePairList.add(new BasicNameValuePair("total_service_price", "" + totalServicePrice));
                                nameValuePairList.add(new BasicNameValuePair("total_duration", "" + totalDuration));
                                nameValuePairList.add(new BasicNameValuePair("appointment_note", ""));
                                nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));

                                new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/service_book", this, "BOOK_SERVICE", true).execute();
                            }
                        }
                    }
                }
            }
        }
    }

    ///call calenderslot service
    public void calender() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        activityCalenderDateBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_calender_date, viewGroup, false);
        activityCalenderDateBinding.setClickListener(this);
        builder.setView(activityCalenderDateBinding.getRoot());
        InitialteCalender(activityCalenderDateBinding);

        alertDialog = builder.create();
        alertDialog.show();

        getSlotServicesApi(appointmentSelectedData);
        fragmentScheduleNewBinding.executePendingBindings();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    public void showBookedAppointmentDetail(ScheduleNewServiceModel.DataBean.PersonnelDataBean.BookedAptmtBean bookedAptmtBean) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        bookedAppointmentDescriptionBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.booked_appointment_description, viewGroup, false);
        bookedAppointmentDescriptionBinding.setClickListner(this);
        bookedAppointmentDescriptionBinding.setBarberName(barberName);
        bookedAppointmentDescriptionBinding.setData(bookedAptmtBean);

        builder.setView(bookedAppointmentDescriptionBinding.getRoot());

        alertDialog = builder.create();
        alertDialog.show();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    public void showServiceSpinner() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        serviceRecyclerviewLayoutBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.service_recyclerview_layout, viewGroup, false);
        ScheduleServices_RecyclerViewAdapter adapter = new ScheduleServices_RecyclerViewAdapter(context, scheduleNewServiceModel.getData().getPersonnel_data().get(clickedPosition).getPersonnel_service(), this);
        serviceRecyclerviewLayoutBinding.setAdapterServiceData(adapter);

        serviceRecyclerviewLayoutBinding.saveButtonId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                totalDuration = 0;

                selectedData = "";
                for (int i = 0; i < scheduleNewServiceModel.getData().getPersonnel_data().get(clickedPosition).getPersonnel_service().size(); i++) {
                    if ("Y".equals(scheduleNewServiceModel.getData().getPersonnel_data().get(clickedPosition).getPersonnel_service().get(i).getIsSelected())) {
                        selectedData = selectedData + ", " + scheduleNewServiceModel.getData().getPersonnel_data().get(clickedPosition).getPersonnel_service().get(i).getService_name();
                        String duration = scheduleNewServiceModel.getData().getPersonnel_data().get(clickedPosition).getPersonnel_service().get(i).getService_duration();
                        int durationIs = Integer.parseInt(duration);
                        totalDuration = totalDuration + durationIs;
                    }
                }
                if (selectedData.equals("")) {
                    fragmentScheduleNewBinding.selectServiceTextViewId.setText(getResources().getString(R.string.selectservice));
                } else {
                    String[] split = selectedData.split(",", 2);
                    String firstSubString = split[0];
                    String valueIs = split[1];

                    fragmentScheduleNewBinding.selectServiceTextViewId.setText(valueIs);
                    getSlotServicesApi(appointmentSelectedData);
                }
                alertDialog.dismiss();


            }
        });

        builder.setView(serviceRecyclerviewLayoutBinding.getRoot());

        alertDialog = builder.create();
        alertDialog.show();


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    private void InitialteCalender(ActivityCalenderDateBinding viewA) {
        viewA.markDayButton.setOnClickListener(view -> {
            Calendar calendar = Calendar.getInstance();
            Random random = new Random(System.currentTimeMillis());
            int style = random.nextInt(2);
            int daySelected = random.nextInt(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.DAY_OF_MONTH, daySelected);
            switch (style) {
                case 0:
                    viewA.robotoCalendarPicker.markCircleImage1(calendar.getTime());
                    break;
                case 1:
                    viewA.robotoCalendarPicker.markCircleImage2(calendar.getTime());
                    break;
                default:
                    break;
            }
        });
        viewA.clearSelectedDayButton.setOnClickListener(v -> viewA.robotoCalendarPicker.clearSelectedDay());
        // Set listener, in this case, the same activity
        viewA.robotoCalendarPicker.setRobotoCalendarListener(this);

        viewA.robotoCalendarPicker.setShortWeekDays(false);

        viewA.robotoCalendarPicker.showDateTitle(true);

        viewA.robotoCalendarPicker.setDate(new Date());
    }

    public void getSlotServicesApi(String date) {
        personnel_slot = new ArrayList<>();
        nameValuePairList = new ArrayList<>();

        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        nameValuePairList.add(new BasicNameValuePair("personnel_id", personal_id));
        nameValuePairList.add(new BasicNameValuePair("choosen_date", date));
        nameValuePairList.add(new BasicNameValuePair("serviceduration", "" + totalDuration));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/fetch_personnel_open_slots", this, "TIME_SLOT", true).execute();
    }

    @Override
    public void onDayClick(String date, ArrayList<String> stringArrayList) {
        DateFormat inputFormat = new SimpleDateFormat("dd-MMM-yyyy");
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dateis = inputFormat.parse(date);
            String outputDateStr = outputFormat.format(dateis);

            // Toast.makeText(context, outputDateStr, Toast.LENGTH_SHORT).show();

            appointmentSelectedData = outputDateStr;
            getSlotServicesApi(appointmentSelectedData);
            fragmentScheduleNewBinding.setDate(appointmentSelectedData);
            fragmentScheduleNewBinding.executePendingBindings();

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDayLongClick(Date date, ArrayList<String> stringArrayList) {

    }

    @Override
    public void onRightButtonClick() {

    }

    @Override
    public void onLeftButtonClick() {

    }
}
