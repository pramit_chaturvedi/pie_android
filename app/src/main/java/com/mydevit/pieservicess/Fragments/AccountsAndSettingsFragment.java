package com.mydevit.pieservicess.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.mydevit.pieservicess.Fragments.Scheduler_TabFragments.ConfigureAvailability_Fragment;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.activities.ChangePasswordActivity;
import com.mydevit.pieservicess.databinding.FragmentAccountsSettingsBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.navigationDrawer.SlidingActivity;


public class AccountsAndSettingsFragment extends Fragment implements ClickListenerPC {

    Context context;
    private FragmentAccountsSettingsBinding fragmentAccountSettings;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentAccountSettings = DataBindingUtil.inflate(inflater, R.layout.fragment_accounts_settings, container, false);
        fragmentAccountSettings.setClickListener(this);

        return fragmentAccountSettings.getRoot();

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
        if ("Change Password".equals(tag_FromWhere)) {
            Intent intent = new Intent(context, ChangePasswordActivity.class);
            startActivity(intent);
            getActivity().overridePendingTransition(0, 0);
        }
        if ("Configuration Availability".equals(tag_FromWhere)) {
            ((SlidingActivity) getActivity()).transactionFragments(new ConfigureAvailability_Fragment(), R.id.container);
        }
    }

}
