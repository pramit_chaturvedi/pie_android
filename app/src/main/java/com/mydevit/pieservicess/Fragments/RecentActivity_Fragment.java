package com.mydevit.pieservicess.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.RecentActivitBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.model.recent_servicemodel;
import com.mydevit.pieservicess.recyclerviewadapters.RequestsRecyclerViewAdapter;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class RecentActivity_Fragment extends Fragment implements ClickListener, ServiceReponseInterface_Duplicate {

    List<recent_servicemodel.DataBean> dataArrayList;
    RecentActivitBinding recentActivityBinding;
    List<NameValuePair> nameValuePairList;
    String token = "";
    Context context;
    private Handler mWaitHandler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        recentActivityBinding = DataBindingUtil.inflate(inflater, R.layout.recent_activit, container, false);
        return recentActivityBinding.getRoot();
    }
    @Override
    public void onResume() {
        super.onResume();
        serviceInitialization();
    }
    private void serviceInitialization() {
        dataArrayList = new ArrayList();
        RequestsRecyclerViewAdapter requestsRecyclerViewAdapter = new RequestsRecyclerViewAdapter(dataArrayList, context, this);
        recentActivityBinding.setAdapter(requestsRecyclerViewAdapter);
        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        new Service_Integration_Duplicate(context, SharedPreferences_Util.getRoleId(context), nameValuePairList, "business/dashboard/recent_activity", this, "RECENT_ACTIVITY", true).execute();

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        recent_servicemodel.DataBean dataBean = (recent_servicemodel.DataBean) object;
        Toast.makeText(context, dataBean.getName(), Toast.LENGTH_SHORT).show();

    }
    @Override
    public void onSuccess(String result, String type) {
        if ("RECENT_ACTIVITY".equals(type)) {
            //  Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            Gson gson = new Gson();
            recent_servicemodel recent_servicemodel = gson.fromJson(result, com.mydevit.pieservicess.model.recent_servicemodel.class);
            dataArrayList.addAll(recent_servicemodel.getData());
            recentActivityBinding.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onFailed(String result) {

    }
}
