package com.mydevit.pieservicess.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.service.pos_set_up_model;
import java.util.ArrayList;


public class Pos_SetUp_TabsFragment extends Fragment implements TabLayout.OnTabSelectedListener {
    private final String[] PAGE_TITLES = new String[]{
            "Options",
            "Personals"
    };
    Activity activity;
    Context context;
    View view;
    String location_id;
    ArrayList<pos_set_up_model.DataBean> arraylist;
    private Fragment[] PAGES = null;
    private ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pos__set_up__tabs, container, false);
        TabLayout(view);
        return view;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
    public void TabLayout(View view) {
        arraylist = this.getArguments().getParcelableArrayList("locationArrayList");
        location_id = this.getArguments().getString("location_id");

        PersonalsFragment personalsFragment = new PersonalsFragment();

        OptionPosUp_Fragment optionPosUpFragment = new OptionPosUp_Fragment();
        Bundle bundle = new Bundle();
        bundle.putString("location_id", location_id);
        bundle.putParcelableArrayList("locationArrayList", arraylist);


        optionPosUpFragment.setArguments(bundle);
        personalsFragment.setArguments(bundle);

        PAGES = new Fragment[]{
                optionPosUpFragment,
                personalsFragment
        };
        mViewPager = view.findViewById(R.id.pager);
        mViewPager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));
        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
//        Toast.makeText(getActivity(),"Reselect",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        //   Toast.makeText(getActivity(),"Reselect",Toast.LENGTH_LONG).show();

    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return PAGES[position];
        }

        @Override
        public int getCount() {
            return PAGES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return PAGE_TITLES[position];
        }

    }

}
