package com.mydevit.pieservicess.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.activities.AppointmentsActivity;
import com.mydevit.pieservicess.activities.BillActivity;
import com.mydevit.pieservicess.databinding.FragmentAppointmentsTabProductsBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.model.SelectedCustomerModel;
import com.mydevit.pieservicess.recyclerviewadapters.ProductsTabRecyclerViewAdapter;
import com.mydevit.pieservicess.service.Product_List_Model;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class AppointmentsTabThreeFragment extends Fragment implements ServiceReponseInterface_Duplicate, ClickListener {

    public static String location_id, personeel_id = "0", selectUserId = "";
    public static boolean isPersonnelLoggedIn = false;
    Context context;
    List<Product_List_Model.DataBean.LocationItemsBean> locationItemsBeanList;
    ArrayList<Boolean> isCheckedArrayList;
    ProductsTabRecyclerViewAdapter productsTabRecyclerViewAdapter;
    Product_List_Model product_list_model;
    List<NameValuePair> nameValuePairList;
    FragmentAppointmentsTabProductsBinding fragmentAppointmentsTabProductsBinding;
    private List<Product_List_Model.DataBean.CustomersBean> customerBeanList;

    @BindingAdapter("bind:filterSpinner")
    public static void filterSpinner(Spinner spinner, Product_List_Model product_list_model) {
        List<String> stringList = new ArrayList<>();
        stringList.add(spinner.getContext().getResources().getString(R.string.allproducts));
        if (product_list_model != null) {
            List<Product_List_Model.DataBean.LocationItemsCategoryBean> categoryBeans = product_list_model.getData().getLocation_items_category();
            if (categoryBeans.size() > 0) {
                for (int i = 0; i < categoryBeans.size(); i++) {
                    String categoryName = categoryBeans.get(i).getCategory_name();
                    stringList.add(categoryName);

                    if (i == categoryBeans.size() - 1) {
                        ArrayAdapter arrayAdapter = new ArrayAdapter(spinner.getContext(), R.layout.spinner_layout, stringList);
                        spinner.setAdapter(arrayAdapter);
                    }
                }
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentAppointmentsTabProductsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_appointments_tab_products, container, false);
        fragmentAppointmentsTabProductsBinding.setClickListener(this);
        getData();
        getPersonnelIfPersonnelLogin();
        initialization();
        autoCompleteTextView();
        spinnerFilter();
        return fragmentAppointmentsTabProductsBinding.getRoot();
    }

    private void getPersonnelIfPersonnelLogin() {
        isPersonnelLoggedIn = AlphaHolder.isLoginWithPersonnel(SharedPreferences_Util.getRoleId(context));
        if (isPersonnelLoggedIn) {
            personeel_id = SharedPreferences_Util.getLoggedInPersonnelId(context);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        //id customer already selected then selectedservice model will have the data so
        if (AlphaHolder.selectedCustomerModel != null) {
            if (fragmentAppointmentsTabProductsBinding != null && fragmentAppointmentsTabProductsBinding.autocompleteEditTextView != null) {
                selectUserId = AlphaHolder.selectedCustomerModel.getCustomer_id();
                fragmentAppointmentsTabProductsBinding.autocompleteEditTextView.setText(AlphaHolder.selectedCustomerModel.getCustomerName());
                fragmentAppointmentsTabProductsBinding.autocompleteEditTextView.dismissDropDown();
                getPersonnelIfPersonnelLogin();
                initialization();
            }
        }
    }

    private void spinnerFilter() {
        fragmentAppointmentsTabProductsBinding.filterSpinnerId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position > 0) {
                    String category = product_list_model.getData().getLocation_items_category().get(position - 1).getCategory_id();
                    List<Product_List_Model.DataBean.LocationItemsBean> tempItemsBeans = new ArrayList<>();
                    for (int k = 0; k < locationItemsBeanList.size(); k++) {
                        if (category.equals(locationItemsBeanList.get(k).getItem_category_id())) {
                            tempItemsBeans.add(locationItemsBeanList.get(k));
                        }
                        if (k == locationItemsBeanList.size() - 1) {
                            loadProductData(tempItemsBeans);
                        }
                    }
                } else {
                    loadProductData(locationItemsBeanList);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void autoCompleteTextView() {
        fragmentAppointmentsTabProductsBinding.autocompleteEditTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                TextView rl = (TextView) view;
                fragmentAppointmentsTabProductsBinding.autocompleteEditTextView.setText(rl.getText().toString());
                for (int i = 0; i < customerBeanList.size(); i++) {
                    if (rl.getText().toString().equals(customerBeanList.get(i).getLabel())) {
                        selectUserId = customerBeanList.get(i).getId();

                        //if u update the value from here then it will ch ange cutomer selection on all pages
                        AlphaHolder.selectedCustomerModel = new SelectedCustomerModel();
                        AlphaHolder.selectedCustomerModel.setCustomer_id(selectUserId);
                        AlphaHolder.selectedCustomerModel.setCustomerName(customerBeanList.get(i).getLabel());
                    }
                }
            }
        });
    }

    private void getData() {
        location_id = getArguments().getString("location_id");
        //personeel_id = getArguments().getString("personal_id");
    }

    private void initialization() {
        customerBeanList = new ArrayList<>();
        locationItemsBeanList = new ArrayList<>();
        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/checkout_launch_products", this, "PRODUCTSLIST", true).execute();

       /*isCheckedArrayList = new ArrayList<>();
        dataArrayList = new ArrayList();
        for (int j = 0; j < 10; j++) {
            dataArrayList.add(null);
            isCheckedArrayList.add(false);
        }
        productsFragmentRecycView.setHasFixedSize(true);
        productsFragmentRecycView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        productsTabRecyclerViewAdapter = new ProductsTabRecyclerViewAdapter(context, dataArrayList, isCheckedArrayList);
        productsFragmentRecycView.setAdapter(productsTabRecyclerViewAdapter);*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSuccess(String result, String type) {
        List<String> userList;
        if ("PRODUCTSLIST".equals(type)) {
            product_list_model = new Gson().fromJson(result, Product_List_Model.class);
            if (product_list_model != null) {
                fragmentAppointmentsTabProductsBinding.setAlldata(product_list_model);
            }
            if (product_list_model.getData().getLocation_items().size() > 0) {
                locationItemsBeanList.addAll(product_list_model.getData().getLocation_items());
                loadProductData(locationItemsBeanList);
            }
            customerBeanList = new ArrayList<>();
            userList = new ArrayList<>();
            customerBeanList.addAll(product_list_model.getData().getCustomers());
            for (int k = 0; k < product_list_model.getData().getCustomers().size(); k++) {
                userList.add(product_list_model.getData().getCustomers().get(k).getLabel());
                if (k == product_list_model.getData().getCustomers().size() - 1) {
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, userList);
                    fragmentAppointmentsTabProductsBinding.autocompleteEditTextView.setAdapter(arrayAdapter);
                    fragmentAppointmentsTabProductsBinding.autocompleteEditTextView.setThreshold(1);
                    fragmentAppointmentsTabProductsBinding.executePendingBindings();
                }
            }
            if (product_list_model.getData().getCart_data() != null) {
                AppointmentsActivity.cartId = product_list_model.getData().getCart_data().getCart_id();

            }
            ///if not null

        }
        if ("CHECKOUT_SAVE".equals(type)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                if ("Product Added Successfully".equals(jsonObject.getString("message"))) {
                    fragmentAppointmentsTabProductsBinding.autocompleteEditTextView.setText("");

                    selectUserId = "";
                    locationItemsBeanList.clear();


                    Intent intent = new Intent(context, BillActivity.class);
                    intent.putExtra("location_id", location_id);
                    intent.putExtra("personal_id", personeel_id);

                    if (product_list_model.getData().getCart_data() != null) {
                        intent.putExtra("card_id", product_list_model.getData().getCart_data().getCart_id());
                    } else {
                        intent.putExtra("card_id", "");
                    }
                    startActivity(intent);
                    ((Activity) context).overridePendingTransition(0, 0);
                    ((Activity) context).finish();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadProductData(List<Product_List_Model.DataBean.LocationItemsBean> locationItemsBeanList) {
        productsTabRecyclerViewAdapter = new ProductsTabRecyclerViewAdapter(context, locationItemsBeanList);
        fragmentAppointmentsTabProductsBinding.setSetAdapter(productsTabRecyclerViewAdapter);
        ((SimpleItemAnimator) fragmentAppointmentsTabProductsBinding.serviceFragmentRecycViewId.getItemAnimator()).setSupportsChangeAnimations(false);
        fragmentAppointmentsTabProductsBinding.executePendingBindings();
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("Checkout".equals(tag_FromWhere)) {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject;
            for (int i = 0; i < locationItemsBeanList.size(); i++) {
                if ("Y".equals(locationItemsBeanList.get(i).getIsSelected())) {
                    try {
                        jsonObject = new JSONObject();
                        jsonObject.put("item_name", locationItemsBeanList.get(i).getItem_name());
                        jsonObject.put("item_id", locationItemsBeanList.get(i).getItem_id());

                        for (int j = 0; j < locationItemsBeanList.get(i).getPrice_variations().size(); j++) {
                            if ("Y".equals(locationItemsBeanList.get(i).getPrice_variations().get(j).getIsSelected())) {

                                jsonObject.put("item_price", locationItemsBeanList.get(i).getPrice_variations().get(j).getItem_price());
                                jsonObject.put("item_variation_id", locationItemsBeanList.get(i).getPrice_variations().get(j).getItem_variation_id());
                                jsonObject.put("item_option_id", locationItemsBeanList.get(i).getPrice_variations().get(j).getItem_option_id());
                                jsonObject.put("item_price_id", locationItemsBeanList.get(i).getPrice_variations().get(j).getItem_price_id());
                                jsonArray.put(jsonObject);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (i == locationItemsBeanList.size() - 1) {
                    String jsonArrayString = jsonArray.toString();
                    String serviceText = fragmentAppointmentsTabProductsBinding.autocompleteEditTextView.getText().toString();
                    if ("[]".equals(jsonArrayString)) {
                        AlphaHolder.customToast(context, getResources().getString(R.string.selectalteastoneproduct));
                    } else if ("".equals(selectUserId) || "".equals(serviceText)) {
                        AlphaHolder.customToast(context, getResources().getString(R.string.selectcustomer));
                    } else {
                        String cart_id = "";
                        if (product_list_model.getData().getCart_data() != null) {
                            cart_id = product_list_model.getData().getCart_data().getCart_id();
                        }

                        if (personeel_id == null)
                            personeel_id = "0";

                        nameValuePairList = new ArrayList<>();
                        nameValuePairList.add(new BasicNameValuePair("item", jsonArrayString));
                        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
                        nameValuePairList.add(new BasicNameValuePair("cart_id", cart_id));
                        nameValuePairList.add(new BasicNameValuePair("personnel_id", personeel_id));
                        nameValuePairList.add(new BasicNameValuePair("client_id", selectUserId));
                        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
                        new Service_Integration_Duplicate(context, nameValuePairList, "business/pos_setup/save_checkout_items", this, "CHECKOUT_SAVE", true).execute();

                    }
                }
            }
        }

          /*  List<Product_List_Model.DataBean.LocationItemsBean> mFilteredMyObjectList = locationItemsBeanList.stream()
                    .filter(d -> d.getIsSelected().equals("Y")).collect(Collectors.toList());*/




      /*      List<Product_List_Model.DataBean.LocationItemsBean> mFilteredMyObjectList = locationItemsBeanList.stream()
                    .filter(d -> d.getIsSelected().equals("Y")
                            || d.getMiddleName().toString().toLowerCase().indexOf(sv) >= 0
                            || d.getLastName().toString().toLowerCase().indexOf(sv) >= 0).collect(Collectors.toList());*/


    }
}
