package com.mydevit.pieservicess.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.activities.AddCustomerActivity;
import com.mydevit.pieservicess.activities.BusinessCustomerActivity;
import com.mydevit.pieservicess.databinding.FragmentCustomerManagementBinding;
import com.mydevit.pieservicess.databinding.LogoutdialogDesignBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.CustomerMangmnt_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.customer_management_model;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class CustomerManagement_Fragment extends Fragment implements ServiceReponseInterface_Duplicate, ClickListenerPC, ClickListener {

    View view;
    List<customer_management_model.DataBean> dataArrayList;
    Context context;

    @BindView(R.id.mainRecyclerViewId)
    RecyclerView custmerRecyclerView;


    CustomerMangmnt_RecyclerViewAdapter customerMangmnt_recyclerViewAdapter;
    FragmentCustomerManagementBinding binding;
    List<NameValuePair> nameValuePairList;
    LogoutdialogDesignBinding logoutdialogDesignBinding;
    AlertDialog alertDialog;
    customer_management_model.DataBean customerDataAfterDeleteClicked;
    int deleteClickedPosition;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_customer_management_, container, false);
        binding.setClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        serviceInitialization();
    }

    @Override
    public boolean getUserVisibleHint() {
        return super.getUserVisibleHint();
    }

    private void serviceInitialization() {
        dataArrayList = new ArrayList<>();
        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        new Service_Integration_Duplicate(context, nameValuePairList, "business/customer/get_all_customer", this, "CUSTOMER_LIST", true).execute();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("CUSTOMER_LIST".equals(type)) {
            customer_management_model customer_management_model = new Gson().fromJson(result, com.mydevit.pieservicess.service.customer_management_model.class);
            if (customer_management_model != null && customer_management_model.getData() != null) {
                dataArrayList.addAll(customer_management_model.getData());
                customerMangmnt_recyclerViewAdapter = new CustomerMangmnt_RecyclerViewAdapter(dataArrayList, context, this::onClickPC);
                binding.setCustomerManagment(customerMangmnt_recyclerViewAdapter);

            }
        }
        if ("DELETE_CUSTOMER".equals(type)) {
            customer_management_model customer_management_model = new Gson().fromJson(result, com.mydevit.pieservicess.service.customer_management_model.class);
            if (customer_management_model != null && "Customer Deleted Successfully".equals(customer_management_model.getMessage())) {
                dataArrayList.remove(deleteClickedPosition);
                customerMangmnt_recyclerViewAdapter.notifyItemRemoved(deleteClickedPosition);
            } else {
                AlphaHolder.customToast(context, customer_management_model.getMessage());
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
        customer_management_model.DataBean dataBean = (customer_management_model.DataBean) object;
        if ("eyeclicked".equals(tag_FromWhere)) {
            Intent intent = new Intent(context, BusinessCustomerActivity.class);
            intent.putExtra("usetomerDataObject", dataBean);
            startActivity(intent);
            ((Activity) context).overridePendingTransition(0, 0);
        }
        if ("delete".equals(tag_FromWhere)) {
            deleteClickedPosition = parentPosition;
            customerDataAfterDeleteClicked = (com.mydevit.pieservicess.service.customer_management_model.DataBean) object;
            deleteCsutomer();
        }if ("isemailverified".equals(tag_FromWhere)) {
            Toast.makeText(context,getResources().getString(R.string.emailnotverified),Toast.LENGTH_SHORT).show();

        }if ("isphonenoverified".equals(tag_FromWhere)) {
            Toast.makeText(context,getResources().getString(R.string.phonenonotverified),Toast.LENGTH_SHORT).show();

        }

    }

    public void deleteCsutomer() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        logoutdialogDesignBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.logoutdialog_design, viewGroup, false);
        logoutdialogDesignBinding.setClickListener(this);
        builder.setView(logoutdialogDesignBinding.getRoot());

        logoutdialogDesignBinding.mainHeaderTextViewId.setText(getResources().getString(R.string.deletetitle));
        logoutdialogDesignBinding.textForIndicationTextViewId.setText(getResources().getString(R.string.deletetext));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        alertDialog = builder.create();
        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);

        alertDialog.show();
        alertDialog.getWindow().setLayout(900, 620);
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("Cancel".equals(tag_FromWhere)) {
            alertDialog.dismiss();
        }
        if ("Yes".equals(tag_FromWhere)) {
            alertDialog.dismiss();

            nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
            nameValuePairList.add(new BasicNameValuePair("customer_id", customerDataAfterDeleteClicked.getCustomer_id()));

            new Service_Integration_Duplicate(context, nameValuePairList, "business/customer/delete_business_customer", this, "DELETE_CUSTOMER", true).execute();

        }
        if ("Add Customer".equals(tag_FromWhere)) {
            Intent intent = new Intent(context, AddCustomerActivity.class);
            startActivity(intent);
            ((Activity) context).overridePendingTransition(0, 0);
        }
    }
}
