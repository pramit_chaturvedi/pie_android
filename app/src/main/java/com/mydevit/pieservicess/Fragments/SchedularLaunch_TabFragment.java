package com.mydevit.pieservicess.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mydevit.pieservicess.Fragments.Scheduler_TabFragments.ClientFragmentTab;
import com.mydevit.pieservicess.Fragments.Scheduler_TabFragments.ConfigureAvailability_Fragment;
import com.mydevit.pieservicess.Fragments.Scheduler_TabFragments.ScheduleNewFragment;
import com.mydevit.pieservicess.Fragments.Scheduler_TabFragments.ViewScheduleFragment;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.navigationDrawer.SlidingActivity;

public class SchedularLaunch_TabFragment extends Fragment implements TabLayout.OnTabSelectedListener {

    public static String[] PAGE_TITLES = null;
    public static Fragment[] PAGES = null;
    public static ViewPager mViewPager;
    public static Activity schedularMainContext;
    Activity activity;
    Context context;
    View view;
    String location_id;
    String isFrom = "";
    String isFromToday;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.schedule_launch_tab_layout, container, false);
        ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.scheduler));
        PAGE_TITLES = new String[]{
                //context.getResources().getString(R.string.configureavailability),
                context.getResources().getString(R.string.schedulenew),
                context.getResources().getString(R.string.notificationfortab),
                context.getResources().getString(R.string.viewview),
                context.getResources().getString(R.string.clientlist)
        };
        getDataFrom();

        return view;
    }

    private void getDataFrom() {
        location_id = getArguments().getString("location_id");
        if (getArguments().containsKey("FROMTODAY")) {
            isFromToday = getArguments().getString("FROMTODAY");
        }
        TabLayout(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (Activity) context;
        this.context = context;
        schedularMainContext = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void TabLayout(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("location_id", location_id);
        bundle.putString("FROMTODAY", isFromToday);

//        ConfigureAvailability_Fragment configureAvailability_fragment = new ConfigureAvailability_Fragment();
//        configureAvailability_fragment.setArguments(bundle);

        ClientFragmentTab clientFragmentTab = new ClientFragmentTab();
        clientFragmentTab.setArguments(bundle);

        ScheduleNewFragment scheduleNewFragment = new ScheduleNewFragment();
        scheduleNewFragment.setArguments(bundle);

        NotificationMainFragment notificationMainFr = new NotificationMainFragment();
        notificationMainFr.setArguments(bundle);

        ViewScheduleFragment viewScheduleFragment = new ViewScheduleFragment();
        viewScheduleFragment.setArguments(bundle);

        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        PAGES = new Fragment[]{
//                configureAvailability_fragment,
                scheduleNewFragment,
                notificationMainFr,
                viewScheduleFragment,
                clientFragmentTab
        };
        mViewPager = view.findViewById(R.id.pager);

        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(myPagerAdapter);
        mViewPager.setOffscreenPageLimit(3 - 1);

        mViewPager.setCurrentItem(2);

        tabLayout.setupWithViewPager(mViewPager);

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
//        Toast.makeText(getActivity(),"Reselect",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        Toast.makeText(getActivity(), "Reselect", Toast.LENGTH_LONG).show();
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return PAGES[position];
        }

        @Override
        public int getCount() {
            return PAGES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return PAGE_TITLES[position];
        }

    }

}
