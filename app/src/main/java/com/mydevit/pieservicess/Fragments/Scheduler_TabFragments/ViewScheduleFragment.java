package com.mydevit.pieservicess.Fragments.Scheduler_TabFragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.gson.Gson;
import com.mydevit.pieservicess.Fragments.SchedularLaunch_TabFragment;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.activities.ShowServiceCalendarActivity;
import com.mydevit.pieservicess.databinding.FragmentViewScheduleBinding;
import com.mydevit.pieservicess.databinding.LocationLayoutSpinnerBinding;
import com.mydevit.pieservicess.databinding.SelectOperationBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.model.ParentServiceModel;
import com.mydevit.pieservicess.model.ServiceModel;
import com.mydevit.pieservicess.navigationDrawer.SlidingActivity;
import com.mydevit.pieservicess.service.SchedulerServiceModel;
import com.mydevit.pieservicess.service.ViewScheduleModel;
import com.mydevit.pieservicess.service.today_schedule;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.EventBus.Events;
import com.mydevit.pieservicess.utils.EventBus.GlobalBus;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.GoogleProgressDialog;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import sun.bob.mcalendarview.MCalendarViewEvent;
import sun.bob.mcalendarview.vo.DateDataEvent;
import sun.bob.mcalendarview.vo.MarkedDatesEvent;

public class ViewScheduleFragment extends Fragment implements ServiceReponseInterface_Duplicate, ClickListener {

    public static String currentMonthDate = "";
    //   public static Date clickDateDateFormat = null;
    View view;
    Context context;
    String selectedDate = "";
    String isFromToday = ""; ///this is only have meaning on this page on other fragment of this tab no use of this variable
    List<NameValuePair> nameValuePairList;
    List<String> calendarList;
    String location_id, firstDateMonth, lastDateMonth, dateString;
    ArrayList<SchedulerServiceModel.DataBean.PersonnelsBean> personnelsBeanArrayList;
    List<today_schedule.DataBean.PersonnelTimingsBean> personnelsBeanTodayArrayList;
    AlphaHolder alphaHolder;
    FragmentViewScheduleBinding fragmentViewScheduleBinding;
    String selectedPersonalId = "0";
    MarkedDatesEvent markedDatesEvent;
    ViewScheduleModel viewScheduleModel;
    ArrayList<ViewScheduleModel.DataBean> dataBeanArrayList;
    List<String> selectedServiceList;
    ArrayList<ParentServiceModel> serviceModelParentArrayList;
    String selectedPersonnelName = "";
    List<String> eventDateArrayList;
    SelectOperationBinding activityCalenderBinding;
    LocationLayoutSpinnerBinding locationLayoutSpinnerBinding;
    GoogleProgressDialog googleProgressDialog;
    AlertDialog alertDialog;
    List<SchedulerServiceModel.DataBean> locationArrayListFromSP;
    int checkSpinnerIsClicked = 0;
    int selectedPosition = 100;
    SharedPreferences_Util sharedPreferences_util;
    ArrayAdapter arrayAdapter;
    boolean isFromFirstEvent = false; ///if first time screen is loaded then show 0 index personnel else made it false again and now dont load data on service hit
    private List<ServiceModel> serviceModelList;
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());

    //BindingFunction
    @BindingAdapter("bind:loadPersonalSpinnerAdapter")
    public static void loadPersonalSpinnerData(Spinner spinner, ViewScheduleModel.DataBean viewScheduleModel) {

    }
    @Override
    public void onStart() {
        super.onStart();
        if (!GlobalBus.getBus().isRegistered(this)) {
            GlobalBus.getBus().register(this);
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (GlobalBus.getBus().isRegistered(this)) {
            GlobalBus.getBus().unregister(this);
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void getMessage(Events sendIsFrom) {
        if ("YES".equals(sendIsFrom.getMessage())) {
            ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.scheduler));
            AlphaHolder.isShowing = false;
            googleProgressDialog = new GoogleProgressDialog(context);
            googleProgressDialog.showDialoge();
            getMainServiceExecutiionAgain();
        }

        //sendIsFrom.getMessage();
        //AlphaHolder.customToast(context, sendIsFrom.getMessage());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentViewScheduleBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_view_schedule, container, false);
        alphaHolder = new AlphaHolder();
        Log.e("FROMTHE", "ONCREATE");

        hidePersonnelSpinnerOnPersonnelLogin();
        getIntentData();
        manipulateLocationSpinner();
        manipulateCalender();


        //initiateCateOnCalendar(AlphaHolder.calendarList, fragmentViewScheduleBinding.calenderAAA, "mark");
        Log.e("size", "" + AlphaHolder.calendarList.size());
        fragmentViewScheduleBinding.setDatedata(dateFormatForMonth.format(fragmentViewScheduleBinding.compactcalendarView.getFirstDayOfCurrentMonth()));
        fragmentViewScheduleBinding.compactcalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                fragmentViewScheduleBinding.setDatedata(dateFormatForMonth.format(dateClicked));
                serviceModelParentArrayList = new ArrayList<>();
                serviceModelList = new ArrayList<>();
                String selectedDateIs = "";

                SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
                selectedDateIs = outputFormat.format(dateClicked);

                if ("0".equals(selectedPersonalId)) {
                    ParentServiceModel parentServiceModel = null;

                    boolean isHave = false;
                    if (personnelsBeanArrayList != null) {
                        for (int k = 0; k < personnelsBeanArrayList.size(); k++) {
                            String personalIdTempParent = personnelsBeanArrayList.get(k).getPersonnel_id();
                            for (int i = 0; i < viewScheduleModel.getData().size(); i++) {
                                serviceModelList = new ArrayList<>();
                                ViewScheduleModel.DataBean dataBean = viewScheduleModel.getData().get(i);
                                String eventDate = dataBean.getStart_date();
                                String personalTempChild = dataBean.getPersonnel_id();

                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                                Date date1 = null;
                                Date date2 = null;

                                try {
                                    date1 = sdf.parse(eventDate);
                                    date2 = sdf.parse(selectedDateIs);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                if (date1.compareTo(date2) == 0 && personalIdTempParent.equals(personalTempChild)) {
                                    isHave = true;
                                    parentServiceModel = new ParentServiceModel();
                                    parentServiceModel.setName(personnelsBeanArrayList.get(k).getName());

                                    ViewScheduleModel.DataBean object = viewScheduleModel.getData().get(i);
                                    //dataBeanArrayList.add(object);

                                    ServiceModel serviceModel = new ServiceModel();
                                    serviceModel.setTimeSlot(object.getTime());
                                    serviceModel.setClient_Id(object.getClient_id());
                                    serviceModel.setAppointment_id(object.getAppointment_id());
                                    serviceModel.setPersonal_Id(object.getPersonnel_id());
                                    serviceModel.setService_duration(object.getServiceduration());
                                    serviceModel.setStringArrayList(object.getService_data());
                                    serviceModelList.add(serviceModel);
                                    parentServiceModel.setServiceModelList(serviceModelList);
                                    serviceModelParentArrayList.add(parentServiceModel);

                                }

                            }
                            if (k == personnelsBeanArrayList.size() - 1) {
                                if (serviceModelParentArrayList.size() == 0) {

                                    Bundle bundle = new Bundle();
                                    bundle.putString("isFromDateClick", "YES");
                                    bundle.putString("clickedDate", selectedDateIs);
                                    bundle.putString("location_id", location_id);
                                    ScheduleNewDateFragment scheduleNewFragment = new ScheduleNewDateFragment();
                                    scheduleNewFragment.setArguments(bundle);
                                    transactionFragments(scheduleNewFragment, R.id.container);
                                    // AlphaHolder.customToast(context, "Null");

                                } else {
                                        /*alphaHolder.saveServiceArrayList(serviceModelParentArrayList, context);
                                        Intent intent = new Intent(context, ShowServiceCalendarActivity.class);
                                        intent.putExtra("selectedata", selectedDateIs);
                                        intent.putExtra("location_id", location_id);
                                        startActivity(intent);*/

                                    SelectOperation(selectedDateIs);
                                }

                            }
                        }
                    }
                    /*if (isFromToday != null && isFromToday.equals("YES")) {
                        if (personnelsBeanTodayArrayList != null) {
                            for (int k = 0; k < personnelsBeanTodayArrayList.size(); k++) {
                                String personalIdTempParent = personnelsBeanTodayArrayList.get(k).getPersonnel_id();
                                for (int i = 0; i < viewScheduleModel.getData().size(); i++) {
                                    serviceModelList = new ArrayList<>();
                                    ViewScheduleModel.DataBean dataBean = viewScheduleModel.getData().get(i);
                                    String eventDate = dataBean.getStart_date();
                                    String personalTempChild = dataBean.getPersonnel_id();

                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                    Date date1 = null;
                                    Date date2 = null;


                                    try {
                                        date1 = sdf.parse(eventDate);
                                        date2 = sdf.parse(selectedDateIs);

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    if (date1.compareTo(date2) == 0 && personalIdTempParent.equals(personalTempChild)) {
                                        isHave = true;
                                        parentServiceModel = new ParentServiceModel();
                                        parentServiceModel.setName(personnelsBeanTodayArrayList.get(k).getPersonnel_name());

                                        ViewScheduleModel.DataBean object = viewScheduleModel.getData().get(i);
                                        //dataBeanArrayList.add(object);

                                        ServiceModel serviceModel = new ServiceModel();
                                        serviceModel.setTimeSlot(object.getTime());
                                        serviceModel.setClient_Id(object.getClient_id());
                                        serviceModel.setAppointment_id(object.getAppointment_id());
                                        serviceModel.setPersonal_Id(object.getPersonnel_id());
                                        serviceModel.setService_duration(object.getServiceduration());
                                        serviceModel.setStringArrayList(object.getService_data());
                                        serviceModelList.add(serviceModel);
                                        parentServiceModel.setServiceModelList(serviceModelList);
                                        serviceModelParentArrayList.add(parentServiceModel);

                                    }
                                }
                                if (k == personnelsBeanTodayArrayList.size() - 1) {
                                    if (serviceModelParentArrayList.size() == 0) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("isFromDateClick", "YES");
                                        bundle.putString("clickedDate", selectedDateIs);
                                        bundle.putString("location_id", location_id);
                                        ScheduleNewDateFragment scheduleNewFragment = new ScheduleNewDateFragment();
                                        scheduleNewFragment.setArguments(bundle);
                                        transactionFragments(scheduleNewFragment, R.id.container);

                                    } else {
                                        SelectOperation(selectedDateIs);
                                    }

                                }
                            }
                        }
                    } else {
                        if (personnelsBeanArrayList != null) {
                            for (int k = 0; k < personnelsBeanArrayList.size(); k++) {
                                String personalIdTempParent = personnelsBeanArrayList.get(k).getPersonnel_id();
                                for (int i = 0; i < viewScheduleModel.getData().size(); i++) {
                                    serviceModelList = new ArrayList<>();
                                    ViewScheduleModel.DataBean dataBean = viewScheduleModel.getData().get(i);
                                    String eventDate = dataBean.getStart_date();
                                    String personalTempChild = dataBean.getPersonnel_id();

                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                                    Date date1 = null;
                                    Date date2 = null;

                                    try {
                                        date1 = sdf.parse(eventDate);
                                        date2 = sdf.parse(selectedDateIs);

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    if (date1.compareTo(date2) == 0 && personalIdTempParent.equals(personalTempChild)) {
                                        isHave = true;
                                        parentServiceModel = new ParentServiceModel();
                                        parentServiceModel.setName(personnelsBeanArrayList.get(k).getName());

                                        ViewScheduleModel.DataBean object = viewScheduleModel.getData().get(i);
                                        //dataBeanArrayList.add(object);

                                        ServiceModel serviceModel = new ServiceModel();
                                        serviceModel.setTimeSlot(object.getTime());
                                        serviceModel.setClient_Id(object.getClient_id());
                                        serviceModel.setAppointment_id(object.getAppointment_id());
                                        serviceModel.setPersonal_Id(object.getPersonnel_id());
                                        serviceModel.setService_duration(object.getServiceduration());
                                        serviceModel.setStringArrayList(object.getService_data());
                                        serviceModelList.add(serviceModel);
                                        parentServiceModel.setServiceModelList(serviceModelList);
                                        serviceModelParentArrayList.add(parentServiceModel);

                                    }

                                }
                                if (k == personnelsBeanArrayList.size() - 1) {
                                    if (serviceModelParentArrayList.size() == 0) {

                                        Bundle bundle = new Bundle();
                                        bundle.putString("isFromDateClick", "YES");
                                        bundle.putString("clickedDate", selectedDateIs);
                                        bundle.putString("location_id", location_id);
                                        ScheduleNewDateFragment scheduleNewFragment = new ScheduleNewDateFragment();
                                        scheduleNewFragment.setArguments(bundle);
                                        transactionFragments(scheduleNewFragment, R.id.container);
                                        // AlphaHolder.customToast(context, "Null");

                                    } else {
                                        *//*alphaHolder.saveServiceArrayList(serviceModelParentArrayList, context);
                                        Intent intent = new Intent(context, ShowServiceCalendarActivity.class);
                                        intent.putExtra("selectedata", selectedDateIs);
                                        intent.putExtra("location_id", location_id);
                                        startActivity(intent);*//*

                                        SelectOperation(selectedDateIs);
                                    }

                                }
                            }
                        }
                    }*/
                } else {
                    ParentServiceModel parentServiceModel = null;
                    boolean isHave = false;

                    if (personnelsBeanArrayList != null) {
                        String personalIdTempParent = selectedPersonalId;
                        for (int i = 0; i < viewScheduleModel.getData().size(); i++) {
                            serviceModelList = new ArrayList<>();
                            ViewScheduleModel.DataBean dataBean = viewScheduleModel.getData().get(i);
                            String eventDate = dataBean.getStart_date();
                            String personalTempChild = dataBean.getPersonnel_id();

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                            Date date1 = null;
                            Date date2 = null;

                            try {
                                date1 = sdf.parse(eventDate);
                                date2 = sdf.parse(selectedDateIs);

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (date1.compareTo(date2) == 0 && personalIdTempParent.equals(personalTempChild)) {
                                isHave = true;
                                parentServiceModel = new ParentServiceModel();
                                parentServiceModel.setName(selectedPersonnelName);

                                ViewScheduleModel.DataBean object = viewScheduleModel.getData().get(i);
                                //dataBeanArrayList.add(object);

                                ServiceModel serviceModel = new ServiceModel();
                                serviceModel.setTimeSlot(object.getTime());
                                serviceModel.setAppointment_id(object.getAppointment_id());
                                serviceModel.setPersonal_Id(object.getPersonnel_id());
                                serviceModel.setClient_Id(object.getClient_id());
                                serviceModel.setService_duration(object.getServiceduration());
                                serviceModel.setStringArrayList(object.getService_data());
                                serviceModelList.add(serviceModel);
                                parentServiceModel.setServiceModelList(serviceModelList);
                                serviceModelParentArrayList.add(parentServiceModel);

                            }
                            if (i == viewScheduleModel.getData().size() - 1) {
                                if (serviceModelParentArrayList.size() == 0) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("location_id", location_id);
                                    bundle.putString("clickedDate", selectedDateIs);
                                    bundle.putString("isFromDateClick", "YES");
                                    ScheduleNewDateFragment scheduleNewFragment = new ScheduleNewDateFragment();
                                    scheduleNewFragment.setArguments(bundle);
                                    transactionFragments(scheduleNewFragment, R.id.container);
                                } else {
                                      /*  alphaHolder.saveServiceArrayList(serviceModelParentArrayList, context);
                                        Intent intent = new Intent(context, ShowServiceCalendarActivity.class);
                                        intent.putExtra("selectedata", selectedDateIs);
                                        intent.putExtra("location_id", location_id);
                                        startActivity(intent);*/

                                    SelectOperation(selectedDateIs);
                                }
                            }
                        }

                    }



                    /*if (isFromToday != null && isFromToday.equals("YES")) {
                        if (personnelsBeanTodayArrayList != null) {

                            String personalIdTempParent = selectedPersonalId;
                            for (int i = 0; i < viewScheduleModel.getData().size(); i++) {
                                serviceModelList = new ArrayList<>();
                                ViewScheduleModel.DataBean dataBean = viewScheduleModel.getData().get(i);
                                String eventDate = dataBean.getStart_date();
                                String personalTempChild = dataBean.getPersonnel_id();

                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                                Date date1 = null;
                                Date date2 = null;

                                try {
                                    date1 = sdf.parse(eventDate);
                                    date2 = sdf.parse(selectedDateIs);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                if (date1.compareTo(date2) == 0 && personalIdTempParent.equals(personalTempChild)) {
                                    isHave = true;
                                    parentServiceModel = new ParentServiceModel();
                                    parentServiceModel.setName(selectedPersonnelName);

                                    ViewScheduleModel.DataBean object = viewScheduleModel.getData().get(i);
                                    //dataBeanArrayList.add(object);

                                    ServiceModel serviceModel = new ServiceModel();
                                    serviceModel.setTimeSlot(object.getTime());
                                    serviceModel.setClient_Id(object.getClient_id());
                                    serviceModel.setAppointment_id(object.getAppointment_id());
                                    serviceModel.setPersonal_Id(object.getPersonnel_id());
                                    serviceModel.setService_duration(object.getServiceduration());
                                    serviceModel.setStringArrayList(object.getService_data());
                                    serviceModelList.add(serviceModel);
                                    parentServiceModel.setServiceModelList(serviceModelList);
                                    serviceModelParentArrayList.add(parentServiceModel);

                                }

                                if (i == viewScheduleModel.getData().size() - 1) {

                                    if (serviceModelParentArrayList.size() == 0) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("isFromDateClick", "YES");
                                        bundle.putString("clickedDate", selectedDateIs);
                                        bundle.putString("location_id", location_id);
                                        ScheduleNewDateFragment scheduleNewFragment = new ScheduleNewDateFragment();
                                        scheduleNewFragment.setArguments(bundle);
                                        transactionFragments(scheduleNewFragment, R.id.container);
                                    } else {
                                        *//*alphaHolder.saveServiceArrayList(serviceModelParentArrayList, context);
                                        Intent intent = new Intent(context, ShowServiceCalendarActivity.class);
                                        intent.putExtra("selectedata", selectedDateIs);
                                        intent.putExtra("location_id", location_id);
                                        startActivity(intent);*//*

                                        SelectOperation(selectedDateIs);
                                    }


                                }

                            }

                        }


                    } else {
                        if (personnelsBeanArrayList != null) {
                            String personalIdTempParent = selectedPersonalId;
                            for (int i = 0; i < viewScheduleModel.getData().size(); i++) {
                                serviceModelList = new ArrayList<>();
                                ViewScheduleModel.DataBean dataBean = viewScheduleModel.getData().get(i);
                                String eventDate = dataBean.getStart_date();
                                String personalTempChild = dataBean.getPersonnel_id();

                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                                Date date1 = null;
                                Date date2 = null;

                                try {
                                    date1 = sdf.parse(eventDate);
                                    date2 = sdf.parse(selectedDateIs);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if (date1.compareTo(date2) == 0 && personalIdTempParent.equals(personalTempChild)) {
                                    isHave = true;
                                    parentServiceModel = new ParentServiceModel();
                                    parentServiceModel.setName(selectedPersonnelName);

                                    ViewScheduleModel.DataBean object = viewScheduleModel.getData().get(i);
                                    //dataBeanArrayList.add(object);

                                    ServiceModel serviceModel = new ServiceModel();
                                    serviceModel.setTimeSlot(object.getTime());
                                    serviceModel.setAppointment_id(object.getAppointment_id());
                                    serviceModel.setPersonal_Id(object.getPersonnel_id());
                                    serviceModel.setClient_Id(object.getClient_id());
                                    serviceModel.setService_duration(object.getServiceduration());
                                    serviceModel.setStringArrayList(object.getService_data());
                                    serviceModelList.add(serviceModel);
                                    parentServiceModel.setServiceModelList(serviceModelList);
                                    serviceModelParentArrayList.add(parentServiceModel);

                                }
                                if (i == viewScheduleModel.getData().size() - 1) {

                                    if (serviceModelParentArrayList.size() == 0) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("location_id", location_id);
                                        bundle.putString("clickedDate", selectedDateIs);
                                        bundle.putString("isFromDateClick", "YES");
                                        ScheduleNewDateFragment scheduleNewFragment = new ScheduleNewDateFragment();
                                        scheduleNewFragment.setArguments(bundle);
                                        transactionFragments(scheduleNewFragment, R.id.container);
                                    } else {
                                      *//*  alphaHolder.saveServiceArrayList(serviceModelParentArrayList, context);
                                        Intent intent = new Intent(context, ShowServiceCalendarActivity.class);
                                        intent.putExtra("selectedata", selectedDateIs);
                                        intent.putExtra("location_id", location_id);
                                        startActivity(intent);*//*

                                        SelectOperation(selectedDateIs);
                                    }
                                }
                            }

                        }
                    }*/
                }


              /*  toolbar.setTitle(dateFormatForMonth.format(dateClicked));
                List<Event> bookingsFromMap = compactCalendarView.getEvents(dateClicked);
                Log.d(TAG, "inside onclick " + dateFormatForDisplaying.format(dateClicked));
                if (bookingsFromMap != null) {
                    Log.d(TAG, bookingsFromMap.toString());
                    mutableBookings.clear();
                    for (Event booking : bookingsFromMap) {
                        mutableBookings.add((String) booking.getData());
                    }
                    adapter.notifyDataSetChanged();
                }*/
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                //  toolbar.setTitle(dateFormatForMonth.format(firstDayOfNewMonth));
                //clickDateDateFormat = firstDayOfNewMonth;

                fragmentViewScheduleBinding.setDatedata(dateFormatForMonth.format(firstDayOfNewMonth));
                SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss 'GMT' yyyy", Locale.US);
                inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));

                SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy/MM/dd");
                SimpleDateFormat outputFormatCalenderDate = new SimpleDateFormat("dd-MM-yyyy");

                currentMonthDate = outputFormatCalenderDate.format(firstDayOfNewMonth);
                //String outputText = outputFormat.format(firstDayOfNewMonth);

                Date date = getDateInDateFormat(currentMonthDate);
                firstDateMonth = getFirstDay(date);
                lastDateMonth = getLastDay(date);
                getDatafromService(selectedPersonalId);
            }

        });
        /*fragmentViewScheduleBinding.calenderAAA.setOnDateClickListener(new OnDateClickListenerEvent() {
            @Override
            public void onDateClick(View view, DateDataEvent date) {
                serviceModelParentArrayList = new ArrayList<>();
                serviceModelList = new ArrayList<>();

                String selectedDateIs = "";

                if (date.getDay() < 10) {
                    selectedDateIs = date.getYear() + "-" + date.getMonth() + "-0" + date.getDay();
                } else {
                    selectedDateIs = date.getYear() + "-" + date.getMonth() + "-" + date.getDay();
                }

                if ("0".equals(selectedPersonalId)) {
                    ParentServiceModel parentServiceModel = null;
                    boolean isHave = false;

                    for (int k = 0; k < personnelsBeanArrayList.size(); k++) {
                        String personalIdTempParent = personnelsBeanArrayList.get(k).getPersonnel_id();

                        for (int i = 0; i < viewScheduleModel.getData().size(); i++) {
                            serviceModelList = new ArrayList<>();
                            ViewScheduleModel.DataBean dataBean = viewScheduleModel.getData().get(i);
                            String eventDate = dataBean.getStart_date();
                            String personalTempChild = dataBean.getPersonnel_id();

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                            Date date1 = null;
                            Date date2 = null;

                            try {
                                date1 = sdf.parse(eventDate);
                                date2 = sdf.parse(selectedDateIs);

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            if (date1.compareTo(date2) == 0 && personalIdTempParent.equals(personalTempChild)) {
                                isHave = true;
                                parentServiceModel = new ParentServiceModel();
                                parentServiceModel.setName(personnelsBeanArrayList.get(k).getName());

                                ViewScheduleModel.DataBean object = viewScheduleModel.getData().get(i);
                                //dataBeanArrayList.add(object);

                                ServiceModel serviceModel = new ServiceModel();
                                serviceModel.setTimeSlot(object.getTime());
                                serviceModel.setAppointment_id(object.getAppointment_id());
                                serviceModel.setPersonal_Id(object.getPersonnel_id());
                                serviceModel.setService_duration(object.getServiceduration());
                                serviceModel.setStringArrayList(object.getService_data());
                                serviceModelList.add(serviceModel);
                                parentServiceModel.setServiceModelList(serviceModelList);
                                serviceModelParentArrayList.add(parentServiceModel);

                            }

                        }
                        if (k == personnelsBeanArrayList.size() - 1) {
                            alphaHolder.saveServiceArrayList(serviceModelParentArrayList, context);
                            Intent intent = new Intent(context, ShowServiceCalendarActivity.class);
                            intent.putExtra("selectedata", selectedDateIs);
                            intent.putExtra("location_id", location_id);
                            startActivity(intent);
                        }
                    }
                } else {
                    //Toast.makeText(context, selectedDateIs, Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < viewScheduleModel.getData().size(); i++) {
                        ViewScheduleModel.DataBean dataBean = viewScheduleModel.getData().get(i);
                        String eventDate = dataBean.getStart_date();
                        if (selectedDateIs.equals(eventDate)) {
                            ViewScheduleModel.DataBean object = viewScheduleModel.getData().get(i);
                            dataBeanArrayList.add(object);

                        }
                        *//*if (i == viewScheduleModel.getData().size() - 1) {
                            alphaHolder.saveServiceArrayList(dataBeanArrayList, context);
                            Intent intent = new Intent(context, ShowServiceCalendarActivity.class);
                            intent.putExtra("selectedata", selectedDateIs);
                            startActivity(intent);
                        }*//*
                    }
                }

               *//* //
                AlphaHolder.selectedDate = date.getDay() + "-" + date.getMonth() + "-" + date.getYear();
                //  initiateCateOnCalendar(AlphaHolder.calendarList, fragmentViewScheduleBinding.calenderAAA, "unMark");
                AlphaHolder.calendarList = new ArrayList<>();
                calendarList = getDay(date.getDayString(), date.getMonthString(), "" + date.getYear());
                //initiateCateOnCalendar(calendarList, fragmentViewScheduleBinding.calenderAAA, "mark");*//*

            }
        });*/
     /*   fragmentViewScheduleBinding.calenderAAA.setOnMonthChangeListener(new OnMonthChangeListenerEvent() {
            @Override
            public void onMonthChange(int year, int month) {


            }
        });*/
        fragmentViewScheduleBinding.personalSpinnerId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Log.e("GETTINGCALL", "SPINNER");
                if (position > 0) {

                    if (isFromToday != null && isFromToday.equals("YES")) {
                        selectedPersonalId = personnelsBeanTodayArrayList.get(position - 1).getPersonnel_id();
                        selectedPersonnelName = personnelsBeanTodayArrayList.get(position - 1).getPersonnel_name();
                        getDatafromService(selectedPersonalId);

                    } else {
                        selectedPersonalId = personnelsBeanArrayList.get(position - 1).getPersonnel_id();
                        selectedPersonnelName = personnelsBeanArrayList.get(position - 1).getName();
                        getDatafromService(selectedPersonalId);
                    }

                } else {
                    selectedPersonalId = "0";
                    getDatafromService(selectedPersonalId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        fragmentViewScheduleBinding.executePendingBindings();
        return fragmentViewScheduleBinding.getRoot();
    }

    private void manipulateCalender() {
        fragmentViewScheduleBinding.compactcalendarView.setUseThreeLetterAbbreviation(false);
        fragmentViewScheduleBinding.compactcalendarView.setFirstDayOfWeek(Calendar.MONDAY);
        fragmentViewScheduleBinding.compactcalendarView.setIsRtl(false);
        fragmentViewScheduleBinding.compactcalendarView.displayOtherMonthDays(false);
        fragmentViewScheduleBinding.compactcalendarView.setIsRtl(true);
      /*  loadEvents();
        loadEventsForYear(2017);
        compactCalendarView.invalidate();*/
    }

    private void hidePersonnelSpinnerOnPersonnelLogin() {
        sharedPreferences_util = new SharedPreferences_Util(context);
        boolean isNeedToHide = AlphaHolder.isLoginWithPersonnel(SharedPreferences_Util.getRoleId(context));
        selectedPersonalId = AlphaHolder.getLoggedInPersonneld(context);
        if (isNeedToHide) {
            fragmentViewScheduleBinding.spinnerLinLayoutId.setVisibility(View.GONE);
        } else {
            fragmentViewScheduleBinding.spinnerLinLayoutId.setVisibility(View.VISIBLE);
        }
    }
    private void manipulateLocationSpinner() {
        fragmentViewScheduleBinding.selectLocationsLinLayoutId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLocationSpinnerData(arrayAdapter, true);
                // getPersonalSpinnerData(position);
            }
        });
    }
    public void SelectOperation(String selectedDateIs) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        activityCalenderBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.select_operation, viewGroup, false);
        builder.setView(activityCalenderBinding.getRoot());

        activityCalenderBinding.addOperationLinLayoutId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("isFromDateClick", "YES");
                bundle.putString("clickedDate", selectedDateIs);
                bundle.putString("location_id", location_id);
                ScheduleNewDateFragment scheduleNewFragment = new ScheduleNewDateFragment();
                scheduleNewFragment.setArguments(bundle);
                transactionFragments(scheduleNewFragment, R.id.container);
                alertDialog.dismiss();


            }
        });
        activityCalenderBinding.viewOperationLinLayoutId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alphaHolder.saveServiceArrayList(serviceModelParentArrayList, context);
                Intent intent = new Intent(context, ShowServiceCalendarActivity.class);
                intent.putExtra("selectedata", selectedDateIs);
                intent.putExtra("location_id", location_id);
                startActivity(intent);

                alertDialog.dismiss();

            }
        });

        activityCalenderBinding.closeButtonImageViewId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        if (alertDialog != null && !alertDialog.isShowing()) {
            alertDialog.show();

        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());


        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    public void transactionFragments(Fragment fragment, int viewResource) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(viewResource, fragment);
        ft.addToBackStack(null);
        ft.commit();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("FROMTHE", "INACTCRE");

    }

    private void getDataFromService() {
        if (!"".equals(currentMonthDate) && currentMonthDate != null) {
            //  String date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
            int dayInt = Integer.parseInt(currentMonthDate.substring(0, 2));
            int monthInt = Integer.parseInt(currentMonthDate.substring(3, 5));
            int yearInt = Integer.parseInt(currentMonthDate.substring(6, 10));
            calendarList = getDay("" + dayInt, "" + monthInt, "" + yearInt);
            //  initiateCateOnCalendar(calendarList, fragmentViewScheduleBinding.calenderAAA, "mark");
            try {
                //dateString = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());
                firstDateMonth = getFirstDay(getDateInDateFormat(currentMonthDate));
                lastDateMonth = getLastDay(getDateInDateFormat(currentMonthDate));

                getDatafromService(selectedPersonalId);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            String date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
            int dayInt = Integer.parseInt(date.substring(0, 2));
            int monthInt = Integer.parseInt(date.substring(3, 5));
            int yearInt = Integer.parseInt(date.substring(6, 10));
            calendarList = getDay("" + dayInt, "" + monthInt, "" + yearInt);
            //  initiateCateOnCalendar(calendarList, fragmentViewScheduleBinding.calenderAAA, "mark");
            try {
                //dateString = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());
                firstDateMonth = getFirstDay(getDateInDateFormat(date));
                lastDateMonth = getLastDay(getDateInDateFormat(date));


                getDatafromService(selectedPersonalId);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("FROMTHE", "RESUME");
        fragmentViewScheduleBinding.setDatedata(dateFormatForMonth.format(fragmentViewScheduleBinding.compactcalendarView.getFirstDayOfCurrentMonth()));
        ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.scheduler));
        getDataFromService();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.e("VISIBLE", "YES");

        alphaHolder = new AlphaHolder();
        locationArrayListFromSP = alphaHolder.getScheduledArrayList(SchedularLaunch_TabFragment.schedularMainContext);
        location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();

        if (fragmentViewScheduleBinding != null && fragmentViewScheduleBinding.selectUserLocaitonTextView != null) {
            isFromFirstEvent = false;
            List<String> locationNameList = new ArrayList();
            if (locationArrayListFromSP.size() > 0) {
                for (int i = 0; i < locationArrayListFromSP.size(); i++) {
                    locationNameList.add(locationArrayListFromSP.get(i).getLocation_name());
                    if (i == locationArrayListFromSP.size() - 1) {
                        fragmentViewScheduleBinding.selectUserLocaitonTextView.setText(locationNameList.get(0));
                        arrayAdapter = new ArrayAdapter(context, R.layout.spinner_layout, locationNameList);
                        getPersonalSpinnerData(AlphaHolder.selectedLocationSpinnerPosition);
                    }
                }
            }
        } else {
            isFromFirstEvent = true;
        }
    }

    public void showLocationSpinnerData(ArrayAdapter arrayAdapter, boolean shouldShowDialog) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        locationLayoutSpinnerBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.location_layout_spinner, viewGroup, false);
        locationLayoutSpinnerBinding.setClickListener(this);
        builder.setView(locationLayoutSpinnerBinding.getRoot());

        locationLayoutSpinnerBinding.closeImageViewId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        locationLayoutSpinnerBinding.mainListViewLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int positionClick, long l) {
                //Log.e("GETTINGCALL","LISTVIEW");
                AlphaHolder.selectedLocationSpinnerPosition = positionClick;
                location_id = locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_id();
                fragmentViewScheduleBinding.selectUserLocaitonTextView.setText(locationArrayListFromSP.get(positionClick).getLocation_name());
                //   updateDataInLocationMainList(positionClick);
                getPersonalSpinnerData(positionClick);
                getMainServiceExecutiionAgain();

                alertDialog.dismiss();

            }
        });

        locationLayoutSpinnerBinding.mainListViewLocation.setAdapter(arrayAdapter);
        alertDialog = builder.create();

        if (shouldShowDialog && alertDialog != null && !alertDialog.isShowing()) {
            alertDialog.show();
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance

        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        alertDialog.getWindow().setAttributes(layoutParams);
    }

    private void getMainServiceExecutiionAgain() {
        boolean isPersonnelIsLoggedIn = AlphaHolder.isLoginWithPersonnel(SharedPreferences_Util.getRoleId(context));
        if (isPersonnelIsLoggedIn) {

            selectedPersonalId = AlphaHolder.getLoggedInPersonneld(context);
            getDatafromService(selectedPersonalId);

        } else {
            getDatafromService(selectedPersonalId);
        }
    }

    public Date getDateInDateFormat(String dateFormatString) {
        SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
        Date dateObj = null;
        try {
            dateObj = curFormater.parse(dateFormatString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateObj;
    }

    private void getPersonalSpinnerData(int position) {
        dataBeanArrayList = new ArrayList<>();
        List<String> stringList = new ArrayList<>();
        stringList.add("All");

        personnelsBeanArrayList = (ArrayList<SchedulerServiceModel.DataBean.PersonnelsBean>) locationArrayListFromSP.get(position).getPersonnels();
        location_id = locationArrayListFromSP.get(position).getLocation_id();
        fragmentViewScheduleBinding.selectUserLocaitonTextView.setText(locationArrayListFromSP.get(position).getLocation_name());

        if (personnelsBeanArrayList != null && personnelsBeanArrayList.size() > 0) {
            for (int i = 0; i < personnelsBeanArrayList.size(); i++) {
                stringList.add(personnelsBeanArrayList.get(i).getName());

                if (i == personnelsBeanArrayList.size() - 1) {
                    ArrayAdapter arrayAdapter = new ArrayAdapter(context, R.layout.spinner_layout, stringList);
                    fragmentViewScheduleBinding.setPersonalSpinner(arrayAdapter);
                    // updateDataInLocationMainList(position);
                }
            }
        }

        /*if ("YES".equals(isFromToday)) {
            personnelsBeanTodayArrayList = alphaHolder.getArrayListToday(context);
            if (personnelsBeanTodayArrayList != null && personnelsBeanTodayArrayList.size() > 0) {
                for (int i = 0; i < personnelsBeanTodayArrayList.size(); i++) {
                    stringList.add(personnelsBeanTodayArrayList.get(i).getPersonnel_name());
                }
                ArrayAdapter arrayAdapter = new ArrayAdapter(context, R.layout.spinner_layout, stringList);
                fragmentViewScheduleBinding.setPersonalSpinner(arrayAdapter);
            }

        } else {
            //personnelsBeanArrayList = alphaHolder.getArrayList(context);
            personnelsBeanArrayList = (ArrayList<SchedulerServiceModel.DataBean.PersonnelsBean>) locationArrayListFromSP.get(position).getPersonnels();
            location_id = locationArrayListFromSP.get(position).getLocation_id();
            fragmentViewScheduleBinding.selectUserLocaitonTextView.setText(locationArrayListFromSP.get(position).getLocation_name());

            if (personnelsBeanArrayList != null && personnelsBeanArrayList.size() > 0) {
                for (int i = 0; i < personnelsBeanArrayList.size(); i++) {
                    stringList.add(personnelsBeanArrayList.get(i).getName());

                    if (i == personnelsBeanArrayList.size() - 1) {
                        ArrayAdapter arrayAdapter = new ArrayAdapter(context, R.layout.spinner_layout, stringList);
                        fragmentViewScheduleBinding.setPersonalSpinner(arrayAdapter);
                       // updateDataInLocationMainList(position);
                    }
                }
            }
        }*/
    }

    /*private void updateDataInLocationMainList(int position) {
        for (int k = 0; k < locationArrayListFromSP.size(); k++) {
            if (k == position) {
                locationArrayListFromSP.get(k).setSelected(true);
            } else {
                locationArrayListFromSP.get(k).setSelected(false);
            }

            if (k == locationArrayListFromSP.size() - 1) {
                alphaHolder.saveScheduledArrayList(locationArrayListFromSP, context);
            }
        }
    }*/

    private void getIntentData() {
        // location_id = getArguments().getString("location_id");
        if (getArguments().containsKey("FROMTODAY"))
            isFromToday = getArguments().getString("FROMTODAY");
    }

    private void getDatafromService(String personnal_Id) {
        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        nameValuePairList.add(new BasicNameValuePair("personnel_id", personnal_Id));
        nameValuePairList.add(new BasicNameValuePair("start_date", firstDateMonth));
        nameValuePairList.add(new BasicNameValuePair("end_date", lastDateMonth));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/Scheduler/location_calendar_events", this, "ViewScheduleService", true).execute();
    }

    private void initiateCateOnCalendar(List<String> calendarList, MCalendarViewEvent mCalendarView, String type) {
        ArrayList<DateDataEvent> dates = new ArrayList<>();
        if (calendarList.size() > 0) {
            for (int j = 0; j < calendarList.size(); j++) {
                String dateValue = calendarList.get(j);

                int dayInt = Integer.parseInt(dateValue.substring(0, 2));
                int monthInt = Integer.parseInt(dateValue.substring(3, 5));
                int yearInt = Integer.parseInt(dateValue.substring(6, 10));
                // dates.add(new DateDataEvent(yearInt, monthInt, dayInt));
                if ("mark".equals(type)) {
                    // fragmentViewScheduleBinding.calenderAAA.markDate(yearInt, monthInt, dayInt, MarkStyleEvent.DOT);//mark multiple dates with this code.
                    Log.e("DATEISS", "" + dayInt + " " + monthInt + " " + yearInt);
                } else {
                    // fragmentViewScheduleBinding.calenderAAA.unMarkDate(yearInt, monthInt, dayInt, MarkStyleEvent.DOT);//mark multiple dates with this code.
                    Log.e("DATEISS", "" + dayInt + " " + monthInt + " " + yearInt);
                }
            }
        }

        /*      Log.d("marked dates:-", "" + calendarView.getMarkedDates());//get all marked dates.*/
    }

    private List<String> getDay(String day, String month, String year) {
        List<String> selectedDateNew = new ArrayList<>();
        String string;

        Calendar cal = Calendar.getInstance();
        if ("".equals(day)) {
            string = " 08/11/2019 15:15:50";
        } else {
            string = day + "/" + month + "/" + year + " 15:15:50";
        }
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
        try {
            Date date = format.parse(string);
            cal.setTime(date);//Set specific Date if you want to

        } catch (ParseException e) {
            e.printStackTrace();
        }
        for (int i = Calendar.SUNDAY; i <= Calendar.SATURDAY; i++) {
            cal.set(Calendar.DAY_OF_WEEK, i);
            //  System.out.println(cal.getTime());

            SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
            System.out.println(cal.getTime());

            String formatted = format1.format(cal.getTime());
            System.out.println(formatted);

            //System.out.println();
            selectedDateNew.add(format1.format(cal.getTime()));
            Log.e("DATEISS", format1.format(cal.getTime()));

            //Returns Date
        }


        return selectedDateNew;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("ViewScheduleService".equals(type)) {
            if (googleProgressDialog != null) {
                googleProgressDialog.dismiss();
            }
            viewScheduleModel = new Gson().fromJson(result, ViewScheduleModel.class);
            fragmentViewScheduleBinding.compactcalendarView.removeAllEvents();
            for (int i = 0; i < viewScheduleModel.getData().size(); i++) {
                String date = viewScheduleModel.getData().get(i).getStart_date();

                int yearInt = Integer.parseInt(date.substring(0, 4));
                int monthInt = Integer.parseInt(date.substring(5, 7));
                int dayInt = Integer.parseInt(date.substring(8, 10));

                //fragmentViewScheduleBinding.calenderAAA.markDate(yearInt, monthInt, dayInt, MarkStyleEvent.DEFAULT); //mark multiple dates with this code.

                String dateAfter = dayInt + "/" + monthInt + "/" + yearInt;

                Random random = new Random();

                fragmentViewScheduleBinding.compactcalendarView.addEvent(new Event(Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256)), getMilliFromDate(dateAfter), "Event at " + new Date(getMilliFromDate(dateAfter))));
                if (i == viewScheduleModel.getData().size() - 1) {
                    fragmentViewScheduleBinding.compactcalendarView.invalidate();

                }

                //Log.e("ALL DATES", "" + dayInt + " " + monthInt + " " + yearInt);
            }
            manipulateLocationSpinnerData();
            if (isFromFirstEvent) {
                isFromFirstEvent = false;
                getPersonalSpinnerData(AlphaHolder.selectedLocationSpinnerPosition);

            }
        }
    }

    private void manipulateLocationSpinnerData() {
        if (fragmentViewScheduleBinding != null && fragmentViewScheduleBinding.selectLocationsLinLayoutId != null) {
            List locationNameList = new ArrayList();
            if (locationArrayListFromSP.size() > 0) {
                for (int i = 0; i < locationArrayListFromSP.size(); i++) {
                    locationNameList.add(locationArrayListFromSP.get(i).getLocation_name());
                    if (i == locationArrayListFromSP.size() - 1) {
                        arrayAdapter = new ArrayAdapter(context, R.layout.spinner_layout, locationNameList);
                        showLocationSpinnerData(arrayAdapter, false);
                        for (int j = 0; j < locationArrayListFromSP.size(); j++) {
                            if (locationArrayListFromSP.get(j).isSelected()) {
                                fragmentViewScheduleBinding.selectUserLocaitonTextView.setText(locationArrayListFromSP.get(j).getLocation_name());
                            }
                        }
                        fragmentViewScheduleBinding.selectUserLocaitonTextView.setText(locationArrayListFromSP.get(AlphaHolder.selectedLocationSpinnerPosition).getLocation_name());
                    }
                }
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }

    //get start date and end date
    public String getFirstDay(Date d) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date dddd = calendar.getTime();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
        return sdf1.format(dddd);
    }

    public String getLastDay(Date d) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date dddd = calendar.getTime();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
        return sdf1.format(dddd);
    }

    public long getMilliFromDate(String dateFormat) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = formatter.parse(dateFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("Today is " + date);
        return date.getTime();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {

    }
}
