package com.mydevit.pieservicess.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.FragmentNotificationOneTabBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.NotificationReminderTimeSlot_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.NotificationMainModel;
import com.mydevit.pieservicess.service.NotificationTabOneModel;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class NotificationTabOneFragment extends Fragment implements ClickListenerPC, ServiceReponseInterface_Duplicate {

    Context context;
    List<NameValuePair> nameValuePairList;
    String location_id;
    FragmentNotificationOneTabBinding fragmentNotificationOneTabBinding;
    NotificationMainModel notificationMainModel;

    List<String> dayBeforeSpinnerList;
    NotificationReminderTimeSlot_RecyclerViewAdapter notificationReminderTimeSlot_recyclerViewAdapter;
    int selectedEmailSpinnerId = 0;
    String outputIs;

    NotificationTabOneModel notificationTabOneModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentNotificationOneTabBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification_one_tab, container, false);
        fragmentNotificationOneTabBinding.setClickListener(this);

        return fragmentNotificationOneTabBinding.getRoot();

    }

    @Override
    public void onResume() {
        super.onResume();
        getDataFromService();
        manipulateSpinner();
        textChangeListener();
    }

    private void textChangeListener() {
        fragmentNotificationOneTabBinding.emailFromNameEditTextId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                notificationMainModel.getData().getEmail_notify_data().setEmail_from(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        fragmentNotificationOneTabBinding.emailSubjectEditTextId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                notificationMainModel.getData().getEmail_notify_data().setEmail_subject(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        fragmentNotificationOneTabBinding.replayToEditTextId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                notificationMainModel.getData().getEmail_notify_data().setEmail_reply(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void manipulateSpinner() {
        dayBeforeSpinnerList = new ArrayList<>();
        dayBeforeSpinnerList.add("One day before appointment");
        dayBeforeSpinnerList.add("Two days before appointment");
        dayBeforeSpinnerList.add("Three days before appointment");
        dayBeforeSpinnerList.add("One Week");
        dayBeforeSpinnerList.add("Two Weeks");

        ArrayAdapter arrayAdapter = new ArrayAdapter(context, R.layout.spinner_layout, dayBeforeSpinnerList);
        fragmentNotificationOneTabBinding.setSetDataSpinner(arrayAdapter);

        fragmentNotificationOneTabBinding.emailSpinnerId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                selectedEmailSpinnerId = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getDataFromService() {
        location_id = getArguments().getString("location_id");
        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));

        new Service_Integration_Duplicate(context, nameValuePairList, "business/Scheduler/get_email_notify", this, "NOTIFICATION_MAIN_DATA", true).execute();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("NOTIFICATION_MAIN_DATA".equals(type)) {
            notificationMainModel = new Gson().fromJson(result, NotificationMainModel.class);
            notificationReminderTimeSlot_recyclerViewAdapter = new NotificationReminderTimeSlot_RecyclerViewAdapter(context, notificationMainModel.getData().getEmail_reminder_data(), this);
            fragmentNotificationOneTabBinding.setDataBeanAdapter(notificationReminderTimeSlot_recyclerViewAdapter);

            fragmentNotificationOneTabBinding.setDataBeanData(notificationMainModel.getData().getEmail_notify_data());
            if (notificationMainModel.getData().getEmail_notify_data() != null && !"".equals(notificationMainModel.getData().getEmail_notify_data()))
                fragmentNotificationOneTabBinding.emailSpinnerId.setSelection(Integer.parseInt(notificationMainModel.getData().getEmail_notify_data().getEmail_trigger()) - 1);

            fragmentNotificationOneTabBinding.executePendingBindings();
        }
        if ("NOTIFICATION_TAB".equals(type)) {
            notificationTabOneModel = new Gson().fromJson(result, NotificationTabOneModel.class);
            if (null != notificationTabOneModel) {
                getDataFromService();

            }
        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
        if ("addnewreminder".equals(tag_FromWhere)) {
            if (notificationMainModel.getData().getEmail_reminder_data().size() < 4) {

                NotificationMainModel.DataBean.EmailReminderDataBean emailReminderDataBean = new NotificationMainModel.DataBean.EmailReminderDataBean();
                notificationMainModel.getData().getEmail_reminder_data().add(emailReminderDataBean);
                fragmentNotificationOneTabBinding.getDataBeanAdapter().notifyDataSetChanged();
            } else {
                AlphaHolder.customToast(getActivity(), "You can add only 4 slot for email notification");
            }
        }
        if ("Save".equals(tag_FromWhere)) {
            List<String> getReminderList = getRemiderData(notificationMainModel.getData().getEmail_reminder_data());

            JSONArray jsonArray = new JSONArray(getReminderList);
            String dataToSendTemp = jsonArray.toString();

            nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(context)));
            nameValuePairList.add(new BasicNameValuePair("email_status", notificationMainModel.getData().getEmail_notify_data().getEmail_status()));
            nameValuePairList.add(new BasicNameValuePair("email_trigger", String.valueOf(selectedEmailSpinnerId + 1)));
            nameValuePairList.add(new BasicNameValuePair("email_from", notificationMainModel.getData().getEmail_notify_data().getEmail_from()));
            nameValuePairList.add(new BasicNameValuePair("email_subject", notificationMainModel.getData().getEmail_notify_data().getEmail_subject()));
            nameValuePairList.add(new BasicNameValuePair("email_reply", notificationMainModel.getData().getEmail_notify_data().getEmail_reply()));
            nameValuePairList.add(new BasicNameValuePair("reminder_time", dataToSendTemp));
            nameValuePairList.add(new BasicNameValuePair("notify_id", notificationMainModel.getData().getEmail_notify_data().getNotify_id()));
            nameValuePairList.add(new BasicNameValuePair("location_id", location_id));

            new Service_Integration_Duplicate(context, nameValuePairList, "business/Scheduler/save_schedule_notification_reminder", this, "NOTIFICATION_TAB", true).execute();
        }
        if ("scheduleappointmentclick".equals(tag_FromWhere)) {
            if (notificationMainModel.getData().getEmail_notify_data().getEmail_status().equals("N")) {
                notificationMainModel.getData().getEmail_notify_data().setEmail_status("Y");
            } else {
                notificationMainModel.getData().getEmail_notify_data().setEmail_status("N");
            }
            fragmentNotificationOneTabBinding.invalidateAll();
            Log.d("checkbox check", String.valueOf(notificationMainModel.getData().getEmail_notify_data().getEmail_status()));
        }
    }

    private List<String> getRemiderData(List<NotificationMainModel.DataBean.EmailReminderDataBean> email_reminder_data) {
        List<String> stringList = new ArrayList<>();
        for (int j = 0; j < email_reminder_data.size(); j++) {
            stringList.add(email_reminder_data.get(j).getReminder_time());

            if (j == email_reminder_data.size() - 1) {
                return stringList;
            }
        }
        return stringList;
    }
}
