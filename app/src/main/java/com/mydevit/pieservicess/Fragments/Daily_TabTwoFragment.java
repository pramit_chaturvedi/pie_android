package com.mydevit.pieservicess.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.recyclerviewadapters.Appointments_RecyclerViewAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class Daily_TabTwoFragment extends Fragment implements ClickListener {

    View view;
    @BindView(R.id.appointmentsRecyclerViewId)
    RecyclerView posSetUpRecyclerView;

    @BindView(R.id.checkoutTextViewId)
    TextView bookNowTextView;

    ArrayList setDataArrayList;
    Appointments_RecyclerViewAdapter appointments_recyclerViewAdapter;
    Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_daily__tab_one, container, false);
        ButterKnife.bind(this, view);
        Initialize();
        return view;
    }

    private void Initialize() {
        setDataArrayList = new ArrayList();
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);
        setDataArrayList.add(null);

        posSetUpRecyclerView.setHasFixedSize(true);
        posSetUpRecyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
      /*  appointments_recyclerViewAdapter = new Appointments_RecyclerViewAdapter(context, setDataArrayList, this);
        posSetUpRecyclerView.setAdapter(appointments_recyclerViewAdapter);*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @OnClick({R.id.checkoutTextViewId})
    public void onEventListner(View view) {
        if (view == bookNowTextView) {

        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {

    }
}
