package com.mydevit.pieservicess.utils.ServicesUtilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mydevit.pieservicess.service.signin_model;


public class SharedPreferences_Util {

    Context context;
    SharedPreferences prefs;

    public SharedPreferences_Util(Context context) {
        this.context = context;
    }

    public static String getToken(Context context) {
        SharedPreferences_Util sharedPreferences_util = new SharedPreferences_Util(context);
        return sharedPreferences_util.getLoginModel().getData().getToken();
    }

    public static String getLoggedInPersonnelId(Context context) {
        SharedPreferences_Util sharedPreferences_util = new SharedPreferences_Util(context);
        signin_model signin_model = sharedPreferences_util.getLoginModel();
        if (signin_model != null && signin_model.getData().getId() != null) {
            return signin_model.getData().getId();
        }
        return "";
    }

    public static String getRoleId(Context context) {
        SharedPreferences_Util sharedPreferences_util = new SharedPreferences_Util(context);
        return sharedPreferences_util.getLoginModel().getData().getRole_id();
    }

    public void saveLoginModel(signin_model obj) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("MYLABEL", "myStringToSave").apply();
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        editor.putString("LOGIN", json);
        editor.commit();     // This line is IMPORTANT !!!
    }

    public signin_model getLoginModel() {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString("LOGIN", "");
        java.lang.reflect.Type type = new TypeToken<signin_model>() {
        }.getType();
        // LinkedTreeMap linkedTreeMap = gson.fromJson(json, type);
        signin_model loginModel = gson.fromJson(json, type);
        return loginModel;
    }

    public void logout() {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }
}
