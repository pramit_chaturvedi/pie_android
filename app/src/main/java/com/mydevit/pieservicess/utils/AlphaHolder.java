package com.mydevit.pieservicess.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.model.ParentServiceModel;
import com.mydevit.pieservicess.model.SelectedCustomerModel;
import com.mydevit.pieservicess.service.ConfigureAvailabiltyModel;
import com.mydevit.pieservicess.service.SchedulerServiceModel;
import com.mydevit.pieservicess.service.signin_model;
import com.mydevit.pieservicess.service.today_schedule;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Banna ji on 12/25/2017.
 */
public class AlphaHolder {

    public static String product_id = "";
    public static String order_id = "";
    public static boolean IsFromNotificationOrStart = false;
    public static boolean IsShowHomeLoader = false;
    public static String check = "FALSE";
    public static String editOutletFrom = "";
    public static String editProductFrom = "";
    public static String deviceName = "";
    public static String isFromEditPost = "";
    public static SelectedCustomerModel selectedCustomerModel = null;
    public static String fromWhere = "";
    public static String selectedDate = "";
    public static boolean isShowing = false;
    public static int selectedLocationSpinnerPosition = 0;


    public static List<String> calendarList = new ArrayList<>();
    public static List<ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean> personnelTimingsList = new ArrayList<>();


    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);

        if (activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } else {

        }
    }

    public static String ConvertFirstCharacterCappital(String value) {
        if (value == null || value.equalsIgnoreCase("") || value.equalsIgnoreCase(" ")) {
            return "No Record Available";
        } else {
            String first = value.substring(0, 1);
            int length = value.length();
            return first.toUpperCase() + value.substring(1, length);
        }
    }

    public static String SearchProduct(String value) {
        if (value == null || value.equalsIgnoreCase("") || value.equalsIgnoreCase(" ")) {
            return "";
        } else {
            String first = value.substring(0, 1);
            int length = value.length();
            return first.toUpperCase() + value.substring(1, length);
        }
    }

    public static void ClearEditText(EditText editText) {
        editText.setText("");
    }

    public static String GetExtras(String s, Activity activity) {
        String value = null;
        if (activity.getIntent().getExtras() != null) {
            value = activity.getIntent().getExtras().getString(s);
        }

        return value;
    }
    public static boolean isLoginWithPersonnel(String roleId) {
        if("3".equals(roleId)){
            return true;
        }
        return false;
    }
    public static String getLoggedInPersonneld(Context context){
        SharedPreferences_Util sharedPreferences_util = new SharedPreferences_Util(context);
        signin_model signin_model = sharedPreferences_util.getLoginModel();
        if(signin_model!=null){
            return signin_model.getData().getId();

        }
        return "";
    }
    public static void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Quicksand_Regular.ttf"));
            }
        } catch (Exception e) {
        }
    }
   /* public static void ManipulateTextView(String value, TextView textView, Activity activity) {
        if (value == null) {
            textView.setText(activity.getResources().getString(R.string.norecordfound));
        }
        else if (value != null && textView != null) {
            textView.setText(value);
        } else if (value != null && textView == null) {
            Toast.makeText(activity, activity.getResources().getString(R.string.nullobjectfound), Toast.LENGTH_SHORT).show();
        } else if (value.equalsIgnoreCase("") || value.equalsIgnoreCase(" ")) {
            textView.setText(activity.getResources().getString(R.string.norecordfound));
        } else {
            textView.setText(activity.getResources().getString(R.string.norecordfound));
        }
    }

    public static void ManipulateEditText(String value, EditText editText, Activity activity) {
        if (value != null && editText != null) {
            editText.setText(value);
        }
        if (value != null && editText == null) {
            Toast.makeText(activity, activity.getResources().getString(R.string.nullobjectfound), Toast.LENGTH_SHORT).show();
        }
    }*/

    public static void Margin(final Activity activity, final View v, final ScrollView scrollView) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(activity, child);
                }
            } else if (v instanceof EditText) {
                EditText editText = (EditText) v;
                editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            scrollView.scrollBy(0, 250);
                        } else {

                        }
                    }
                });
            }
        } catch (Exception e) {
        }
    }

    public static void customToast(Context context, String textToShow) {
        View layout = LayoutInflater.from(context).inflate(R.layout.custom_toast, null);
        TextView text = layout.findViewById(R.id.text);
        text.setText(textToShow);
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.BOTTOM, 0, 120);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public static Object getPojoFromString(Object object, String stringResponse) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            object = mapper.readValue(stringResponse, Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return object;
    }

    public static List<String> getSlotList(String startTime, String endTime) {
        List<String> timeSlotList = new ArrayList<>();

        String date1 = "13/11/2019";
        String time1 = "10:00 AM";
        String date2 = "13/11/2019";
        String time2 = "07:00 PM";

        String format = "dd/MM/yyyy hh:mm a";

        SimpleDateFormat sdf = new SimpleDateFormat(format);

        Date dateObj1 = null;
        try {
            dateObj1 = sdf.parse(date1 + " " + startTime);
            Date dateObj2 = sdf.parse(date2 + " " + endTime);

            System.out.println("Date Start: " + dateObj1);
            System.out.println("Date End: " + dateObj2);

            //Date d = new Date(dateObj1.getTime() + 3600000);

            long dif = dateObj1.getTime();
            while (dif < dateObj2.getTime()) {
                Date slot = new Date(dif);
                String stringDate = DateFormat.getTimeInstance(DateFormat.SHORT).format(slot);
                timeSlotList.add(stringDate);
                //System.out.println("MalingaData --->" + stringDate);
                dif += 300000;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeSlotList;
    }

    public String ConvertFirstCharacterCappitals(String value) {
        if (value == null || value.equalsIgnoreCase("") || value.equalsIgnoreCase(" ")) {
            return "No Record Available";
        } else {
            String first = value.substring(0, 1);
            int length = value.length();
            return first.toUpperCase() + value.substring(1, length);
        }
    }

    public String convertIntToString(int value) {
        return String.valueOf(value);
    }

    public String getYear(String time) {
        String date = time.substring(0, 10);
        String timesub = time.substring(11, 19);
        return date + " " + timesub;
    }

    public void playSound(Context context, Uri uri) {
        Ringtone r = RingtoneManager.getRingtone(context, uri);
        r.play();
    }

    public void ringColorDynamically(Context context, int color) {
        LayerDrawable shape = (LayerDrawable) ContextCompat.getDrawable(context, R.drawable.round_border);
        GradientDrawable gradientDrawable = (GradientDrawable) shape.findDrawableByLayerId(R.id.shape);
        // if you want to change the color of background of textview dynamically
        //gradientDrawable.setColor(ContextCompat.getColor(demo.this,R.color.colorAccent));
        // This is mangage the storke width and it's color by this shape.setStroke(strokeWidth,color);
        gradientDrawable.setStroke(10, color);
    }

    public void saveArrayList(ArrayList<SchedulerServiceModel.DataBean.PersonnelsBean> list, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString("SCHEDULE_DATA_LIST", json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public ArrayList<SchedulerServiceModel.DataBean.PersonnelsBean> getArrayList(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString("SCHEDULE_DATA_LIST", null);
        Type type = new TypeToken<ArrayList<SchedulerServiceModel.DataBean.PersonnelsBean>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void saveScheduledArrayList(List<SchedulerServiceModel.DataBean> list, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString("SCHEDULE_DATA_LIST_PARENT", json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public List<SchedulerServiceModel.DataBean> getScheduledArrayList(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString("SCHEDULE_DATA_LIST_PARENT", null);
        Type type = new TypeToken<List<SchedulerServiceModel.DataBean>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public void saveArrayListToday(List<today_schedule.DataBean.PersonnelTimingsBean> list, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString("SCHEDULE_DATA_LIST_TODAY", json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public List<today_schedule.DataBean.PersonnelTimingsBean> getArrayListToday(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString("SCHEDULE_DATA_LIST_TODAY", null);
        Type type = new TypeToken<List<today_schedule.DataBean.PersonnelTimingsBean>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public void saveServiceArrayList(ArrayList<ParentServiceModel> list, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString("SERVICEDATA", json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public ArrayList<ParentServiceModel> getServiceArrayList(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString("SERVICEDATA", null);
        Type type = new TypeToken<ArrayList<ParentServiceModel>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

}
