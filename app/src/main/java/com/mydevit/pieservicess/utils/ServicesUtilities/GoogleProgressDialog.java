package com.mydevit.pieservicess.utils.ServicesUtilities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.ProgressWheel;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;



/**
 * Created by Rishi on 9/6/2017.
 */

public class GoogleProgressDialog {
    ///FOR RUN THIS DIAOLG BOX SUCCESSFULLY YOU HAVE TO COMPILE THIS DEPENDENCY IN GRADLE.BUILD FILE  compile 'com.github.castorflex.smoothprogressbar:library-circular:1.1.0'
    Context context;
    ProgressDialog dialog;
    private ProgressWheel mCircularProgressBar;
    private Interpolator mCurrentInterpolator;
    private int mStrokeWidth = 4;
    private float mSpeed = 1f;

    public GoogleProgressDialog(Context context) {
        this.context = context;
    }

    public void showDialoge() {
        try {
            if (!AlphaHolder.isShowing) {
                dialog = new ProgressDialog(context, R.style.full_screen_dialog) {
                    @Override
                    protected void onCreate(Bundle savedInstanceState) {
                        super.onCreate(savedInstanceState);
                        setContentView(R.layout.full_dialoge);

                        dialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
                        mCircularProgressBar = findViewById(R.id.power_spinner);
                        updateValues();
                    }
                };
                dialog.show();
                AlphaHolder.isShowing = true;
            }

        } catch (Exception e) {
            dismiss();
        }
    }

    public void dismiss() {
        AlphaHolder.isShowing = false;
        if (dialog != null) {
            dialog.dismiss();
        }

    }
    private void updateValues() {
        CircularProgressDrawable circularProgressDrawable;
        CircularProgressDrawable.Builder b = new CircularProgressDrawable.Builder(context)
                .sweepSpeed(mSpeed)
                .rotationSpeed(mSpeed)
                .strokeWidth(dpToPx(mStrokeWidth));
        if (mCurrentInterpolator != null) {
            b.sweepInterpolator(mCurrentInterpolator);
        }
        // mCircularProgressBar.setIndeterminateDrawable(circularProgressDrawable = b.build());

        // /!\ Terrible hack, do not do this at home!
       /* circularProgressDrawable.setBounds(0,
                0,
                mCircularProgressBar.getWidth(),
                mCircularProgressBar.getHeight());
        mCircularProgressBar.setVisibility(View.INVISIBLE);
        mCircularProgressBar.setVisibility(View.VISIBLE);*/
    }

    private int dpToPx(int dp) {
        Resources r = context.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp, r.getDisplayMetrics());
        return px;
    }


}
