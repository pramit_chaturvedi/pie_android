package com.mydevit.pieservicess.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.GoogleProgressDialog;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

public class Service_Integration_Duplicate extends AsyncTask<String, Void, String> {

    Context context;
    List<NameValuePair> nameValuePairs;
    String url, roleId = "";
    String type;
    GoogleProgressDialog googleProgressDialog;
    ServiceReponseInterface_Duplicate serviceReponseInterface;
    boolean isShowLoader;

    public Service_Integration_Duplicate(Context context, List<NameValuePair> nameValuePairs, String url, ServiceReponseInterface_Duplicate serviceReponseInterface, String type) {
        this.context = context;
        this.nameValuePairs = nameValuePairs;
        this.url = url;
        this.type = type;
        this.roleId = SharedPreferences_Util.getRoleId(context);
        this.serviceReponseInterface = serviceReponseInterface;
        googleProgressDialog = new GoogleProgressDialog(context);
    }

    public Service_Integration_Duplicate(Context context, List<NameValuePair> nameValuePairs, String url, ServiceReponseInterface_Duplicate serviceReponseInterface, String type, boolean isShowLoader) {
        this.context = context;
        this.nameValuePairs = nameValuePairs;
        this.url = url;
        this.type = type;
        this.serviceReponseInterface = serviceReponseInterface;
        this.isShowLoader = isShowLoader;
        this.roleId = SharedPreferences_Util.getRoleId(context);
        googleProgressDialog = new GoogleProgressDialog(context);
    }

    public Service_Integration_Duplicate(Context context, String roleId, List<NameValuePair> nameValuePairs, String url, ServiceReponseInterface_Duplicate serviceReponseInterface, String type, boolean isShowLoader) {
        this.context = context;
        this.nameValuePairs = nameValuePairs;
        this.url = url;
        this.type = type;
        this.serviceReponseInterface = serviceReponseInterface;
        this.isShowLoader = isShowLoader;
        this.roleId = roleId;
        googleProgressDialog = new GoogleProgressDialog(context);
    }

    @Override
    protected void onPreExecute() {
        if (isShowLoader) {
            googleProgressDialog.showDialoge();
            Log.e("ParamToService" + "  " + type, new Gson().toJson(nameValuePairs));
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String responseString = null;
        HttpPost httppost = null;
        HttpClient httpclient = new DefaultHttpClient();
        if ("Y".equals(roleId)) {
            httppost = new HttpPost("http://piebooking.com/api/auth_api" + url); // HERE ALL CONDITION FOR CHECK URL BECAUSE HERE URL WILL BE CHANGE ACCORDING TO THE CUSTOMER AND PERSONNEL LOGIN, DUE TO THE THIRD CLASS PHP BECKEND.

        } else {
            if (roleId.equals("1") || "".equals(roleId)) {
                httppost = new HttpPost("http://piebooking.com/api/" + url);
            } else {
                String[] split = url.split("business", 2);

                String firstSubString = split[0];
                String secondSubString = split[1];
                httppost = new HttpPost("http://piebooking.com/api/" + "personnel" + secondSubString);
            }
        }


        try {
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            responseString = EntityUtils.toString(entity, "UTF-8");

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
        return responseString;
    }

    @Override
    protected void onPostExecute(String result) {
        Log.e("response", type + "-------" + result);
        if (isShowLoader) {
            googleProgressDialog.dismiss();
        }

        if (isJSONValid(result))
            serviceReponseInterface.onSuccess(result, type);
        else
            AlphaHolder.customToast(context, context.getResources().getString(R.string.jsonformatexception));



    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }

    public boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
}
