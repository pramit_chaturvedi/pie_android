package com.mydevit.pieservicess.utils.EventBus;

public class Events {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
