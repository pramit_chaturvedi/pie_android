package com.mydevit.pieservicess.service;

import java.util.List;

public class Payment_Setting_Model {


    /**
     * status : success
     * status_code : 200
     * message : Payment Settings Show Successfully
     * data : {"payment_mode":[{"payment_mode_id":"1","payment_mode_name":"Cash","payment_mode_status":"Y"},{"payment_mode_id":"2","payment_mode_name":"Credit Card","payment_mode_status":"Y"},{"payment_mode_id":"3","payment_mode_name":"Check","payment_mode_status":"N"},{"payment_mode_id":"4","payment_mode_name":"Invoice","payment_mode_status":"N"}],"payment_processor":[{"payment_processor_id":"1","payment_processor_name":"Stripe","payment_mode_status":"N","secret_key":"","publishable_key":""},{"payment_processor_id":"2","payment_processor_name":"Paypal","payment_mode_status":"Y","secret_key":"cdfvgbt","publishable_key":"gergr"},{"payment_processor_id":"3","payment_processor_name":"Autherize.net","payment_mode_status":"N","secret_key":"","publishable_key":""}],"payment_tax_service":{"payment_tax_services":"Y","payment_tax_service_value":"5"},"payment_tax_product":{"payment_tax_product":"Y","payment_tax_product_value":"0"},"gratuity":{"gratuity_id":"4","gratuity_data":[{"gratuity_setting_id":"1","gratuity_percentage":"5","gratuity_type":"P","gratuity_amount":0,"gratuity_setting_status":"Y"},{"gratuity_setting_id":"2","gratuity_percentage":"10","gratuity_type":"P","gratuity_amount":0,"gratuity_setting_status":"N"},{"gratuity_setting_id":"3","gratuity_percentage":"custom_amount","gratuity_type":"C","gratuity_amount":0,"gratuity_setting_status":"N"}]},"Gratuity_Cash_Withdrawl":"Y","Gift_Cards_Coupons":[{"discount_offer_id":"2","discount_category":"coupon","discount_code":"NEWYEAR19","discount_type":"percentage","discount_value":"2","discount_limit":"19"},{"discount_offer_id":"3","discount_category":"coupon","discount_code":"OCT123","discount_type":"percentage","discount_value":"5","discount_limit":"20"},{"discount_offer_id":"4","discount_category":"coupon","discount_code":"TESTCODE","discount_type":"percentage","discount_value":"5","discount_limit":"100"}]}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * payment_mode : [{"payment_mode_id":"1","payment_mode_name":"Cash","payment_mode_status":"Y"},{"payment_mode_id":"2","payment_mode_name":"Credit Card","payment_mode_status":"Y"},{"payment_mode_id":"3","payment_mode_name":"Check","payment_mode_status":"N"},{"payment_mode_id":"4","payment_mode_name":"Invoice","payment_mode_status":"N"}]
         * payment_processor : [{"payment_processor_id":"1","payment_processor_name":"Stripe","payment_mode_status":"N","secret_key":"","publishable_key":""},{"payment_processor_id":"2","payment_processor_name":"Paypal","payment_mode_status":"Y","secret_key":"cdfvgbt","publishable_key":"gergr"},{"payment_processor_id":"3","payment_processor_name":"Autherize.net","payment_mode_status":"N","secret_key":"","publishable_key":""}]
         * payment_tax_service : {"payment_tax_services":"Y","payment_tax_service_value":"5"}
         * payment_tax_product : {"payment_tax_product":"Y","payment_tax_product_value":"0"}
         * gratuity : {"gratuity_id":"4","gratuity_data":[{"gratuity_setting_id":"1","gratuity_percentage":"5","gratuity_type":"P","gratuity_amount":0,"gratuity_setting_status":"Y"},{"gratuity_setting_id":"2","gratuity_percentage":"10","gratuity_type":"P","gratuity_amount":0,"gratuity_setting_status":"N"},{"gratuity_setting_id":"3","gratuity_percentage":"custom_amount","gratuity_type":"C","gratuity_amount":0,"gratuity_setting_status":"N"}]}
         * Gratuity_Cash_Withdrawl : Y
         * Gift_Cards_Coupons : [{"discount_offer_id":"2","discount_category":"coupon","discount_code":"NEWYEAR19","discount_type":"percentage","discount_value":"2","discount_limit":"19"},{"discount_offer_id":"3","discount_category":"coupon","discount_code":"OCT123","discount_type":"percentage","discount_value":"5","discount_limit":"20"},{"discount_offer_id":"4","discount_category":"coupon","discount_code":"TESTCODE","discount_type":"percentage","discount_value":"5","discount_limit":"100"}]
         */

        private PaymentTaxServiceBean payment_tax_service;
        private PaymentTaxProductBean payment_tax_product;
        private GratuityBean gratuity;
        private String Gratuity_Cash_Withdrawl;
        private List<PaymentModeBean> payment_mode;
        private List<PaymentProcessorBean> payment_processor;
        private List<GiftCardsCouponsBean> Gift_Cards_Coupons;

        public PaymentTaxServiceBean getPayment_tax_service() {
            return payment_tax_service;
        }

        public void setPayment_tax_service(PaymentTaxServiceBean payment_tax_service) {
            this.payment_tax_service = payment_tax_service;
        }

        public PaymentTaxProductBean getPayment_tax_product() {
            return payment_tax_product;
        }

        public void setPayment_tax_product(PaymentTaxProductBean payment_tax_product) {
            this.payment_tax_product = payment_tax_product;
        }

        public GratuityBean getGratuity() {
            return gratuity;
        }

        public void setGratuity(GratuityBean gratuity) {
            this.gratuity = gratuity;
        }

        public String getGratuity_Cash_Withdrawl() {
            return Gratuity_Cash_Withdrawl;
        }

        public void setGratuity_Cash_Withdrawl(String Gratuity_Cash_Withdrawl) {
            this.Gratuity_Cash_Withdrawl = Gratuity_Cash_Withdrawl;
        }

        public List<PaymentModeBean> getPayment_mode() {
            return payment_mode;
        }

        public void setPayment_mode(List<PaymentModeBean> payment_mode) {
            this.payment_mode = payment_mode;
        }

        public List<PaymentProcessorBean> getPayment_processor() {
            return payment_processor;
        }

        public void setPayment_processor(List<PaymentProcessorBean> payment_processor) {
            this.payment_processor = payment_processor;
        }

        public List<GiftCardsCouponsBean> getGift_Cards_Coupons() {
            return Gift_Cards_Coupons;
        }

        public void setGift_Cards_Coupons(List<GiftCardsCouponsBean> Gift_Cards_Coupons) {
            this.Gift_Cards_Coupons = Gift_Cards_Coupons;
        }

        public static class PaymentTaxServiceBean {
            /**
             * payment_tax_services : Y
             * payment_tax_service_value : 5
             */

            private String payment_tax_services;
            private String payment_tax_service_value;

            public String getPayment_tax_services() {
                return payment_tax_services;
            }

            public void setPayment_tax_services(String payment_tax_services) {
                this.payment_tax_services = payment_tax_services;
            }

            public String getPayment_tax_service_value() {
                return payment_tax_service_value;
            }

            public void setPayment_tax_service_value(String payment_tax_service_value) {
                this.payment_tax_service_value = payment_tax_service_value;
            }
        }

        public static class PaymentTaxProductBean {
            /**
             * payment_tax_product : Y
             * payment_tax_product_value : 0
             */

            private String payment_tax_product;
            private String payment_tax_product_value;

            public String getPayment_tax_product() {
                return payment_tax_product;
            }

            public void setPayment_tax_product(String payment_tax_product) {
                this.payment_tax_product = payment_tax_product;
            }

            public String getPayment_tax_product_value() {
                return payment_tax_product_value;
            }

            public void setPayment_tax_product_value(String payment_tax_product_value) {
                this.payment_tax_product_value = payment_tax_product_value;
            }
        }

        public static class GratuityBean {
            /**
             * gratuity_id : 4
             * gratuity_data : [{"gratuity_setting_id":"1","gratuity_percentage":"5","gratuity_type":"P","gratuity_amount":0,"gratuity_setting_status":"Y"},{"gratuity_setting_id":"2","gratuity_percentage":"10","gratuity_type":"P","gratuity_amount":0,"gratuity_setting_status":"N"},{"gratuity_setting_id":"3","gratuity_percentage":"custom_amount","gratuity_type":"C","gratuity_amount":0,"gratuity_setting_status":"N"}]
             */

            private String gratuity_id;
            private List<GratuityDataBean> gratuity_data;

            public String getGratuity_id() {
                return gratuity_id;
            }

            public void setGratuity_id(String gratuity_id) {
                this.gratuity_id = gratuity_id;
            }

            public List<GratuityDataBean> getGratuity_data() {
                return gratuity_data;
            }

            public void setGratuity_data(List<GratuityDataBean> gratuity_data) {
                this.gratuity_data = gratuity_data;
            }

            public static class GratuityDataBean {
                /**
                 * gratuity_setting_id : 1
                 * gratuity_percentage : 5
                 * gratuity_type : P
                 * gratuity_amount : 0
                 * gratuity_setting_status : Y
                 */

                private String gratuity_setting_id;
                private String gratuity_percentage;
                private String gratuity_type;
                private String gratuity_amount;
                private String gratuity_setting_status;

                public String getGratuity_setting_id() {
                    return gratuity_setting_id;
                }

                public void setGratuity_setting_id(String gratuity_setting_id) {
                    this.gratuity_setting_id = gratuity_setting_id;
                }

                public String getGratuity_percentage() {
                    return gratuity_percentage;
                }

                public void setGratuity_percentage(String gratuity_percentage) {
                    this.gratuity_percentage = gratuity_percentage;
                }

                public String getGratuity_type() {
                    return gratuity_type;
                }

                public void setGratuity_type(String gratuity_type) {
                    this.gratuity_type = gratuity_type;
                }

                public String getGratuity_amount() {
                    return gratuity_amount;
                }

                public void setGratuity_amount(String gratuity_amount) {
                    this.gratuity_amount = gratuity_amount;
                }

                public String getGratuity_setting_status() {
                    return gratuity_setting_status;
                }

                public void setGratuity_setting_status(String gratuity_setting_status) {
                    this.gratuity_setting_status = gratuity_setting_status;
                }
            }
        }

        public static class PaymentModeBean {
            /**
             * payment_mode_id : 1
             * payment_mode_name : Cash
             * payment_mode_status : Y
             */

            private String payment_mode_id;
            private String payment_mode_name;
            private String payment_mode_status;

            public String getPayment_mode_id() {
                return payment_mode_id;
            }

            public void setPayment_mode_id(String payment_mode_id) {
                this.payment_mode_id = payment_mode_id;
            }

            public String getPayment_mode_name() {
                return payment_mode_name;
            }

            public void setPayment_mode_name(String payment_mode_name) {
                this.payment_mode_name = payment_mode_name;
            }

            public String getPayment_mode_status() {
                return payment_mode_status;
            }

            public void setPayment_mode_status(String payment_mode_status) {
                this.payment_mode_status = payment_mode_status;
            }
        }

        public static class PaymentProcessorBean {
            /**
             * payment_processor_id : 1
             * payment_processor_name : Stripe
             * payment_mode_status : N
             * secret_key :
             * publishable_key :
             */

            private String payment_processor_id;
            private String payment_processor_name;
            private String payment_mode_status;
            private String secret_key;
            private String publishable_key;

            public String getPayment_processor_id() {
                return payment_processor_id;
            }

            public void setPayment_processor_id(String payment_processor_id) {
                this.payment_processor_id = payment_processor_id;
            }

            public String getPayment_processor_name() {
                return payment_processor_name;
            }

            public void setPayment_processor_name(String payment_processor_name) {
                this.payment_processor_name = payment_processor_name;
            }

            public String getPayment_mode_status() {
                return payment_mode_status;
            }

            public void setPayment_mode_status(String payment_mode_status) {
                this.payment_mode_status = payment_mode_status;
            }

            public String getSecret_key() {
                return secret_key;
            }

            public void setSecret_key(String secret_key) {
                this.secret_key = secret_key;
            }

            public String getPublishable_key() {
                return publishable_key;
            }

            public void setPublishable_key(String publishable_key) {
                this.publishable_key = publishable_key;
            }
        }

        public static class GiftCardsCouponsBean {
            /**
             * discount_offer_id : 2
             * discount_category : coupon
             * discount_code : NEWYEAR19
             * discount_type : percentage
             * discount_value : 2
             * discount_limit : 19
             */

            private String discount_offer_id;
            private String discount_category;
            private String discount_code;
            private String discount_type;
            private String discount_value;
            private String discount_status;
            private String discount_limit;

            public String getDiscount_status() {
                return discount_status;
            }

            public void setDiscount_status(String discount_status) {
                this.discount_status = discount_status;
            }

            public String getDiscount_offer_id() {
                return discount_offer_id;
            }

            public void setDiscount_offer_id(String discount_offer_id) {
                this.discount_offer_id = discount_offer_id;
            }

            public String getDiscount_category() {
                return discount_category;
            }

            public void setDiscount_category(String discount_category) {
                this.discount_category = discount_category;
            }

            public String getDiscount_code() {
                return discount_code;
            }

            public void setDiscount_code(String discount_code) {
                this.discount_code = discount_code;
            }

            public String getDiscount_type() {
                return discount_type;
            }

            public void setDiscount_type(String discount_type) {
                this.discount_type = discount_type;
            }

            public String getDiscount_value() {
                return discount_value;
            }

            public void setDiscount_value(String discount_value) {
                this.discount_value = discount_value;
            }

            public String getDiscount_limit() {
                return discount_limit;
            }

            public void setDiscount_limit(String discount_limit) {
                this.discount_limit = discount_limit;
            }
        }
    }
}
