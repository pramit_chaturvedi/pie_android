package com.mydevit.pieservicess.service;

import java.util.List;

public class BilllNow_Model {

    /**
     * status : success
     * status_code : 200
     * message : Payment Mode Show Successfully
     * data : [{"payment_mode_id":"1","payment_mode_name":"Cash","payment_mode_status":"Y","created_at":"2019-09-06 10:40:07","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"0","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":null,"processor_secret_key":null,"processor_publishable_key":null,"created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null},{"payment_mode_id":"3","payment_mode_name":"Check","payment_mode_status":"Y","created_at":"2019-09-06 10:40:07","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"0","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":null,"processor_secret_key":null,"processor_publishable_key":null,"created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null}]
     */

    private String status;
    private String status_code;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * payment_mode_id : 1
         * payment_mode_name : Cash
         * payment_mode_status : Y
         * created_at : 2019-09-06 10:40:07
         * payment_setting_id : 6
         * business_id : 13
         * location_id : 10
         * payment_tax_services : Y
         * payment_tax_service_value : 0
         * payment_tax_products : Y
         * payment_tax_product_value : 0
         * payment_processor_id : null
         * processor_secret_key : null
         * processor_publishable_key : null
         * created_by : 13
         * edited_by : 0
         * created_role_id : 0
         * edited_role_id : 0
         * updated_at : null
         */

        private String payment_mode_id;
        private String payment_mode_name;
        private String payment_mode_status;
        private String created_at;
        private String payment_setting_id;
        private String business_id;
        private String location_id;
        private String isChecked = "N";

        private String payment_tax_services;
        private String payment_tax_service_value;
        private String payment_tax_products;
        private String payment_tax_product_value;
        private String payment_processor_id;
        private String processor_secret_key;
        private String processor_publishable_key;
        private String created_by;
        private String edited_by;
        private String created_role_id;
        private String edited_role_id;
        private String updated_at;

        public String getIsChecked() {
            return isChecked;
        }

        public void setIsChecked(String isChecked) {
            this.isChecked = isChecked;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public String getPayment_processor_id() {
            return payment_processor_id;
        }

        public void setPayment_processor_id(String payment_processor_id) {
            this.payment_processor_id = payment_processor_id;
        }

        public void setProcessor_secret_key(String processor_secret_key) {
            this.processor_secret_key = processor_secret_key;
        }

        public String getProcessor_publishable_key() {
            return processor_publishable_key;
        }

        public void setProcessor_publishable_key(String processor_publishable_key) {
            this.processor_publishable_key = processor_publishable_key;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getPayment_mode_id() {
            return payment_mode_id;
        }

        public void setPayment_mode_id(String payment_mode_id) {
            this.payment_mode_id = payment_mode_id;
        }

        public String getPayment_mode_name() {
            return payment_mode_name;
        }

        public void setPayment_mode_name(String payment_mode_name) {
            this.payment_mode_name = payment_mode_name;
        }

        public String getPayment_mode_status() {
            return payment_mode_status;
        }

        public void setPayment_mode_status(String payment_mode_status) {
            this.payment_mode_status = payment_mode_status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getPayment_setting_id() {
            return payment_setting_id;
        }

        public void setPayment_setting_id(String payment_setting_id) {
            this.payment_setting_id = payment_setting_id;
        }

        public String getBusiness_id() {
            return business_id;
        }

        public void setBusiness_id(String business_id) {
            this.business_id = business_id;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getPayment_tax_services() {
            return payment_tax_services;
        }

        public void setPayment_tax_services(String payment_tax_services) {
            this.payment_tax_services = payment_tax_services;
        }

        public String getPayment_tax_service_value() {
            return payment_tax_service_value;
        }

        public void setPayment_tax_service_value(String payment_tax_service_value) {
            this.payment_tax_service_value = payment_tax_service_value;
        }

        public String getPayment_tax_products() {
            return payment_tax_products;
        }

        public void setPayment_tax_products(String payment_tax_products) {
            this.payment_tax_products = payment_tax_products;
        }

        public String getPayment_tax_product_value() {
            return payment_tax_product_value;
        }

        public void setPayment_tax_product_value(String payment_tax_product_value) {
            this.payment_tax_product_value = payment_tax_product_value;
        }


        public Object getProcessor_secret_key() {
            return processor_secret_key;
        }





        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public String getEdited_by() {
            return edited_by;
        }

        public void setEdited_by(String edited_by) {
            this.edited_by = edited_by;
        }

        public String getCreated_role_id() {
            return created_role_id;
        }

        public void setCreated_role_id(String created_role_id) {
            this.created_role_id = created_role_id;
        }

        public String getEdited_role_id() {
            return edited_role_id;
        }

        public void setEdited_role_id(String edited_role_id) {
            this.edited_role_id = edited_role_id;
        }


    }
}
