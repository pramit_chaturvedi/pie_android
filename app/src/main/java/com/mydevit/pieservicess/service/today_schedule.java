package com.mydevit.pieservicess.service;

import java.util.List;

public class today_schedule {


    /**
     * status : success
     * status_code : 200
     * message : Business Today Schedule Show Successfully
     * data : [{"location_id":"17","location_name":"Charles","personnel_timings":[{"personnel_name":"Damien","personnel_color":"#000000","timing":"09:00 AM - 05:00 PM","is_off":"N"},{"personnel_name":"Jevel Santhana","personnel_color":"#b45c5c","timing":"09:00 AM - 05:00 PM","is_off":"N"},{"personnel_name":"test","personnel_color":"#481313","timing":"09:00 AM - 05:00 PM","is_off":"N"}]},{"location_id":"16","location_name":"Model Style","personnel_timings":[{"personnel_name":"test","personnel_color":"#481313","timing":"08:00 AM - 09:00 PM","is_off":"N"}]},{"location_id":"27","location_name":"New Tempa","personnel_timings":[{"personnel_name":"Damien","personnel_color":"#000000","timing":"09:00 AM - 05:00 PM","is_off":"N"},{"personnel_name":"Jevel Santhana","personnel_color":"#b45c5c","timing":"09:00 AM - 05:00 PM","is_off":"N"},{"personnel_name":"test","personnel_color":"#481313","timing":"09:00 AM - 05:00 PM","is_off":"N"}]},{"location_id":"10","location_name":"Style Location","personnel_timings":[{"personnel_name":"Damien","personnel_color":"#000000","timing":"09:00 AM - 05:00 PM","is_off":"N"},{"personnel_name":"Jevel Santhana","personnel_color":"#b45c5c","timing":"09:00 AM - 05:00 PM","is_off":"N"},{"personnel_name":"test","personnel_color":"#481313","timing":"09:00 AM - 05:00 PM","is_off":"N"}]}]
     */

    private String status;
    private int status_code;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatus_code() {
        return status_code;
    }

    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        public List<PersonnelTimingsBean> personnel_timings;
        /**
         * location_id : 17
         * location_name : Charles
         * personnel_timings : [{"personnel_name":"Damien","personnel_color":"#000000","timing":"09:00 AM - 05:00 PM","is_off":"N"},{"personnel_name":"Jevel Santhana","personnel_color":"#b45c5c","timing":"09:00 AM - 05:00 PM","is_off":"N"},{"personnel_name":"test","personnel_color":"#481313","timing":"09:00 AM - 05:00 PM","is_off":"N"}]
         */

        private String location_id;
        private String location_name;

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getLocation_name() {
            return location_name;
        }

        public void setLocation_name(String location_name) {
            this.location_name = location_name;
        }

        public List<PersonnelTimingsBean> getPersonnel_timings() {
            return personnel_timings;
        }

        public void setPersonnel_timings(List<PersonnelTimingsBean> personnel_timings) {
            this.personnel_timings = personnel_timings;
        }

        public static class PersonnelTimingsBean {
            /**
             * personnel_name : Damien
             * personnel_color : #000000
             * timing : 09:00 AM - 05:00 PM
             * is_off : N
             */

            private String personnel_id;
            private String personnel_name;
            private String personnel_color;
            private String timing;
            private String is_off;

            public String getPersonnel_id() {
                return personnel_id;
            }

            public void setPersonnel_id(String personnel_id) {
                this.personnel_id = personnel_id;
            }

            public String getPersonnel_name() {
                return personnel_name;
            }

            public void setPersonnel_name(String personnel_name) {
                this.personnel_name = personnel_name;
            }

            public String getPersonnel_color() {
                return personnel_color;
            }

            public void setPersonnel_color(String personnel_color) {
                this.personnel_color = personnel_color;
            }

            public String getTiming() {
                return timing;
            }

            public void setTiming(String timing) {
                this.timing = timing;
            }

            public String getIs_off() {
                return is_off;
            }

            public void setIs_off(String is_off) {
                this.is_off = is_off;
            }
        }
    }
}
