package com.mydevit.pieservicess.service;

import java.util.List;

public class Data_cart_model {
    /**
     * status : success
     * status_code : 200
     * message : Cart data show successfully
     * data : {"customer_detail":{"customer_id":"21","first_name":"Borris","last_name":"Gill","phone":"7845123659"},"cart_item_detail":[{"cart_item_id":"1","item_id":"2","item_name":"Hair cut","item_code":"HAI#2","item_qty":"1","item_price":"50.00","item_type":"S","quantity":"2"},{"cart_item_id":"2","item_id":"3","item_name":"Spa","item_code":"SPA#3","item_qty":"1","item_price":"800.00","item_type":"S","quantity":"1"}],"cart_detail":{"cart_id":"1","order_number":"#MATO64125","customer_id":"21","appointment_id":"2, 3","discount_type":null,"discount_percent":null,"discount_amount":"0.00","tax_percent":"0","tax_amount":"0.00","tip_amount":null,"sub_total":"900.00","total":"900.00"}}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * customer_detail : {"customer_id":"21","first_name":"Borris","last_name":"Gill","phone":"7845123659"}
         * cart_item_detail : [{"cart_item_id":"1","item_id":"2","item_name":"Hair cut","item_code":"HAI#2","item_qty":"1","item_price":"50.00","item_type":"S","quantity":"2"},{"cart_item_id":"2","item_id":"3","item_name":"Spa","item_code":"SPA#3","item_qty":"1","item_price":"800.00","item_type":"S","quantity":"1"}]
         * cart_detail : {"cart_id":"1","order_number":"#MATO64125","customer_id":"21","appointment_id":"2, 3","discount_type":null,"discount_percent":null,"discount_amount":"0.00","tax_percent":"0","tax_amount":"0.00","tip_amount":null,"sub_total":"900.00","total":"900.00"}
         */

        private DataBean.CustomerDetailBean customer_detail;
        private DataBean.CartDetailBean cart_detail;
        private List<DataBean.CartItemDetailBean> cart_item_detail;

        public DataBean.CustomerDetailBean getCustomer_detail() {
            return customer_detail;
        }

        public void setCustomer_detail(DataBean.CustomerDetailBean customer_detail) {
            this.customer_detail = customer_detail;
        }

        public DataBean.CartDetailBean getCart_detail() {
            return cart_detail;
        }

        public void setCart_detail(DataBean.CartDetailBean cart_detail) {
            this.cart_detail = cart_detail;
        }

        public List<DataBean.CartItemDetailBean> getCart_item_detail() {
            return cart_item_detail;
        }

        public void setCart_item_detail(List<DataBean.CartItemDetailBean> cart_item_detail) {
            this.cart_item_detail = cart_item_detail;
        }

        public static class CustomerDetailBean {
            /**
             * customer_id : 21
             * first_name : Borris
             * last_name : Gill
             * phone : 7845123659
             */

            private String customer_id;
            private String first_name;
            private String last_name;
            private String phone;

            public String getCustomer_id() {
                return customer_id;
            }

            public void setCustomer_id(String customer_id) {
                this.customer_id = customer_id;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }
        }

        public static class CartDetailBean {
            /**
             * cart_id : 1
             * order_number : #MATO64125
             * customer_id : 21
             * appointment_id : 2, 3
             * discount_type : null
             * discount_percent : null
             * discount_amount : 0.00
             * tax_percent : 0
             * tax_amount : 0.00
             * tip_amount : null
             * sub_total : 900.00
             * total : 900.00
             */

            private String cart_id;
            private String order_number;
            private String customer_id;
            private String appointment_id;
            private Object discount_type;
            private Object discount_percent;
            private String discount_amount;
            private String tax_percent;
            private String tax_amount;
            private Object tip_amount;
            private String sub_total;
            private String total;

            public String getCart_id() {
                return cart_id;
            }

            public void setCart_id(String cart_id) {
                this.cart_id = cart_id;
            }

            public String getOrder_number() {
                return order_number;
            }

            public void setOrder_number(String order_number) {
                this.order_number = order_number;
            }

            public String getCustomer_id() {
                return customer_id;
            }

            public void setCustomer_id(String customer_id) {
                this.customer_id = customer_id;
            }

            public String getAppointment_id() {
                return appointment_id;
            }

            public void setAppointment_id(String appointment_id) {
                this.appointment_id = appointment_id;
            }

            public Object getDiscount_type() {
                return discount_type;
            }

            public void setDiscount_type(Object discount_type) {
                this.discount_type = discount_type;
            }

            public Object getDiscount_percent() {
                return discount_percent;
            }

            public void setDiscount_percent(Object discount_percent) {
                this.discount_percent = discount_percent;
            }

            public String getDiscount_amount() {
                return discount_amount;
            }

            public void setDiscount_amount(String discount_amount) {
                this.discount_amount = discount_amount;
            }

            public String getTax_percent() {
                return tax_percent;
            }

            public void setTax_percent(String tax_percent) {
                this.tax_percent = tax_percent;
            }

            public String getTax_amount() {
                return tax_amount;
            }

            public void setTax_amount(String tax_amount) {
                this.tax_amount = tax_amount;
            }

            public Object getTip_amount() {
                return tip_amount;
            }

            public void setTip_amount(Object tip_amount) {
                this.tip_amount = tip_amount;
            }

            public String getSub_total() {
                return sub_total;
            }

            public void setSub_total(String sub_total) {
                this.sub_total = sub_total;
            }

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }
        }

        public static class CartItemDetailBean {
            /**
             * cart_item_id : 1
             * item_id : 2
             * item_name : Hair cut
             * item_code : HAI#2
             * item_qty : 1
             * item_price : 50.00
             * item_type : S
             * quantity : 2
             */

            private String cart_item_id;
            private String item_id;
            private String item_name;
            private String item_price_id;



            private String item_code;
            private String item_qty;
            private String item_price;
            private String item_type;
            private String quantity;

            public String getItem_price_id() {
                return item_price_id;
            }

            public void setItem_price_id(String item_price_id) {
                this.item_price_id = item_price_id;
            }

            public String getCart_item_id() {
                return cart_item_id;
            }

            public void setCart_item_id(String cart_item_id) {
                this.cart_item_id = cart_item_id;
            }

            public String getItem_id() {
                return item_id;
            }

            public void setItem_id(String item_id) {
                this.item_id = item_id;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public String getItem_code() {
                return item_code;
            }

            public void setItem_code(String item_code) {
                this.item_code = item_code;
            }

            public String getItem_qty() {
                return item_qty;
            }

            public void setItem_qty(String item_qty) {
                this.item_qty = item_qty;
            }

            public String getItem_price() {
                return item_price;
            }

            public void setItem_price(String item_price) {
                this.item_price = item_price;
            }

            public String getItem_type() {
                return item_type;
            }

            public void setItem_type(String item_type) {
                this.item_type = item_type;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }
        }
    }
}
