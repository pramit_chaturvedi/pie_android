package com.mydevit.pieservicess.service;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Save_Persionnel_Schedule_ServiceModel {

    /**
     * status : success
     * status_code : 200
     * message : Schedule successfully added for all personnels
     * data : {"personnel_ids":[7],"business_id":"13","location_id":"27","business_days":{"1":"Mon","2":"Tue","3":"Wed","4":"Thur","5":"Fri","6":"Sat","7":"Sun"},"business_offs":{"1":"N","2":"N","3":"N","4":"N","5":"N","6":"N","7":"Y"},"all_week_time_inn":null,"all_week_time_out":"8:00 AM","week_start_date":"2020-01-27","week_end_date":"2020-02-02","week_number":"5"}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * personnel_ids : [7]
         * business_id : 13
         * location_id : 27
         * business_days : {"1":"Mon","2":"Tue","3":"Wed","4":"Thur","5":"Fri","6":"Sat","7":"Sun"}
         * business_offs : {"1":"N","2":"N","3":"N","4":"N","5":"N","6":"N","7":"Y"}
         * all_week_time_inn : null
         * all_week_time_out : 8:00 AM
         * week_start_date : 2020-01-27
         * week_end_date : 2020-02-02
         * week_number : 5
         */

        private String business_id;
        private String location_id;
        private BusinessDaysBean business_days;
        private BusinessOffsBean business_offs;
        private String all_week_time_inn;
        private String all_week_time_out;
        private String week_start_date;
        private String week_end_date;
        private String week_number;
        private List<Integer> personnel_ids;

        public String getBusiness_id() {
            return business_id;
        }

        public void setBusiness_id(String business_id) {
            this.business_id = business_id;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public BusinessDaysBean getBusiness_days() {
            return business_days;
        }

        public void setBusiness_days(BusinessDaysBean business_days) {
            this.business_days = business_days;
        }

        public BusinessOffsBean getBusiness_offs() {
            return business_offs;
        }

        public void setBusiness_offs(BusinessOffsBean business_offs) {
            this.business_offs = business_offs;
        }

        public String getAll_week_time_inn() {
            return all_week_time_inn;
        }

        public void setAll_week_time_inn(String all_week_time_inn) {
            this.all_week_time_inn = all_week_time_inn;
        }

        public String getAll_week_time_out() {
            return all_week_time_out;
        }

        public void setAll_week_time_out(String all_week_time_out) {
            this.all_week_time_out = all_week_time_out;
        }

        public String getWeek_start_date() {
            return week_start_date;
        }

        public void setWeek_start_date(String week_start_date) {
            this.week_start_date = week_start_date;
        }

        public String getWeek_end_date() {
            return week_end_date;
        }

        public void setWeek_end_date(String week_end_date) {
            this.week_end_date = week_end_date;
        }

        public String getWeek_number() {
            return week_number;
        }

        public void setWeek_number(String week_number) {
            this.week_number = week_number;
        }

        public List<Integer> getPersonnel_ids() {
            return personnel_ids;
        }

        public void setPersonnel_ids(List<Integer> personnel_ids) {
            this.personnel_ids = personnel_ids;
        }

        public static class BusinessDaysBean {
            /**
             * 1 : Mon
             * 2 : Tue
             * 3 : Wed
             * 4 : Thur
             * 5 : Fri
             * 6 : Sat
             * 7 : Sun
             */

            @SerializedName("1")
            private String _$1;
            @SerializedName("2")
            private String _$2;
            @SerializedName("3")
            private String _$3;
            @SerializedName("4")
            private String _$4;
            @SerializedName("5")
            private String _$5;
            @SerializedName("6")
            private String _$6;
            @SerializedName("7")
            private String _$7;

            public String get_$1() {
                return _$1;
            }

            public void set_$1(String _$1) {
                this._$1 = _$1;
            }

            public String get_$2() {
                return _$2;
            }

            public void set_$2(String _$2) {
                this._$2 = _$2;
            }

            public String get_$3() {
                return _$3;
            }

            public void set_$3(String _$3) {
                this._$3 = _$3;
            }

            public String get_$4() {
                return _$4;
            }

            public void set_$4(String _$4) {
                this._$4 = _$4;
            }

            public String get_$5() {
                return _$5;
            }

            public void set_$5(String _$5) {
                this._$5 = _$5;
            }

            public String get_$6() {
                return _$6;
            }

            public void set_$6(String _$6) {
                this._$6 = _$6;
            }

            public String get_$7() {
                return _$7;
            }

            public void set_$7(String _$7) {
                this._$7 = _$7;
            }
        }

        public static class BusinessOffsBean {
            /**
             * 1 : N
             * 2 : N
             * 3 : N
             * 4 : N
             * 5 : N
             * 6 : N
             * 7 : Y
             */

            @SerializedName("1")
            private String _$1;
            @SerializedName("2")
            private String _$2;
            @SerializedName("3")
            private String _$3;
            @SerializedName("4")
            private String _$4;
            @SerializedName("5")
            private String _$5;
            @SerializedName("6")
            private String _$6;
            @SerializedName("7")
            private String _$7;

            public String get_$1() {
                return _$1;
            }

            public void set_$1(String _$1) {
                this._$1 = _$1;
            }

            public String get_$2() {
                return _$2;
            }

            public void set_$2(String _$2) {
                this._$2 = _$2;
            }

            public String get_$3() {
                return _$3;
            }

            public void set_$3(String _$3) {
                this._$3 = _$3;
            }

            public String get_$4() {
                return _$4;
            }

            public void set_$4(String _$4) {
                this._$4 = _$4;
            }

            public String get_$5() {
                return _$5;
            }

            public void set_$5(String _$5) {
                this._$5 = _$5;
            }

            public String get_$6() {
                return _$6;
            }

            public void set_$6(String _$6) {
                this._$6 = _$6;
            }

            public String get_$7() {
                return _$7;
            }

            public void set_$7(String _$7) {
                this._$7 = _$7;
            }
        }
    }
}
