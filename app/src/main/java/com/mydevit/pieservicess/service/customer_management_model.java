package com.mydevit.pieservicess.service;

import java.io.Serializable;
import java.util.List;

public class customer_management_model {


    /**
     * status : success
     * status_code : 200
     * message : Customer list Show Successfully
     * data : [{"customer_id":"22","first_name":"Annie","last_name":"Johnson","phone":"4654475432","mob_verified":"Y","email":"annie@hotmail.com","email_verified":"Y","address":"Jaipur","latitude":null,"longitude":null,"city":"Jaipur","state":"Rajasthan ","country":"","zipcode":"12345","instagram_user":"","twitter_user":"","facebook_user":"","status":"0"},{"customer_id":"24","first_name":"Davrer","last_name":"True","phone":"7989445466","mob_verified":"N","email":"davrer@gmail.com","email_verified":"N","address":null,"latitude":null,"longitude":null,"city":null,"state":null,"country":null,"zipcode":null,"instagram_user":"","twitter_user":"","facebook_user":"","status":"1"},{"customer_id":"26","first_name":"","last_name":"","phone":"","mob_verified":"N","email":"","email_verified":"N","address":null,"latitude":null,"longitude":null,"city":null,"state":null,"country":null,"zipcode":null,"instagram_user":"","twitter_user":"","facebook_user":"","status":"1"},{"customer_id":"27","first_name":"test","last_name":"test","phone":"8523698521","mob_verified":"N","email":"test@mailinatpr.com","email_verified":"N","address":"","latitude":"0.00000000","longitude":"0.00000000","city":"","state":"","country":"","zipcode":"","instagram_user":"test12","twitter_user":"","facebook_user":"test12","status":"0"},{"customer_id":"39","first_name":"test12","last_name":"test","phone":"8523698521","mob_verified":"N","email":"test12@mailinator.com","email_verified":"N","address":null,"latitude":null,"longitude":null,"city":null,"state":null,"country":null,"zipcode":null,"instagram_user":"","twitter_user":"","facebook_user":"","status":"1"},{"customer_id":"40","first_name":"t","last_name":"t","phone":"4178523698","mob_verified":"N","email":"test12@gmail.com","email_verified":"N","address":null,"latitude":null,"longitude":null,"city":null,"state":null,"country":null,"zipcode":null,"instagram_user":"","twitter_user":"","facebook_user":"","status":"1"}]
     */

    private String status;
    private String status_code;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * customer_id : 22
         * first_name : Annie
         * last_name : Johnson
         * phone : 4654475432
         * mob_verified : Y
         * email : annie@hotmail.com
         * email_verified : Y
         * address : Jaipur
         * latitude : null
         * longitude : null
         * city : Jaipur
         * state : Rajasthan
         * country :
         * zipcode : 12345
         * instagram_user :
         * twitter_user :
         * facebook_user :
         * status : 0
         */

        private String customer_id;
        private String first_name;
        private String last_name;
        private String phone;
        private String mob_verified;
        private String email;
        private String email_verified;
        private String address;
        private String latitude;
        private String longitude;
        private String city;
        private String state;
        private String country;
        private String password;
        private String zipcode;
        private String instagram_user;
        private String twitter_user;
        private String facebook_user;
        private String status = "";

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getCustomer_id() {
            return customer_id;
        }

        public void setCustomer_id(String customer_id) {
            this.customer_id = customer_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getMob_verified() {
            return mob_verified;
        }

        public void setMob_verified(String mob_verified) {
            this.mob_verified = mob_verified;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getEmail_verified() {
            return email_verified;
        }

        public void setEmail_verified(String email_verified) {
            this.email_verified = email_verified;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }



        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getInstagram_user() {
            return instagram_user;
        }

        public void setInstagram_user(String instagram_user) {
            this.instagram_user = instagram_user;
        }

        public String getTwitter_user() {
            return twitter_user;
        }

        public void setTwitter_user(String twitter_user) {
            this.twitter_user = twitter_user;
        }

        public String getFacebook_user() {
            return facebook_user;
        }

        public void setFacebook_user(String facebook_user) {
            this.facebook_user = facebook_user;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
