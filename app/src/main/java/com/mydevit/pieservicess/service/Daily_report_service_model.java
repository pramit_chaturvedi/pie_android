package com.mydevit.pieservicess.service;

import java.util.List;

public class Daily_report_service_model {

    /**
     * status : success
     * status_code : 200
     * message : POS Report Show Successfully
     * data : {"Order_Data":[{"Order_id":"45","Order_number":"#MATO35025","Client_Name":"Leonardo Marchetti","Phone_Number":"6472221787","Bill_Date_Time":"2019-10-22","Amount_Paid":21,"Pay_Mode":"Cash"}],"Total_Sale":21}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * Order_Data : [{"Order_id":"45","Order_number":"#MATO35025","Client_Name":"Leonardo Marchetti","Phone_Number":"6472221787","Bill_Date_Time":"2019-10-22","Amount_Paid":21,"Pay_Mode":"Cash"}]
         * Total_Sale : 21
         */

        private String Total_Sale;
        private String Total_Cash;
        private List<OrderDataBean> Order_Data;

        public String getTotal_Cash() {
            return Total_Cash;
        }

        public void setTotal_Cash(String total_Cash) {
            Total_Cash = total_Cash;
        }

        public String getTotal_Sale() {
            return Total_Sale;
        }

        public void setTotal_Sale(String Total_Sale) {
            this.Total_Sale = Total_Sale;
        }

        public List<OrderDataBean> getOrder_Data() {
            return Order_Data;
        }

        public void setOrder_Data(List<OrderDataBean> Order_Data) {
            this.Order_Data = Order_Data;
        }

        public static class OrderDataBean {
            /**
             * Order_id : 45
             * Order_number : #MATO35025
             * Client_Name : Leonardo Marchetti
             * Phone_Number : 6472221787
             * Bill_Date_Time : 2019-10-22
             * Amount_Paid : 21
             * Pay_Mode : Cash
             */

            private String Order_id;
            private String Order_number;
            private String Client_Name;
            private String Phone_Number;
            private String Bill_Date_Time;
            private String Amount_Paid;
            private String Pay_Mode;


            public String getAmount_Paid() {
                return Amount_Paid;
            }

            public void setAmount_Paid(String amount_Paid) {
                Amount_Paid = amount_Paid;
            }

            public String getOrder_id() {
                return Order_id;
            }

            public void setOrder_id(String Order_id) {
                this.Order_id = Order_id;
            }

            public String getOrder_number() {
                return Order_number;
            }

            public void setOrder_number(String Order_number) {
                this.Order_number = Order_number;
            }

            public String getClient_Name() {
                return Client_Name;
            }

            public void setClient_Name(String Client_Name) {
                this.Client_Name = Client_Name;
            }

            public String getPhone_Number() {
                return Phone_Number;
            }

            public void setPhone_Number(String Phone_Number) {
                this.Phone_Number = Phone_Number;
            }

            public String getBill_Date_Time() {
                return Bill_Date_Time;
            }

            public void setBill_Date_Time(String Bill_Date_Time) {
                this.Bill_Date_Time = Bill_Date_Time;
            }


            public String getPay_Mode() {
                return Pay_Mode;
            }

            public void setPay_Mode(String Pay_Mode) {
                this.Pay_Mode = Pay_Mode;
            }
        }
    }
}
