package com.mydevit.pieservicess.service;

public class NotificationTabOneModel {

    /**
     * status : success
     * status_code : 200
     * message : Appointment Notification Configured Updated Successfully
     * data : {"business_id":"13","location_id":"10","email_trigger":"5","email_from":"gmghnhmn","email_subject":"ghnghj","email_reply":"ghjghjk","email_status":"Y","updated_at":"2019-12-02 04:54:37"}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;


    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * business_id : 13
         * location_id : 10
         * email_trigger : 5
         * email_from : gmghnhmn
         * email_subject : ghnghj
         * email_reply : ghjghjk
         * email_status : Y
         * updated_at : 2019-12-02 04:54:37
         */

        private String business_id;
        private String location_id;
        private String email_trigger;
        private String email_from;
        private String email_subject;
        private String email_reply;
        private String email_status;
        private String updated_at;

        public String getBusiness_id() {
            return business_id;
        }

        public void setBusiness_id(String business_id) {
            this.business_id = business_id;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getEmail_trigger() {
            return email_trigger;
        }

        public void setEmail_trigger(String email_trigger) {
            this.email_trigger = email_trigger;
        }

        public String getEmail_from() {
            return email_from;
        }

        public void setEmail_from(String email_from) {
            this.email_from = email_from;
        }

        public String getEmail_subject() {
            return email_subject;
        }

        public void setEmail_subject(String email_subject) {
            this.email_subject = email_subject;
        }

        public String getEmail_reply() {
            return email_reply;
        }

        public void setEmail_reply(String email_reply) {
            this.email_reply = email_reply;
        }

        public String getEmail_status() {
            return email_status;
        }

        public void setEmail_status(String email_status) {
            this.email_status = email_status;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
