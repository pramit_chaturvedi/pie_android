package com.mydevit.pieservicess.service;

import java.util.List;

public class ClientAppointment_ServiceModel {

    /**
     * status : success
     * status_code : 200
     * message : Customer Appointment Data Show Successfully
     * data : [{"appointment_number":"#MATB10787","personnel_id":"22","personnel_name":"test","customer_name":"AnnieupdatedNow0000 Johnsonbonenow0000","appointment_date":"2019-11-06","appointment_duration":"0","appointment_notes":""},{"appointment_number":"#MATB45017","personnel_id":"14","personnel_name":"Damien","customer_name":"AnnieupdatedNow0000 Johnsonbonenow0000","appointment_date":"2019-11-25","appointment_duration":"0","appointment_notes":"Test"},{"appointment_number":"#MATB10004","personnel_id":"22","personnel_name":"test","customer_name":"AnnieupdatedNow0000 Johnsonbonenow0000","appointment_date":"2019-11-28","appointment_duration":"0","appointment_notes":""}]
     */

    private String status;
    private String status_code;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * appointment_number : #MATB10787
         * personnel_id : 22
         * personnel_name : test
         * customer_name : AnnieupdatedNow0000 Johnsonbonenow0000
         * appointment_date : 2019-11-06
         * appointment_duration : 0
         * appointment_notes :
         */

        private String appointment_number;
        private String personnel_id;
        private String personnel_name;
        private String customer_name;
        private String appointment_date;
        private String appointment_duration;
        private String appointment_notes;

        public String getAppointment_number() {
            return appointment_number;
        }

        public void setAppointment_number(String appointment_number) {
            this.appointment_number = appointment_number;
        }

        public String getPersonnel_id() {
            return personnel_id;
        }

        public void setPersonnel_id(String personnel_id) {
            this.personnel_id = personnel_id;
        }

        public String getPersonnel_name() {
            return personnel_name;
        }

        public void setPersonnel_name(String personnel_name) {
            this.personnel_name = personnel_name;
        }

        public String getCustomer_name() {
            return customer_name;
        }

        public void setCustomer_name(String customer_name) {
            this.customer_name = customer_name;
        }

        public String getAppointment_date() {
            return appointment_date;
        }

        public void setAppointment_date(String appointment_date) {
            this.appointment_date = appointment_date;
        }

        public String getAppointment_duration() {
            return appointment_duration;
        }

        public void setAppointment_duration(String appointment_duration) {
            this.appointment_duration = appointment_duration;
        }

        public String getAppointment_notes() {
            return appointment_notes;
        }

        public void setAppointment_notes(String appointment_notes) {
            this.appointment_notes = appointment_notes;
        }
    }
}
