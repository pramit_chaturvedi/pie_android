package com.mydevit.pieservicess.service;

public class signin_model {

    /**
     * status : success
     * status_code : 200
     * message : Business logged in successfully
     * data : {"token":"eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9.eyJjb25zdW1lcktleSI6InBpZWJvb2tpbmcjMjAxOCIsInVzZXJJZCI6IjEzIiwiaXNzdWVkQXQiOiIyMDE5LTEwLTA4VDA1OjA0OjM4KzAwMDAiLCJ0dGwiOjYzMDcyMDAwfQ.RVQiff_6RFCOxJ5hpFjVPaBJiIV1v--xDh_jGzAo80M","business_name":"Matrix Styles edit","first_name":"Davis","last_name":"Morgan","email":"davis@hotmail.com","mobile_no":"(541) 754-3010","role_id":"1"}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * token : eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9.eyJjb25zdW1lcktleSI6InBpZWJvb2tpbmcjMjAxOCIsInVzZXJJZCI6IjEzIiwiaXNzdWVkQXQiOiIyMDE5LTEwLTA4VDA1OjA0OjM4KzAwMDAiLCJ0dGwiOjYzMDcyMDAwfQ.RVQiff_6RFCOxJ5hpFjVPaBJiIV1v--xDh_jGzAo80M
         * business_name : Matrix Styles edit
         * first_name : Davis
         * last_name : Morgan
         * email : davis@hotmail.com
         * mobile_no : (541) 754-3010
         * role_id : 1
         */

        private String token;
        private String business_name;
        private String first_name;
        private String last_name;
        private String email;
        private String mobile_no;
        private String role_id;
        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getBusiness_name() {
            return business_name;
        }

        public void setBusiness_name(String business_name) {
            this.business_name = business_name;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile_no() {
            return mobile_no;
        }

        public void setMobile_no(String mobile_no) {
            this.mobile_no = mobile_no;
        }

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }
    }
}
