package com.mydevit.pieservicess.service;

import java.util.List;

public class Product_List_Model {


    /**
     * status : success
     * status_code : 200
     * message : Product Show Successfully
     * data : {"cart_data":{"cart_id":"128","order_number":"#MATO39491","business_id":"13","location_id":"10","personnel_id":null,"customer_id":"61","appointment_id":null,"items_id":null,"discount_type":null,"discount_percent":null,"discount_amount":"0.00","tax_percent":"5","tax_amount":"20.00","tip_amount":null,"sub_total":"400.00","total":"420.00","coupon_code":"","payment_mode":"","order_date":"2019-12-24","created_by":"13","edited_by":"13","created_at":"2019-12-24 00:00:00","updated_at":"2019-12-24 05:39:50"},"paymode_options":[{"payment_mode_id":"1","payment_mode_name":"Cash","payment_mode_status":"Y","created_at":"2019-12-17 11:32:15","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"5","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":"1","processor_secret_key":"","processor_publishable_key":"","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null},{"payment_mode_id":"2","payment_mode_name":"Credit Card","payment_mode_status":"Y","created_at":"2019-12-17 11:32:15","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"5","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":"1","processor_secret_key":"","processor_publishable_key":"","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null}],"customers":[{"id":"39","label":"test12 test"},{"id":"61","label":"Jerry Furgu"},{"id":"62","label":"Donal Veez"},{"id":"63","label":"David Johnson"},{"id":"64","label":"Venture Zoris"},{"id":"65","label":"Wesley Poul"},{"id":"66","label":"Lowis Kohen"},{"id":"67","label":"ffghtf threher"},{"id":"68","label":"Zulia Casina"},{"id":"69","label":"testing testuser"},{"id":"72","label":"test45 test45"},{"id":"73","label":"dummy test"},{"id":"74","label":"dumy1 test"},{"id":"75","label":"dummy3 test"},{"id":"76","label":"dummy4 test"},{"id":"77","label":"test10 test"},{"id":"78","label":"gyhg yyh"},{"id":"79","label":"rrrrr oooo"},{"id":"80","label":"jaglast oo"}],"cart_items":[{"cart_item_id":"343","item_id":"53","item_price_id":"32","item_name":"test","item_code":"TES#53","item_qty":"1","item_price":"200.00","item_type":"I","quantity":"2"}],"location_items_category":[{"category_id":"7","category_name":"Gels","category_icon":"5597c78389c8e9846dd5140cce5f2188.jpg","business_id":"13","category_status":"1","created_at":"2019-05-24 07:37:39","updated_at":"0000-00-00 00:00:00"},{"category_id":"13","category_name":"testing","category_icon":"04aa26901f69ae30970f349bd04061fb.png","business_id":"13","category_status":"1","created_at":"2019-12-23 09:06:56","updated_at":"0000-00-00 00:00:00"}],"location_items":[{"item_id":"53","business_id":"13","item_name":"test","item_category_id":"7","item_category_name":"Gels","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/7ca047488438e7173f169685cb65fc6a.jpg","price_variations":[{"item_price_id":"32","item_variation_id":"1","varition_name":"Size","item_option_id":"1","variation_option_name":"Small","item_price":"200.00","item_stock":"63"},{"item_price_id":"33","item_variation_id":"1","varition_name":"Size","item_option_id":"2","variation_option_name":"Large","item_price":"270.00","item_stock":"60"}]},{"item_id":"54","business_id":"13","item_name":"test item","item_category_id":"7","item_category_name":"Gels","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/60a1b2d7309b6078d6f3cc9f2b0ff132.jpg","price_variations":[{"item_price_id":"34","item_variation_id":"1","varition_name":"Size","item_option_id":"1","variation_option_name":"Small","item_price":"280.00","item_stock":"19"},{"item_price_id":"35","item_variation_id":"1","varition_name":"Size","item_option_id":"2","variation_option_name":"Large","item_price":"240.00","item_stock":"55"}]},{"item_id":"56","business_id":"13","item_name":"test item","item_category_id":"7","item_category_name":"Gels","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/98222b70c5919af61c7b848538ab4382.jpg","price_variations":[{"item_price_id":"39","item_variation_id":"2","varition_name":"Color","item_option_id":"3","variation_option_name":"Yellow","item_price":"100.00","item_stock":"4"},{"item_price_id":"40","item_variation_id":"2","varition_name":"Color","item_option_id":"5","variation_option_name":"Green","item_price":"200.00","item_stock":"10"}]},{"item_id":"57","business_id":"13","item_name":"test item","item_category_id":"7","item_category_name":"Gels","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/d72032e53adf0cb24f81f7a1fb6ec57d.jpg","price_variations":[{"item_price_id":"46","item_variation_id":"1","varition_name":"Size","item_option_id":"1","variation_option_name":"Small","item_price":"400.00","item_stock":"45"}]},{"item_id":"58","business_id":"13","item_name":"test","item_category_id":"7","item_category_name":"Gels","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/2877c3e5cc1e6273034d950ab7ced71a.jpg","price_variations":[{"item_price_id":"47","item_variation_id":"4","varition_name":"No variation","item_option_id":null,"variation_option_name":"","item_price":"120.00","item_stock":"20"}]},{"item_id":"61","business_id":"13","item_name":"testing","item_category_id":"7","item_category_name":"Gels","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/2b7fa761013c9ca02bcabf4e50dc6097.jpg","price_variations":[{"item_price_id":"51","item_variation_id":null,"varition_name":"No variation","item_option_id":null,"variation_option_name":"","item_price":"100.00","item_stock":"5"}]},{"item_id":"62","business_id":"13","item_name":"test item","item_category_id":"13","item_category_name":"testing","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/e34436e2b3af4d857de5e4eedf9a35dc.jpg","price_variations":[{"item_price_id":"52","item_variation_id":null,"varition_name":"No variation","item_option_id":null,"variation_option_name":"","item_price":"200.00","item_stock":"10"}]}]}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * cart_data : {"cart_id":"128","order_number":"#MATO39491","business_id":"13","location_id":"10","personnel_id":null,"customer_id":"61","appointment_id":null,"items_id":null,"discount_type":null,"discount_percent":null,"discount_amount":"0.00","tax_percent":"5","tax_amount":"20.00","tip_amount":null,"sub_total":"400.00","total":"420.00","coupon_code":"","payment_mode":"","order_date":"2019-12-24","created_by":"13","edited_by":"13","created_at":"2019-12-24 00:00:00","updated_at":"2019-12-24 05:39:50"}
         * paymode_options : [{"payment_mode_id":"1","payment_mode_name":"Cash","payment_mode_status":"Y","created_at":"2019-12-17 11:32:15","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"5","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":"1","processor_secret_key":"","processor_publishable_key":"","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null},{"payment_mode_id":"2","payment_mode_name":"Credit Card","payment_mode_status":"Y","created_at":"2019-12-17 11:32:15","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"5","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":"1","processor_secret_key":"","processor_publishable_key":"","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null}]
         * customers : [{"id":"39","label":"test12 test"},{"id":"61","label":"Jerry Furgu"},{"id":"62","label":"Donal Veez"},{"id":"63","label":"David Johnson"},{"id":"64","label":"Venture Zoris"},{"id":"65","label":"Wesley Poul"},{"id":"66","label":"Lowis Kohen"},{"id":"67","label":"ffghtf threher"},{"id":"68","label":"Zulia Casina"},{"id":"69","label":"testing testuser"},{"id":"72","label":"test45 test45"},{"id":"73","label":"dummy test"},{"id":"74","label":"dumy1 test"},{"id":"75","label":"dummy3 test"},{"id":"76","label":"dummy4 test"},{"id":"77","label":"test10 test"},{"id":"78","label":"gyhg yyh"},{"id":"79","label":"rrrrr oooo"},{"id":"80","label":"jaglast oo"}]
         * cart_items : [{"cart_item_id":"343","item_id":"53","item_price_id":"32","item_name":"test","item_code":"TES#53","item_qty":"1","item_price":"200.00","item_type":"I","quantity":"2"}]
         * location_items_category : [{"category_id":"7","category_name":"Gels","category_icon":"5597c78389c8e9846dd5140cce5f2188.jpg","business_id":"13","category_status":"1","created_at":"2019-05-24 07:37:39","updated_at":"0000-00-00 00:00:00"},{"category_id":"13","category_name":"testing","category_icon":"04aa26901f69ae30970f349bd04061fb.png","business_id":"13","category_status":"1","created_at":"2019-12-23 09:06:56","updated_at":"0000-00-00 00:00:00"}]
         * location_items : [{"item_id":"53","business_id":"13","item_name":"test","item_category_id":"7","item_category_name":"Gels","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/7ca047488438e7173f169685cb65fc6a.jpg","price_variations":[{"item_price_id":"32","item_variation_id":"1","varition_name":"Size","item_option_id":"1","variation_option_name":"Small","item_price":"200.00","item_stock":"63"},{"item_price_id":"33","item_variation_id":"1","varition_name":"Size","item_option_id":"2","variation_option_name":"Large","item_price":"270.00","item_stock":"60"}]},{"item_id":"54","business_id":"13","item_name":"test item","item_category_id":"7","item_category_name":"Gels","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/60a1b2d7309b6078d6f3cc9f2b0ff132.jpg","price_variations":[{"item_price_id":"34","item_variation_id":"1","varition_name":"Size","item_option_id":"1","variation_option_name":"Small","item_price":"280.00","item_stock":"19"},{"item_price_id":"35","item_variation_id":"1","varition_name":"Size","item_option_id":"2","variation_option_name":"Large","item_price":"240.00","item_stock":"55"}]},{"item_id":"56","business_id":"13","item_name":"test item","item_category_id":"7","item_category_name":"Gels","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/98222b70c5919af61c7b848538ab4382.jpg","price_variations":[{"item_price_id":"39","item_variation_id":"2","varition_name":"Color","item_option_id":"3","variation_option_name":"Yellow","item_price":"100.00","item_stock":"4"},{"item_price_id":"40","item_variation_id":"2","varition_name":"Color","item_option_id":"5","variation_option_name":"Green","item_price":"200.00","item_stock":"10"}]},{"item_id":"57","business_id":"13","item_name":"test item","item_category_id":"7","item_category_name":"Gels","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/d72032e53adf0cb24f81f7a1fb6ec57d.jpg","price_variations":[{"item_price_id":"46","item_variation_id":"1","varition_name":"Size","item_option_id":"1","variation_option_name":"Small","item_price":"400.00","item_stock":"45"}]},{"item_id":"58","business_id":"13","item_name":"test","item_category_id":"7","item_category_name":"Gels","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/2877c3e5cc1e6273034d950ab7ced71a.jpg","price_variations":[{"item_price_id":"47","item_variation_id":"4","varition_name":"No variation","item_option_id":null,"variation_option_name":"","item_price":"120.00","item_stock":"20"}]},{"item_id":"61","business_id":"13","item_name":"testing","item_category_id":"7","item_category_name":"Gels","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/2b7fa761013c9ca02bcabf4e50dc6097.jpg","price_variations":[{"item_price_id":"51","item_variation_id":null,"varition_name":"No variation","item_option_id":null,"variation_option_name":"","item_price":"100.00","item_stock":"5"}]},{"item_id":"62","business_id":"13","item_name":"test item","item_category_id":"13","item_category_name":"testing","item_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/e34436e2b3af4d857de5e4eedf9a35dc.jpg","price_variations":[{"item_price_id":"52","item_variation_id":null,"varition_name":"No variation","item_option_id":null,"variation_option_name":"","item_price":"200.00","item_stock":"10"}]}]
         */

        private CartDataBean cart_data;
        private List<PaymodeOptionsBean> paymode_options;
        private List<CustomersBean> customers;
        private List<CartItemsBean> cart_items;
        private List<LocationItemsCategoryBean> location_items_category;
        private List<LocationItemsBean> location_items;

        public CartDataBean getCart_data() {
            return cart_data;
        }

        public void setCart_data(CartDataBean cart_data) {
            this.cart_data = cart_data;
        }

        public List<PaymodeOptionsBean> getPaymode_options() {
            return paymode_options;
        }

        public void setPaymode_options(List<PaymodeOptionsBean> paymode_options) {
            this.paymode_options = paymode_options;
        }

        public List<CustomersBean> getCustomers() {
            return customers;
        }

        public void setCustomers(List<CustomersBean> customers) {
            this.customers = customers;
        }

        public List<CartItemsBean> getCart_items() {
            return cart_items;
        }

        public void setCart_items(List<CartItemsBean> cart_items) {
            this.cart_items = cart_items;
        }

        public List<LocationItemsCategoryBean> getLocation_items_category() {
            return location_items_category;
        }

        public void setLocation_items_category(List<LocationItemsCategoryBean> location_items_category) {
            this.location_items_category = location_items_category;
        }

        public List<LocationItemsBean> getLocation_items() {
            return location_items;
        }

        public void setLocation_items(List<LocationItemsBean> location_items) {
            this.location_items = location_items;
        }

        public static class CartDataBean {
            /**
             * cart_id : 128
             * order_number : #MATO39491
             * business_id : 13
             * location_id : 10
             * personnel_id : null
             * customer_id : 61
             * appointment_id : null
             * items_id : null
             * discount_type : null
             * discount_percent : null
             * discount_amount : 0.00
             * tax_percent : 5
             * tax_amount : 20.00
             * tip_amount : null
             * sub_total : 400.00
             * total : 420.00
             * coupon_code :
             * payment_mode :
             * order_date : 2019-12-24
             * created_by : 13
             * edited_by : 13
             * created_at : 2019-12-24 00:00:00
             * updated_at : 2019-12-24 05:39:50
             */

            private String cart_id;
            private String order_number;
            private String business_id;
            private String location_id;
            private String personnel_id;
            private String customer_id;
            private String appointment_id;
            private String items_id;
            private String discount_type;
            private String discount_percent;
            private String discount_amount;
            private String tax_percent;
            private String tax_amount;
            private String tip_amount;
            private String sub_total;
            private String total;
            private String coupon_code;
            private String payment_mode;
            private String order_date;
            private String created_by;
            private String edited_by;
            private String created_at;
            private String updated_at;

            public String getCart_id() {
                return cart_id;
            }

            public void setCart_id(String cart_id) {
                this.cart_id = cart_id;
            }

            public String getOrder_number() {
                return order_number;
            }

            public void setOrder_number(String order_number) {
                this.order_number = order_number;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getPersonnel_id() {
                return personnel_id;
            }

            public void setPersonnel_id(String personnel_id) {
                this.personnel_id = personnel_id;
            }

            public String getCustomer_id() {
                return customer_id;
            }

            public void setCustomer_id(String customer_id) {
                this.customer_id = customer_id;
            }

            public String getAppointment_id() {
                return appointment_id;
            }

            public void setAppointment_id(String appointment_id) {
                this.appointment_id = appointment_id;
            }

            public String getItems_id() {
                return items_id;
            }

            public void setItems_id(String items_id) {
                this.items_id = items_id;
            }

            public String getDiscount_type() {
                return discount_type;
            }

            public void setDiscount_type(String discount_type) {
                this.discount_type = discount_type;
            }

            public String getDiscount_percent() {
                return discount_percent;
            }

            public void setDiscount_percent(String discount_percent) {
                this.discount_percent = discount_percent;
            }

            public String getDiscount_amount() {
                return discount_amount;
            }

            public void setDiscount_amount(String discount_amount) {
                this.discount_amount = discount_amount;
            }

            public String getTax_percent() {
                return tax_percent;
            }

            public void setTax_percent(String tax_percent) {
                this.tax_percent = tax_percent;
            }

            public String getTax_amount() {
                return tax_amount;
            }

            public void setTax_amount(String tax_amount) {
                this.tax_amount = tax_amount;
            }

            public String getTip_amount() {
                return tip_amount;
            }

            public void setTip_amount(String tip_amount) {
                this.tip_amount = tip_amount;
            }

            public String getSub_total() {
                return sub_total;
            }

            public void setSub_total(String sub_total) {
                this.sub_total = sub_total;
            }

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getCoupon_code() {
                return coupon_code;
            }

            public void setCoupon_code(String coupon_code) {
                this.coupon_code = coupon_code;
            }

            public String getPayment_mode() {
                return payment_mode;
            }

            public void setPayment_mode(String payment_mode) {
                this.payment_mode = payment_mode;
            }

            public String getOrder_date() {
                return order_date;
            }

            public void setOrder_date(String order_date) {
                this.order_date = order_date;
            }

            public String getCreated_by() {
                return created_by;
            }

            public void setCreated_by(String created_by) {
                this.created_by = created_by;
            }

            public String getEdited_by() {
                return edited_by;
            }

            public void setEdited_by(String edited_by) {
                this.edited_by = edited_by;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class PaymodeOptionsBean {
            /**
             * payment_mode_id : 1
             * payment_mode_name : Cash
             * payment_mode_status : Y
             * created_at : 2019-12-17 11:32:15
             * payment_setting_id : 6
             * business_id : 13
             * location_id : 10
             * payment_tax_services : Y
             * payment_tax_service_value : 5
             * payment_tax_products : Y
             * payment_tax_product_value : 0
             * payment_processor_id : 1
             * processor_secret_key :
             * processor_publishable_key :
             * created_by : 13
             * edited_by : 0
             * created_role_id : 0
             * edited_role_id : 0
             * updated_at : null
             */

            private String payment_mode_id;
            private String payment_mode_name;
            private String payment_mode_status;
            private String created_at;
            private String payment_setting_id;
            private String business_id;
            private String location_id;
            private String payment_tax_services;
            private String payment_tax_service_value;
            private String payment_tax_products;
            private String payment_tax_product_value;
            private String payment_processor_id;
            private String processor_secret_key;
            private String processor_publishable_key;
            private String created_by;
            private String edited_by;
            private String created_role_id;
            private String edited_role_id;
            private String updated_at;

            public String getPayment_mode_id() {
                return payment_mode_id;
            }

            public void setPayment_mode_id(String payment_mode_id) {
                this.payment_mode_id = payment_mode_id;
            }

            public String getPayment_mode_name() {
                return payment_mode_name;
            }

            public void setPayment_mode_name(String payment_mode_name) {
                this.payment_mode_name = payment_mode_name;
            }

            public String getPayment_mode_status() {
                return payment_mode_status;
            }

            public void setPayment_mode_status(String payment_mode_status) {
                this.payment_mode_status = payment_mode_status;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getPayment_setting_id() {
                return payment_setting_id;
            }

            public void setPayment_setting_id(String payment_setting_id) {
                this.payment_setting_id = payment_setting_id;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getPayment_tax_services() {
                return payment_tax_services;
            }

            public void setPayment_tax_services(String payment_tax_services) {
                this.payment_tax_services = payment_tax_services;
            }

            public String getPayment_tax_service_value() {
                return payment_tax_service_value;
            }

            public void setPayment_tax_service_value(String payment_tax_service_value) {
                this.payment_tax_service_value = payment_tax_service_value;
            }

            public String getPayment_tax_products() {
                return payment_tax_products;
            }

            public void setPayment_tax_products(String payment_tax_products) {
                this.payment_tax_products = payment_tax_products;
            }

            public String getPayment_tax_product_value() {
                return payment_tax_product_value;
            }

            public void setPayment_tax_product_value(String payment_tax_product_value) {
                this.payment_tax_product_value = payment_tax_product_value;
            }

            public String getPayment_processor_id() {
                return payment_processor_id;
            }

            public void setPayment_processor_id(String payment_processor_id) {
                this.payment_processor_id = payment_processor_id;
            }

            public String getProcessor_secret_key() {
                return processor_secret_key;
            }

            public void setProcessor_secret_key(String processor_secret_key) {
                this.processor_secret_key = processor_secret_key;
            }

            public String getProcessor_publishable_key() {
                return processor_publishable_key;
            }

            public void setProcessor_publishable_key(String processor_publishable_key) {
                this.processor_publishable_key = processor_publishable_key;
            }

            public String getCreated_by() {
                return created_by;
            }

            public void setCreated_by(String created_by) {
                this.created_by = created_by;
            }

            public String getEdited_by() {
                return edited_by;
            }

            public void setEdited_by(String edited_by) {
                this.edited_by = edited_by;
            }

            public String getCreated_role_id() {
                return created_role_id;
            }

            public void setCreated_role_id(String created_role_id) {
                this.created_role_id = created_role_id;
            }

            public String getEdited_role_id() {
                return edited_role_id;
            }

            public void setEdited_role_id(String edited_role_id) {
                this.edited_role_id = edited_role_id;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class CustomersBean {
            /**
             * id : 39
             * label : test12 test
             */

            private String id;
            private String label;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getLabel() {
                return label;
            }

            public void setLabel(String label) {
                this.label = label;
            }
        }

        public static class CartItemsBean {
            /**
             * cart_item_id : 343
             * item_id : 53
             * item_price_id : 32
             * item_name : test
             * item_code : TES#53
             * item_qty : 1
             * item_price : 200.00
             * item_type : I
             * quantity : 2
             */

            private String cart_item_id;
            private String item_id;
            private String item_price_id;
            private String item_name;
            private String item_code;
            private String item_qty;
            private String item_price;
            private String item_type;
            private String quantity;

            public String getCart_item_id() {
                return cart_item_id;
            }

            public void setCart_item_id(String cart_item_id) {
                this.cart_item_id = cart_item_id;
            }

            public String getItem_id() {
                return item_id;
            }

            public void setItem_id(String item_id) {
                this.item_id = item_id;
            }

            public String getItem_price_id() {
                return item_price_id;
            }

            public void setItem_price_id(String item_price_id) {
                this.item_price_id = item_price_id;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public String getItem_code() {
                return item_code;
            }

            public void setItem_code(String item_code) {
                this.item_code = item_code;
            }

            public String getItem_qty() {
                return item_qty;
            }

            public void setItem_qty(String item_qty) {
                this.item_qty = item_qty;
            }

            public String getItem_price() {
                return item_price;
            }

            public void setItem_price(String item_price) {
                this.item_price = item_price;
            }

            public String getItem_type() {
                return item_type;
            }

            public void setItem_type(String item_type) {
                this.item_type = item_type;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }
        }

        public static class LocationItemsCategoryBean {
            /**
             * category_id : 7
             * category_name : Gels
             * category_icon : 5597c78389c8e9846dd5140cce5f2188.jpg
             * business_id : 13
             * category_status : 1
             * created_at : 2019-05-24 07:37:39
             * updated_at : 0000-00-00 00:00:00
             */

            private String category_id;
            private String category_name;
            private String category_icon;
            private String business_id;
            private String category_status;
            private String created_at;
            private String updated_at;

            public String getCategory_id() {
                return category_id;
            }

            public void setCategory_id(String category_id) {
                this.category_id = category_id;
            }

            public String getCategory_name() {
                return category_name;
            }

            public void setCategory_name(String category_name) {
                this.category_name = category_name;
            }

            public String getCategory_icon() {
                return category_icon;
            }

            public void setCategory_icon(String category_icon) {
                this.category_icon = category_icon;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getCategory_status() {
                return category_status;
            }

            public void setCategory_status(String category_status) {
                this.category_status = category_status;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class LocationItemsBean {
            /**
             * item_id : 53
             * business_id : 13
             * item_name : test
             * item_category_id : 7
             * item_category_name : Gels
             * item_image : https://storage.googleapis.com/piesvc_gcp/public/business_data/13/items/7ca047488438e7173f169685cb65fc6a.jpg
             * price_variations : [{"item_price_id":"32","item_variation_id":"1","varition_name":"Size","item_option_id":"1","variation_option_name":"Small","item_price":"200.00","item_stock":"63"},{"item_price_id":"33","item_variation_id":"1","varition_name":"Size","item_option_id":"2","variation_option_name":"Large","item_price":"270.00","item_stock":"60"}]
             */

            private String item_id;
            private String business_id;
            private String item_name;
            private String item_category_id;
            private String item_category_name;
            private String item_image;
            private String isSelected = "N";
            private List<PriceVariationsBean> price_variations;

            public String getIsSelected() {
                return isSelected;
            }

            public void setIsSelected(String isSelected) {
                this.isSelected = isSelected;
            }

            public String getItem_id() {
                return item_id;
            }

            public void setItem_id(String item_id) {
                this.item_id = item_id;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public String getItem_category_id() {
                return item_category_id;
            }

            public void setItem_category_id(String item_category_id) {
                this.item_category_id = item_category_id;
            }

            public String getItem_category_name() {
                return item_category_name;
            }

            public void setItem_category_name(String item_category_name) {
                this.item_category_name = item_category_name;
            }

            public String getItem_image() {
                return item_image;
            }

            public void setItem_image(String item_image) {
                this.item_image = item_image;
            }

            public List<PriceVariationsBean> getPrice_variations() {
                return price_variations;
            }

            public void setPrice_variations(List<PriceVariationsBean> price_variations) {
                this.price_variations = price_variations;
            }

            public static class PriceVariationsBean {
                /**
                 * item_price_id : 32
                 * item_variation_id : 1
                 * varition_name : Size
                 * item_option_id : 1
                 * variation_option_name : Small
                 * item_price : 200.00
                 * item_stock : 63
                 */

                private String item_price_id;
                private String item_variation_id;
                private String varition_name;
                private String item_option_id;
                private String variation_option_name;
                private String item_price;
                private String item_stock;
                private String isSelected = "N";

                public String getIsSelected() {
                    return isSelected;
                }

                public void setIsSelected(String isSelected) {
                    this.isSelected = isSelected;
                }

                public String getItem_price_id() {
                    return item_price_id;
                }

                public void setItem_price_id(String item_price_id) {
                    this.item_price_id = item_price_id;
                }

                public String getItem_variation_id() {
                    return item_variation_id;
                }

                public void setItem_variation_id(String item_variation_id) {
                    this.item_variation_id = item_variation_id;
                }

                public String getVarition_name() {
                    return varition_name;
                }

                public void setVarition_name(String varition_name) {
                    this.varition_name = varition_name;
                }

                public String getItem_option_id() {
                    return item_option_id;
                }

                public void setItem_option_id(String item_option_id) {
                    this.item_option_id = item_option_id;
                }

                public String getVariation_option_name() {
                    return variation_option_name;
                }

                public void setVariation_option_name(String variation_option_name) {
                    this.variation_option_name = variation_option_name;
                }

                public String getItem_price() {
                    return item_price;
                }

                public void setItem_price(String item_price) {
                    this.item_price = item_price;
                }

                public String getItem_stock() {
                    return item_stock;
                }

                public void setItem_stock(String item_stock) {
                    this.item_stock = item_stock;
                }
            }
        }
    }
}
