package com.mydevit.pieservicess.service;

import java.util.List;

public class AddCustomer_DialogModel {

    /**
     * status : success
     * status_code : 200
     * message : Customer Added Successfully
     * data : [{"id":"39","label":"test12 test"},{"id":"61","label":"Jerry Furgu"},{"id":"62","label":"Donal Veez"},{"id":"63","label":"David Johnson"},{"id":"64","label":"Venture Zoris"},{"id":"65","label":"Wesley Poul"},{"id":"66","label":"Lowis Kohen"},{"id":"67","label":"ffghtf threher"},{"id":"68","label":"Zulia Casina"},{"id":"69","label":"testing testuser"},{"id":"72","label":"test45 test45"},{"id":"73","label":"dummy test"},{"id":"74","label":"dumy1 test"},{"id":"75","label":"dummy3 test"},{"id":"76","label":"dummy4 test"},{"id":"77","label":"test10 test"},{"id":"78","label":"gyhg yyh"}]
     */

    private String status;
    private String status_code;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 39
         * label : test12 test
         */

        private String id;
        private String label;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }
    }
}
