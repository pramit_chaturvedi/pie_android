package com.mydevit.pieservicess.service;

public class EditCustomer_DataModel {


    /**
     * status : success
     * status_code : 200
     * message : Customer Data Updated Successfully
     * data : {"business_id":"13","first_name":"Annieupdated","last_name":"Johnsonbone","instagram_user":"","twitter_user":"","facebook_user":"","address":"Jaipur ANAA","city":"Jaipur ","state":"Rajasthan ","country":"","zipcode":"1234599999999","status":"1","email_verified":"Y","sms_verified":"Y","updated_at":"2019-12-05 05:30:18","email":"annieeee@hotmail.com","phone":"4654475432"}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * business_id : 13
         * first_name : Annieupdated
         * last_name : Johnsonbone
         * instagram_user :
         * twitter_user :
         * facebook_user :
         * address : Jaipur ANAA
         * city : Jaipur
         * state : Rajasthan
         * country :
         * zipcode : 1234599999999
         * status : 1
         * email_verified : Y
         * sms_verified : Y
         * updated_at : 2019-12-05 05:30:18
         * email : annieeee@hotmail.com
         * phone : 4654475432
         */

        private String business_id;
        private String first_name;
        private String customer_id;

        public String getCustomer_id() {
            return customer_id;
        }

        public void setCustomer_id(String customer_id) {
            this.customer_id = customer_id;
        }

        private String last_name;
        private String instagram_user;
        private String twitter_user;
        private String facebook_user;
        private String address;
        private String city;
        private String state;
        private String country;
        private String zipcode;
        private String status;
        private String email_verified;
        private String sms_verified;
        private String updated_at;
        private String email;
        private String phone;

        public String getBusiness_id() {
            return business_id;
        }

        public void setBusiness_id(String business_id) {
            this.business_id = business_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getInstagram_user() {
            return instagram_user;
        }

        public void setInstagram_user(String instagram_user) {
            this.instagram_user = instagram_user;
        }

        public String getTwitter_user() {
            return twitter_user;
        }

        public void setTwitter_user(String twitter_user) {
            this.twitter_user = twitter_user;
        }

        public String getFacebook_user() {
            return facebook_user;
        }

        public void setFacebook_user(String facebook_user) {
            this.facebook_user = facebook_user;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getEmail_verified() {
            return email_verified;
        }

        public void setEmail_verified(String email_verified) {
            this.email_verified = email_verified;
        }

        public String getSms_verified() {
            return sms_verified;
        }

        public void setSms_verified(String sms_verified) {
            this.sms_verified = sms_verified;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}
