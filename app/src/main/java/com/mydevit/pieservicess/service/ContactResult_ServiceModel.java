package com.mydevit.pieservicess.service;

public class ContactResult_ServiceModel {

    /**
     * status : success
     * status_code : 200
     * message : Customer Data Show Successfully
     * data : {"customer_id":"63","business_id":"13","first_name":"David","last_name":"Johnson","phone":"+1 5435654321","email":"david12@hotmail.com","password":"$2y$10$k13lBpgKF9ZwFOKn.f56gOBVPY39eFl6XmwD2TTtCNefS11Vo75X6","address":"30 Rockefeller Plaza, New York, NY, USA","latitude":"40.75896320","longitude":"-73.97933740","city":"New York","state":"NY","country":"United States","zipcode":"10112","instagram_user":"","twitter_user":"","facebook_user":"","sms_trigger":"1","email_trigger":"1","status":"1","email_verified":"N","email_hash":null,"sms_verified":"N","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","created_at":"2019-12-18 10:22:04","updated_at":"0000-00-00 00:00:00"}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * customer_id : 63
         * business_id : 13
         * first_name : David
         * last_name : Johnson
         * phone : +1 5435654321
         * email : david12@hotmail.com
         * password : $2y$10$k13lBpgKF9ZwFOKn.f56gOBVPY39eFl6XmwD2TTtCNefS11Vo75X6
         * address : 30 Rockefeller Plaza, New York, NY, USA
         * latitude : 40.75896320
         * longitude : -73.97933740
         * city : New York
         * state : NY
         * country : United States
         * zipcode : 10112
         * instagram_user :
         * twitter_user :
         * facebook_user :
         * sms_trigger : 1
         * email_trigger : 1
         * status : 1
         * email_verified : N
         * email_hash : null
         * sms_verified : N
         * created_by : 13
         * edited_by : 0
         * created_role_id : 0
         * edited_role_id : 0
         * created_at : 2019-12-18 10:22:04
         * updated_at : 0000-00-00 00:00:00
         */

        private String customer_id;
        private String business_id;
        private String first_name;
        private String last_name;
        private String phone;
        private String email;
        private String password;
        private String address;
        private String latitude;
        private String longitude;
        private String city;
        private String state;
        private String country;
        private String zipcode;
        private String instagram_user;
        private String twitter_user;
        private String facebook_user;
        private String sms_trigger;
        private String email_trigger;
        private String status;
        private String email_verified;
        private Object email_hash;
        private String sms_verified;
        private String created_by;
        private String edited_by;
        private String created_role_id;
        private String edited_role_id;
        private String created_at;
        private String updated_at;

        public String getCustomer_id() {
            return customer_id;
        }

        public void setCustomer_id(String customer_id) {
            this.customer_id = customer_id;
        }

        public String getBusiness_id() {
            return business_id;
        }

        public void setBusiness_id(String business_id) {
            this.business_id = business_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getInstagram_user() {
            return instagram_user;
        }

        public void setInstagram_user(String instagram_user) {
            this.instagram_user = instagram_user;
        }

        public String getTwitter_user() {
            return twitter_user;
        }

        public void setTwitter_user(String twitter_user) {
            this.twitter_user = twitter_user;
        }

        public String getFacebook_user() {
            return facebook_user;
        }

        public void setFacebook_user(String facebook_user) {
            this.facebook_user = facebook_user;
        }

        public String getSms_trigger() {
            return sms_trigger;
        }

        public void setSms_trigger(String sms_trigger) {
            this.sms_trigger = sms_trigger;
        }

        public String getEmail_trigger() {
            return email_trigger;
        }

        public void setEmail_trigger(String email_trigger) {
            this.email_trigger = email_trigger;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getEmail_verified() {
            return email_verified;
        }

        public void setEmail_verified(String email_verified) {
            this.email_verified = email_verified;
        }

        public Object getEmail_hash() {
            return email_hash;
        }

        public void setEmail_hash(Object email_hash) {
            this.email_hash = email_hash;
        }

        public String getSms_verified() {
            return sms_verified;
        }

        public void setSms_verified(String sms_verified) {
            this.sms_verified = sms_verified;
        }

        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public String getEdited_by() {
            return edited_by;
        }

        public void setEdited_by(String edited_by) {
            this.edited_by = edited_by;
        }

        public String getCreated_role_id() {
            return created_role_id;
        }

        public void setCreated_role_id(String created_role_id) {
            this.created_role_id = created_role_id;
        }

        public String getEdited_role_id() {
            return edited_role_id;
        }

        public void setEdited_role_id(String edited_role_id) {
            this.edited_role_id = edited_role_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
