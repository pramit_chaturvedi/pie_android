package com.mydevit.pieservicess.service;

import java.util.List;

public class appointment_personal_model {


    /**
     * status : success
     * status_code : 200
     * message : No Data Available
     * data : {"loc_aptmt":[],"cart_data":{"cart_id":"164","order_number":"#MATO13106","business_id":"13","location_id":"10","personnel_id":"0","customer_id":"25","appointment_id":"118","items_id":null,"discount_type":null,"discount_percent":null,"discount_amount":"0.00","tax_percent":"5","tax_amount":"1.00","tip_amount":null,"sub_total":"20.00","total":"21.00","coupon_code":"","payment_mode":"","order_date":"2020-01-03","created_by":"13","edited_by":"0","created_at":"2020-01-03 00:00:00","updated_at":"0000-00-00 00:00:00"},"paymode_options":[{"payment_mode_id":"1","payment_mode_name":"Cash","payment_mode_status":"Y","created_at":"2019-12-17 11:32:15","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"5","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":"1","processor_secret_key":"","processor_publishable_key":"","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null},{"payment_mode_id":"2","payment_mode_name":"Credit Card","payment_mode_status":"Y","created_at":"2019-12-17 11:32:15","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"5","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":"1","processor_secret_key":"","processor_publishable_key":"","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null}],"customers":[{"id":"39","label":"test12 test"},{"id":"61","label":"Jerry Furgu"},{"id":"62","label":"Donal Veez"},{"id":"63","label":"David Johnson"},{"id":"64","label":"Venture Zoris"},{"id":"65","label":"Wesley Poul"},{"id":"66","label":"Lowis Kohen"},{"id":"67","label":"ffghtf threher"},{"id":"68","label":"Zulia Casina"},{"id":"69","label":"testing testuser"},{"id":"72","label":"test45 test45"},{"id":"73","label":"dummy test"},{"id":"74","label":"dumy1 test"},{"id":"75","label":"dummy3 test"},{"id":"76","label":"dummy4 test"},{"id":"77","label":"test10 test"},{"id":"78","label":"gyhg yyh"},{"id":"79","label":"rrrrr oooo"},{"id":"80","label":"jaglast oo"},{"id":"81","label":"test test"},{"id":"82","label":"test test"},{"id":"83","label":"Hg he"}],"cart_items":[{"cart_item_id":"479","item_id":"2","item_price_id":null,"item_name":"Hair cut","item_code":"HAI#2","item_qty":"1","item_price":"20.00","item_type":"S","quantity":"1"}]}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * loc_aptmt : []
         * cart_data : {"cart_id":"164","order_number":"#MATO13106","business_id":"13","location_id":"10","personnel_id":"0","customer_id":"25","appointment_id":"118","items_id":null,"discount_type":null,"discount_percent":null,"discount_amount":"0.00","tax_percent":"5","tax_amount":"1.00","tip_amount":null,"sub_total":"20.00","total":"21.00","coupon_code":"","payment_mode":"","order_date":"2020-01-03","created_by":"13","edited_by":"0","created_at":"2020-01-03 00:00:00","updated_at":"0000-00-00 00:00:00"}
         * paymode_options : [{"payment_mode_id":"1","payment_mode_name":"Cash","payment_mode_status":"Y","created_at":"2019-12-17 11:32:15","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"5","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":"1","processor_secret_key":"","processor_publishable_key":"","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null},{"payment_mode_id":"2","payment_mode_name":"Credit Card","payment_mode_status":"Y","created_at":"2019-12-17 11:32:15","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"5","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":"1","processor_secret_key":"","processor_publishable_key":"","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null}]
         * customers : [{"id":"39","label":"test12 test"},{"id":"61","label":"Jerry Furgu"},{"id":"62","label":"Donal Veez"},{"id":"63","label":"David Johnson"},{"id":"64","label":"Venture Zoris"},{"id":"65","label":"Wesley Poul"},{"id":"66","label":"Lowis Kohen"},{"id":"67","label":"ffghtf threher"},{"id":"68","label":"Zulia Casina"},{"id":"69","label":"testing testuser"},{"id":"72","label":"test45 test45"},{"id":"73","label":"dummy test"},{"id":"74","label":"dumy1 test"},{"id":"75","label":"dummy3 test"},{"id":"76","label":"dummy4 test"},{"id":"77","label":"test10 test"},{"id":"78","label":"gyhg yyh"},{"id":"79","label":"rrrrr oooo"},{"id":"80","label":"jaglast oo"},{"id":"81","label":"test test"},{"id":"82","label":"test test"},{"id":"83","label":"Hg he"}]
         * cart_items : [{"cart_item_id":"479","item_id":"2","item_price_id":null,"item_name":"Hair cut","item_code":"HAI#2","item_qty":"1","item_price":"20.00","item_type":"S","quantity":"1"}]
         */

        private CartDataBean cart_data;
        private List<MainAppointmentData> loc_aptmt;
        private List<PaymodeOptionsBean> paymode_options;
        private List<CustomersBean> customers;
        private List<CartItemsBean> cart_items;


        public CartDataBean getCart_data() {
            return cart_data;
        }

        public void setCart_data(CartDataBean cart_data) {
            this.cart_data = cart_data;
        }

        public List<MainAppointmentData> getLoc_aptmt() {
            return loc_aptmt;
        }

        public void setLoc_aptmt(List<MainAppointmentData> loc_aptmt) {
            this.loc_aptmt = loc_aptmt;
        }

        public List<PaymodeOptionsBean> getPaymode_options() {
            return paymode_options;
        }

        public void setPaymode_options(List<PaymodeOptionsBean> paymode_options) {
            this.paymode_options = paymode_options;
        }

        public List<CustomersBean> getCustomers() {
            return customers;
        }

        public void setCustomers(List<CustomersBean> customers) {
            this.customers = customers;
        }

        public List<CartItemsBean> getCart_items() {
            return cart_items;
        }

        public void setCart_items(List<CartItemsBean> cart_items) {
            this.cart_items = cart_items;
        }

        public static class MainAppointmentData {
            /**
             * appointment_id : 92
             * appointment_number : #MATB91117
             * client_id : 22
             * Client_name : Annie
             * appointment_date : 2019-10-12
             * appointment_duration : 10
             * appointment_notes : null
             * appointment_service : [{"service_id":"4","service_price":"20.00"}]
             */

            private String appointment_id;
            private String appointment_number;
            private String client_id;
            private String Client_name;
            private String appointment_date;
            private String appointment_duration;
            private String appointment_notes;
            private List<AppointmentServiceBean> appointment_service;

            public String getAppointment_id() {
                return appointment_id;
            }

            public void setAppointment_id(String appointment_id) {
                this.appointment_id = appointment_id;
            }

            public String getAppointment_notes() {
                return appointment_notes;
            }

            public void setAppointment_notes(String appointment_notes) {
                this.appointment_notes = appointment_notes;
            }

            public String getAppointment_number() {
                return appointment_number;
            }

            public void setAppointment_number(String appointment_number) {
                this.appointment_number = appointment_number;
            }

            public String getClient_id() {
                return client_id;
            }

            public void setClient_id(String client_id) {
                this.client_id = client_id;
            }

            public String getClient_name() {
                return Client_name;
            }

            public void setClient_name(String Client_name) {
                this.Client_name = Client_name;
            }

            public String getAppointment_date() {
                return appointment_date;
            }

            public void setAppointment_date(String appointment_date) {
                this.appointment_date = appointment_date;
            }

            public String getAppointment_duration() {
                return appointment_duration;
            }

            public void setAppointment_duration(String appointment_duration) {
                this.appointment_duration = appointment_duration;
            }


            public List<AppointmentServiceBean> getAppointment_service() {
                return appointment_service;
            }

            public void setAppointment_service(List<AppointmentServiceBean> appointment_service) {
                this.appointment_service = appointment_service;
            }

            public static class AppointmentServiceBean {
                /**
                 * service_id : 4
                 * service_price : 20.00
                 */

                private String service_id;
                private String service_price;

                public String getService_id() {
                    return service_id;
                }

                public void setService_id(String service_id) {
                    this.service_id = service_id;
                }

                public String getService_price() {
                    return service_price;
                }

                public void setService_price(String service_price) {
                    this.service_price = service_price;
                }
            }
        }

        public static class CartDataBean {
            /**
             * cart_id : 164
             * order_number : #MATO13106
             * business_id : 13
             * location_id : 10
             * personnel_id : 0
             * customer_id : 25
             * appointment_id : 118
             * items_id : null
             * discount_type : null
             * discount_percent : null
             * discount_amount : 0.00
             * tax_percent : 5
             * tax_amount : 1.00
             * tip_amount : null
             * sub_total : 20.00
             * total : 21.00
             * coupon_code :
             * payment_mode :
             * order_date : 2020-01-03
             * created_by : 13
             * edited_by : 0
             * created_at : 2020-01-03 00:00:00
             * updated_at : 0000-00-00 00:00:00
             */

            private String cart_id;
            private String order_number;
            private String business_id;
            private String location_id;
            private String personnel_id;
            private String customer_id;
            private String appointment_id;
            private Object items_id;
            private Object discount_type;
            private Object discount_percent;
            private String discount_amount;
            private String tax_percent;
            private String tax_amount;
            private Object tip_amount;
            private String sub_total;
            private String total;
            private String coupon_code;
            private String payment_mode;
            private String order_date;
            private String created_by;
            private String edited_by;
            private String created_at;
            private String updated_at;

            public String getCart_id() {
                return cart_id;
            }

            public void setCart_id(String cart_id) {
                this.cart_id = cart_id;
            }

            public String getOrder_number() {
                return order_number;
            }

            public void setOrder_number(String order_number) {
                this.order_number = order_number;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getPersonnel_id() {
                return personnel_id;
            }

            public void setPersonnel_id(String personnel_id) {
                this.personnel_id = personnel_id;
            }

            public String getCustomer_id() {
                return customer_id;
            }

            public void setCustomer_id(String customer_id) {
                this.customer_id = customer_id;
            }

            public String getAppointment_id() {
                return appointment_id;
            }

            public void setAppointment_id(String appointment_id) {
                this.appointment_id = appointment_id;
            }

            public Object getItems_id() {
                return items_id;
            }

            public void setItems_id(Object items_id) {
                this.items_id = items_id;
            }

            public Object getDiscount_type() {
                return discount_type;
            }

            public void setDiscount_type(Object discount_type) {
                this.discount_type = discount_type;
            }

            public Object getDiscount_percent() {
                return discount_percent;
            }

            public void setDiscount_percent(Object discount_percent) {
                this.discount_percent = discount_percent;
            }

            public String getDiscount_amount() {
                return discount_amount;
            }

            public void setDiscount_amount(String discount_amount) {
                this.discount_amount = discount_amount;
            }

            public String getTax_percent() {
                return tax_percent;
            }

            public void setTax_percent(String tax_percent) {
                this.tax_percent = tax_percent;
            }

            public String getTax_amount() {
                return tax_amount;
            }

            public void setTax_amount(String tax_amount) {
                this.tax_amount = tax_amount;
            }

            public Object getTip_amount() {
                return tip_amount;
            }

            public void setTip_amount(Object tip_amount) {
                this.tip_amount = tip_amount;
            }

            public String getSub_total() {
                return sub_total;
            }

            public void setSub_total(String sub_total) {
                this.sub_total = sub_total;
            }

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getCoupon_code() {
                return coupon_code;
            }

            public void setCoupon_code(String coupon_code) {
                this.coupon_code = coupon_code;
            }

            public String getPayment_mode() {
                return payment_mode;
            }

            public void setPayment_mode(String payment_mode) {
                this.payment_mode = payment_mode;
            }

            public String getOrder_date() {
                return order_date;
            }

            public void setOrder_date(String order_date) {
                this.order_date = order_date;
            }

            public String getCreated_by() {
                return created_by;
            }

            public void setCreated_by(String created_by) {
                this.created_by = created_by;
            }

            public String getEdited_by() {
                return edited_by;
            }

            public void setEdited_by(String edited_by) {
                this.edited_by = edited_by;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class PaymodeOptionsBean {
            /**
             * payment_mode_id : 1
             * payment_mode_name : Cash
             * payment_mode_status : Y
             * created_at : 2019-12-17 11:32:15
             * payment_setting_id : 6
             * business_id : 13
             * location_id : 10
             * payment_tax_services : Y
             * payment_tax_service_value : 5
             * payment_tax_products : Y
             * payment_tax_product_value : 0
             * payment_processor_id : 1
             * processor_secret_key :
             * processor_publishable_key :
             * created_by : 13
             * edited_by : 0
             * created_role_id : 0
             * edited_role_id : 0
             * updated_at : null
             */

            private String payment_mode_id;
            private String payment_mode_name;
            private String payment_mode_status;
            private String created_at;
            private String payment_setting_id;
            private String business_id;
            private String location_id;
            private String payment_tax_services;
            private String payment_tax_service_value;
            private String payment_tax_products;
            private String payment_tax_product_value;
            private String payment_processor_id;
            private String processor_secret_key;
            private String processor_publishable_key;
            private String created_by;
            private String edited_by;
            private String created_role_id;
            private String edited_role_id;
            private Object updated_at;

            public String getPayment_mode_id() {
                return payment_mode_id;
            }

            public void setPayment_mode_id(String payment_mode_id) {
                this.payment_mode_id = payment_mode_id;
            }

            public String getPayment_mode_name() {
                return payment_mode_name;
            }

            public void setPayment_mode_name(String payment_mode_name) {
                this.payment_mode_name = payment_mode_name;
            }

            public String getPayment_mode_status() {
                return payment_mode_status;
            }

            public void setPayment_mode_status(String payment_mode_status) {
                this.payment_mode_status = payment_mode_status;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getPayment_setting_id() {
                return payment_setting_id;
            }

            public void setPayment_setting_id(String payment_setting_id) {
                this.payment_setting_id = payment_setting_id;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getPayment_tax_services() {
                return payment_tax_services;
            }

            public void setPayment_tax_services(String payment_tax_services) {
                this.payment_tax_services = payment_tax_services;
            }

            public String getPayment_tax_service_value() {
                return payment_tax_service_value;
            }

            public void setPayment_tax_service_value(String payment_tax_service_value) {
                this.payment_tax_service_value = payment_tax_service_value;
            }

            public String getPayment_tax_products() {
                return payment_tax_products;
            }

            public void setPayment_tax_products(String payment_tax_products) {
                this.payment_tax_products = payment_tax_products;
            }

            public String getPayment_tax_product_value() {
                return payment_tax_product_value;
            }

            public void setPayment_tax_product_value(String payment_tax_product_value) {
                this.payment_tax_product_value = payment_tax_product_value;
            }

            public String getPayment_processor_id() {
                return payment_processor_id;
            }

            public void setPayment_processor_id(String payment_processor_id) {
                this.payment_processor_id = payment_processor_id;
            }

            public String getProcessor_secret_key() {
                return processor_secret_key;
            }

            public void setProcessor_secret_key(String processor_secret_key) {
                this.processor_secret_key = processor_secret_key;
            }

            public String getProcessor_publishable_key() {
                return processor_publishable_key;
            }

            public void setProcessor_publishable_key(String processor_publishable_key) {
                this.processor_publishable_key = processor_publishable_key;
            }

            public String getCreated_by() {
                return created_by;
            }

            public void setCreated_by(String created_by) {
                this.created_by = created_by;
            }

            public String getEdited_by() {
                return edited_by;
            }

            public void setEdited_by(String edited_by) {
                this.edited_by = edited_by;
            }

            public String getCreated_role_id() {
                return created_role_id;
            }

            public void setCreated_role_id(String created_role_id) {
                this.created_role_id = created_role_id;
            }

            public String getEdited_role_id() {
                return edited_role_id;
            }

            public void setEdited_role_id(String edited_role_id) {
                this.edited_role_id = edited_role_id;
            }

            public Object getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(Object updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class CustomersBean {
            /**
             * id : 39
             * label : test12 test
             */

            private String id;
            private String label;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getLabel() {
                return label;
            }

            public void setLabel(String label) {
                this.label = label;
            }
        }

        public static class CartItemsBean {
            /**
             * cart_item_id : 479
             * item_id : 2
             * item_price_id : null
             * item_name : Hair cut
             * item_code : HAI#2
             * item_qty : 1
             * item_price : 20.00
             * item_type : S
             * quantity : 1
             */

            private String cart_item_id;
            private String item_id;
            private Object item_price_id;
            private String item_name;
            private String item_code;
            private String item_qty;
            private String item_price;
            private String item_type;
            private String quantity;

            public String getCart_item_id() {
                return cart_item_id;
            }

            public void setCart_item_id(String cart_item_id) {
                this.cart_item_id = cart_item_id;
            }

            public String getItem_id() {
                return item_id;
            }

            public void setItem_id(String item_id) {
                this.item_id = item_id;
            }

            public Object getItem_price_id() {
                return item_price_id;
            }

            public void setItem_price_id(Object item_price_id) {
                this.item_price_id = item_price_id;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public String getItem_code() {
                return item_code;
            }

            public void setItem_code(String item_code) {
                this.item_code = item_code;
            }

            public String getItem_qty() {
                return item_qty;
            }

            public void setItem_qty(String item_qty) {
                this.item_qty = item_qty;
            }

            public String getItem_price() {
                return item_price;
            }

            public void setItem_price(String item_price) {
                this.item_price = item_price;
            }

            public String getItem_type() {
                return item_type;
            }

            public void setItem_type(String item_type) {
                this.item_type = item_type;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }
        }
    }
}
