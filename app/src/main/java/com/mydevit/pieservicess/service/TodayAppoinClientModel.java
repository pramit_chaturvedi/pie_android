package com.mydevit.pieservicess.service;

import java.util.List;

public class TodayAppoinClientModel {

    /**
     * status : success
     * status_code : 200
     * message : Client Data Show Successfully
     * data : {"customer_data":[{"client_id":"7","client_name":"jonson Kent","client_phone":"854-859-6589","client_email":"jonson@mailinator.com"}],"today_appointments":[],"upcoming_appointment":[],"past_events":[{"client_id":"7","personnel_name":"Damien","service_name":"","total_price":0,"appointment_date":"0000-00-00","appointment_time":"10:02 AM - 10:12 AM"},{"client_id":"7","personnel_name":"Damien","service_name":"Shaving","total_price":30,"appointment_date":"2019-10-18","appointment_time":"11:31 AM - 11:51 AM"},{"client_id":"7","personnel_name":"Damien","service_name":"Shaving","total_price":30,"appointment_date":"2019-10-18","appointment_time":"12:11 PM - 12:31 PM"},{"client_id":"7","personnel_name":"Damien","service_name":"Shaving","total_price":30,"appointment_date":"2019-10-18","appointment_time":"12:31 PM - 12:51 PM"},{"client_id":"7","personnel_name":"Damien","service_name":"Shaving","total_price":30,"appointment_date":"2019-10-23","appointment_time":"05:22 AM - 05:32 AM"},{"client_id":"7","personnel_name":"Damien","service_name":"Hair cut, Hair cut","total_price":80,"appointment_date":"2019-10-24","appointment_time":"10:20 AM - 10:30 AM"},{"client_id":"7","personnel_name":"Damien","service_name":"Shaving","total_price":20,"appointment_date":"2019-11-25","appointment_time":"09:20 AM - 09:30 AM"},{"client_id":"7","personnel_name":"Damien","service_name":"Shaving, Hair cut, Spa, Color, Curly Hair, Nail Polish","total_price":120,"appointment_date":"2019-11-25","appointment_time":"12:00 PM - 01:00 PM"},{"client_id":"7","personnel_name":"Damien","service_name":"Shaving","total_price":20,"appointment_date":"2019-11-12","appointment_time":"04:25 PM - 04:35 PM"},{"client_id":"7","personnel_name":"Damien","service_name":"Hair cut, Spa, Color, Curly Hair, Nail Polish","total_price":100,"appointment_date":"2019-11-16","appointment_time":"09:50 AM - 10:40 AM"},{"client_id":"7","personnel_name":"Damien","service_name":"Shaving","total_price":20,"appointment_date":"2019-11-20","appointment_time":"12:36 PM - 12:46 PM"},{"client_id":"7","personnel_name":"Damien","service_name":"Shaving","total_price":30,"appointment_date":"2019-11-21","appointment_time":"03:19 AM - 03:39 AM"},{"client_id":"7","personnel_name":"Damien","service_name":"Shaving","total_price":30,"appointment_date":"2019-11-22","appointment_time":"09:20 AM - 09:40 AM"},{"client_id":"7","personnel_name":"Damien","service_name":"Shaving","total_price":30,"appointment_date":"2019-11-21","appointment_time":"05:35 AM - 05:55 AM"}]}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<CustomerDataBean> customer_data;
        private List<PastEventsBean> today_appointments;
        private List<PastEventsBean> upcoming_appointment;
        private List<PastEventsBean> past_events;

        public List<PastEventsBean> getToday_appointments() {
            return today_appointments;
        }

        public void setToday_appointments(List<PastEventsBean> today_appointments) {
            this.today_appointments = today_appointments;
        }

        public List<PastEventsBean> getUpcoming_appointment() {
            return upcoming_appointment;
        }

        public void setUpcoming_appointment(List<PastEventsBean> upcoming_appointment) {
            this.upcoming_appointment = upcoming_appointment;
        }

        public List<CustomerDataBean> getCustomer_data() {
            return customer_data;
        }

        public void setCustomer_data(List<CustomerDataBean> customer_data) {
            this.customer_data = customer_data;
        }


        public List<PastEventsBean> getPast_events() {
            return past_events;
        }

        public void setPast_events(List<PastEventsBean> past_events) {
            this.past_events = past_events;
        }

        public static class CustomerDataBean {
            /**
             * client_id : 7
             * client_name : jonson Kent
             * client_phone : 854-859-6589
             * client_email : jonson@mailinator.com
             */

            private String client_id;
            private String client_name;
            private String client_phone;
            private String client_email;

            public String getClient_id() {
                return client_id;
            }

            public void setClient_id(String client_id) {
                this.client_id = client_id;
            }

            public String getClient_name() {
                return client_name;
            }

            public void setClient_name(String client_name) {
                this.client_name = client_name;
            }

            public String getClient_phone() {
                return client_phone;
            }

            public void setClient_phone(String client_phone) {
                this.client_phone = client_phone;
            }

            public String getClient_email() {
                return client_email;
            }

            public void setClient_email(String client_email) {
                this.client_email = client_email;
            }
        }

        public static class PastEventsBean {
            /**
             * client_id : 7
             * personnel_name : Damien
             * service_name :
             * total_price : 0
             * appointment_date : 0000-00-00
             * appointment_time : 10:02 AM - 10:12 AM
             */

            private String client_id;
            private String personnel_name;
            private String service_name;
            private String total_price;
            private String appointment_date;
            private String appointment_time;

            public String getClient_id() {
                return client_id;
            }

            public void setClient_id(String client_id) {
                this.client_id = client_id;
            }

            public String getPersonnel_name() {
                return personnel_name;
            }

            public void setPersonnel_name(String personnel_name) {
                this.personnel_name = personnel_name;
            }

            public String getService_name() {
                return service_name;
            }

            public void setService_name(String service_name) {
                this.service_name = service_name;
            }

            public String getTotal_price() {
                return total_price;
            }

            public void setTotal_price(String total_price) {
                this.total_price = total_price;
            }

            public String getAppointment_date() {
                return appointment_date;
            }

            public void setAppointment_date(String appointment_date) {
                this.appointment_date = appointment_date;
            }

            public String getAppointment_time() {
                return appointment_time;
            }

            public void setAppointment_time(String appointment_time) {
                this.appointment_time = appointment_time;
            }
        }
    }
}
