package com.mydevit.pieservicess.service;

public class CartDelete_ServiceModel {

    /**
     * status : success
     * status_code : 200
     * message : Cart Item deleted successfully
     * data : {"discount_amount":"0.00","tax_amount":"70.00","sub_total":"1400.00","total":"1470.00","edited_by":"13","updated_at":"2019-12-24 11:37:44"}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * discount_amount : 0.00
         * tax_amount : 70.00
         * sub_total : 1400.00
         * total : 1470.00
         * edited_by : 13
         * updated_at : 2019-12-24 11:37:44
         */

        private String discount_amount;
        private String tax_amount;
        private String sub_total;
        private String total;
        private String edited_by;
        private String updated_at;

        public String getDiscount_amount() {
            return discount_amount;
        }

        public void setDiscount_amount(String discount_amount) {
            this.discount_amount = discount_amount;
        }

        public String getTax_amount() {
            return tax_amount;
        }

        public void setTax_amount(String tax_amount) {
            this.tax_amount = tax_amount;
        }

        public String getSub_total() {
            return sub_total;
        }

        public void setSub_total(String sub_total) {
            this.sub_total = sub_total;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getEdited_by() {
            return edited_by;
        }

        public void setEdited_by(String edited_by) {
            this.edited_by = edited_by;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
