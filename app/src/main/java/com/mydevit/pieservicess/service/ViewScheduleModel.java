package com.mydevit.pieservicess.service;

import java.util.List;

public class ViewScheduleModel {


    /**
     * status : success
     * status_code : 200
     * message : Show Personnel Location Appointments Successfully
     * data : [{"title":"|Damien","start":"2019-11-01 02:45:00","start_date":"2019-11-01","end":"2019-11-01 03:05:00","time":"02:45 AM - 03:05 AM","allDay":false,"location_id":"10","serviceduration":"600","client_name":"jonson Kent","personnel_name":"Damien","personnel_id":"14","appointment_id":"46","service_data":[{"service_id":"1","service_name":"Shaving"}],"backgroundColor":"","borderColor":""},{"title":"|Damien","start":"2019-11-04 13:22:00","start_date":"2019-11-04","end":"2019-11-04 13:32:00","time":"01:22 PM - 01:32 PM","allDay":false,"location_id":"10","serviceduration":"10","client_name":"jonson Kent","personnel_name":"Damien","personnel_id":"14","appointment_id":"48","service_data":[{"service_id":"1","service_name":"Shaving"}],"backgroundColor":"","borderColor":""},{"title":"|Damien","start":"2019-11-04 13:52:00","start_date":"2019-11-04","end":"2019-11-04 14:12:00","time":"01:52 PM - 02:12 PM","allDay":false,"location_id":"10","serviceduration":"60","client_name":"jonson Kent","personnel_name":"Damien","personnel_id":"14","appointment_id":"49","service_data":[{"service_id":"1","service_name":"Shaving"},{"service_id":"2","service_name":"Hair cut"},{"service_id":"3","service_name":"Spa"},{"service_id":"4","service_name":"Color"},{"service_id":"5","service_name":"Curly Hair"},{"service_id":"6","service_name":"Nail Polish"}],"backgroundColor":"","borderColor":""},{"title":"|Damien","start":"2019-11-16 15:40:00","start_date":"2019-11-16","end":"2019-11-16 16:55:00","time":"03:40 PM - 04:55 PM","allDay":false,"location_id":"10","serviceduration":"75","client_name":"Taylor Norsworthy","personnel_name":"Damien","personnel_id":"14","appointment_id":"50","service_data":[{"service_id":"3","service_name":"Spa"}],"backgroundColor":"","borderColor":""},{"title":"|Damien","start":"2019-11-05 18:40:00","start_date":"2019-11-05","end":"2019-11-05 19:10:00","time":"06:40 PM - 07:10 PM","allDay":false,"location_id":"10","serviceduration":"30","client_name":"Joe Hastin","personnel_name":"Damien","personnel_id":"14","appointment_id":"51","service_data":[{"service_id":"1","service_name":"Shaving"},{"service_id":"2","service_name":"Hair cut"},{"service_id":"3","service_name":"Spa"}],"backgroundColor":"","borderColor":""},{"title":"|Damien","start":"2019-11-12 16:25:00","start_date":"2019-11-12","end":"2019-11-12 16:35:00","time":"04:25 PM - 04:35 PM","allDay":false,"location_id":"10","serviceduration":"10","client_name":"jonson Kent","personnel_name":"Damien","personnel_id":"14","appointment_id":"52","service_data":[{"service_id":"1","service_name":"Shaving"}],"backgroundColor":"","borderColor":""},{"title":"|Damien","start":"2019-11-16 09:50:00","start_date":"2019-11-16","end":"2019-11-16 10:40:00","time":"09:50 AM - 10:40 AM","allDay":false,"location_id":"10","serviceduration":"50","client_name":"jonson Kent","personnel_name":"Damien","personnel_id":"14","appointment_id":"53","service_data":[{"service_id":"2","service_name":"Hair cut"},{"service_id":"3","service_name":"Spa"},{"service_id":"4","service_name":"Color"},{"service_id":"5","service_name":"Curly Hair"},{"service_id":"6","service_name":"Nail Polish"}],"backgroundColor":"","borderColor":""},{"title":"|test","start":"2019-11-06 05:09:00","start_date":"2019-11-06","end":"2019-11-06 05:29:00","time":"05:09 AM - 05:29 AM","allDay":false,"location_id":"10","serviceduration":"0","client_name":"Annie Johnson","personnel_name":"test","personnel_id":"22","appointment_id":"57","service_data":[{"service_id":"2","service_name":"Hair cut"},{"service_id":"1","service_name":"Shaving"}],"backgroundColor":"#481313","borderColor":"#481313"},{"title":"|Damien","start":"2019-11-20 12:36:00","start_date":"2019-11-20","end":"2019-11-20 12:46:00","time":"12:36 PM - 12:46 PM","allDay":false,"location_id":"10","serviceduration":"10","client_name":"jonson Kent","personnel_name":"Damien","personnel_id":"14","appointment_id":"62","service_data":[{"service_id":"1","service_name":"Shaving"}],"backgroundColor":"","borderColor":""},{"title":"|Damien","start":"2019-11-20 15:36:00","start_date":"2019-11-20","end":"2019-11-20 15:46:00","time":"03:36 PM - 03:46 PM","allDay":false,"location_id":"10","serviceduration":"40","client_name":"Borris Gill","personnel_name":"Damien","personnel_id":"14","appointment_id":"63","service_data":[{"service_id":"1","service_name":"Shaving"},{"service_id":"2","service_name":"Hair cut"},{"service_id":"3","service_name":"Spa"},{"service_id":"4","service_name":"Color"}],"backgroundColor":"","borderColor":""},{"title":"|test","start":"2019-11-21 10:10:00","start_date":"2019-11-21","end":"2019-11-21 11:20:00","time":"10:10 AM - 11:20 AM","allDay":false,"location_id":"10","serviceduration":"70","client_name":"jonson Kent","personnel_name":"test","personnel_id":"22","appointment_id":"64","service_data":[{"service_id":"1","service_name":"Shaving"}],"backgroundColor":"#481313","borderColor":"#481313"}]
     */

    private String status;
    private String status_code;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : |Damien
         * start : 2019-11-01 02:45:00
         * start_date : 2019-11-01
         * end : 2019-11-01 03:05:00
         * time : 02:45 AM - 03:05 AM
         * allDay : false
         * location_id : 10
         * serviceduration : 600
         * client_name : jonson Kent
         * personnel_name : Damien
         * personnel_id : 14
         * appointment_id : 46
         * service_data : [{"service_id":"1","service_name":"Shaving"}]
         * backgroundColor :
         * borderColor :
         */

        private String title;
        private String start;
        private String start_date;
        private String client_id;
        private String end;
        private String time;
        private String allDay;
        private String location_id;
        private String serviceduration;
        private String client_name;
        private String personnel_name;
        private String personnel_id;
        private String appointment_id;
        private String backgroundColor;
        private String borderColor;
        private List<ServiceDataBean> service_data;

        public String getClient_id() {
            return client_id;
        }

        public void setClient_id(String client_id) {
            this.client_id = client_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getStart_date() {
            return start_date;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getAllDay() {
            return allDay;
        }

        public void setAllDay(String allDay) {
            this.allDay = allDay;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getServiceduration() {
            return serviceduration;
        }

        public void setServiceduration(String serviceduration) {
            this.serviceduration = serviceduration;
        }

        public String getClient_name() {
            return client_name;
        }

        public void setClient_name(String client_name) {
            this.client_name = client_name;
        }

        public String getPersonnel_name() {
            return personnel_name;
        }

        public void setPersonnel_name(String personnel_name) {
            this.personnel_name = personnel_name;
        }

        public String getPersonnel_id() {
            return personnel_id;
        }

        public void setPersonnel_id(String personnel_id) {
            this.personnel_id = personnel_id;
        }

        public String getAppointment_id() {
            return appointment_id;
        }

        public void setAppointment_id(String appointment_id) {
            this.appointment_id = appointment_id;
        }

        public String getBackgroundColor() {
            return backgroundColor;
        }

        public void setBackgroundColor(String backgroundColor) {
            this.backgroundColor = backgroundColor;
        }

        public String getBorderColor() {
            return borderColor;
        }

        public void setBorderColor(String borderColor) {
            this.borderColor = borderColor;
        }

        public List<ServiceDataBean> getService_data() {
            return service_data;
        }

        public void setService_data(List<ServiceDataBean> service_data) {
            this.service_data = service_data;
        }

        public static class ServiceDataBean {
            /**
             * service_id : 1
             * service_name : Shaving
             */

            private String service_id;
            private String service_name;

            public String getService_id() {
                return service_id;
            }

            public void setService_id(String service_id) {
                this.service_id = service_id;
            }

            public String getService_name() {
                return service_name;
            }

            public void setService_name(String service_name) {
                this.service_name = service_name;
            }
        }
    }
}
