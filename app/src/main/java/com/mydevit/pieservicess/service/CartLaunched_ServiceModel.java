package com.mydevit.pieservicess.service;

import java.util.List;

public class CartLaunched_ServiceModel {

    /**
     * status : success
     * status_code : 200
     * message : Service Successfully Added To Cart
     * data : {"cart_items":[{"cart_item_id":"317","item_id":"2","item_price_id":null,"item_name":"Hair cut","item_code":"HAI#2","item_qty":"1","item_price":"20.00","item_type":"S","quantity":"1"},{"cart_item_id":"318","item_id":"1","item_price_id":null,"item_name":"Shaving","item_code":"SHA#1","item_qty":"1","item_price":"10.00","item_type":"S","quantity":"1"}],"cart_data":{"cart_id":"124","order_number":"#MATO76367","business_id":"13","location_id":"10","personnel_id":"14","customer_id":"0","appointment_id":null,"items_id":null,"discount_type":null,"discount_percent":null,"discount_amount":"0.00","tax_percent":"5","tax_amount":"1.50","tip_amount":null,"sub_total":"30.00","total":"31.50","coupon_code":"","payment_mode":"","order_date":"2019-12-21","created_by":"13","edited_by":"13","created_at":"2019-12-21 00:00:00","updated_at":"2019-12-21 00:00:00"}}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * cart_items : [{"cart_item_id":"317","item_id":"2","item_price_id":null,"item_name":"Hair cut","item_code":"HAI#2","item_qty":"1","item_price":"20.00","item_type":"S","quantity":"1"},{"cart_item_id":"318","item_id":"1","item_price_id":null,"item_name":"Shaving","item_code":"SHA#1","item_qty":"1","item_price":"10.00","item_type":"S","quantity":"1"}]
         * cart_data : {"cart_id":"124","order_number":"#MATO76367","business_id":"13","location_id":"10","personnel_id":"14","customer_id":"0","appointment_id":null,"items_id":null,"discount_type":null,"discount_percent":null,"discount_amount":"0.00","tax_percent":"5","tax_amount":"1.50","tip_amount":null,"sub_total":"30.00","total":"31.50","coupon_code":"","payment_mode":"","order_date":"2019-12-21","created_by":"13","edited_by":"13","created_at":"2019-12-21 00:00:00","updated_at":"2019-12-21 00:00:00"}
         */

        private CartDataBean cart_data;
        private List<CartItemsBean> cart_items;

        public CartDataBean getCart_data() {
            return cart_data;
        }

        public void setCart_data(CartDataBean cart_data) {
            this.cart_data = cart_data;
        }

        public List<CartItemsBean> getCart_items() {
            return cart_items;
        }

        public void setCart_items(List<CartItemsBean> cart_items) {
            this.cart_items = cart_items;
        }

        public static class CartDataBean {
            /**
             * cart_id : 124
             * order_number : #MATO76367
             * business_id : 13
             * location_id : 10
             * personnel_id : 14
             * customer_id : 0
             * appointment_id : null
             * items_id : null
             * discount_type : null
             * discount_percent : null
             * discount_amount : 0.00
             * tax_percent : 5
             * tax_amount : 1.50
             * tip_amount : null
             * sub_total : 30.00
             * total : 31.50
             * coupon_code :
             * payment_mode :
             * order_date : 2019-12-21
             * created_by : 13
             * edited_by : 13
             * created_at : 2019-12-21 00:00:00
             * updated_at : 2019-12-21 00:00:00
             */

            private String cart_id;
            private String order_number;
            private String business_id;
            private String location_id;
            private String personnel_id;
            private String customer_id;
            private String appointment_id;
            private String items_id;
            private String discount_type;
            private String discount_percent;
            private String discount_amount;
            private String tax_percent;
            private String tax_amount;
            private String tip_amount;
            private String sub_total;
            private String total;
            private String coupon_code;
            private String payment_mode;
            private String order_date;
            private String created_by;
            private String edited_by;
            private String created_at;
            private String updated_at;

            public String getAppointment_id() {
                return appointment_id;
            }

            public void setAppointment_id(String appointment_id) {
                this.appointment_id = appointment_id;
            }

            public String getItems_id() {
                return items_id;
            }

            public void setItems_id(String items_id) {
                this.items_id = items_id;
            }

            public String getDiscount_type() {
                return discount_type;
            }

            public void setDiscount_type(String discount_type) {
                this.discount_type = discount_type;
            }

            public String getDiscount_percent() {
                return discount_percent;
            }

            public void setDiscount_percent(String discount_percent) {
                this.discount_percent = discount_percent;
            }

            public String getTip_amount() {
                return tip_amount;
            }

            public void setTip_amount(String tip_amount) {
                this.tip_amount = tip_amount;
            }

            public String getCart_id() {
                return cart_id;
            }

            public void setCart_id(String cart_id) {
                this.cart_id = cart_id;
            }

            public String getOrder_number() {
                return order_number;
            }

            public void setOrder_number(String order_number) {
                this.order_number = order_number;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getPersonnel_id() {
                return personnel_id;
            }

            public void setPersonnel_id(String personnel_id) {
                this.personnel_id = personnel_id;
            }

            public String getCustomer_id() {
                return customer_id;
            }

            public void setCustomer_id(String customer_id) {
                this.customer_id = customer_id;
            }


            public String getDiscount_amount() {
                return discount_amount;
            }

            public void setDiscount_amount(String discount_amount) {
                this.discount_amount = discount_amount;
            }

            public String getTax_percent() {
                return tax_percent;
            }

            public void setTax_percent(String tax_percent) {
                this.tax_percent = tax_percent;
            }

            public String getTax_amount() {
                return tax_amount;
            }

            public void setTax_amount(String tax_amount) {
                this.tax_amount = tax_amount;
            }


            public String getSub_total() {
                return sub_total;
            }

            public void setSub_total(String sub_total) {
                this.sub_total = sub_total;
            }

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getCoupon_code() {
                return coupon_code;
            }

            public void setCoupon_code(String coupon_code) {
                this.coupon_code = coupon_code;
            }

            public String getPayment_mode() {
                return payment_mode;
            }

            public void setPayment_mode(String payment_mode) {
                this.payment_mode = payment_mode;
            }

            public String getOrder_date() {
                return order_date;
            }

            public void setOrder_date(String order_date) {
                this.order_date = order_date;
            }

            public String getCreated_by() {
                return created_by;
            }

            public void setCreated_by(String created_by) {
                this.created_by = created_by;
            }

            public String getEdited_by() {
                return edited_by;
            }

            public void setEdited_by(String edited_by) {
                this.edited_by = edited_by;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class CartItemsBean {
            /**
             * cart_item_id : 317
             * item_id : 2
             * item_price_id : null
             * item_name : Hair cut
             * item_code : HAI#2
             * item_qty : 1
             * item_price : 20.00
             * item_type : S
             * quantity : 1
             */

            private String cart_item_id;
            private String item_id;
            private String item_price_id;
            private String item_name;
            private String item_code;
            private String item_qty;
            private String item_price;
            private String item_type;
            private String quantity;


            public String getItem_price_id() {
                return item_price_id;
            }

            public void setItem_price_id(String item_price_id) {
                this.item_price_id = item_price_id;
            }

            public String getCart_item_id() {
                return cart_item_id;
            }

            public void setCart_item_id(String cart_item_id) {
                this.cart_item_id = cart_item_id;
            }

            public String getItem_id() {
                return item_id;
            }

            public void setItem_id(String item_id) {
                this.item_id = item_id;
            }


            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public String getItem_code() {
                return item_code;
            }

            public void setItem_code(String item_code) {
                this.item_code = item_code;
            }

            public String getItem_qty() {
                return item_qty;
            }

            public void setItem_qty(String item_qty) {
                this.item_qty = item_qty;
            }

            public String getItem_price() {
                return item_price;
            }

            public void setItem_price(String item_price) {
                this.item_price = item_price;
            }

            public String getItem_type() {
                return item_type;
            }

            public void setItem_type(String item_type) {
                this.item_type = item_type;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }
        }
    }
}
