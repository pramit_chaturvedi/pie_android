package com.mydevit.pieservicess.service;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class pos_set_up_model{

    /**
     * status : success
     * status_code : 200
     * message : Location Personnel Appointments Show Successfully
     * data : [{"location_id":"10","location_name":"Style Location","personnel_count":4,"appointment_count":4},{"location_id":"16","location_name":"Model Style","personnel_count":1,"appointment_count":0},{"location_id":"17","location_name":"Charles","personnel_count":1,"appointment_count":0},{"location_id":"27","location_name":"New Tempa","personnel_count":4,"appointment_count":0}]
     */

    private String status;
    private String status_code;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * location_id : 10
         * location_name : Style Location
         * personnel_count : 4
         * appointment_count : 4
         */

        private String location_id;
        private String location_name;
        private String personnel_count;
        private String appointment_count;

        protected DataBean(Parcel in) {
            location_id = in.readString();
            location_name = in.readString();
            personnel_count = in.readString();
            appointment_count = in.readString();
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getLocation_name() {
            return location_name;
        }

        public void setLocation_name(String location_name) {
            this.location_name = location_name;
        }

        public String getPersonnel_count() {
            return personnel_count;
        }

        public void setPersonnel_count(String personnel_count) {
            this.personnel_count = personnel_count;
        }

        public String getAppointment_count() {
            return appointment_count;
        }

        public void setAppointment_count(String appointment_count) {
            this.appointment_count = appointment_count;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(location_id);
            parcel.writeString(location_name);
            parcel.writeString(personnel_count);
            parcel.writeString(appointment_count);
        }
    }
}
