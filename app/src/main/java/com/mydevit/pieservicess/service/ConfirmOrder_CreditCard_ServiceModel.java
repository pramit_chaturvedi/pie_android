package com.mydevit.pieservicess.service;

import java.util.List;

public class ConfirmOrder_CreditCard_ServiceModel {

    /**
     * status : success
     * status_code : 200
     * message : Data Show Successfully
     * data : {"customer_data":[{"customer_id":"61","business_id":"13","first_name":"Jerry","last_name":"Furgu","phone":"+1 606 465 8183","email":"jerry@hotmail.com","password":"$2y$10$exRAcOZcjFgT0BPANV6uEux//qcF7FwTzFsgnGhKFt6x03E8Ag4xG","address":"456 New Jersey 17, Ramsey, NJ, USA","latitude":"41.05489700","longitude":"-74.11958350","city":"Ramsey","state":"NJ","country":"United States","zipcode":"07446","instagram_user":"","twitter_user":"","facebook_user":"","sms_trigger":"1","email_trigger":"1","status":"1","email_verified":"N","email_hash":null,"sms_verified":"N","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","created_at":"2019-12-18 10:02:25","updated_at":"0000-00-00 00:00:00"}],"customers":[{"id":"39","label":"test12 test"},{"id":"61","label":"Jerry Furgu"},{"id":"62","label":"Donal Veez"},{"id":"63","label":"David Johnson"},{"id":"64","label":"Venture Zoris"},{"id":"65","label":"Wesley Poul"},{"id":"66","label":"Lowis Kohen"},{"id":"67","label":"ffghtf threher"},{"id":"68","label":"Zulia Casina"},{"id":"69","label":"testing testuser"},{"id":"72","label":"test45 test45"},{"id":"73","label":"dummy test"},{"id":"74","label":"dumy1 test"},{"id":"75","label":"dummy3 test"},{"id":"76","label":"dummy4 test"},{"id":"77","label":"test10 test"},{"id":"78","label":"gyhg yyh"},{"id":"79","label":"rrrrr oooo"},{"id":"80","label":"jaglast oo"},{"id":"81","label":"test test"},{"id":"82","label":"test test"},{"id":"83","label":"Hg he"}],"location_id":"10","business_address":"456 Tampa Road, Palm Harbor, FL, USA","pay_type":"2","card_screen_data":{"cart_id":"149","pay_type":"2","location_id":"10","tip_amount":"74.55","created_by":"13","receipt_mode":"sms","role_id":"1","total_amount":"$820.05"}}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * customer_data : [{"customer_id":"61","business_id":"13","first_name":"Jerry","last_name":"Furgu","phone":"+1 606 465 8183","email":"jerry@hotmail.com","password":"$2y$10$exRAcOZcjFgT0BPANV6uEux//qcF7FwTzFsgnGhKFt6x03E8Ag4xG","address":"456 New Jersey 17, Ramsey, NJ, USA","latitude":"41.05489700","longitude":"-74.11958350","city":"Ramsey","state":"NJ","country":"United States","zipcode":"07446","instagram_user":"","twitter_user":"","facebook_user":"","sms_trigger":"1","email_trigger":"1","status":"1","email_verified":"N","email_hash":null,"sms_verified":"N","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","created_at":"2019-12-18 10:02:25","updated_at":"0000-00-00 00:00:00"}]
         * customers : [{"id":"39","label":"test12 test"},{"id":"61","label":"Jerry Furgu"},{"id":"62","label":"Donal Veez"},{"id":"63","label":"David Johnson"},{"id":"64","label":"Venture Zoris"},{"id":"65","label":"Wesley Poul"},{"id":"66","label":"Lowis Kohen"},{"id":"67","label":"ffghtf threher"},{"id":"68","label":"Zulia Casina"},{"id":"69","label":"testing testuser"},{"id":"72","label":"test45 test45"},{"id":"73","label":"dummy test"},{"id":"74","label":"dumy1 test"},{"id":"75","label":"dummy3 test"},{"id":"76","label":"dummy4 test"},{"id":"77","label":"test10 test"},{"id":"78","label":"gyhg yyh"},{"id":"79","label":"rrrrr oooo"},{"id":"80","label":"jaglast oo"},{"id":"81","label":"test test"},{"id":"82","label":"test test"},{"id":"83","label":"Hg he"}]
         * location_id : 10
         * business_address : 456 Tampa Road, Palm Harbor, FL, USA
         * pay_type : 2
         * card_screen_data : {"cart_id":"149","pay_type":"2","location_id":"10","tip_amount":"74.55","created_by":"13","receipt_mode":"sms","role_id":"1","total_amount":"$820.05"}
         */

        private String location_id;
        private String business_address;
        private String pay_type;
        private CardScreenDataBean card_screen_data;
        private List<CustomerDataBean> customer_data;
        private List<CustomersBean> customers;

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getBusiness_address() {
            return business_address;
        }

        public void setBusiness_address(String business_address) {
            this.business_address = business_address;
        }

        public String getPay_type() {
            return pay_type;
        }

        public void setPay_type(String pay_type) {
            this.pay_type = pay_type;
        }

        public CardScreenDataBean getCard_screen_data() {
            return card_screen_data;
        }

        public void setCard_screen_data(CardScreenDataBean card_screen_data) {
            this.card_screen_data = card_screen_data;
        }

        public List<CustomerDataBean> getCustomer_data() {
            return customer_data;
        }

        public void setCustomer_data(List<CustomerDataBean> customer_data) {
            this.customer_data = customer_data;
        }

        public List<CustomersBean> getCustomers() {
            return customers;
        }

        public void setCustomers(List<CustomersBean> customers) {
            this.customers = customers;
        }

        public static class CardScreenDataBean {
            /**
             * cart_id : 149
             * pay_type : 2
             * location_id : 10
             * tip_amount : 74.55
             * created_by : 13
             * receipt_mode : sms
             * role_id : 1
             * total_amount : $820.05
             */

            private String cart_id;
            private String pay_type;
            private String location_id;
            private String tip_amount;
            private String created_by;
            private String receipt_mode;
            private String role_id;
            private String total_amount;

            public String getCart_id() {
                return cart_id;
            }

            public void setCart_id(String cart_id) {
                this.cart_id = cart_id;
            }

            public String getPay_type() {
                return pay_type;
            }

            public void setPay_type(String pay_type) {
                this.pay_type = pay_type;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getTip_amount() {
                return tip_amount;
            }

            public void setTip_amount(String tip_amount) {
                this.tip_amount = tip_amount;
            }

            public String getCreated_by() {
                return created_by;
            }

            public void setCreated_by(String created_by) {
                this.created_by = created_by;
            }

            public String getReceipt_mode() {
                return receipt_mode;
            }

            public void setReceipt_mode(String receipt_mode) {
                this.receipt_mode = receipt_mode;
            }

            public String getRole_id() {
                return role_id;
            }

            public void setRole_id(String role_id) {
                this.role_id = role_id;
            }

            public String getTotal_amount() {
                return total_amount;
            }

            public void setTotal_amount(String total_amount) {
                this.total_amount = total_amount;
            }
        }

        public static class CustomerDataBean {
            /**
             * customer_id : 61
             * business_id : 13
             * first_name : Jerry
             * last_name : Furgu
             * phone : +1 606 465 8183
             * email : jerry@hotmail.com
             * password : $2y$10$exRAcOZcjFgT0BPANV6uEux//qcF7FwTzFsgnGhKFt6x03E8Ag4xG
             * address : 456 New Jersey 17, Ramsey, NJ, USA
             * latitude : 41.05489700
             * longitude : -74.11958350
             * city : Ramsey
             * state : NJ
             * country : United States
             * zipcode : 07446
             * instagram_user :
             * twitter_user :
             * facebook_user :
             * sms_trigger : 1
             * email_trigger : 1
             * status : 1
             * email_verified : N
             * email_hash : null
             * sms_verified : N
             * created_by : 13
             * edited_by : 0
             * created_role_id : 0
             * edited_role_id : 0
             * created_at : 2019-12-18 10:02:25
             * updated_at : 0000-00-00 00:00:00
             */

            private String customer_id;
            private String business_id;
            private String first_name;
            private String last_name;
            private String phone;
            private String email;
            private String password;
            private String address;
            private String latitude;
            private String longitude;
            private String city;
            private String state;
            private String country;
            private String zipcode;
            private String instagram_user;
            private String twitter_user;
            private String facebook_user;
            private String sms_trigger;
            private String email_trigger;
            private String status;
            private String email_verified;
            private String email_hash;
            private String sms_verified;
            private String created_by;
            private String edited_by;
            private String created_role_id;
            private String edited_role_id;
            private String created_at;
            private String updated_at;

            public String getEmail_hash() {
                return email_hash;
            }

            public void setEmail_hash(String email_hash) {
                this.email_hash = email_hash;
            }

            public String getCustomer_id() {
                return customer_id;
            }

            public void setCustomer_id(String customer_id) {
                this.customer_id = customer_id;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getZipcode() {
                return zipcode;
            }

            public void setZipcode(String zipcode) {
                this.zipcode = zipcode;
            }

            public String getInstagram_user() {
                return instagram_user;
            }

            public void setInstagram_user(String instagram_user) {
                this.instagram_user = instagram_user;
            }

            public String getTwitter_user() {
                return twitter_user;
            }

            public void setTwitter_user(String twitter_user) {
                this.twitter_user = twitter_user;
            }

            public String getFacebook_user() {
                return facebook_user;
            }

            public void setFacebook_user(String facebook_user) {
                this.facebook_user = facebook_user;
            }

            public String getSms_trigger() {
                return sms_trigger;
            }

            public void setSms_trigger(String sms_trigger) {
                this.sms_trigger = sms_trigger;
            }

            public String getEmail_trigger() {
                return email_trigger;
            }

            public void setEmail_trigger(String email_trigger) {
                this.email_trigger = email_trigger;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getEmail_verified() {
                return email_verified;
            }

            public void setEmail_verified(String email_verified) {
                this.email_verified = email_verified;
            }


            public String getSms_verified() {
                return sms_verified;
            }

            public void setSms_verified(String sms_verified) {
                this.sms_verified = sms_verified;
            }

            public String getCreated_by() {
                return created_by;
            }

            public void setCreated_by(String created_by) {
                this.created_by = created_by;
            }

            public String getEdited_by() {
                return edited_by;
            }

            public void setEdited_by(String edited_by) {
                this.edited_by = edited_by;
            }

            public String getCreated_role_id() {
                return created_role_id;
            }

            public void setCreated_role_id(String created_role_id) {
                this.created_role_id = created_role_id;
            }

            public String getEdited_role_id() {
                return edited_role_id;
            }

            public void setEdited_role_id(String edited_role_id) {
                this.edited_role_id = edited_role_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class CustomersBean {
            /**
             * id : 39
             * label : test12 test
             */

            private String id;
            private String label;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getLabel() {
                return label;
            }

            public void setLabel(String label) {
                this.label = label;
            }
        }
    }
}
