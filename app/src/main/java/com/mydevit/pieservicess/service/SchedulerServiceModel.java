package com.mydevit.pieservicess.service;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class SchedulerServiceModel {


    /**
     * status : success
     * status_code : 200
     * message : Location Personnel Appointments Show Successfully
     * data : [{"location_id":"10","location_name":"Style's Location","personnel_count":4,"appointment_count":0,"personnels":[{"personnel_id":"14","username":"dam_en","name":"Damien","role_id":"3","personnel_color":""},{"personnel_id":"22","username":"test1234","name":"test","role_id":"3","personnel_color":"#481313"},{"personnel_id":"7","username":"martha_barb","name":"Martha","role_id":"3","personnel_color":"#000000"},{"personnel_id":"20","username":"jevel_sant","name":"Jevel Santhana","role_id":"3","personnel_color":"#b45c5c"}]},{"location_id":"16","location_name":"Model Style's","personnel_count":1,"appointment_count":0,"personnels":[{"personnel_id":"22","username":"test1234","name":"test","role_id":"3","personnel_color":"#481313"}]},{"location_id":"17","location_name":"Charles","personnel_count":1,"appointment_count":0,"personnels":[{"personnel_id":"22","username":"test1234","name":"test","role_id":"3","personnel_color":"#481313"}]},{"location_id":"27","location_name":"New Tempa","personnel_count":4,"appointment_count":0,"personnels":[{"personnel_id":"14","username":"dam_en","name":"Damien","role_id":"3","personnel_color":""},{"personnel_id":"15","username":"charlie_dsouza","name":"Charlie","role_id":"3","personnel_color":""},{"personnel_id":"23","username":"mendos@123","name":"Mendos","role_id":"3","personnel_color":"#fafafa"},{"personnel_id":"7","username":"martha_barb","name":"Martha","role_id":"3","personnel_color":"#000000"}]},{"location_id":"28","location_name":"test location","personnel_count":0,"appointment_count":0,"personnels":[]}]
     */

    private String status;
    private String status_code;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        public static final Creator<SchedulerServiceModel.DataBean> CREATOR = new Creator<SchedulerServiceModel.DataBean>() {
            @Override
            public SchedulerServiceModel.DataBean createFromParcel(Parcel in) {
                return new SchedulerServiceModel.DataBean(in);
            }

            @Override
            public SchedulerServiceModel.DataBean[] newArray(int size) {
                return new SchedulerServiceModel.DataBean[size];
            }
        };
        /**
         * location_id : 10
         * location_name : Style's Location
         * personnel_count : 4
         * appointment_count : 0
         * personnels : [{"personnel_id":"14","username":"dam_en","name":"Damien","role_id":"3","personnel_color":""},{"personnel_id":"22","username":"test1234","name":"test","role_id":"3","personnel_color":"#481313"},{"personnel_id":"7","username":"martha_barb","name":"Martha","role_id":"3","personnel_color":"#000000"},{"personnel_id":"20","username":"jevel_sant","name":"Jevel Santhana","role_id":"3","personnel_color":"#b45c5c"}]
         */

        private String location_id;
        private String location_name;
        private String personnel_count;
        private String appointment_count;
        private boolean isSelected;

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        private List<PersonnelsBean> personnels;

        protected DataBean(Parcel in) {
            location_id = in.readString();
            location_name = in.readString();
            personnel_count = in.readString();
            appointment_count = in.readString();
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getPersonnel_count() {
            return personnel_count;
        }

        public void setPersonnel_count(String personnel_count) {
            this.personnel_count = personnel_count;
        }

        public String getAppointment_count() {
            return appointment_count;
        }

        public void setAppointment_count(String appointment_count) {
            this.appointment_count = appointment_count;
        }

        public String getLocation_name() {
            return location_name;
        }

        public void setLocation_name(String location_name) {
            this.location_name = location_name;
        }


        public List<PersonnelsBean> getPersonnels() {
            return personnels;
        }

        public void setPersonnels(List<PersonnelsBean> personnels) {
            this.personnels = personnels;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {

        }

        public static class PersonnelsBean {
            /**
             * personnel_id : 14
             * username : dam_en
             * name : Damien
             * role_id : 3
             * personnel_color :
             */

            private String personnel_id;
            private String username;
            private String name;
            private String role_id;
            private String personnel_color;

            public String getPersonnel_id() {
                return personnel_id;
            }

            public void setPersonnel_id(String personnel_id) {
                this.personnel_id = personnel_id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getRole_id() {
                return role_id;
            }

            public void setRole_id(String role_id) {
                this.role_id = role_id;
            }

            public String getPersonnel_color() {
                return personnel_color;
            }

            public void setPersonnel_color(String personnel_color) {
                this.personnel_color = personnel_color;
            }
        }
    }
}
