package com.mydevit.pieservicess.service;

public class NotificationTabTwoModel {

    /**
     * status : success
     * status_code : 200
     * message : SMS Notification Updated Successfully
     * data : {"business_id":"13","location_id":"10","sms_trigger":"3","sms_status":"Y","updated_at":"2019-12-04 05:12:26"}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * business_id : 13
         * location_id : 10
         * sms_trigger : 3
         * sms_status : Y
         * updated_at : 2019-12-04 05:12:26
         */

        private String business_id;
        private String location_id;
        private String sms_trigger;
        private String sms_status;
        private String updated_at;

        public String getBusiness_id() {
            return business_id;
        }

        public void setBusiness_id(String business_id) {
            this.business_id = business_id;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getSms_trigger() {
            return sms_trigger;
        }

        public void setSms_trigger(String sms_trigger) {
            this.sms_trigger = sms_trigger;
        }

        public String getSms_status() {
            return sms_status;
        }

        public void setSms_status(String sms_status) {
            this.sms_status = sms_status;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
