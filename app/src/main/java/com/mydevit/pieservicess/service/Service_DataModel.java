package com.mydevit.pieservicess.service;

import java.util.List;

public class Service_DataModel {


    /**
     * status : success
     * status_code : 200
     * message : location Services Show Successfully
     * data : {"location_services":[{"service_id":"1","category_id":"4","service_name":"Shaving","service_price":"30.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/shaving.jpg","persoonels":[{"personnel_id":"7","personnel_name":"Martha","personnel_service_price":"10.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg"},{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"},{"personnel_id":"22","personnel_name":"test","personnel_service_price":"200.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/80990fc4e1683afa9e014b2dc03e3403.jpg"},{"personnel_id":"20","personnel_name":"Jevel Santhana","personnel_service_price":"100.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/1e42ebad59e070c146387f8a8dce40f4.jpg"}]},{"service_id":"2","category_id":"4","service_name":"Hair cut","service_price":"50.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/haircut.jpg","persoonels":[{"personnel_id":"7","personnel_name":"Martha","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg"},{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"},{"personnel_id":"22","personnel_name":"test","personnel_service_price":"350.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/80990fc4e1683afa9e014b2dc03e3403.jpg"},{"personnel_id":"20","personnel_name":"Jevel Santhana","personnel_service_price":"250.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/1e42ebad59e070c146387f8a8dce40f4.jpg"}]},{"service_id":"3","category_id":"3","service_name":"Spa","service_price":"800.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/spa.jpg","persoonels":[{"personnel_id":"7","personnel_name":"Martha","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg"},{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"}]},{"service_id":"4","category_id":"4","service_name":"Color","service_price":"60.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/color.jpg","persoonels":[{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"}]},{"service_id":"5","category_id":"4","service_name":"Curly Hair","service_price":"60.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/curly_hair.jpg","persoonels":[{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"}]},{"service_id":"6","category_id":"5","service_name":"Nail Polish","service_price":"120.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/nail_polish.jpg","persoonels":[{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"}]},{"service_id":"7","category_id":"7","service_name":"Waxing","service_price":"210.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/waxing.jpg","persoonels":[]},{"service_id":"8","category_id":"7","service_name":"Facial treatment","service_price":"110.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/facial_treatment.jpg","persoonels":[]},{"service_id":"9","category_id":"3","service_name":"Sports massage","service_price":"300.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/sports_masage.jpg","persoonels":[]},{"service_id":"10","category_id":"1","service_name":"Straight Hair","service_price":"120.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/straight_hair.jpg","persoonels":[]},{"service_id":"11","category_id":"3","service_name":"Girls Facial","service_price":"99.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/images/placeholder.png","persoonels":[]},{"service_id":"12","category_id":"1","service_name":"spa","service_price":"300.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/images/placeholder.png","persoonels":[]},{"service_id":"18","category_id":"4","service_name":"testing","service_price":"200.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/4782de12b2a41093d638622f1da87ecf.jpg","persoonels":[]}],"cart_data":{"cart_id":"120","order_number":"#MATO23741","business_id":"13","location_id":"10","personnel_id":null,"customer_id":"10","appointment_id":null,"items_id":null,"discount_type":null,"discount_percent":null,"discount_amount":"0.00","tax_percent":"5","tax_amount":"6.00","tip_amount":null,"sub_total":"120.00","total":"126.00","coupon_code":"","payment_mode":"","order_date":"2019-12-20","created_by":"13","edited_by":"13","created_at":"2019-12-20 00:00:00","updated_at":"2019-12-20 00:00:00"},"paymode_options":[{"payment_mode_id":"1","payment_mode_name":"Cash","payment_mode_status":"Y","created_at":"2019-12-17 11:32:15","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"5","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":"1","processor_secret_key":"","processor_publishable_key":"","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null},{"payment_mode_id":"2","payment_mode_name":"Credit Card","payment_mode_status":"Y","created_at":"2019-12-17 11:32:15","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"5","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":"1","processor_secret_key":"","processor_publishable_key":"","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null}],"customers":[{"id":"7","label":"jonson Kent"},{"id":"10","label":"Joe Hastin"},{"id":"11","label":"Taylor Norsworthy"},{"id":"14","label":"yoyo dude"},{"id":"15","label":"jevel santana"},{"id":"16","label":"Jerry Pitts"},{"id":"17","label":"Jerry Pitts"},{"id":"19","label":"Vantage Clara"},{"id":"20","label":"Vitori Karls"},{"id":"21","label":"Borris Gill"},{"id":"23","label":"Levi Norsworthy"},{"id":"25","label":"Leonardo Marchetti"},{"id":"28","label":"Daniell Dsouza"},{"id":"29","label":"Keith Hamilton"},{"id":"30","label":"Joshua Linkon"},{"id":"31","label":"Test 123"},{"id":"32","label":"Kullinan Thale"},{"id":"33","label":"dawson hamm"},{"id":"34","label":"Adam Norsworthy"},{"id":"35","label":"Nunja Lomn"},{"id":"36","label":"Vitori Kolis"},{"id":"37","label":"Dawson Hamm"},{"id":"38","label":"Dawson Hamm"},{"id":"39","label":"test12 test"},{"id":"40","label":"t trans"},{"id":"41","label":"Dawson Hamm"},{"id":"42","label":"Dawson Hamm"},{"id":"43","label":"Dawson Hamm"},{"id":"44","label":"Dawson Hamm"},{"id":"45","label":"Dawson Hamm"},{"id":"46","label":"Dawson Hamm"},{"id":"47","label":"Lowry Geor"},{"id":"48","label":"handsome asfasdf"},{"id":"50","label":"Test Test"},{"id":"52","label":"testnew1 test"},{"id":"58","label":"Daws Hamm"},{"id":"59","label":"Daisy Ham"},{"id":"60","label":"Poul Joi"},{"id":"61","label":"Jerry Furgu"},{"id":"62","label":"Donal Veez"},{"id":"63","label":"David Johnson"},{"id":"64","label":"Venture Zoris"},{"id":"65","label":"Wesley Poul"},{"id":"66","label":"Lowis Kohen"},{"id":"67","label":"ffghtf threher"},{"id":"68","label":"Zulia Casina"},{"id":"69","label":"testing testuser"}],"service_categories":[{"category_id":"1","category_name":"Hair Salons","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:06","updated_at":"0000-00-00 00:00:00"},{"category_id":"2","category_name":"Hair Stylists","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:18","updated_at":"0000-00-00 00:00:00"},{"category_id":"3","category_name":"Spa & Massage","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:28","updated_at":"0000-00-00 00:00:00"},{"category_id":"4","category_name":"Nail Spa","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:43","updated_at":"0000-00-00 00:00:00"},{"category_id":"5","category_name":"Brows and Lashes","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:57","updated_at":"0000-00-00 00:00:00"},{"category_id":"6","category_name":"Body Art","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:36","updated_at":"0000-00-00 00:00:00"},{"category_id":"7","category_name":"Bridal Makeup","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:09","updated_at":"0000-00-00 00:00:00"}],"service_personnels":[{"personnel_id":"7","name":"Martha"},{"personnel_id":"14","name":"Damien"},{"personnel_id":"15","name":"Charlie"},{"personnel_id":"20","name":"Jevel Santhana"},{"personnel_id":"22","name":"test"},{"personnel_id":"23","name":"Mendos"}],"cart_items":[{"cart_item_id":"313","item_id":"4","item_price_id":null,"item_name":"Color","item_code":"COL#4","item_qty":"1","item_price":"20.00","item_type":"S","quantity":"1"},{"cart_item_id":"312","item_id":"1","item_price_id":null,"item_name":"Shaving","item_code":"SHA#1","item_qty":"1","item_price":"100.00","item_type":"S","quantity":"1"}]}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * location_services : [{"service_id":"1","category_id":"4","service_name":"Shaving","service_price":"30.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/shaving.jpg","persoonels":[{"personnel_id":"7","personnel_name":"Martha","personnel_service_price":"10.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg"},{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"},{"personnel_id":"22","personnel_name":"test","personnel_service_price":"200.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/80990fc4e1683afa9e014b2dc03e3403.jpg"},{"personnel_id":"20","personnel_name":"Jevel Santhana","personnel_service_price":"100.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/1e42ebad59e070c146387f8a8dce40f4.jpg"}]},{"service_id":"2","category_id":"4","service_name":"Hair cut","service_price":"50.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/haircut.jpg","persoonels":[{"personnel_id":"7","personnel_name":"Martha","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg"},{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"},{"personnel_id":"22","personnel_name":"test","personnel_service_price":"350.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/80990fc4e1683afa9e014b2dc03e3403.jpg"},{"personnel_id":"20","personnel_name":"Jevel Santhana","personnel_service_price":"250.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/1e42ebad59e070c146387f8a8dce40f4.jpg"}]},{"service_id":"3","category_id":"3","service_name":"Spa","service_price":"800.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/spa.jpg","persoonels":[{"personnel_id":"7","personnel_name":"Martha","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg"},{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"}]},{"service_id":"4","category_id":"4","service_name":"Color","service_price":"60.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/color.jpg","persoonels":[{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"}]},{"service_id":"5","category_id":"4","service_name":"Curly Hair","service_price":"60.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/curly_hair.jpg","persoonels":[{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"}]},{"service_id":"6","category_id":"5","service_name":"Nail Polish","service_price":"120.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/nail_polish.jpg","persoonels":[{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"}]},{"service_id":"7","category_id":"7","service_name":"Waxing","service_price":"210.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/waxing.jpg","persoonels":[]},{"service_id":"8","category_id":"7","service_name":"Facial treatment","service_price":"110.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/facial_treatment.jpg","persoonels":[]},{"service_id":"9","category_id":"3","service_name":"Sports massage","service_price":"300.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/sports_masage.jpg","persoonels":[]},{"service_id":"10","category_id":"1","service_name":"Straight Hair","service_price":"120.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/straight_hair.jpg","persoonels":[]},{"service_id":"11","category_id":"3","service_name":"Girls Facial","service_price":"99.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/images/placeholder.png","persoonels":[]},{"service_id":"12","category_id":"1","service_name":"spa","service_price":"300.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/images/placeholder.png","persoonels":[]},{"service_id":"18","category_id":"4","service_name":"testing","service_price":"200.00","service_image":"https://storage.googleapis.com/piesvc_gcp/public/service_images/4782de12b2a41093d638622f1da87ecf.jpg","persoonels":[]}]
         * cart_data : {"cart_id":"120","order_number":"#MATO23741","business_id":"13","location_id":"10","personnel_id":null,"customer_id":"10","appointment_id":null,"items_id":null,"discount_type":null,"discount_percent":null,"discount_amount":"0.00","tax_percent":"5","tax_amount":"6.00","tip_amount":null,"sub_total":"120.00","total":"126.00","coupon_code":"","payment_mode":"","order_date":"2019-12-20","created_by":"13","edited_by":"13","created_at":"2019-12-20 00:00:00","updated_at":"2019-12-20 00:00:00"}
         * paymode_options : [{"payment_mode_id":"1","payment_mode_name":"Cash","payment_mode_status":"Y","created_at":"2019-12-17 11:32:15","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"5","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":"1","processor_secret_key":"","processor_publishable_key":"","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null},{"payment_mode_id":"2","payment_mode_name":"Credit Card","payment_mode_status":"Y","created_at":"2019-12-17 11:32:15","payment_setting_id":"6","business_id":"13","location_id":"10","payment_tax_services":"Y","payment_tax_service_value":"5","payment_tax_products":"Y","payment_tax_product_value":"0","payment_processor_id":"1","processor_secret_key":"","processor_publishable_key":"","created_by":"13","edited_by":"0","created_role_id":"0","edited_role_id":"0","updated_at":null}]
         * customers : [{"id":"7","label":"jonson Kent"},{"id":"10","label":"Joe Hastin"},{"id":"11","label":"Taylor Norsworthy"},{"id":"14","label":"yoyo dude"},{"id":"15","label":"jevel santana"},{"id":"16","label":"Jerry Pitts"},{"id":"17","label":"Jerry Pitts"},{"id":"19","label":"Vantage Clara"},{"id":"20","label":"Vitori Karls"},{"id":"21","label":"Borris Gill"},{"id":"23","label":"Levi Norsworthy"},{"id":"25","label":"Leonardo Marchetti"},{"id":"28","label":"Daniell Dsouza"},{"id":"29","label":"Keith Hamilton"},{"id":"30","label":"Joshua Linkon"},{"id":"31","label":"Test 123"},{"id":"32","label":"Kullinan Thale"},{"id":"33","label":"dawson hamm"},{"id":"34","label":"Adam Norsworthy"},{"id":"35","label":"Nunja Lomn"},{"id":"36","label":"Vitori Kolis"},{"id":"37","label":"Dawson Hamm"},{"id":"38","label":"Dawson Hamm"},{"id":"39","label":"test12 test"},{"id":"40","label":"t trans"},{"id":"41","label":"Dawson Hamm"},{"id":"42","label":"Dawson Hamm"},{"id":"43","label":"Dawson Hamm"},{"id":"44","label":"Dawson Hamm"},{"id":"45","label":"Dawson Hamm"},{"id":"46","label":"Dawson Hamm"},{"id":"47","label":"Lowry Geor"},{"id":"48","label":"handsome asfasdf"},{"id":"50","label":"Test Test"},{"id":"52","label":"testnew1 test"},{"id":"58","label":"Daws Hamm"},{"id":"59","label":"Daisy Ham"},{"id":"60","label":"Poul Joi"},{"id":"61","label":"Jerry Furgu"},{"id":"62","label":"Donal Veez"},{"id":"63","label":"David Johnson"},{"id":"64","label":"Venture Zoris"},{"id":"65","label":"Wesley Poul"},{"id":"66","label":"Lowis Kohen"},{"id":"67","label":"ffghtf threher"},{"id":"68","label":"Zulia Casina"},{"id":"69","label":"testing testuser"}]
         * service_categories : [{"category_id":"1","category_name":"Hair Salons","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:06","updated_at":"0000-00-00 00:00:00"},{"category_id":"2","category_name":"Hair Stylists","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:18","updated_at":"0000-00-00 00:00:00"},{"category_id":"3","category_name":"Spa & Massage","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:28","updated_at":"0000-00-00 00:00:00"},{"category_id":"4","category_name":"Nail Spa","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:43","updated_at":"0000-00-00 00:00:00"},{"category_id":"5","category_name":"Brows and Lashes","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:57","updated_at":"0000-00-00 00:00:00"},{"category_id":"6","category_name":"Body Art","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:36","updated_at":"0000-00-00 00:00:00"},{"category_id":"7","category_name":"Bridal Makeup","category_status":"Y","is_admin":"1","business_id":null,"created_at":"2019-05-30 08:05:09","updated_at":"0000-00-00 00:00:00"}]
         * service_personnels : [{"personnel_id":"7","name":"Martha"},{"personnel_id":"14","name":"Damien"},{"personnel_id":"15","name":"Charlie"},{"personnel_id":"20","name":"Jevel Santhana"},{"personnel_id":"22","name":"test"},{"personnel_id":"23","name":"Mendos"}]
         * cart_items : [{"cart_item_id":"313","item_id":"4","item_price_id":null,"item_name":"Color","item_code":"COL#4","item_qty":"1","item_price":"20.00","item_type":"S","quantity":"1"},{"cart_item_id":"312","item_id":"1","item_price_id":null,"item_name":"Shaving","item_code":"SHA#1","item_qty":"1","item_price":"100.00","item_type":"S","quantity":"1"}]
         */

        private CartDataBean cart_data;
        private List<LocationServicesBean> location_services;
        private List<PaymodeOptionsBean> paymode_options;
        private List<CustomersBean> customers;
        private List<ServiceCategoriesBean> service_categories;
        private List<ServicePersonnelsBean> service_personnels;
        private List<CartItemsBean> cart_items;

        public CartDataBean getCart_data() {
            return cart_data;
        }

        public void setCart_data(CartDataBean cart_data) {
            this.cart_data = cart_data;
        }

        public List<LocationServicesBean> getLocation_services() {
            return location_services;
        }

        public void setLocation_services(List<LocationServicesBean> location_services) {
            this.location_services = location_services;
        }

        public List<PaymodeOptionsBean> getPaymode_options() {
            return paymode_options;
        }

        public void setPaymode_options(List<PaymodeOptionsBean> paymode_options) {
            this.paymode_options = paymode_options;
        }

        public List<CustomersBean> getCustomers() {
            return customers;
        }

        public void setCustomers(List<CustomersBean> customers) {
            this.customers = customers;
        }

        public List<ServiceCategoriesBean> getService_categories() {
            return service_categories;
        }

        public void setService_categories(List<ServiceCategoriesBean> service_categories) {
            this.service_categories = service_categories;
        }

        public List<ServicePersonnelsBean> getService_personnels() {
            return service_personnels;
        }

        public void setService_personnels(List<ServicePersonnelsBean> service_personnels) {
            this.service_personnels = service_personnels;
        }

        public List<CartItemsBean> getCart_items() {
            return cart_items;
        }

        public void setCart_items(List<CartItemsBean> cart_items) {
            this.cart_items = cart_items;
        }

        public static class CartDataBean {
            /**
             * cart_id : 120
             * order_number : #MATO23741
             * business_id : 13
             * location_id : 10
             * personnel_id : null
             * customer_id : 10
             * appointment_id : null
             * items_id : null
             * discount_type : null
             * discount_percent : null
             * discount_amount : 0.00
             * tax_percent : 5
             * tax_amount : 6.00
             * tip_amount : null
             * sub_total : 120.00
             * total : 126.00
             * coupon_code :
             * payment_mode :
             * order_date : 2019-12-20
             * created_by : 13
             * edited_by : 13
             * created_at : 2019-12-20 00:00:00
             * updated_at : 2019-12-20 00:00:00
             */

            private String cart_id;
            private String order_number;
            private String business_id;
            private String location_id;
            private String personnel_id;
            private String customer_id;
            private String appointment_id;
            private String items_id;
            private String discount_type;
            private String discount_percent;
            private String discount_amount;
            private String tax_percent;
            private String tax_amount;
            private String tip_amount;
            private String sub_total;
            private String total;
            private String coupon_code;
            private String payment_mode;
            private String order_date;
            private String created_by;
            private String edited_by;
            private String created_at;
            private String updated_at;

            public String getCart_id() {
                return cart_id;
            }

            public void setCart_id(String cart_id) {
                this.cart_id = cart_id;
            }

            public String getOrder_number() {
                return order_number;
            }

            public void setOrder_number(String order_number) {
                this.order_number = order_number;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getPersonnel_id() {
                return personnel_id;
            }

            public void setPersonnel_id(String personnel_id) {
                this.personnel_id = personnel_id;
            }

            public String getCustomer_id() {
                return customer_id;
            }

            public void setCustomer_id(String customer_id) {
                this.customer_id = customer_id;
            }

            public String getAppointment_id() {
                return appointment_id;
            }

            public void setAppointment_id(String appointment_id) {
                this.appointment_id = appointment_id;
            }

            public String getItems_id() {
                return items_id;
            }

            public void setItems_id(String items_id) {
                this.items_id = items_id;
            }

            public String getDiscount_type() {
                return discount_type;
            }

            public void setDiscount_type(String discount_type) {
                this.discount_type = discount_type;
            }

            public String getDiscount_percent() {
                return discount_percent;
            }

            public void setDiscount_percent(String discount_percent) {
                this.discount_percent = discount_percent;
            }

            public String getDiscount_amount() {
                return discount_amount;
            }

            public void setDiscount_amount(String discount_amount) {
                this.discount_amount = discount_amount;
            }

            public String getTax_percent() {
                return tax_percent;
            }

            public void setTax_percent(String tax_percent) {
                this.tax_percent = tax_percent;
            }

            public String getTax_amount() {
                return tax_amount;
            }

            public void setTax_amount(String tax_amount) {
                this.tax_amount = tax_amount;
            }

            public String getTip_amount() {
                return tip_amount;
            }

            public void setTip_amount(String tip_amount) {
                this.tip_amount = tip_amount;
            }

            public String getSub_total() {
                return sub_total;
            }

            public void setSub_total(String sub_total) {
                this.sub_total = sub_total;
            }

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getCoupon_code() {
                return coupon_code;
            }

            public void setCoupon_code(String coupon_code) {
                this.coupon_code = coupon_code;
            }

            public String getPayment_mode() {
                return payment_mode;
            }

            public void setPayment_mode(String payment_mode) {
                this.payment_mode = payment_mode;
            }

            public String getOrder_date() {
                return order_date;
            }

            public void setOrder_date(String order_date) {
                this.order_date = order_date;
            }

            public String getCreated_by() {
                return created_by;
            }

            public void setCreated_by(String created_by) {
                this.created_by = created_by;
            }

            public String getEdited_by() {
                return edited_by;
            }

            public void setEdited_by(String edited_by) {
                this.edited_by = edited_by;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class LocationServicesBean {
            /**
             * service_id : 1
             * category_id : 4
             * service_name : Shaving
             * service_price : 30.00
             * service_image : https://storage.googleapis.com/piesvc_gcp/public/service_images/shaving.jpg
             * persoonels : [{"personnel_id":"7","personnel_name":"Martha","personnel_service_price":"10.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg"},{"personnel_id":"14","personnel_name":"Damien","personnel_service_price":"20.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg"},{"personnel_id":"22","personnel_name":"test","personnel_service_price":"200.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/80990fc4e1683afa9e014b2dc03e3403.jpg"},{"personnel_id":"20","personnel_name":"Jevel Santhana","personnel_service_price":"100.00","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/1e42ebad59e070c146387f8a8dce40f4.jpg"}]
             */

            private String service_id;
            private String category_id;
            private String service_name;
            private String service_price;
            private String service_image;
            private String priceWillBeShow = "";
            private List<PersoonelsBean> persoonels;

            public String getPriceWillBeShow() {
                return priceWillBeShow;
            }

            public void setPriceWillBeShow(String priceWillBeShow) {
                this.priceWillBeShow = priceWillBeShow;
            }

            public String getService_id() {
                return service_id;
            }

            public void setService_id(String service_id) {
                this.service_id = service_id;
            }

            public String getCategory_id() {
                return category_id;
            }

            public void setCategory_id(String category_id) {
                this.category_id = category_id;
            }

            public String getService_name() {
                return service_name;
            }

            public void setService_name(String service_name) {
                this.service_name = service_name;
            }

            public String getService_price() {
                return service_price;
            }

            public void setService_price(String service_price) {
                this.service_price = service_price;
            }

            public String getService_image() {
                return service_image;
            }

            public void setService_image(String service_image) {
                this.service_image = service_image;
            }

            public List<PersoonelsBean> getPersoonels() {
                return persoonels;
            }

            public void setPersoonels(List<PersoonelsBean> persoonels) {
                this.persoonels = persoonels;

            }

            public static class PersoonelsBean {
                /**
                 * personnel_id : 7
                 * personnel_name : Martha
                 * personnel_service_price : 10.00
                 * personnel_image : https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg
                 */

                private String personnel_id;
                private String personnel_name;
                private String personnel_service_price;
                private String personnel_image;

                public String getPersonnel_id() {
                    return personnel_id;
                }

                public void setPersonnel_id(String personnel_id) {
                    this.personnel_id = personnel_id;
                }

                public String getPersonnel_name() {
                    return personnel_name;
                }

                public void setPersonnel_name(String personnel_name) {
                    this.personnel_name = personnel_name;
                }

                public String getPersonnel_service_price() {
                    return personnel_service_price;
                }

                public void setPersonnel_service_price(String personnel_service_price) {
                    this.personnel_service_price = personnel_service_price;
                }

                public String getPersonnel_image() {
                    return personnel_image;
                }

                public void setPersonnel_image(String personnel_image) {
                    this.personnel_image = personnel_image;
                }
            }
        }

        public static class PaymodeOptionsBean {
            /**
             * payment_mode_id : 1
             * payment_mode_name : Cash
             * payment_mode_status : Y
             * created_at : 2019-12-17 11:32:15
             * payment_setting_id : 6
             * business_id : 13
             * location_id : 10
             * payment_tax_services : Y
             * payment_tax_service_value : 5
             * payment_tax_products : Y
             * payment_tax_product_value : 0
             * payment_processor_id : 1
             * processor_secret_key :
             * processor_publishable_key :
             * created_by : 13
             * edited_by : 0
             * created_role_id : 0
             * edited_role_id : 0
             * updated_at : null
             */

            private String payment_mode_id;
            private String payment_mode_name;
            private String payment_mode_status;
            private String created_at;
            private String payment_setting_id;
            private String business_id;
            private String location_id;
            private String payment_tax_services;
            private String payment_tax_service_value;
            private String payment_tax_products;
            private String payment_tax_product_value;
            private String payment_processor_id;
            private String processor_secret_key;
            private String processor_publishable_key;
            private String created_by;
            private String edited_by;
            private String created_role_id;
            private String edited_role_id;
            private String updated_at;

            public String getPayment_mode_id() {
                return payment_mode_id;
            }

            public void setPayment_mode_id(String payment_mode_id) {
                this.payment_mode_id = payment_mode_id;
            }

            public String getPayment_mode_name() {
                return payment_mode_name;
            }

            public void setPayment_mode_name(String payment_mode_name) {
                this.payment_mode_name = payment_mode_name;
            }

            public String getPayment_mode_status() {
                return payment_mode_status;
            }

            public void setPayment_mode_status(String payment_mode_status) {
                this.payment_mode_status = payment_mode_status;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getPayment_setting_id() {
                return payment_setting_id;
            }

            public void setPayment_setting_id(String payment_setting_id) {
                this.payment_setting_id = payment_setting_id;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getPayment_tax_services() {
                return payment_tax_services;
            }

            public void setPayment_tax_services(String payment_tax_services) {
                this.payment_tax_services = payment_tax_services;
            }

            public String getPayment_tax_service_value() {
                return payment_tax_service_value;
            }

            public void setPayment_tax_service_value(String payment_tax_service_value) {
                this.payment_tax_service_value = payment_tax_service_value;
            }

            public String getPayment_tax_products() {
                return payment_tax_products;
            }

            public void setPayment_tax_products(String payment_tax_products) {
                this.payment_tax_products = payment_tax_products;
            }

            public String getPayment_tax_product_value() {
                return payment_tax_product_value;
            }

            public void setPayment_tax_product_value(String payment_tax_product_value) {
                this.payment_tax_product_value = payment_tax_product_value;
            }

            public String getPayment_processor_id() {
                return payment_processor_id;
            }

            public void setPayment_processor_id(String payment_processor_id) {
                this.payment_processor_id = payment_processor_id;
            }

            public String getProcessor_secret_key() {
                return processor_secret_key;
            }

            public void setProcessor_secret_key(String processor_secret_key) {
                this.processor_secret_key = processor_secret_key;
            }

            public String getProcessor_publishable_key() {
                return processor_publishable_key;
            }

            public void setProcessor_publishable_key(String processor_publishable_key) {
                this.processor_publishable_key = processor_publishable_key;
            }

            public String getCreated_by() {
                return created_by;
            }

            public void setCreated_by(String created_by) {
                this.created_by = created_by;
            }

            public String getEdited_by() {
                return edited_by;
            }

            public void setEdited_by(String edited_by) {
                this.edited_by = edited_by;
            }

            public String getCreated_role_id() {
                return created_role_id;
            }

            public void setCreated_role_id(String created_role_id) {
                this.created_role_id = created_role_id;
            }

            public String getEdited_role_id() {
                return edited_role_id;
            }

            public void setEdited_role_id(String edited_role_id) {
                this.edited_role_id = edited_role_id;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class CustomersBean {
            /**
             * id : 7
             * label : jonson Kent
             */

            private String id;
            private String label;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getLabel() {
                return label;
            }

            public void setLabel(String label) {
                this.label = label;
            }
        }

        public static class ServiceCategoriesBean {
            /**
             * category_id : 1
             * category_name : Hair Salons
             * category_status : Y
             * is_admin : 1
             * business_id : null
             * created_at : 2019-05-30 08:05:06
             * updated_at : 0000-00-00 00:00:00
             */

            private String category_id;
            private String category_name;
            private String category_status;
            private String is_admin;
            private String business_id;
            private String created_at;
            private String updated_at;

            public String getCategory_id() {
                return category_id;
            }

            public void setCategory_id(String category_id) {
                this.category_id = category_id;
            }

            public String getCategory_name() {
                return category_name;
            }

            public void setCategory_name(String category_name) {
                this.category_name = category_name;
            }

            public String getCategory_status() {
                return category_status;
            }

            public void setCategory_status(String category_status) {
                this.category_status = category_status;
            }

            public String getIs_admin() {
                return is_admin;
            }

            public void setIs_admin(String is_admin) {
                this.is_admin = is_admin;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class ServicePersonnelsBean {
            /**
             * personnel_id : 7
             * name : Martha
             */

            private String personnel_id;
            private String name;

            public String getPersonnel_id() {
                return personnel_id;
            }

            public void setPersonnel_id(String personnel_id) {
                this.personnel_id = personnel_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public static class CartItemsBean {
            /**
             * cart_item_id : 313
             * item_id : 4
             * item_price_id : null
             * item_name : Color
             * item_code : COL#4
             * item_qty : 1
             * item_price : 20.00
             * item_type : S
             * quantity : 1
             */

            private String cart_item_id;
            private String item_id;
            private String item_price_id;
            private String item_name;
            private String item_code;
            private String item_qty;
            private String item_price;
            private String item_type;
            private String quantity;

            public String getCart_item_id() {
                return cart_item_id;
            }

            public void setCart_item_id(String cart_item_id) {
                this.cart_item_id = cart_item_id;
            }

            public String getItem_id() {
                return item_id;
            }

            public void setItem_id(String item_id) {
                this.item_id = item_id;
            }

            public String getItem_price_id() {
                return item_price_id;
            }

            public void setItem_price_id(String item_price_id) {
                this.item_price_id = item_price_id;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public String getItem_code() {
                return item_code;
            }

            public void setItem_code(String item_code) {
                this.item_code = item_code;
            }

            public String getItem_qty() {
                return item_qty;
            }

            public void setItem_qty(String item_qty) {
                this.item_qty = item_qty;
            }

            public String getItem_price() {
                return item_price;
            }

            public void setItem_price(String item_price) {
                this.item_price = item_price;
            }

            public String getItem_type() {
                return item_type;
            }

            public void setItem_type(String item_type) {
                this.item_type = item_type;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }
        }
    }
}
