package com.mydevit.pieservicess.service;


import java.util.List;

public class NotificationMainModel {

    /**
     * status : success
     * status_code : 200
     * message : Notification Configuration Setting Show Successfully
     * data : {"email_notify_data":{"notify_id":"2","business_id":"13","location_id":"10","email_trigger":"1","email_from":"gmghnhmn","email_subject":"ghnghj","email_reply":"ghjghjk","email_status":"Y","created_at":"2019-11-20 09:45:55","updated_at":"0000-00-00 00:00:00"},"sms_notify_data":{"notify_id":"3","business_id":"13","location_id":"10","sms_trigger":"3","sms_status":"Y","created_at":"2019-11-21 12:28:15","updated_at":"2019-11-25 08:38:12"},"email_reminder_data":[{"reminder_id":"9","notify_id":"2","reminder_time":"12:00 AM"},{"reminder_id":"10","notify_id":"2","reminder_time":"12:00 AM"},{"reminder_id":"11","notify_id":"2","reminder_time":"12:00 AM"},{"reminder_id":"12","notify_id":"2","reminder_time":"12:00 AM"},{"reminder_id":"13","notify_id":"2","reminder_time":"12:00 AM"}],"sms_reminder_data":[{"reminder_id":"15","notify_id":"3","reminder_time":"12:05 AM"},{"reminder_id":"16","notify_id":"3","reminder_time":"12:00 AM"},{"reminder_id":"17","notify_id":"3","reminder_time":"01:10 AM"}]}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * email_notify_data : {"notify_id":"2","business_id":"13","location_id":"10","email_trigger":"1","email_from":"gmghnhmn","email_subject":"ghnghj","email_reply":"ghjghjk","email_status":"Y","created_at":"2019-11-20 09:45:55","updated_at":"0000-00-00 00:00:00"}
         * sms_notify_data : {"notify_id":"3","business_id":"13","location_id":"10","sms_trigger":"3","sms_status":"Y","created_at":"2019-11-21 12:28:15","updated_at":"2019-11-25 08:38:12"}
         * email_reminder_data : [{"reminder_id":"9","notify_id":"2","reminder_time":"12:00 AM"},{"reminder_id":"10","notify_id":"2","reminder_time":"12:00 AM"},{"reminder_id":"11","notify_id":"2","reminder_time":"12:00 AM"},{"reminder_id":"12","notify_id":"2","reminder_time":"12:00 AM"},{"reminder_id":"13","notify_id":"2","reminder_time":"12:00 AM"}]
         * sms_reminder_data : [{"reminder_id":"15","notify_id":"3","reminder_time":"12:05 AM"},{"reminder_id":"16","notify_id":"3","reminder_time":"12:00 AM"},{"reminder_id":"17","notify_id":"3","reminder_time":"01:10 AM"}]
         */

        private EmailNotifyDataBean email_notify_data;
        private SmsNotifyDataBean sms_notify_data;
        private List<EmailReminderDataBean> email_reminder_data;
        private List<SmsReminderDataBean> sms_reminder_data;

        public EmailNotifyDataBean getEmail_notify_data() {
            return email_notify_data;
        }

        public void setEmail_notify_data(EmailNotifyDataBean email_notify_data) {
            this.email_notify_data = email_notify_data;
        }

        public SmsNotifyDataBean getSms_notify_data() {
            return sms_notify_data;
        }

        public void setSms_notify_data(SmsNotifyDataBean sms_notify_data) {
            this.sms_notify_data = sms_notify_data;
        }

        public List<EmailReminderDataBean> getEmail_reminder_data() {
            return email_reminder_data;
        }

        public void setEmail_reminder_data(List<EmailReminderDataBean> email_reminder_data) {
            this.email_reminder_data = email_reminder_data;
        }

        public List<SmsReminderDataBean> getSms_reminder_data() {
            return sms_reminder_data;
        }

        public void setSms_reminder_data(List<SmsReminderDataBean> sms_reminder_data) {
            this.sms_reminder_data = sms_reminder_data;
        }

        public static class EmailNotifyDataBean {
            /**
             * notify_id : 2
             * business_id : 13
             * location_id : 10
             * email_trigger : 1
             * email_from : gmghnhmn
             * email_subject : ghnghj
             * email_reply : ghjghjk
             * email_status : Y
             * created_at : 2019-11-20 09:45:55
             * updated_at : 0000-00-00 00:00:00
             */

            private String notify_id;
            private String business_id;
            private String location_id;
            private String email_trigger;
            private String email_from;
            private String email_subject;
            private String email_reply;
            private String email_status;
            private String created_at;
            private String updated_at;

            public String getNotify_id() {
                return notify_id;
            }

            public void setNotify_id(String notify_id) {
                this.notify_id = notify_id;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getEmail_trigger() {
                return email_trigger;
            }

            public void setEmail_trigger(String email_trigger) {
                this.email_trigger = email_trigger;
            }

            public String getEmail_from() {
                return email_from;
            }

            public void setEmail_from(String email_from) {
                this.email_from = email_from;
            }

            public String getEmail_subject() {
                return email_subject;
            }

            public void setEmail_subject(String email_subject) {
                this.email_subject = email_subject;
            }

            public String getEmail_reply() {
                return email_reply;
            }

            public void setEmail_reply(String email_reply) {
                this.email_reply = email_reply;
            }

            public String getEmail_status() {
                return email_status;
            }

            public void setEmail_status(String email_status) {
                this.email_status = email_status;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class SmsNotifyDataBean {
            /**
             * notify_id : 3
             * business_id : 13
             * location_id : 10
             * sms_trigger : 3
             * sms_status : Y
             * created_at : 2019-11-21 12:28:15
             * updated_at : 2019-11-25 08:38:12
             */

            private String notify_id;
            private String business_id;
            private String location_id;
            private String sms_trigger = "0";
            private String sms_status;
            private String created_at;
            private String updated_at;

            public String getNotify_id() {
                return notify_id;
            }

            public void setNotify_id(String notify_id) {
                this.notify_id = notify_id;
            }

            public String getBusiness_id() {
                return business_id;
            }

            public void setBusiness_id(String business_id) {
                this.business_id = business_id;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getSms_trigger() {
                return sms_trigger;
            }

            public void setSms_trigger(String sms_trigger) {
                this.sms_trigger = sms_trigger;
            }

            public String getSms_status() {
                return sms_status;
            }

            public void setSms_status(String sms_status) {
                this.sms_status = sms_status;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }

        public static class EmailReminderDataBean {
            /**
             * reminder_id : 9
             * notify_id : 2
             * reminder_time : 12:00 AM
             */

            private String reminder_id;
            private String notify_id;
            private String reminder_time = "";

            public String getReminder_id() {
                return reminder_id;
            }

            public void setReminder_id(String reminder_id) {
                this.reminder_id = reminder_id;
            }

            public String getNotify_id() {
                return notify_id;
            }

            public void setNotify_id(String notify_id) {
                this.notify_id = notify_id;
            }

            public String getReminder_time() {
                return reminder_time;
            }

            public void setReminder_time(String reminder_time) {
                this.reminder_time = reminder_time;
            }
        }

        public static class SmsReminderDataBean {
            /**
             * reminder_id : 15
             * notify_id : 3
             * reminder_time : 12:05 AM
             */

            private String reminder_id;
            private String notify_id;
            private String reminder_time;

            public String getReminder_id() {
                return reminder_id;
            }

            public void setReminder_id(String reminder_id) {
                this.reminder_id = reminder_id;
            }

            public String getNotify_id() {
                return notify_id;
            }

            public void setNotify_id(String notify_id) {
                this.notify_id = notify_id;
            }

            public String getReminder_time() {
                return reminder_time;
            }

            public void setReminder_time(String reminder_time) {
                this.reminder_time = reminder_time;
            }
        }
    }
}
