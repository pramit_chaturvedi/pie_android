package com.mydevit.pieservicess.service;

import java.util.List;

public class ConfigureAvailabiltyModel {


    /**
     * status : success
     * status_code : 200
     * message : Personnel Timings show Successfully
     * data : {"configure_availability":{"start_time":"09:00 AM","end_time":"05:00 PM"},"personnel":[{"personnel_id":"14","personnel_name":"Damien","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg","personnel_timings":[{"personnel_schedule_id":"939","day_num":"1","day_name":"Mon","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"},{"open_time":"12:05 AM","close_time":"12:20 AM"}]},{"personnel_schedule_id":"940","day_num":"2","day_name":"Tue","is_off":"Y","day_timings":[{"open_time":"12:00 AM","close_time":"12:00 AM"}]},{"personnel_schedule_id":"941","day_num":"3","day_name":"Wed","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"942","day_num":"4","day_name":"Thur","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"943","day_num":"5","day_name":"Fri","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"944","day_num":"6","day_name":"Sat","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"945","day_num":"7","day_name":"Sun","is_off":"Y","day_timings":[{"open_time":"12:00 AM","close_time":"12:00 AM"}]}]},{"personnel_id":"22","personnel_name":"test","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/80990fc4e1683afa9e014b2dc03e3403.jpg","personnel_timings":[{"personnel_schedule_id":"953","day_num":"1","day_name":"Mon","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"954","day_num":"2","day_name":"Tue","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"955","day_num":"3","day_name":"Wed","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"956","day_num":"4","day_name":"Thur","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"957","day_num":"5","day_name":"Fri","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"958","day_num":"6","day_name":"Sat","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"959","day_num":"7","day_name":"Sun","is_off":"Y","day_timings":[{"open_time":"12:00 AM","close_time":"12:00 AM"}]}]},{"personnel_id":"7","personnel_name":"Martha","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg","personnel_timings":[{"personnel_schedule_id":"960","day_num":"1","day_name":"Mon","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"961","day_num":"2","day_name":"Tue","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"962","day_num":"3","day_name":"Wed","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"963","day_num":"4","day_name":"Thur","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"964","day_num":"5","day_name":"Fri","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"965","day_num":"6","day_name":"Sat","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"966","day_num":"7","day_name":"Sun","is_off":"Y","day_timings":[{"open_time":"12:00 AM","close_time":"12:00 AM"}]}]},{"personnel_id":"20","personnel_name":"Jevel Santhana","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/1e42ebad59e070c146387f8a8dce40f4.jpg","personnel_timings":[{"personnel_schedule_id":"946","day_num":"1","day_name":"Mon","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"947","day_num":"2","day_name":"Tue","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"948","day_num":"3","day_name":"Wed","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"949","day_num":"4","day_name":"Thur","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"950","day_num":"5","day_name":"Fri","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"951","day_num":"6","day_name":"Sat","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"952","day_num":"7","day_name":"Sun","is_off":"Y","day_timings":[{"open_time":"12:00 AM","close_time":"12:00 AM"}]}]}]}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * configure_availability : {"start_time":"09:00 AM","end_time":"05:00 PM"}
         * personnel : [{"personnel_id":"14","personnel_name":"Damien","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg","personnel_timings":[{"personnel_schedule_id":"939","day_num":"1","day_name":"Mon","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"},{"open_time":"12:05 AM","close_time":"12:20 AM"}]},{"personnel_schedule_id":"940","day_num":"2","day_name":"Tue","is_off":"Y","day_timings":[{"open_time":"12:00 AM","close_time":"12:00 AM"}]},{"personnel_schedule_id":"941","day_num":"3","day_name":"Wed","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"942","day_num":"4","day_name":"Thur","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"943","day_num":"5","day_name":"Fri","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"944","day_num":"6","day_name":"Sat","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"945","day_num":"7","day_name":"Sun","is_off":"Y","day_timings":[{"open_time":"12:00 AM","close_time":"12:00 AM"}]}]},{"personnel_id":"22","personnel_name":"test","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/80990fc4e1683afa9e014b2dc03e3403.jpg","personnel_timings":[{"personnel_schedule_id":"953","day_num":"1","day_name":"Mon","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"954","day_num":"2","day_name":"Tue","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"955","day_num":"3","day_name":"Wed","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"956","day_num":"4","day_name":"Thur","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"957","day_num":"5","day_name":"Fri","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"958","day_num":"6","day_name":"Sat","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"959","day_num":"7","day_name":"Sun","is_off":"Y","day_timings":[{"open_time":"12:00 AM","close_time":"12:00 AM"}]}]},{"personnel_id":"7","personnel_name":"Martha","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg","personnel_timings":[{"personnel_schedule_id":"960","day_num":"1","day_name":"Mon","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"961","day_num":"2","day_name":"Tue","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"962","day_num":"3","day_name":"Wed","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"963","day_num":"4","day_name":"Thur","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"964","day_num":"5","day_name":"Fri","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"965","day_num":"6","day_name":"Sat","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"966","day_num":"7","day_name":"Sun","is_off":"Y","day_timings":[{"open_time":"12:00 AM","close_time":"12:00 AM"}]}]},{"personnel_id":"20","personnel_name":"Jevel Santhana","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/1e42ebad59e070c146387f8a8dce40f4.jpg","personnel_timings":[{"personnel_schedule_id":"946","day_num":"1","day_name":"Mon","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"947","day_num":"2","day_name":"Tue","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"948","day_num":"3","day_name":"Wed","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"949","day_num":"4","day_name":"Thur","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"950","day_num":"5","day_name":"Fri","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"951","day_num":"6","day_name":"Sat","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"952","day_num":"7","day_name":"Sun","is_off":"Y","day_timings":[{"open_time":"12:00 AM","close_time":"12:00 AM"}]}]}]
         */

        private ConfigureAvailabilityBean configure_availability;
        private List<PersonnelBean> personnel;

        public ConfigureAvailabilityBean getConfigure_availability() {
            return configure_availability;
        }

        public void setConfigure_availability(ConfigureAvailabilityBean configure_availability) {
            this.configure_availability = configure_availability;
        }

        public List<PersonnelBean> getPersonnel() {
            return personnel;
        }

        public void setPersonnel(List<PersonnelBean> personnel) {
            this.personnel = personnel;
        }

        public static class ConfigureAvailabilityBean {
            /**
             * start_time : 09:00 AM
             * end_time : 05:00 PM
             */

            private String start_time;
            private String end_time;

            public String getStart_time() {
                return start_time;
            }

            public void setStart_time(String start_time) {
                this.start_time = start_time;
            }

            public String getEnd_time() {
                return end_time;
            }

            public void setEnd_time(String end_time) {
                this.end_time = end_time;
            }
        }

        public static class PersonnelBean {
            /**
             * personnel_id : 14
             * personnel_name : Damien
             * personnel_image : https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg
             * personnel_timings : [{"personnel_schedule_id":"939","day_num":"1","day_name":"Mon","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"},{"open_time":"12:05 AM","close_time":"12:20 AM"}]},{"personnel_schedule_id":"940","day_num":"2","day_name":"Tue","is_off":"Y","day_timings":[{"open_time":"12:00 AM","close_time":"12:00 AM"}]},{"personnel_schedule_id":"941","day_num":"3","day_name":"Wed","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"942","day_num":"4","day_name":"Thur","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"943","day_num":"5","day_name":"Fri","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"944","day_num":"6","day_name":"Sat","is_off":"N","day_timings":[{"open_time":"09:00 AM","close_time":"11:30 PM"}]},{"personnel_schedule_id":"945","day_num":"7","day_name":"Sun","is_off":"Y","day_timings":[{"open_time":"12:00 AM","close_time":"12:00 AM"}]}]
             */

            private String personnel_id;
            private String personnel_name;
            private String personnel_image;
            private List<PersonnelTimingsBean> personnel_timings;

            public String getPersonnel_id() {
                return personnel_id;
            }

            public void setPersonnel_id(String personnel_id) {
                this.personnel_id = personnel_id;
            }

            public String getPersonnel_name() {
                return personnel_name;
            }

            public void setPersonnel_name(String personnel_name) {
                this.personnel_name = personnel_name;
            }

            public String getPersonnel_image() {
                return personnel_image;
            }

            public void setPersonnel_image(String personnel_image) {
                this.personnel_image = personnel_image;
            }

            public List<PersonnelTimingsBean> getPersonnel_timings() {
                return personnel_timings;
            }

            public void setPersonnel_timings(List<PersonnelTimingsBean> personnel_timings) {
                this.personnel_timings = personnel_timings;
            }

            public static class PersonnelTimingsBean {

                /**
                 * personnel_schedule_id : 939
                 * day_num : 1
                 * day_name : Mon
                 * is_off : N
                 * day_timings : [{"open_time":"09:00 AM","close_time":"11:30 PM"},{"open_time":"12:05 AM","close_time":"12:20 AM"}]
                 */

                private String personnel_schedule_id = "";
                private String day_num = "";
                private String day_name = "";
                private String isSelected = "";
                private String is_off = "";
                private List<DayTimingsBean> day_timings;

                public String getIsSelected() {
                    return isSelected;
                }

                public void setIsSelected(String isSelected) {
                    this.isSelected = isSelected;
                }

                public String getPersonnel_schedule_id() {
                    return personnel_schedule_id;
                }

                public void setPersonnel_schedule_id(String personnel_schedule_id) {
                    this.personnel_schedule_id = personnel_schedule_id;
                }

                public String getDay_num() {
                    return day_num;
                }

                public void setDay_num(String day_num) {
                    this.day_num = day_num;
                }

                public String getDay_name() {
                    return day_name;
                }

                public void setDay_name(String day_name) {
                    this.day_name = day_name;
                }

                public String getIs_off() {
                    return is_off;
                }

                public void setIs_off(String is_off) {
                    this.is_off = is_off;
                }

                public List<DayTimingsBean> getDay_timings() {
                    return day_timings;
                }

                public void setDay_timings(List<DayTimingsBean> day_timings) {
                    this.day_timings = day_timings;
                }

                public static class DayTimingsBean {
                    /**
                     * open_time : 09:00 AM
                     * close_time : 11:30 PM
                     */

                    private String open_time = "";
                    private String close_time = "";


                    public String getOpen_time() {
                        return open_time;
                    }

                    public void setOpen_time(String open_time) {
                        this.open_time = open_time;
                    }

                    public String getClose_time() {
                        return close_time;
                    }

                    public void setClose_time(String close_time) {
                        this.close_time = close_time;
                    }
                }
            }
        }
    }
}
