package com.mydevit.pieservicess.service;

import java.util.List;

public class ClientListModel {

    /**
     * status : success
     * status_code : 200
     * message : Client Data Show Successfully
     * data : [{"customer_id":"7","first_name":"jonson","last_name":"Kent","phone":"854-859-6589","email":"jonson@mailinator.com","status":"1"},{"customer_id":"10","first_name":"Joe","last_name":"Hastin","phone":"7845125896","email":"joe@hotmail.com","status":"1"},{"customer_id":"11","first_name":"Taylor","last_name":"Norsworthy","phone":"8594904844","email":"taywhit84@gmail.com","status":"1"},{"customer_id":"15","first_name":"jevel","last_name":"santana","phone":"4567891235","email":"jevel123@mailinator.com","status":"1"},{"customer_id":"17","first_name":"Jerry","last_name":"Pitts","phone":"555-555-5555","email":"jerrypitts@gmail.com","status":"1"},{"customer_id":"19","first_name":"Vantage","last_name":"Clara","phone":"7894565896","email":"vantageclara@mailinator.com","status":"1"},{"customer_id":"21","first_name":"Borris","last_name":"Gill","phone":"7845123659","email":"borris@mailinator.com","status":"1"},{"customer_id":"22","first_name":"Annie","last_name":"Johnson","phone":"4654475432","email":"annie@hotmail.com","status":"1"}]
     */

    private String status;
    private String status_code;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * customer_id : 7
         * first_name : jonson
         * last_name : Kent
         * phone : 854-859-6589
         * email : jonson@mailinator.com
         * status : 1
         */

        private String customer_id;
        private String first_name;
        private String last_name;
        private String phone;
        private String email;
        private String status;

        public String getCustomer_id() {
            return customer_id;
        }

        public void setCustomer_id(String customer_id) {
            this.customer_id = customer_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
