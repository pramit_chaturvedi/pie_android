package com.mydevit.pieservicess.service;

import java.util.List;

public class salesmodel {

    /**
     * status : success
     * status_code : 200
     * message : Business Sales Data Show Successfully
     * data : {"pie_sales_data":[{"personnel":"Damien","sale_price":"30.00"},{"personnel":"Charlie","sale_price":"83.00"},{"personnel":"test","sale_price":"810.00"}],"monthy_sales_data":null,"monthly_customers_data":2}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * pie_sales_data : [{"personnel":"Damien","sale_price":"30.00"},{"personnel":"Charlie","sale_price":"83.00"},{"personnel":"test","sale_price":"810.00"}]
         * monthy_sales_data : null
         * monthly_customers_data : 2
         */

        private String monthy_sales_data;
        private String monthly_customers_data;
        private List<PieSalesDataBean> pie_sales_data;

        public Object getMonthy_sales_data() {
            return monthy_sales_data;
        }

        public void setMonthy_sales_data(String monthy_sales_data) {
            this.monthy_sales_data = monthy_sales_data;
        }

        public String getMonthly_customers_data() {
            return monthly_customers_data;
        }

        public void setMonthly_customers_data(String monthly_customers_data) {
            this.monthly_customers_data = monthly_customers_data;
        }

        public List<PieSalesDataBean> getPie_sales_data() {
            return pie_sales_data;
        }

        public void setPie_sales_data(List<PieSalesDataBean> pie_sales_data) {
            this.pie_sales_data = pie_sales_data;
        }

        public static class PieSalesDataBean {
            /**
             * personnel : Damien
             * sale_price : 30.00
             */

            private String personnel;
            private String sale_price;

            public String getPersonnel() {
                return personnel;
            }

            public void setPersonnel(String personnel) {
                this.personnel = personnel;
            }

            public String getSale_price() {
                return sale_price;
            }

            public void setSale_price(String sale_price) {
                this.sale_price = sale_price;
            }
        }
    }
}
