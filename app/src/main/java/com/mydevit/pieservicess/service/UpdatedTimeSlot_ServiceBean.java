package com.mydevit.pieservicess.service;

public class UpdatedTimeSlot_ServiceBean {

    /**
     * status : success
     * status_code : 200
     * message : Personnel Appointment Updated Successfully
     * data : {"appointment_week":"48","appointment_date":"2019-11-25","appointment_year":"2019","start_time":"12:00:00","php_start_timestamp":1574424000,"end_time_expected":"13:00:00","php_end_expected_timestamp":1574427600,"end_time":"13:00:00","php_end_timestamp":1574427600,"updated_at":"2019-11-22 07:36:39"}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * appointment_week : 48
         * appointment_date : 2019-11-25
         * appointment_year : 2019
         * start_time : 12:00:00
         * php_start_timestamp : 1574424000
         * end_time_expected : 13:00:00
         * php_end_expected_timestamp : 1574427600
         * end_time : 13:00:00
         * php_end_timestamp : 1574427600
         * updated_at : 2019-11-22 07:36:39
         */

        private String appointment_week;
        private String appointment_date;
        private String appointment_year;
        private String start_time;
        private int php_start_timestamp;
        private String end_time_expected;
        private int php_end_expected_timestamp;
        private String end_time;
        private int php_end_timestamp;
        private String updated_at;

        public String getAppointment_week() {
            return appointment_week;
        }

        public void setAppointment_week(String appointment_week) {
            this.appointment_week = appointment_week;
        }

        public String getAppointment_date() {
            return appointment_date;
        }

        public void setAppointment_date(String appointment_date) {
            this.appointment_date = appointment_date;
        }

        public String getAppointment_year() {
            return appointment_year;
        }

        public void setAppointment_year(String appointment_year) {
            this.appointment_year = appointment_year;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public int getPhp_start_timestamp() {
            return php_start_timestamp;
        }

        public void setPhp_start_timestamp(int php_start_timestamp) {
            this.php_start_timestamp = php_start_timestamp;
        }

        public String getEnd_time_expected() {
            return end_time_expected;
        }

        public void setEnd_time_expected(String end_time_expected) {
            this.end_time_expected = end_time_expected;
        }

        public int getPhp_end_expected_timestamp() {
            return php_end_expected_timestamp;
        }

        public void setPhp_end_expected_timestamp(int php_end_expected_timestamp) {
            this.php_end_expected_timestamp = php_end_expected_timestamp;
        }

        public String getEnd_time() {
            return end_time;
        }

        public void setEnd_time(String end_time) {
            this.end_time = end_time;
        }

        public int getPhp_end_timestamp() {
            return php_end_timestamp;
        }

        public void setPhp_end_timestamp(int php_end_timestamp) {
            this.php_end_timestamp = php_end_timestamp;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
