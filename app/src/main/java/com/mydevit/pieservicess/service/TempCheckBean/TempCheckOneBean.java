package com.mydevit.pieservicess.service.TempCheckBean;

import java.util.List;

public class TempCheckOneBean {

    private String personnel_schedule_id = "";
    private String day_num = "";
    private String day_name = "";
    private String isSelected = "";
    private String is_off = "";
    private List<DayTimingsBean> day_timings;


    public static class DayTimingsBean {
        /**
         * open_time : 09:00 AM
         * close_time : 11:30 PM
         */

        private String open_time = "";
        private String close_time = "";


        public String getOpen_time() {
            return open_time;
        }

        public void setOpen_time(String open_time) {
            this.open_time = open_time;
        }

        public String getClose_time() {
            return close_time;
        }

        public void setClose_time(String close_time) {
            this.close_time = close_time;
        }
    }
}

