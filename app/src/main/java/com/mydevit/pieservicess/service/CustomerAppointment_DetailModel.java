package com.mydevit.pieservicess.service;

import java.util.List;

public class CustomerAppointment_DetailModel {
    /**
     * status : success
     * status_code : 200
     * message : Appointment Data Show Successfully
     * data : {"appointment_number":"#MATB10787","customer_name":"AnnieupdatedNow0000 Johnsonbonenow0000","appointment_date":"2019-11-06","appointment_duration":"0","appointment_notes":"","personnel_data":[{"personnel_id":"22","username":"test1234","name":"test","personnel_color":"#481313","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/80990fc4e1683afa9e014b2dc03e3403.jpg"}],"service_data":[{"service_id":"2","service_name":"Hair cut","service_price":"350.00","service_duration":"9000"},{"service_id":"1","service_name":"Shaving","service_price":"200.00","service_duration":"7800"}]}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * appointment_number : #MATB10787
         * customer_name : AnnieupdatedNow0000 Johnsonbonenow0000
         * appointment_date : 2019-11-06
         * appointment_duration : 0
         * appointment_notes :
         * personnel_data : [{"personnel_id":"22","username":"test1234","name":"test","personnel_color":"#481313","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/80990fc4e1683afa9e014b2dc03e3403.jpg"}]
         * service_data : [{"service_id":"2","service_name":"Hair cut","service_price":"350.00","service_duration":"9000"},{"service_id":"1","service_name":"Shaving","service_price":"200.00","service_duration":"7800"}]
         */
        private String appointment_number;
        private String customer_name;
        private String appointment_date;
        private String appointment_duration;
        private String appointment_notes;
        private List<PersonnelDataBean> personnel_data;
        private List<ServiceDataBean> service_data;

        public String getAppointment_number() {
            return appointment_number;
        }

        public void setAppointment_number(String appointment_number) {
            this.appointment_number = appointment_number;
        }

        public String getCustomer_name() {
            return customer_name;
        }

        public void setCustomer_name(String customer_name) {
            this.customer_name = customer_name;
        }

        public String getAppointment_date() {
            return appointment_date;
        }

        public void setAppointment_date(String appointment_date) {
            this.appointment_date = appointment_date;
        }

        public String getAppointment_duration() {
            return appointment_duration;
        }

        public void setAppointment_duration(String appointment_duration) {
            this.appointment_duration = appointment_duration;
        }

        public String getAppointment_notes() {
            return appointment_notes;
        }

        public void setAppointment_notes(String appointment_notes) {
            this.appointment_notes = appointment_notes;
        }

        public List<PersonnelDataBean> getPersonnel_data() {
            return personnel_data;
        }

        public void setPersonnel_data(List<PersonnelDataBean> personnel_data) {
            this.personnel_data = personnel_data;
        }

        public List<ServiceDataBean> getService_data() {
            return service_data;
        }

        public void setService_data(List<ServiceDataBean> service_data) {
            this.service_data = service_data;
        }

        public static class PersonnelDataBean {
            /**
             * personnel_id : 22
             * username : test1234
             * name : test
             * personnel_color : #481313
             * personnel_image : https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/80990fc4e1683afa9e014b2dc03e3403.jpg
             */

            private String personnel_id;
            private String username;
            private String name;
            private String personnel_color;
            private String personnel_image;

            public String getPersonnel_id() {
                return personnel_id;
            }

            public void setPersonnel_id(String personnel_id) {
                this.personnel_id = personnel_id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPersonnel_color() {
                return personnel_color;
            }

            public void setPersonnel_color(String personnel_color) {
                this.personnel_color = personnel_color;
            }

            public String getPersonnel_image() {
                return personnel_image;
            }

            public void setPersonnel_image(String personnel_image) {
                this.personnel_image = personnel_image;
            }
        }

        public static class ServiceDataBean {
            /**
             * service_id : 2
             * service_name : Hair cut
             * service_price : 350.00
             * service_duration : 9000
             */
            private String service_id;
            private String service_name;
            private String service_price;
            private String service_duration;

            public String getService_id() {
                return service_id;
            }

            public void setService_id(String service_id) {
                this.service_id = service_id;
            }

            public String getService_name() {
                return service_name;
            }

            public void setService_name(String service_name) {
                this.service_name = service_name;
            }

            public String getService_price() {
                return service_price;
            }

            public void setService_price(String service_price) {
                this.service_price = service_price;
            }

            public String getService_duration() {
                return service_duration;
            }

            public void setService_duration(String service_duration) {
                this.service_duration = service_duration;
            }
        }
    }
}
