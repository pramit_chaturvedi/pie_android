package com.mydevit.pieservicess.service;

public class SaveSlotModel {

    /**
     * status : success
     * status_code : 200
     * message : Personnel Appointment Saved Successfully
     * data : {"business_id":"13","location_id":"10","personnel_id":"14","week_number":"49","week_date":"1970-01-07","day":"7","is_off":"N","schedule_year":"2019"}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * business_id : 13
         * location_id : 10
         * personnel_id : 14
         * week_number : 49
         * week_date : 1970-01-07
         * day : 7
         * is_off : N
         * schedule_year : 2019
         */

        private String business_id;
        private String location_id;
        private String personnel_id;
        private String week_number;
        private String week_date;
        private String day;
        private String is_off;
        private String schedule_year;

        public String getBusiness_id() {
            return business_id;
        }

        public void setBusiness_id(String business_id) {
            this.business_id = business_id;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getPersonnel_id() {
            return personnel_id;
        }

        public void setPersonnel_id(String personnel_id) {
            this.personnel_id = personnel_id;
        }

        public String getWeek_number() {
            return week_number;
        }

        public void setWeek_number(String week_number) {
            this.week_number = week_number;
        }

        public String getWeek_date() {
            return week_date;
        }

        public void setWeek_date(String week_date) {
            this.week_date = week_date;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public String getIs_off() {
            return is_off;
        }

        public void setIs_off(String is_off) {
            this.is_off = is_off;
        }

        public String getSchedule_year() {
            return schedule_year;
        }

        public void setSchedule_year(String schedule_year) {
            this.schedule_year = schedule_year;
        }
    }
}
