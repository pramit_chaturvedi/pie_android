package com.mydevit.pieservicess.service;

import java.util.List;

public class Slot_Time_Model {


    /**
     * status : success
     * status_code : 200
     * message : Personnel Service Time Slot Show Successfully
     * data : [[{"personnel_schedule_id":"655","personnel_id":"14","personnel_slot":["11:02 AM - 11:12 AM","11:12 AM - 11:22 AM","11:22 AM - 11:32 AM","11:32 AM - 11:42 AM","11:42 AM - 11:52 AM","11:52 AM - 12:02 PM","12:02 PM - 12:12 PM","12:12 PM - 12:22 PM","12:22 PM - 12:32 PM","12:32 PM - 12:42 PM","12:42 PM - 12:52 PM","12:52 PM - 01:02 PM","01:02 PM - 01:12 PM","01:12 PM - 01:22 PM","01:22 PM - 01:32 PM","01:32 PM - 01:42 PM","01:42 PM - 01:52 PM","01:52 PM - 02:02 PM","02:02 PM - 02:12 PM","02:12 PM - 02:22 PM","02:22 PM - 02:32 PM","02:32 PM - 02:42 PM","02:42 PM - 02:52 PM","02:52 PM - 03:02 PM","03:02 PM - 03:12 PM","03:12 PM - 03:22 PM","03:22 PM - 03:32 PM","03:32 PM - 03:42 PM","03:42 PM - 03:52 PM","03:52 PM - 04:02 PM","04:02 PM - 04:12 PM","04:12 PM - 04:22 PM","04:22 PM - 04:32 PM","04:32 PM - 04:42 PM","04:42 PM - 04:52 PM","04:52 PM - 05:02 PM","05:02 PM - 05:12 PM","05:12 PM - 05:22 PM","05:22 PM - 05:32 PM","05:32 PM - 05:42 PM","05:42 PM - 05:52 PM","05:52 PM - 06:02 PM","06:02 PM - 06:12 PM","06:12 PM - 06:22 PM","06:22 PM - 06:32 PM","06:32 PM - 06:42 PM","06:42 PM - 06:52 PM","06:52 PM - 07:02 PM","07:02 PM - 07:12 PM","07:12 PM - 07:22 PM","07:22 PM - 07:32 PM","07:32 PM - 07:42 PM","07:42 PM - 07:52 PM","07:52 PM - 08:02 PM","08:02 PM - 08:12 PM","08:12 PM - 08:22 PM","08:22 PM - 08:32 PM","08:32 PM - 08:42 PM","08:42 PM - 08:52 PM","08:52 PM - 09:02 PM","09:02 PM - 09:12 PM","09:12 PM - 09:22 PM","09:22 PM - 09:32 PM","09:32 PM - 09:42 PM","09:42 PM - 09:52 PM","09:52 PM - 10:02 PM","10:02 PM - 10:12 PM","10:12 PM - 10:22 PM","10:22 PM - 10:32 PM","10:32 PM - 10:42 PM","10:42 PM - 10:52 PM","10:52 PM - 11:02 PM","11:02 PM - 11:12 PM","11:12 PM - 11:22 PM"],"time_in":"11:02","time_out":"23:30"}]]
     */

    private String status;
    private String status_code;
    private String message;
    private List<List<DataBean>> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<List<DataBean>> getData() {
        return data;
    }

    public void setData(List<List<DataBean>> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * personnel_schedule_id : 655
         * personnel_id : 14
         * personnel_slot : ["11:02 AM - 11:12 AM","11:12 AM - 11:22 AM","11:22 AM - 11:32 AM","11:32 AM - 11:42 AM","11:42 AM - 11:52 AM","11:52 AM - 12:02 PM","12:02 PM - 12:12 PM","12:12 PM - 12:22 PM","12:22 PM - 12:32 PM","12:32 PM - 12:42 PM","12:42 PM - 12:52 PM","12:52 PM - 01:02 PM","01:02 PM - 01:12 PM","01:12 PM - 01:22 PM","01:22 PM - 01:32 PM","01:32 PM - 01:42 PM","01:42 PM - 01:52 PM","01:52 PM - 02:02 PM","02:02 PM - 02:12 PM","02:12 PM - 02:22 PM","02:22 PM - 02:32 PM","02:32 PM - 02:42 PM","02:42 PM - 02:52 PM","02:52 PM - 03:02 PM","03:02 PM - 03:12 PM","03:12 PM - 03:22 PM","03:22 PM - 03:32 PM","03:32 PM - 03:42 PM","03:42 PM - 03:52 PM","03:52 PM - 04:02 PM","04:02 PM - 04:12 PM","04:12 PM - 04:22 PM","04:22 PM - 04:32 PM","04:32 PM - 04:42 PM","04:42 PM - 04:52 PM","04:52 PM - 05:02 PM","05:02 PM - 05:12 PM","05:12 PM - 05:22 PM","05:22 PM - 05:32 PM","05:32 PM - 05:42 PM","05:42 PM - 05:52 PM","05:52 PM - 06:02 PM","06:02 PM - 06:12 PM","06:12 PM - 06:22 PM","06:22 PM - 06:32 PM","06:32 PM - 06:42 PM","06:42 PM - 06:52 PM","06:52 PM - 07:02 PM","07:02 PM - 07:12 PM","07:12 PM - 07:22 PM","07:22 PM - 07:32 PM","07:32 PM - 07:42 PM","07:42 PM - 07:52 PM","07:52 PM - 08:02 PM","08:02 PM - 08:12 PM","08:12 PM - 08:22 PM","08:22 PM - 08:32 PM","08:32 PM - 08:42 PM","08:42 PM - 08:52 PM","08:52 PM - 09:02 PM","09:02 PM - 09:12 PM","09:12 PM - 09:22 PM","09:22 PM - 09:32 PM","09:32 PM - 09:42 PM","09:42 PM - 09:52 PM","09:52 PM - 10:02 PM","10:02 PM - 10:12 PM","10:12 PM - 10:22 PM","10:22 PM - 10:32 PM","10:32 PM - 10:42 PM","10:42 PM - 10:52 PM","10:52 PM - 11:02 PM","11:02 PM - 11:12 PM","11:12 PM - 11:22 PM"]
         * time_in : 11:02
         * time_out : 23:30
         */

        private String personnel_schedule_id;
        private String personnel_id;
        private String time_in;
        private String time_out;
        private String message;
        private List<String> personnel_slot;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getPersonnel_schedule_id() {
            return personnel_schedule_id;
        }

        public void setPersonnel_schedule_id(String personnel_schedule_id) {
            this.personnel_schedule_id = personnel_schedule_id;
        }

        public String getPersonnel_id() {
            return personnel_id;
        }

        public void setPersonnel_id(String personnel_id) {
            this.personnel_id = personnel_id;
        }

        public String getTime_in() {
            return time_in;
        }

        public void setTime_in(String time_in) {
            this.time_in = time_in;
        }

        public String getTime_out() {
            return time_out;
        }

        public void setTime_out(String time_out) {
            this.time_out = time_out;
        }

        public List<String> getPersonnel_slot() {
            return personnel_slot;
        }

        public void setPersonnel_slot(List<String> personnel_slot) {
            this.personnel_slot = personnel_slot;
        }
    }
}
