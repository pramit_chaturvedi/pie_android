package com.mydevit.pieservicess.service;

import java.util.List;

public class personal_fragment_data_model {

    /**
     * status : success
     * status_code : 200
     * message : Personnels Show Successfully
     * data : [{"personnel_id":"14","personnel_name":"Damien","username":"dam_en","bio":"This is damien the barber lore ipsum for my text","email":"dam@mailinator.com","mobile":"8542125698","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg","role_name":"Personnel","is_active":"Active"},{"personnel_id":"20","personnel_name":"Jevel Santhana","username":"jevel_sant","bio":"test personnel","email":"jevel@hotmail.com","mobile":"4578965214","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/473648bfd36bead8be70ec8f9ee50c39.jpg","role_name":"Personnel","is_active":"Active"},{"personnel_id":"22","personnel_name":"test","username":"test1234","bio":"","email":"test@gmail.com","mobile":"5654367890","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/80990fc4e1683afa9e014b2dc03e3403.jpg","role_name":"Personnel","is_active":"Active"},{"personnel_id":"7","personnel_name":"Martha","username":"martha_barb","bio":"Lorem Ipsum dollet amit","email":"martha@gmail.com","mobile":"7418529658","personnel_image":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg","role_name":"Personnel","is_active":"Active"}]
     */

    private String status;
    private String status_code;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * personnel_id : 14
         * personnel_name : Damien
         * username : dam_en
         * bio : This is damien the barber lore ipsum for my text
         * email : dam@mailinator.com
         * mobile : 8542125698
         * personnel_image : https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg
         * role_name : Personnel
         * is_active : Active
         */

        private String personnel_id;
        private String personnel_name;
        private String username;
        private String bio;
        private String email;
        private String mobile;
        private String personnel_image;
        private String role_name;
        private String is_active;

        public String getPersonnel_id() {
            return personnel_id;
        }

        public void setPersonnel_id(String personnel_id) {
            this.personnel_id = personnel_id;
        }

        public String getPersonnel_name() {
            return personnel_name;
        }

        public void setPersonnel_name(String personnel_name) {
            this.personnel_name = personnel_name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getPersonnel_image() {
            return personnel_image;
        }

        public void setPersonnel_image(String personnel_image) {
            this.personnel_image = personnel_image;
        }

        public String getRole_name() {
            return role_name;
        }

        public void setRole_name(String role_name) {
            this.role_name = role_name;
        }

        public String getIs_active() {
            return is_active;
        }

        public void setIs_active(String is_active) {
            this.is_active = is_active;
        }
    }
}
