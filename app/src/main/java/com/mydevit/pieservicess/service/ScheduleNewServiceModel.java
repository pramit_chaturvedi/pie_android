package com.mydevit.pieservicess.service;

import java.util.List;

public class ScheduleNewServiceModel {

    /**
     * status : success
     * status_code : 200
     * message : Personnel Services show Successfully
     * data : {"personnel_data":[{"personnel_id":"14","personnel_name":"Damien","personnel_image_url":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg","personnel_service":[{"personnel_id":"14","service_id":"1","service_name":"Shaving","service_price":"20.00","service_duration":10},{"personnel_id":"14","service_id":"2","service_name":"Hair cut","service_price":"20.00","service_duration":10},{"personnel_id":"14","service_id":"3","service_name":"Spa","service_price":"20.00","service_duration":10},{"personnel_id":"14","service_id":"4","service_name":"Color","service_price":"20.00","service_duration":10},{"personnel_id":"14","service_id":"5","service_name":"Curly Hair","service_price":"20.00","service_duration":10},{"personnel_id":"14","service_id":"6","service_name":"Nail Polish","service_price":"20.00","service_duration":10}],"booked_aptmt":[]},{"personnel_id":"22","personnel_name":"test","personnel_image_url":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/80990fc4e1683afa9e014b2dc03e3403.jpg","personnel_service":[{"personnel_id":"22","service_id":"1","service_name":"Shaving","service_price":"200.00","service_duration":70},{"personnel_id":"22","service_id":"2","service_name":"Hair cut","service_price":"350.00","service_duration":70}],"booked_aptmt":[]},{"personnel_id":"7","personnel_name":"Martha","personnel_image_url":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/3da657cb96cba1c48937550b28df117d.jpg","personnel_service":[{"personnel_id":"7","service_id":"1","service_name":"Shaving","service_price":"10.00","service_duration":10},{"personnel_id":"7","service_id":"2","service_name":"Hair cut","service_price":"20.00","service_duration":10},{"personnel_id":"7","service_id":"3","service_name":"Spa","service_price":"20.00","service_duration":10}],"booked_aptmt":[]},{"personnel_id":"20","personnel_name":"Jevel Santhana","personnel_image_url":"https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/1e42ebad59e070c146387f8a8dce40f4.jpg","personnel_service":[],"booked_aptmt":[]}],"customer_data":[{"id":"22","label":"Annie Johnson"},{"id":"24","label":"Davrer True"},{"id":"26","label":" "},{"id":"27","label":"test test"}]}
     */

    private String status;
    private String status_code;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<PersonnelDataBean> personnel_data;
        private List<CustomerDataBean> customer_data;

        public List<PersonnelDataBean> getPersonnel_data() {
            return personnel_data;
        }

        public void setPersonnel_data(List<PersonnelDataBean> personnel_data) {
            this.personnel_data = personnel_data;
        }

        public List<CustomerDataBean> getCustomer_data() {
            return customer_data;
        }

        public void setCustomer_data(List<CustomerDataBean> customer_data) {
            this.customer_data = customer_data;
        }

        public static class PersonnelDataBean {
            /**
             * personnel_id : 14
             * personnel_name : Damien
             * personnel_image_url : https://storage.googleapis.com/piesvc_gcp/public/business_data/13/location/863ca329dc0be341b404b7c08d6e0a61.jpg
             * personnel_service : [{"personnel_id":"14","service_id":"1","service_name":"Shaving","service_price":"20.00","service_duration":10},{"personnel_id":"14","service_id":"2","service_name":"Hair cut","service_price":"20.00","service_duration":10},{"personnel_id":"14","service_id":"3","service_name":"Spa","service_price":"20.00","service_duration":10},{"personnel_id":"14","service_id":"4","service_name":"Color","service_price":"20.00","service_duration":10},{"personnel_id":"14","service_id":"5","service_name":"Curly Hair","service_price":"20.00","service_duration":10},{"personnel_id":"14","service_id":"6","service_name":"Nail Polish","service_price":"20.00","service_duration":10}]
             * booked_aptmt : []
             */

            private String personnel_id;
            private String personnel_name;
            private String personnel_image_url;
            private String isSelected = "N";
            private List<PersonnelServiceBean> personnel_service;
            private List<BookedAptmtBean> booked_aptmt;

            public String getIsSelected() {
                return isSelected;
            }

            public void setIsSelected(String isSelected) {
                this.isSelected = isSelected;
            }

            public String getPersonnel_id() {
                return personnel_id;
            }

            public void setPersonnel_id(String personnel_id) {
                this.personnel_id = personnel_id;
            }

            public String getPersonnel_name() {
                return personnel_name;
            }

            public void setPersonnel_name(String personnel_name) {
                this.personnel_name = personnel_name;
            }

            public String getPersonnel_image_url() {
                return personnel_image_url;
            }

            public void setPersonnel_image_url(String personnel_image_url) {
                this.personnel_image_url = personnel_image_url;
            }

            public List<PersonnelServiceBean> getPersonnel_service() {
                return personnel_service;
            }

            public void setPersonnel_service(List<PersonnelServiceBean> personnel_service) {
                this.personnel_service = personnel_service;
            }

            public List<BookedAptmtBean> getBooked_aptmt() {
                return booked_aptmt;
            }

            public void setBooked_aptmt(List<BookedAptmtBean> booked_aptmt) {
                this.booked_aptmt = booked_aptmt;
            }

            public static class BookedAptmtBean {
                private String client_name;
                private String personnel_name;
                private String service_names;
                private String start_time;
                private String end_time;

                public String getClient_name() {
                    return client_name;
                }

                public void setClient_name(String client_name) {
                    this.client_name = client_name;
                }

                public String getPersonnel_name() {
                    return personnel_name;
                }

                public void setPersonnel_name(String personnel_name) {
                    this.personnel_name = personnel_name;
                }

                public String getService_names() {
                    return service_names;
                }

                public void setService_names(String service_names) {
                    this.service_names = service_names;
                }

                public String getStart_time() {
                    return start_time;
                }

                public void setStart_time(String start_time) {
                    this.start_time = start_time;
                }

                public String getEnd_time() {
                    return end_time;
                }

                public void setEnd_time(String end_time) {
                    this.end_time = end_time;
                }
            }

            public static class PersonnelServiceBean {
                /**
                 * personnel_id : 14
                 * service_id : 1
                 * service_name : Shaving
                 * service_price : 20.00
                 * service_duration : 10
                 */

                private String personnel_id;
                private String service_id;
                private String service_name;
                private String service_price;
                private String service_duration;
                private String isSelected;


                public String getIsSelected() {
                    return isSelected;
                }

                public void setIsSelected(String isSelected) {
                    this.isSelected = isSelected;
                }

                public String getPersonnel_id() {
                    return personnel_id;
                }

                public void setPersonnel_id(String personnel_id) {
                    this.personnel_id = personnel_id;
                }

                public String getService_id() {
                    return service_id;
                }

                public void setService_id(String service_id) {
                    this.service_id = service_id;
                }

                public String getService_name() {
                    return service_name;
                }

                public void setService_name(String service_name) {
                    this.service_name = service_name;
                }

                public String getService_price() {
                    return service_price;
                }

                public void setService_price(String service_price) {
                    this.service_price = service_price;
                }

                public String getService_duration() {
                    return service_duration;
                }

                public void setService_duration(String service_duration) {
                    this.service_duration = service_duration;
                }
            }
        }

        public static class CustomerDataBean {
            /**
             * id : 22
             * label : Annie Johnson
             */

            private String id;
            private String label;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getLabel() {
                return label;
            }

            public void setLabel(String label) {
                this.label = label;
            }
        }
    }
}
