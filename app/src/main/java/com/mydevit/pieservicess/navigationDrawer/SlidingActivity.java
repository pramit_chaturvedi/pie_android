package com.mydevit.pieservicess.navigationDrawer;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.mydevit.pieservicess.Fragments.POS_SetUp_Fragment;
import com.mydevit.pieservicess.Fragments.SchedularFragment;
import com.mydevit.pieservicess.R;

public class SlidingActivity extends SlidingFragmentActivity implements View.OnClickListener {

    public static Activity activity;
    public TextView titleTextView;
    ActionBar actionBar;
    ImageView drawerToggleImageView;
    LinearLayout parentLinLayout;
    String haveToSenPos;


    private Handler mWaitHandler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sliding);
        activity = SlidingActivity.this;

        getIntentData();
        SetBehindView();
        Initialization();
        ClickListner();
        LoadFirst();
    }

    private void getIntentData() {
        if (getIntent().hasExtra("SENDTOPOS")) {
            haveToSenPos = getIntent().getExtras().getString("SENDTOPOS");
        }
    }

    public void Initialization() {
        SlidingMenu sm = getSlidingMenu();
        sm.setShadowWidthRes(R.dimen.shadow_width);
        sm.setShadowDrawable(R.drawable.shadow);
        sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        sm.setFadeDegree(0.35f);
        sm.setTouchModeAbove(SlidingMenu.LEFT);
        actionBar = getActionBar();

        drawerToggleImageView = (ImageView) findViewById(R.id.drawerToggleImageViewId);
        titleTextView = (TextView) findViewById(R.id.titleTextViewId);

    }

    public void ClickListner() {
        drawerToggleImageView.setOnClickListener(this);

    }

    private void SetBehindView() {
        setBehindContentView(R.layout.menu_slide);
        transactionFragments(MenuFragment.newInstance(), R.id.menu_slide);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                toggle();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void LoadFirst() {
        if (haveToSenPos != null && "YES".equals(haveToSenPos)) {
            titleTextView.setText(getResources().getString(R.string.possetup));
            transactionFragments(new POS_SetUp_Fragment(), R.id.container);
        } else {

            titleTextView.setText(getResources().getString(R.string.scheduler));
            transactionFragments(new SchedularFragment(), R.id.container);

          /*  titleTextView.setText(getResources().getString(R.string.dashboard));
            transactionFragments(new DashboardTab_Fragment(), R.id.container);*/
        }

    }

    public void transactionFragments(Fragment fragment, int viewResource) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(viewResource, fragment);
        ft.commit();
        toggle();
        mWaitHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (null != MenuFragment.adapter)
                        MenuFragment.adapter.notifyDataSetChanged();

                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
            }
        }, 500);  // Give a 5 seconds delay.
    }
    public void transactionFragmentsFromOther(Fragment fragment, int viewResource) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(viewResource, fragment);
        ft.addToBackStack(null);
        ft.commit();
        mWaitHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (null != MenuFragment.adapter)
                        MenuFragment.adapter.notifyDataSetChanged();

                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
            }
        }, 500);  // Give a 5 seconds delay.
    }

    @Override
    public void onClick(View v) {
        if (v == drawerToggleImageView) {
            SlidingMenu sm = getSlidingMenu();
            toggle();
        }

    }
}
