package com.mydevit.pieservicess.navigationDrawer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.Fragments.AccountsAndSettingsFragment;
import com.mydevit.pieservicess.Fragments.CustomerManagement_Fragment;
import com.mydevit.pieservicess.Fragments.DashboardTab_Fragment;
import com.mydevit.pieservicess.Fragments.POS_SetUp_Fragment;
import com.mydevit.pieservicess.Fragments.ReportTab_Fragment;
import com.mydevit.pieservicess.Fragments.SchedularFragment;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.activities.SignInActivity;
import com.mydevit.pieservicess.databinding.FragmentMenuBinding;
import com.mydevit.pieservicess.databinding.ItemMenuBinding;
import com.mydevit.pieservicess.databinding.LogoutdialogDesignBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.OnSwipeTouchListener;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import java.util.ArrayList;

/**
 * Created by Rishi Ojha.
 */
public class MenuFragment extends Fragment implements ClickListener {

    private final static String TAG = "MenuFragment";
    public static SlidingMenuAdapter adapter;
    LinearLayout parentLinLayout;
    Activity activity;
    FragmentMenuBinding fragmentMenuBinding;
    Context context;
    SharedPreferences_Util sharedPreferences_util;
    AlertDialog alertDialog;
    LogoutdialogDesignBinding logoutdialogDesignBinding;
    private View rootView;
    private RecyclerView navigationDrawerReecylerView;
    public static ArrayList<SlidingMenuItem> listMenuItems;

    public static Fragment newInstance() {
        MenuFragment fragment = new MenuFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //create menu items
        sharedPreferences_util = new SharedPreferences_Util(context);

        listMenuItems = new ArrayList<SlidingMenuItem>();
        listMenuItems.add(new SlidingMenuItem(R.mipmap.dashboard, getResources().getString(R.string.dashboard), false));
        listMenuItems.add(new SlidingMenuItem(R.mipmap.customer_management, getResources().getString(R.string.customermanagemtn), false));
        /*listMenuItems.add(new SlidingMenuItem(R.mipmap.location, getResources().getString(R.string.locationmanagement), false));*/
        /*   listMenuItems.add(new SlidingMenuItem(R.mipmap.personel, getResources().getString(R.string.personelmanagemtn), false));*/
        /*    listMenuItems.add(new SlidingMenuItem(R.mipmap.inventory_management, getResources().getString(R.string.inventorylmanagemtn), false));*/
     /*   listMenuItems.add(new SlidingMenuItem(R.mipmap.app_edit, getResources().getString(R.string.appedit), false));
        listMenuItems.add(new SlidingMenuItem(R.mipmap.website_edit, getResources().getString(R.string.websiteedit), false));*/
        listMenuItems.add(new SlidingMenuItem(R.mipmap.schedular, getResources().getString(R.string.scheduler), false));
//      listMenuItems.add(new SlidingMenuItem(R.mipmap.report, getResources().getString(R.string.reports), false));
        /*  listMenuItems.add(new SlidingMenuItem(R.mipmap.marketing, getResources().getString(R.string.marketing), false));*/
        listMenuItems.add(new SlidingMenuItem(R.mipmap.posserup, getResources().getString(R.string.possetup), false));
        listMenuItems.add(new SlidingMenuItem(R.mipmap.account_setting, getResources().getString(R.string.accountandsettings), false));
        listMenuItems.add(new SlidingMenuItem(R.mipmap.logout, getResources().getString(R.string.logout), false));

    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMenuBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu, container, false);
        // navigationDrawerReecylerView = rootView.findViewById(R.id.navigationDraRecyViewId);
      /*  parentLinLayout = (LinearLayout)rootView.findViewById(R.id.parentLinLayoutId);iconImageViewId

        if(AlphaHolder.deviceName.equalsIgnoreCase("MotoG3-TE")){
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, 0, 0, 95);
            parentLinLayout.setLayoutParams(layoutParams);
        }
        else{
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, 0, 0, 0);
            parentLinLayout.setLayoutParams(layoutParams);
        }*/
        return fragmentMenuBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListViewAdapter();
    }

    private void setListViewAdapter() {
       /* navigationDrawerReecylerView.setHasFixedSize(true);
        navigationDrawerReecylerView.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
      */
        adapter = new SlidingMenuAdapter(activity, R.layout.item_menu, listMenuItems, this);
        fragmentMenuBinding.setMenuAdapter(adapter);
    }

    /**
     * Go to fragment when menu item clicked!
     *
     * @return
     */
    public void updateMenuItems(int position) {
        for (int i = 0; i < listMenuItems.size(); i++) {
            if (i == position) listMenuItems.get(i).setClicked(true);
            else listMenuItems.get(i).setClicked(false);
        }
    }

    public void ReplaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("MENU_ITEM_CLICK".equals(tag_FromWhere)) {
            if (position == 0) {
                updateMenuItems(position);
                ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.dashboard));
                ((SlidingActivity) getActivity()).transactionFragments(new DashboardTab_Fragment(), R.id.container);
            }
            if (position == 1) {
                updateMenuItems(position);
                ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.customermanagemtn));
                ((SlidingActivity) getActivity()).transactionFragments(new CustomerManagement_Fragment(), R.id.container);
            }
            if (position == 2) {
                updateMenuItems(position);
                ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.scheduler));
                ((SlidingActivity) getActivity()).transactionFragments(new SchedularFragment(), R.id.container);

                /* updateMenuItems(position);*/
               /* ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.locationmanagement));
                ((SlidingActivity) getActivity()).transactionFragments(new DashboardTab_Fragment(), R.id.container);*/
            }
            if (position == 3) {
                updateMenuItems(position);
                ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.possetup));
                ((SlidingActivity) getActivity()).transactionFragments(new POS_SetUp_Fragment(), R.id.container);

                /*((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.personelmanagemtn));
                updateMenuItems(position);*/
            }
            if (position == 4) {

                updateMenuItems(position);
                ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.accountandsettings));
                ((SlidingActivity) getActivity()).transactionFragments(new AccountsAndSettingsFragment(), R.id.container);
                /*((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.inventorylmanagemtn));
                updateMenuItems(position);*/
            }
            if (position == 5) {
                logoutDialog();

            }
            if (position == 6) {
                ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.websiteedit));
                updateMenuItems(position);
            }

            if (position == 5) {
                ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.scheduler));
                updateMenuItems(position);

            }
            if (position == 6) {
                updateMenuItems(position);
                ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.reports));
                ((SlidingActivity) getActivity()).transactionFragments(new ReportTab_Fragment(), R.id.container);

            }
            if (position == 7) {
                ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.accountandsettings));
                updateMenuItems(position);

            }
            if (position == 8) {
                ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.marketing));
                updateMenuItems(position);

            }
            if (position == 9) {
                updateMenuItems(position);
                ((SlidingActivity) getActivity()).titleTextView.setText(getResources().getString(R.string.possetup));
                ((SlidingActivity) getActivity()).transactionFragments(new POS_SetUp_Fragment(), R.id.container);

            }
            if (position == 10) {
             /*   sharedPreferences_util.logout();
                AlphaHolder.customToast(context, getResources().getString(R.string.logout_successfully));
                Intent intent = new Intent(context, SignInActivity.class);
                startActivity(intent);
                ((Activity)context).overridePendingTransition(0,0);*/
            }
        }
        if ("SLIDED_LEFT".equals(tag_FromWhere)) {
            ((SlidingActivity) getActivity()).toggle();
        }
        if ("Cancel".equals(tag_FromWhere)) {
            alertDialog.dismiss();
        }
        if ("Yes".equals(tag_FromWhere)) {
            sharedPreferences_util.logout();
            AlphaHolder.customToast(context, getResources().getString(R.string.logout_successfully));
            Intent intent = new Intent(context, SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            ((Activity) context).overridePendingTransition(0, 0);

        }
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    public void logoutDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);

        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        logoutdialogDesignBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.logoutdialog_design, viewGroup, false);
        logoutdialogDesignBinding.setClickListener(this);
        builder.setView(logoutdialogDesignBinding.getRoot());

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        alertDialog = builder.create();
        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);


        alertDialog.show();
        alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    /**
     * private class to make a listview adapter based on ArrayAdapter
     */
    public class SlidingMenuAdapter extends RecyclerView.Adapter<SlidingMenuAdapter.CustomViewHolder> {
        ClickListener clickListener;
        private Activity activity;
        private ArrayList<SlidingMenuItem> items;

        public SlidingMenuAdapter(Activity activity, int resource, ArrayList<SlidingMenuItem> objects, ClickListener clickListener) {
            this.activity = activity;
            this.items = objects;
            this.clickListener = clickListener;
        }

        @NonNull
        @Override
        public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            //View view = LayoutInflater.from(activity).inflate(R.layout.item_menu, parent, false);
            ItemMenuBinding itemMenuBinding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.item_menu, parent, false);
            //view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            return new CustomViewHolder(itemMenuBinding);

        }

        @Override
        public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
            SlidingMenuItem slidingMenuItem = getItem(position);
            holder.notifyView(slidingMenuItem);

            Drawable drawable = getResources().getDrawable(slidingMenuItem.getImageResource());
            holder.itemMenuBinding.name.setText(slidingMenuItem.getMenuItemName());
            holder.itemMenuBinding.iconImageViewId.setBackground(drawable);

            if (slidingMenuItem.isClicked()) {
                holder.itemMenuBinding.name.setTextColor(getResources().getColor(R.color.colorPrimary));
                holder.itemMenuBinding.iconImageViewId.setSupportBackgroundTintList(ContextCompat.getColorStateList(activity, R.color.colorPrimary));
            } else {
                holder.itemMenuBinding.name.setTextColor(getResources().getColor(R.color.black));
                holder.itemMenuBinding.iconImageViewId.setSupportBackgroundTintList(ContextCompat.getColorStateList(activity, R.color.black));
            }
            /*holder.menuItemLinLayout.setOnTouchListener(new OnSwipeTouchListener(activity) {
                public void onSwipeRight(View view) {
                    // Do something
                }

                public void onSwipeLeft(View view) {
                    clickListener.onClick(position, getItem(position), "SLIDED_LEFT");
                    //Toast.makeText(activity, "LEFT", Toast.LENGTH_SHORT).show();

                }

                public void onSwipeBottom(View view) {
                    // Do something
                }

                public void onSwipeTop(View view) {
                    // Do something
                }

                public void onClick(View view) {
                    clickListener.onClick(position, getItem(position), "MENU_ITEM_CLICK");
                }

                public boolean onLongClick(View view) {
                    // Do something
                    return true;
                }
            });*/
        }

        private SlidingMenuItem getItem(int position) {
            return items.get(position);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class CustomViewHolder extends RecyclerView.ViewHolder {

            /*@BindView(R.id.name)
            public TextView itemName;

            @BindView(R.id.iconImageViewId)
            public AppCompatImageView itemImage;

            @BindView(R.id.swipwListnerLainLayoutId)
            public LinearLayout swipeListenerLinLayout;

            @BindView(R.id.menuItemLinLayoutId)
            public LinearLayout menuItemLinLayout;*/

            ItemMenuBinding itemMenuBinding;
            RecyclerView.ViewHolder AAA = this;

            public CustomViewHolder(ItemMenuBinding itemMenuBinding) {
                super(itemMenuBinding.getRoot());
                //  ButterKnife.bind(this, itemView);
                this.itemMenuBinding = itemMenuBinding;
            }

            public void notifyView(SlidingMenuItem slidingMenuItem) {
                itemMenuBinding.setMenuData(slidingMenuItem);
                itemMenuBinding.executePendingBindings();
                itemView.setOnTouchListener(new OnSwipeTouchListener(activity) {
                    public void onSwipeRight(View view) {
                        // Do something
                    }

                    public void onSwipeLeft(View view) {
                        clickListener.onClick(AAA.getPosition(), getItem(AAA.getPosition()), "SLIDED_LEFT");
                        //Toast.makeText(activity, "LEFT", Toast.LENGTH_SHORT).show();

                    }

                    public void onSwipeBottom(View view) {
                        // Do something
                    }

                    public void onSwipeTop(View view) {
                        // Do something
                    }

                    public void onClick(View view) {
                        clickListener.onClick(AAA.getPosition(), getItem(AAA.getPosition()), "MENU_ITEM_CLICK");
                    }

                    public boolean onLongClick(View view) {
                        // Do something
                        return true;
                    }
                });
            }

           /* @OnClick({R.id.menuItemLinLayoutId})
            public void onClick(View view) {
                if (view == swipeListenerLinLayout) {
                    clickListener.onClick(this.getPosition(), getItem(this.getPosition()), "MENU_ITEM_CLICK");
                }
            }*/
        }

    }

}