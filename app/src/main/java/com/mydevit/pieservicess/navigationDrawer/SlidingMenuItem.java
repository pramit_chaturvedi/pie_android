package com.mydevit.pieservicess.navigationDrawer;

/**
 * Created by Rishi Ojha.
 */
public class SlidingMenuItem {

    private int imageResource;
    private String menuItemName;
    private boolean isClicked;

    public boolean isClicked() {
        return isClicked;
    }

    public void setClicked(boolean clicked) {
        isClicked = clicked;
    }

    public SlidingMenuItem(int image, String name,boolean isClicked) {
        this.imageResource = image;
        this.menuItemName = name;
        this.isClicked = isClicked;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public String getMenuItemName() {
        return menuItemName;
    }

    public void setMenuItemName(String menuItemName) {
        this.menuItemName = menuItemName;
    }
}
