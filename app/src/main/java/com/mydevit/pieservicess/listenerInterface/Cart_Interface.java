package com.mydevit.pieservicess.listenerInterface;

public interface Cart_Interface {
    public void plusClicked(int position,int quantity,String itemPrice);
    public void minusClicked(int position,int quantity,String itemPrice);
}
