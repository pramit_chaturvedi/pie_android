package com.mydevit.pieservicess.listenerInterface;

public interface ServiceReponseInterface_Duplicate {
    void onSuccess(String result, String type);

    void onFailed(String result);
}
