package com.mydevit.pieservicess.listenerInterface;

public interface ClickListenerPC {
    void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere);
}
