package com.mydevit.pieservicess.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivitySignInBinding;
import com.mydevit.pieservicess.databinding_model.LoginUser;
import com.mydevit.pieservicess.databinding_model.LoginViewModel;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.navigationDrawer.SlidingActivity;
import com.mydevit.pieservicess.service.signin_model;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SignInActivity extends AppCompatActivity implements ServiceReponseInterface_Duplicate, ClickListener {

    private static final String TAG = "MainActivity";
    private static final int RC_SIGN_IN = 9001;
    public static String tokenValue = "";

  /*  @BindView(R.id.googleLoginLinLayoutId)
    LinearLayout googleLoginLinLayout;

    @BindView(R.id.facebookLoginLinLayoutId)
    LinearLayout facebookLoginLinLayout;

    @BindView(R.id.signInButtonId)
    Button signInButton;*/

    ActivitySignInBinding activitySignInBinding;
    List<NameValuePair> nameValuePairList;
    LoginViewModel loginViewModel;
    ServiceReponseInterface_Duplicate serviceReponseInterface_duplicate;
    SharedPreferences_Util sharedPreferences_util;
    String signInAsAString;
    //GOOGLE AUTH VIA FIREBASE
    private LoginButton facebookLoginButton;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private ProgressDialog pDialog;
    private GoogleSignInClient mGoogleSignInClient;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activitySignInBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in);
        activitySignInBinding.setClickListener(this);
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        activitySignInBinding.setLoginviewmodel(loginViewModel);
        activitySignInBinding.setLifecycleOwner(this);

        serviceReponseInterface_duplicate = this;
        nameValuePairList = new ArrayList<>();
        sharedPreferences_util = new SharedPreferences_Util(SignInActivity.this);

        spinner = findViewById(R.id.spinner);

        getSupportActionBar().hide();
        getDataFromIntent();
        LISTENER_INTEGRATION();
        FACEBOOK_INTEGRATION();
        GOOGLE_INTEGRATION();

    }

    private void getDataFromIntent() {
//        signInAsAString = getIntent().getExtras().getString("signInAsA");
    }

    private void LISTENER_INTEGRATION() {
        activitySignInBinding.signInButtonId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginViewModel.onLoginClicked(SignInActivity.this);
            }
        });
        loginViewModel.getUser().observe(this, new Observer<LoginUser>() {
            @Override
            public void onChanged(LoginUser loginUser) {
                if (TextUtils.isEmpty(loginUser.getmEmail())) {
                    AlphaHolder.customToast(SignInActivity.this, getResources().getString(R.string.emailmustrequire));
                } else if (!AlphaHolder.isEmailValid(loginUser.getmEmail())) {
                    AlphaHolder.customToast(SignInActivity.this, getResources().getString(R.string.emailmustvalid));
                } else if (TextUtils.isEmpty(loginUser.getmPassword())) {
                    AlphaHolder.customToast(SignInActivity.this, getResources().getString(R.string.passwordmustrequire));
                } else {
                    nameValuePairList = new ArrayList<>();
                    nameValuePairList.add(new BasicNameValuePair("email", loginUser.getmEmail()));
                    nameValuePairList.add(new BasicNameValuePair("password", loginUser.getmPassword()));
                    signInAsAString = spinner.getSelectedItem().toString();
                    if ("Business".equals(signInAsAString)) {
                        nameValuePairList.add(new BasicNameValuePair("role_id", "1"));
                    } else if ("Personnel".equals(signInAsAString)) {
                        nameValuePairList.add(new BasicNameValuePair("role_id", "2"));
                    }
                    new Service_Integration_Duplicate(SignInActivity.this, "Y", nameValuePairList, "/business_login", serviceReponseInterface_duplicate, "BUSINESS_LOGIN", true).execute();
                }
            }
        });
    }

    private void GOOGLE_INTEGRATION() {
        pDialog = new ProgressDialog(SignInActivity.this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
    }

    /*@OnClick({R.id.googleLoginLinLayoutId, R.id.facebookLoginLinLayoutId,R.id.signInButtonId})
    public void onEventListner(View view) {
        if (view == googleLoginLinLayout) {
            signIn();
        }
        if (view == facebookLoginLinLayout) {
            facebookLoginButton.performClick();
        }
        if (view == signInButton) {
            Intent intent = new Intent(SignInActivity.this, SlidingActivity.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
    }*/
    private void FACEBOOK_INTEGRATION() {
        callbackManager = CallbackManager.Factory.create();
        facebookLoginButton = (LoginButton) findViewById(R.id.facebookLoginButton);
        facebookLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphLoginRequest(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), R.string.cancel_login, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    // Method to access Facebook User Data.
    protected void GraphLoginRequest(AccessToken accessToken) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                        Log.e("OUTPUT", jsonObject.toString());
                        try {

                            String emailString = jsonObject.getString("email");
                            nameValuePairList = new ArrayList<>();
                            nameValuePairList.add(new BasicNameValuePair("email", emailString));
                            signInAsAString = spinner.getSelectedItem().toString();
                            if ("Business".equals(signInAsAString)) {
                                nameValuePairList.add(new BasicNameValuePair("role_id", "1"));
                            } else if ("Personnel".equals(signInAsAString)) {
                                nameValuePairList.add(new BasicNameValuePair("role_id", "2"));
                            }
                            new Service_Integration_Duplicate(SignInActivity.this, "Y", nameValuePairList, "/business_social_login", serviceReponseInterface_duplicate, "SOCIAL_FACEBOOK_LOGIN", true).execute();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id,name,link,email,gender,last_name,first_name,locale,timezone,updated_time,verified");
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Log.w(TAG, "Google sign in failed", e);
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);

        }
    }

    /////for google login
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        pDialog.show();
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Login Failed: ", Toast.LENGTH_SHORT).show();
                        }
                        pDialog.dismiss();
                    }
                });
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            String email = user.getEmail();
            nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("email", email));
            signInAsAString = spinner.getSelectedItem().toString();
            if ("Business".equals(signInAsAString)) {
                nameValuePairList.add(new BasicNameValuePair("role_id", "1"));
            } else if ("Personnel".equals(signInAsAString)) {
                nameValuePairList.add(new BasicNameValuePair("role_id", "2"));
            }
            new Service_Integration_Duplicate(SignInActivity.this, "Y", nameValuePairList, "/business_social_login", serviceReponseInterface_duplicate, "SOCIAL_FACEBOOK_LOGIN", true).execute();
            //Toast.makeText(SignInActivity.this, user.getDisplayName(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("BUSINESS_LOGIN".equals(type)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String statusCode = jsonObject.getString("status_code");
                String message = jsonObject.getString("message");
                if ("400".equals(statusCode)) {
                    Toast.makeText(SignInActivity.this, message, Toast.LENGTH_SHORT).show();
                } else {
                    Gson gson = new Gson();
                    signin_model signin_model = gson.fromJson(result, com.mydevit.pieservicess.service.signin_model.class);
                    tokenValue = signin_model.getData().getToken();
                    sharedPreferences_util.saveLoginModel(signin_model);

                    Toast.makeText(SignInActivity.this, signin_model.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(SignInActivity.this, SlidingActivity.class);
                    startActivity(intent);
                    SignInActivity.this.finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if ("SOCIAL_FACEBOOK_LOGIN".equals(type)) {
            Gson gson = new Gson();
            signin_model signin_model = gson.fromJson(result, com.mydevit.pieservicess.service.signin_model.class);

            if ("failure".equals(signin_model.getStatus())) {
                Toast.makeText(SignInActivity.this, signin_model.getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                tokenValue = signin_model.getData().getToken();
                sharedPreferences_util.saveLoginModel(signin_model);

                Toast.makeText(SignInActivity.this, signin_model.getMessage(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SignInActivity.this, SlidingActivity.class);
                startActivity(intent);
                SignInActivity.this.finish();

                Toast.makeText(SignInActivity.this, signin_model.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }

    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("forgotclicked".equals(tag_FromWhere)) {
            Intent intent = new Intent(SignInActivity.this, ForgotActivity.class);
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
        if ("facebook_tag".equals(tag_FromWhere)) {
            facebookLoginButton.setReadPermissions(Arrays.asList("email"));
            facebookLoginButton.performClick();
        }
        if ("google_tag".equals(tag_FromWhere)) {
            signIn();
        }
    }
   /* private void signOut() {
        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUI(null);
                    }
                });
    }
*/
}
