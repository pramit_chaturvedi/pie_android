package com.mydevit.pieservicess.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.PaymentLayoutBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.navigationDrawer.SlidingActivity;

public class PaymentSuccessActivity extends AppCompatActivity implements ClickListener {

    PaymentLayoutBinding paymentLayoutBinding;
    String paidAmount, orderId, transactionId, location_id;
    SlidingActivity slidingActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paymentLayoutBinding = DataBindingUtil.setContentView(PaymentSuccessActivity.this, R.layout.payment_layout);
        paymentLayoutBinding.setClicklistner(this);
        slidingActivity = new SlidingActivity();
        getSupportActionBar().hide();
        getDataFromIntent();

    }

    private void getDataFromIntent() {
        paidAmount = getIntent().getExtras().getString("amount");
        orderId = getIntent().getExtras().getString("order_id");
        transactionId = getIntent().getExtras().getString("transaction_id");
        location_id = getIntent().getExtras().getString("location_id");

        paymentLayoutBinding.setOrderid(manipulateText(orderId));
        paymentLayoutBinding.setPaidamount(manipulateText(paidAmount));
        paymentLayoutBinding.setTransactionid(manipulateText(transactionId));

    }

    String manipulateText(String value) {
        if (value == null) {
            value = getResources().getString(R.string.norecordfound);
        } else if ("".equals(value)) {
            value = getResources().getString(R.string.norecordfound);
        } else {
            return value;
        }
        return value;
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("Services".equals(tag_FromWhere)) {
            PaymentSuccessActivity.this.finish();
            Intent intent = new Intent(PaymentSuccessActivity.this, AppointmentsActivity.class);
            intent.putExtra("location_id", location_id);
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
        if ("Back To POS".equals(tag_FromWhere)) {
            PaymentSuccessActivity.this.finish();
            Intent intent = new Intent(PaymentSuccessActivity.this, SlidingActivity.class);
            intent.putExtra("SENDTOPOS", "YES");
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
    }
}
