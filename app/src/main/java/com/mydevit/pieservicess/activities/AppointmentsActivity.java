package com.mydevit.pieservicess.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.android.material.tabs.TabLayout;
import com.mydevit.pieservicess.Fragments.AppointmentsTabThreeFragment;
import com.mydevit.pieservicess.Fragments.AppointmentsTabTwoFragment;
import com.mydevit.pieservicess.Fragments.Appointments_TabOneFragment;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityAppointmentsBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.utils.AlphaHolder;

public class AppointmentsActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, ClickListener {

    public static String[] PAGE_TITLES = null;
    public static Fragment[] PAGES = null;
    public static ActivityAppointmentsBinding activityAppointmentsBinding;
    public static String cartId;
    public static Activity appointmentActivityContext;
    Context context;
    View view;
    AlertDialog alertDialog = null;
    String personal_id, location_id;
    Bundle bundle = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityAppointmentsBinding = DataBindingUtil.setContentView(this, R.layout.activity_appointments);
        activityAppointmentsBinding.setClickListner(this);
        appointmentActivityContext = this;
        getSupportActionBar().hide();
        activityAppointmentsBinding.setCo(this);
        activityAppointmentsBinding.setManager(getSupportFragmentManager());
        getIntentData();
        TabLayout();
    }

    private void getIntentData() {
        if (getIntent().getExtras() != null) {
            location_id = getIntent().getExtras().getString("location_id");
            //  personal_id = getIntent().getExtras().getString("personal_id");
        }
    }

    /* @BindingAdapter({"bind:co"})
     public static void con(final ViewPager viewPager, AppointmentsActivity appointmentsActivity) {
         MyPagerAdapter myPagerAdapter = new MyPagerAdapter(appointmentsActivity.getSupportFragmentManager());
         viewPager.setAdapter(myPagerAdapter);
     }

     @BindingAdapter({"bind:vi"})
     public static void viee(final TabLayout tabLayout, final ViewPager viewPager) {
         tabLayout.setupWithViewPager(viewPager,true);
     }*/
    /* @BindingAdapter("bind:handlers")
     public static void bindViewPagerAdapters(final ViewPager viewPager,AppointmentsActivity activity) {

     }
     @BindingAdapter("bind:pagers")
     public static void bindViewPagerTabs(final TabLayout tabLayout, final ViewPager viewPager) {
         tabLayout.setupWithViewPager(viewPager);
     }
 */
    @Override
    protected void onResume() {
        super.onResume();
    }

    public void TabLayout() {
        PAGE_TITLES = new String[]{

                getResources().getString(R.string.services),
                getResources().getString(R.string.product),
                getResources().getString(R.string.appointment),

        };/*  PAGE_TITLES = new String[]{
                getResources().getString(R.string.appointment),
                getResources().getString(R.string.services),
                getResources().getString(R.string.product),
                getResources().getString(R.string.report)

                service,product,appointment
        };*/

        Appointments_TabOneFragment appointments_tabOneFragment = new Appointments_TabOneFragment();
        AppointmentsTabTwoFragment appointmentsTabTwoFragment = new AppointmentsTabTwoFragment();
        AppointmentsTabThreeFragment appointmentsTabThreeFragment = new AppointmentsTabThreeFragment();
        /* ReportTab_Fragment reportTab_fragment = new ReportTab_Fragment();*/

        bundle = new Bundle();
        bundle.putString("location_id", location_id);
        //bundle.putString("personal_id", personal_id);

        appointments_tabOneFragment.setArguments(bundle);
        appointmentsTabTwoFragment.setArguments(bundle);
        appointmentsTabThreeFragment.setArguments(bundle);
        // reportTab_fragment.setArguments(bundle);

        PAGES = new Fragment[]{
                appointmentsTabTwoFragment,
                appointmentsTabThreeFragment,
                appointments_tabOneFragment,
        };
     /*   PAGES = new Fragment[]{
                appointments_tabOneFragment,
                appointmentsTabTwoFragment,
                appointmentsTabThreeFragment,
                reportTab_fragment
        };*/
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        activityAppointmentsBinding.pager.setAdapter(myPagerAdapter);
        activityAppointmentsBinding.tabLayout.setupWithViewPager(activityAppointmentsBinding.pager);
        activityAppointmentsBinding.tabLayout.setOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getPosition() == 0) {
            activityAppointmentsBinding.titleTextViewId.setText(getResources().getString(R.string.appointment));
            //AlphaHolder.customToast(AppointmentsActivity.this, "" + tab.getPosition());

       /*     bookNowTextView.setText(getResources().getString(R.string.booknow));

            roundLinLayout.setVisibility(View.GONE);
            bookNowTextView.setVisibility(View.VISIBLE);*/
        }
        if (tab.getPosition() == 1) {
           // AlphaHolder.customToast(AppointmentsActivity.this, "" + tab.getPosition());
            activityAppointmentsBinding.titleTextViewId.setText(getResources().getString(R.string.product));

        /*    bookNowTextView.setText(getResources().getString(R.string.booknow));

            roundLinLayout.setVisibility(View.VISIBLE);
            bookNowTextView.setVisibility(View.VISIBLE);*/

        }
        if (tab.getPosition() == 2) {
           // AlphaHolder.customToast(AppointmentsActivity.this, "" + tab.getPosition());
            activityAppointmentsBinding.titleTextViewId.setText(getResources().getString(R.string.appointment));

          /*  roundLinLayout.setVisibility(View.GONE);
            bookNowTextView.setVisibility(View.GONE)*/
            ;
        }
        if (tab.getPosition() == 3) {
           // AlphaHolder.customToast(AppointmentsActivity.this, "" + tab.getPosition());

            activityAppointmentsBinding.titleTextViewId.setText(getResources().getString(R.string.resting));
        }
    }

    /* @OnClick({R.id.roundLinLayoutId})
     public void onEventListner(View view){
         if(view==roundLinLayout){
             showDialog();
         }
     }*/
    public void showDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(context).inflate(R.layout.addcustomer_layout, viewGroup, false);
        builder.setView(dialogView);

        ImageView closeIMageView = dialogView.findViewById(R.id.closeIMageViewId);

        closeIMageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.show();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.7f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.7f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
//        Toast.makeText(getActivity(),"Reselect",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        //   Toast.makeText(getActivity(),"Reselect",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("backclicked".equals(tag_FromWhere)) {
            AppointmentsActivity.this.finish();
        }
        if ("cartclicked".equals(tag_FromWhere)) {
            String totalItems = activityAppointmentsBinding.cartItemnumberingTextViewId.getText().toString();
            if ("0".equals(totalItems)) {
                AlphaHolder.customToast(AppointmentsActivity.this, getResources().getString(R.string.cartisempty));
            } else {
                Intent intent = new Intent(AppointmentsActivity.this, BillActivity.class);
                intent.putExtra("location_id", location_id);
                intent.putExtra("personal_id", personal_id);
                intent.putExtra("card_id", cartId);
                startActivity(intent);
            }

        }
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return PAGES[position];
        }

        @Override
        public int getCount() {
            return PAGES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return PAGE_TITLES[position];
        }

    }
}
