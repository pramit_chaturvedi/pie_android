package com.mydevit.pieservicess.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.android.material.tabs.TabLayout;
import com.mydevit.pieservicess.Fragments.AppointmentDataInformationTabs.AppointmentHistoryFragment;
import com.mydevit.pieservicess.Fragments.AppointmentDataInformationTabs.TodayAppointmentsFragment;
import com.mydevit.pieservicess.Fragments.AppointmentDataInformationTabs.UpcomingAppointmentsFragment;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityClientDataInformationTabBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.utils.AlphaHolder;

public class ClientDataInformationTabActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, ClickListener {

    public static String[] PAGE_TITLES = null;
    public static Fragment[] PAGES = null;
    Context context;
    View view;
    AlertDialog alertDialog = null;
    String customer_id, location_id, clientName;
    Bundle bundle = null;

    ActivityClientDataInformationTabBinding activityClientBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityClientBinding = DataBindingUtil.setContentView(this, R.layout.activity_client_data_information_tab);
        activityClientBinding.setClickListner(this);
        getSupportActionBar().hide();
        activityClientBinding.setCo(this);
        activityClientBinding.setManager(getSupportFragmentManager());
        getIntentData();
        TabLayout();
        activityClientBinding.executePendingBindings();
    }

    private void getIntentData() {
        if (getIntent().getExtras() != null) {
            location_id = getIntent().getExtras().getString("location_id");
            customer_id = getIntent().getExtras().getString("customer_id");
            clientName = getIntent().getExtras().getString("clientname");
            activityClientBinding.setClientName(AlphaHolder.ConvertFirstCharacterCappital(clientName));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void TabLayout() {
        PAGE_TITLES = new String[]{
                getResources().getString(R.string.todayappointment),
                getResources().getString(R.string.upcomingappointment),
                getResources().getString(R.string.appointmenthistory),
        };
        TodayAppointmentsFragment todayAppointmentsFragment = new TodayAppointmentsFragment();
        UpcomingAppointmentsFragment upcomingAppointmentsFragment = new UpcomingAppointmentsFragment();
        AppointmentHistoryFragment appointmentHistoryFragment = new AppointmentHistoryFragment();

        bundle = new Bundle();
        bundle.putString("location_id", location_id);
        bundle.putString("customer_id", customer_id);

        todayAppointmentsFragment.setArguments(bundle);
        upcomingAppointmentsFragment.setArguments(bundle);
        appointmentHistoryFragment.setArguments(bundle);

        PAGES = new Fragment[]{
                todayAppointmentsFragment,
                upcomingAppointmentsFragment,
                appointmentHistoryFragment,
        };
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        activityClientBinding.pager.setAdapter(myPagerAdapter);
        activityClientBinding.tabLayout.setupWithViewPager(activityClientBinding.pager);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getPosition() == 0) {
            activityClientBinding.titleTextViewId.setText(getResources().getString(R.string.appointment));

       /*     bookNowTextView.setText(getResources().getString(R.string.booknow));

            roundLinLayout.setVisibility(View.GONE);
            bookNowTextView.setVisibility(View.VISIBLE);*/
        }
        if (tab.getPosition() == 1) {
            activityClientBinding.titleTextViewId.setText(getResources().getString(R.string.services));

        /*    bookNowTextView.setText(getResources().getString(R.string.booknow));

            roundLinLayout.setVisibility(View.VISIBLE);
            bookNowTextView.setVisibility(View.VISIBLE);*/

        }
        if (tab.getPosition() == 2) {
            activityClientBinding.titleTextViewId.setText(getResources().getString(R.string.product));

          /*  roundLinLayout.setVisibility(View.GONE);
            bookNowTextView.setVisibility(View.GONE)*/
            ;
        }
        if (tab.getPosition() == 3) {
            activityClientBinding.titleTextViewId.setText(getResources().getString(R.string.resting));
        }
    }

    public void showDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) context).findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(context).inflate(R.layout.addcustomer_layout, viewGroup, false);
        builder.setView(dialogView);

        ImageView closeIMageView = dialogView.findViewById(R.id.closeIMageViewId);

        closeIMageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog = builder.create();
        alertDialog.show();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.7f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.7f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
//        Toast.makeText(getActivity(),"Reselect",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        //   Toast.makeText(getActivity(),"Reselect",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("backclicked".equals(tag_FromWhere)) {
            ClientDataInformationTabActivity.this.finish();
        }
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return PAGES[position];
        }

        @Override
        public int getCount() {
            return PAGES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return PAGE_TITLES[position];
        }

    }
}
