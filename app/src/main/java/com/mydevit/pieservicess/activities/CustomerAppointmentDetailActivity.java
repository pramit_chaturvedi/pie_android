package com.mydevit.pieservicess.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityCustomerAppointmentDetailBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.PersonnelClientAppointment_RecyclerViewAdapter;
import com.mydevit.pieservicess.recyclerviewadapters.ServiceClientAppointment_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.CustomerAppointment_DetailModel;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class CustomerAppointmentDetailActivity extends AppCompatActivity implements ClickListener, ServiceReponseInterface_Duplicate {

    ServiceClientAppointment_RecyclerViewAdapter serviceClientAppointment_recyclerViewAdapter;
    PersonnelClientAppointment_RecyclerViewAdapter personnelClientAppointment_recyclerViewAdapter;

    ActivityCustomerAppointmentDetailBinding activityCustomerAppointmentDetailBinding;
    CustomerAppointment_DetailModel customerAppointment_detailModel;

    List<CustomerAppointment_DetailModel.DataBean.ServiceDataBean> serviceDataBeanList;
    List<CustomerAppointment_DetailModel.DataBean.PersonnelDataBean> personnelDataBeanList;
    List<NameValuePair> nameValuePairList;

    String customerId, appointmentNo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        activityCustomerAppointmentDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_customer_appointment_detail);
        activityCustomerAppointmentDetailBinding.setClickListener(this);

        initialization();
        getDataFromIntent();
        getCustomerAppointmentService();
    }

    private void initialization() {
        serviceDataBeanList = new ArrayList<>();
        personnelDataBeanList = new ArrayList<>();

    }

    private void getDataFromIntent() {
        customerId = getIntent().getExtras().getString("customerId");
        appointmentNo = getIntent().getExtras().getString("appointmentNo");
    }

    private void getCustomerAppointmentService() {
        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(CustomerAppointmentDetailActivity.this)));
        nameValuePairList.add(new BasicNameValuePair("appointment_id", appointmentNo));

        new Service_Integration_Duplicate(CustomerAppointmentDetailActivity.this, nameValuePairList, "business/customer/Customer_appointment_detail", this, "CUSTOMER_APPOINTMENT_DETAIL", true).execute();


      /*  appointmentsDataArrayList = new ArrayList<>();
        appointmentsDataArrayList.add(null);
        appointmentsDataArrayList.add(null);
        appointmentsDataArrayList.add(null);
        appointmentsDataArrayList.add(null);
        appointmentsDataArrayList.add(null);
        appointmentsDataArrayList.add(null);
        appointmentsDataArrayList.add(null);


        serviceClientAppointment_recyclerViewAdapter = new ServiceClientAppointment_RecyclerViewAdapter(CustomerAppointmentDetailActivity.this, appointmentsDataArrayList, this);
        activityCustomerAppointmentDetailBinding.setServiceClientAppointment(serviceClientAppointment_recyclerViewAdapter);*/

    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("back".equals(tag_FromWhere)) {
            CustomerAppointmentDetailActivity.this.finish();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onSuccess(String result, String type) {
        if ("CUSTOMER_APPOINTMENT_DETAIL".equals(type)) {
            customerAppointment_detailModel = new Gson().fromJson(result, CustomerAppointment_DetailModel.class);
            if (customerAppointment_detailModel.getData() != null) {

                if (customerAppointment_detailModel.getData().getService_data() != null) {
                    serviceDataBeanList.addAll(customerAppointment_detailModel.getData().getService_data());
                    serviceClientAppointment_recyclerViewAdapter = new ServiceClientAppointment_RecyclerViewAdapter(CustomerAppointmentDetailActivity.this, serviceDataBeanList, this);
                    activityCustomerAppointmentDetailBinding.setServiceClientAppointment(serviceClientAppointment_recyclerViewAdapter);


                }

                if (customerAppointment_detailModel.getData().getPersonnel_data() != null) {
                    personnelDataBeanList.addAll(customerAppointment_detailModel.getData().getPersonnel_data());
                    personnelClientAppointment_recyclerViewAdapter = new PersonnelClientAppointment_RecyclerViewAdapter(CustomerAppointmentDetailActivity.this, personnelDataBeanList, this);
                    activityCustomerAppointmentDetailBinding.setPersonnelClientAppointment(personnelClientAppointment_recyclerViewAdapter);
                }
                activityCustomerAppointmentDetailBinding.setTotalData(customerAppointment_detailModel);


            }
        }
    }

    @Override
    public void onFailed(String result) {

    }
}
