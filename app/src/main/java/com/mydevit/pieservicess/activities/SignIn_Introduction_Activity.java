package com.mydevit.pieservicess.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.mydevit.pieservicess.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignIn_Introduction_Activity extends AppCompatActivity {

    @BindView(R.id.signInButtonId)
    Button signInButton;

    @BindView(R.id.loginAsProfessionalButtonId)
    Button loginAsProfessionalButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_introduction);
        ButterKnife.bind(this);
        getSupportActionBar().hide();

    }

    @OnClick({R.id.signInButtonId, R.id.loginAsProfessionalButtonId})
    public void onEventListner(View view) {
        if (view == signInButton) {
            Intent intent = new Intent(SignIn_Introduction_Activity.this, SignInActivity.class);
            intent.putExtra("signInAsA", "USER");
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
        if (view == loginAsProfessionalButton) {
            Intent intent = new Intent(SignIn_Introduction_Activity.this, SignInActivity.class);
            intent.putExtra("signInAsA", "PERSONNEL");
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
    }
}
