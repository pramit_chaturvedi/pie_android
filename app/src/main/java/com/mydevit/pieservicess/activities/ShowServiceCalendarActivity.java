package com.mydevit.pieservicess.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityCalenderServiceBinding;
import com.mydevit.pieservicess.databinding.ActivityShowServiceCalendarBinding;
import com.mydevit.pieservicess.databinding.CustomerDetailDialogBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.model.ParentServiceModel;
import com.mydevit.pieservicess.model.ServiceModel;
import com.mydevit.pieservicess.recyclerviewadapters.ShowService_RecyclerViewAdapter;
import com.mydevit.pieservicess.recyclerviewadapters.TimeSlot_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.ContactResult_ServiceModel;
import com.mydevit.pieservicess.service.Slot_Time_Model;
import com.mydevit.pieservicess.service.UpdatedTimeSlot_ServiceBean;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.CustomCalendarView;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class ShowServiceCalendarActivity extends AppCompatActivity implements ClickListenerPC, ClickListener, ServiceReponseInterface_Duplicate, CustomCalendarView.RobotoCalendarListener {

    ArrayList<ParentServiceModel> dataBeanArrayList;
    AlphaHolder alphaHolder;
    String selectedDateIs;
    ShowService_RecyclerViewAdapter showService_recyclerViewAdapter;
    ActivityShowServiceCalendarBinding binding;
    String day, month;
    String clientId = "";

    ArrayList<ParentServiceModel> serviceModelParentArrayListTemp;
    List<String> selectedIdList;
    ActivityCalenderServiceBinding activityCalenderBinding;
    AlertDialog alertDialog = null;
    String appointmentSelectedData;
    String location_id, personal_id, serviceDuration, appointmentId;
    CustomerDetailDialogBinding customerDetailDialogBinding;

    ArrayList<Boolean> timeSlotSelectedArrayList;
    TimeSlot_RecyclerViewAdapter timeSlot_recyclerViewAdapter;

    private List<ServiceModel> serviceModelListTemp;
    private List<String> personnel_slot;
    private List<NameValuePair> nameValuePairList;

    private String checkInString, checkOutString, selectedDataString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_show_service_calendar);
        binding.setClickListener(this);
        alphaHolder = new AlphaHolder();
        getDataFrm();
    }

    private String getDataName(String input) {
        String goal = "";
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = inFormat.parse(input);
            SimpleDateFormat outFormat = new SimpleDateFormat("MMMM");
            goal = outFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return goal;
    }
    private void getDataFrm() {
        dataBeanArrayList = alphaHolder.getServiceArrayList(ShowServiceCalendarActivity.this);
        if (getIntent().getExtras() != null) {
            selectedDateIs = getIntent().getExtras().getString("selectedata");
            location_id = getIntent().getExtras().getString("location_id");

        }
        day = selectedDateIs.substring(8, 10);
        month = getDataName(selectedDateIs);
        binding.setMonth(month);
        binding.setDay(day);


        Log.e("dateISSSSS", selectedDateIs);

        serviceModelParentArrayListTemp = new ArrayList<>();
        selectedIdList = new ArrayList<>();
        personnel_slot = new ArrayList<>();


        filterDataMore();
    }

    private void filterDataMore() {
        for (int i = 0; i < dataBeanArrayList.size(); i++) {

            serviceModelListTemp = new ArrayList<>();
            String id = dataBeanArrayList.get(i).getServiceModelList().get(0).getPersonal_Id();

            ParentServiceModel parentServiceModel = new ParentServiceModel();
            parentServiceModel.setName(dataBeanArrayList.get(i).getName());

            for (int j = 0; j < dataBeanArrayList.size(); j++) {
                if (id.equals(dataBeanArrayList.get(j).getServiceModelList().get(0).getPersonal_Id())) {
                    serviceModelListTemp.add(dataBeanArrayList.get(j).getServiceModelList().get(0));
                }

                if (j == dataBeanArrayList.size() - 1) {
                    parentServiceModel.setServiceModelList(serviceModelListTemp);
                    serviceModelParentArrayListTemp.add(parentServiceModel);
                }
            }
            if (i == dataBeanArrayList.size() - 1) {
                dataBeanArrayList.clear();
                dataBeanArrayList = removeDuplicates(serviceModelParentArrayListTemp);
                showService_recyclerViewAdapter = new ShowService_RecyclerViewAdapter(ShowServiceCalendarActivity.this, dataBeanArrayList, this);
                binding.setDataOf(showService_recyclerViewAdapter);
                binding.executePendingBindings();
            }

        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public ArrayList<ParentServiceModel> removeDuplicates(ArrayList<ParentServiceModel> list) {
        Set<ParentServiceModel> set = new TreeSet(new Comparator<ParentServiceModel>() {

            @Override
            public int compare(ParentServiceModel o1, ParentServiceModel o2) {
                if (o1.getName().equalsIgnoreCase(o2.getName())) {
                    return 0;
                }
                return 1;
            }
        });
        set.addAll(list);
        final ArrayList newList = new ArrayList(set);
        return newList;
    }

    public void calender() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ShowServiceCalendarActivity.this, R.style.CustomAlertDialog);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        activityCalenderBinding = DataBindingUtil.inflate(LayoutInflater.from(ShowServiceCalendarActivity.this), R.layout.activity_calender_service, viewGroup, false);
        activityCalenderBinding.setClickListener(this);
        builder.setView(activityCalenderBinding.getRoot());
        InitialteCalender(activityCalenderBinding);

        alertDialog = builder.create();
        alertDialog.show();

        getSlotServicesApi(appointmentSelectedData);


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ShowServiceCalendarActivity.this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    public void customerDetail(ContactResult_ServiceModel contactResult_serviceModel) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ShowServiceCalendarActivity.this, R.style.CustomAlertDialog);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        customerDetailDialogBinding = DataBindingUtil.inflate(LayoutInflater.from(ShowServiceCalendarActivity.this), R.layout.customer_detail_dialog, viewGroup, false);
        customerDetailDialogBinding.setClickLisrtener(this);
        builder.setView(customerDetailDialogBinding.getRoot());

        customerDetailDialogBinding.closeImageViewId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        customerDetailDialogBinding.nameTextViewId.setText(contactResult_serviceModel.getData().getFirst_name());
        customerDetailDialogBinding.emailTextViewId.setText(contactResult_serviceModel.getData().getEmail());
        customerDetailDialogBinding.phoneTextViewId.setText(contactResult_serviceModel.getData().getPhone());
        customerDetailDialogBinding.instagrmTextViewId.setText(contactResult_serviceModel.getData().getInstagram_user());
        customerDetailDialogBinding.twitterTextViewId.setText(contactResult_serviceModel.getData().getTwitter_user());
        customerDetailDialogBinding.facebookTextViewId.setText(contactResult_serviceModel.getData().getFacebook_user());
        customerDetailDialogBinding.addreessTextViewId.setText(contactResult_serviceModel.getData().getAddress());

        alertDialog = builder.create();
        alertDialog.show();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ShowServiceCalendarActivity.this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    public void getSlotServicesApi(String date) {

        personnel_slot = new ArrayList<>();
        nameValuePairList = new ArrayList<>();

        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(ShowServiceCalendarActivity.this)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        nameValuePairList.add(new BasicNameValuePair("personnel_id", personal_id));
        nameValuePairList.add(new BasicNameValuePair("choosen_date", date));
        nameValuePairList.add(new BasicNameValuePair("serviceduration", serviceDuration));

        new Service_Integration_Duplicate(ShowServiceCalendarActivity.this, nameValuePairList, "business/pos_setup/fetch_personnel_open_slots", this, "TIME_SLOT", true).execute();
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("TIME_SLOT".equals(type)) {

            timeSlotSelectedArrayList = new ArrayList<>();
            personnel_slot = new ArrayList<>();

            Slot_Time_Model slot_time_model = new Gson().fromJson(result, Slot_Time_Model.class);
            if (slot_time_model.getData().get(0).size() > 0) {
                activityCalenderBinding.slotRecyclerViewId.setVisibility(View.VISIBLE);

                for (int i = 0; i < slot_time_model.getData().get(0).size(); i++) {

                    String message = slot_time_model.getData().get(0).get(i).getMessage();
                    if ("Time Off".equals(message) || "Not working, Today Off".equals(message) || "No slots Available for this week".equals(message)) {
                        activityCalenderBinding.selectButtonParentLainLayoutId.setVisibility(View.GONE);
                        activityCalenderBinding.slotRecyclerViewId.setVisibility(View.GONE);
                        activityCalenderBinding.timeSlotTextViewId.setVisibility(View.VISIBLE);
                        AlphaHolder.customToast(ShowServiceCalendarActivity.this, getResources().getString(R.string.notimeslotavailable));
                        personnel_slot = new ArrayList<>();

                    } else if (slot_time_model.getData().get(0).get(0).getPersonnel_slot() != null && slot_time_model.getData().get(0).get(0).getPersonnel_slot().size() == 0) {
                        activityCalenderBinding.selectButtonParentLainLayoutId.setVisibility(View.GONE);
                        activityCalenderBinding.slotRecyclerViewId.setVisibility(View.GONE);
                        activityCalenderBinding.timeSlotTextViewId.setVisibility(View.VISIBLE);
                        AlphaHolder.customToast(ShowServiceCalendarActivity.this, getResources().getString(R.string.notimeslotavailable));
                        personnel_slot = new ArrayList<>();
                    } else {
                        activityCalenderBinding.selectButtonParentLainLayoutId.setVisibility(View.VISIBLE);
                        activityCalenderBinding.slotRecyclerViewId.setVisibility(View.VISIBLE);
                        activityCalenderBinding.timeSlotTextViewId.setVisibility(View.GONE);

                        personnel_slot.addAll(slot_time_model.getData().get(0).get(i).getPersonnel_slot());
                        if (i == slot_time_model.getData().get(0).size() - 1) {
                            timeSlotSelectedArrayList = new ArrayList<>();
                            for (int j = 0; j < personnel_slot.size(); j++) {
                                if (j == 0) {
                                    timeSlotSelectedArrayList.add(true);
                                } else {
                                    timeSlotSelectedArrayList.add(false);
                                }
                                if (j == personnel_slot.size() - 1) {
                                    timeSlot_recyclerViewAdapter = new TimeSlot_RecyclerViewAdapter(ShowServiceCalendarActivity.this, personnel_slot, timeSlotSelectedArrayList, this);
                                    activityCalenderBinding.slotRecyclerViewId.setLayoutManager(new GridLayoutManager(ShowServiceCalendarActivity.this, 2));
                                    activityCalenderBinding.setDateSlotAdapter(timeSlot_recyclerViewAdapter);
                                    activityCalenderBinding.executePendingBindings();
                                }
                            }
                        }
                    }
                }
            } else {
                activityCalenderBinding.slotRecyclerViewId.setVisibility(View.GONE);
            }
        }
        if ("UPDATE_TIME_SLOT".equals(type)) {
            UpdatedTimeSlot_ServiceBean updatedTimeSlot_serviceBean = new Gson().fromJson(result, UpdatedTimeSlot_ServiceBean.class);
            if ("Personnel Appointment Updated Successfully".equals(updatedTimeSlot_serviceBean.getMessage())) {
                ShowServiceCalendarActivity.this.finish();

            }
        }
        if ("CONTACTRESULT".equals(type)) {
            ContactResult_ServiceModel contactResult_serviceModel = new Gson().fromJson(result, ContactResult_ServiceModel.class);
            customerDetail(contactResult_serviceModel);
        }
        if ("CHECKOUTRESULT".equals(type)) {
            {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String message = jsonObject.getString("message");
                    if ("Appointment Data Show Successfully".equals(message)) {

                        alertDialog.dismiss();
                        Intent intent = new Intent(ShowServiceCalendarActivity.this, AppointmentsActivity.class);
                        intent.putExtra("location_id", location_id);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }

    private void InitialteCalender(ActivityCalenderServiceBinding viewA) {
        viewA.markDayButton.setOnClickListener(view -> {
            Calendar calendar = Calendar.getInstance();
            Random random = new Random(System.currentTimeMillis());
            int style = random.nextInt(2);
            int daySelected = random.nextInt(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.DAY_OF_MONTH, daySelected);
            switch (style) {
                case 0:
                    viewA.robotoCalendarPicker.markCircleImage1(calendar.getTime());
                    break;
                case 1:
                    viewA.robotoCalendarPicker.markCircleImage2(calendar.getTime());
                    break;
                default:
                    break;
            }
        });
        viewA.clearSelectedDayButton.setOnClickListener(v -> viewA.robotoCalendarPicker.clearSelectedDay());
        // Set listener, in this case, the same activity
        viewA.robotoCalendarPicker.setRobotoCalendarListener(this);

        viewA.robotoCalendarPicker.setShortWeekDays(false);

        viewA.robotoCalendarPicker.showDateTitle(true);

        viewA.robotoCalendarPicker.setDate(new Date());
    }

    @Override
    public void onDayClick(String date, ArrayList<String> stringArrayList) {
        DateFormat inputFormat = new SimpleDateFormat("dd-MMM-yyyy");
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dateis = inputFormat.parse(date);
            appointmentSelectedData = outputFormat.format(dateis);
            getSlotServicesApi(appointmentSelectedData);
            /*fragmentScheduleNewBinding.setDate(appointmentSelectedData);
            fragmentScheduleNewBinding.executePendingBindings();*/

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDayLongClick(Date date, ArrayList<String> stringArrayList) {

    }

    @Override
    public void onRightButtonClick() {

    }

    @Override
    public void onLeftButtonClick() {

    }

    @Override
    public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
        if ("serviceclick".equals(tag_FromWhere)) {
            ServiceModel serviceModel = (ServiceModel) object;
            clientId = serviceModel.getClient_Id();

            personal_id = dataBeanArrayList.get(parentPosition).getServiceModelList().get(childPosition).getPersonal_Id();
            serviceDuration = dataBeanArrayList.get(parentPosition).getServiceModelList().get(childPosition).getService_duration();
            appointmentId = dataBeanArrayList.get(parentPosition).getServiceModelList().get(childPosition).getAppointment_id();
            appointmentSelectedData = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            selectedDataString = new Gson().toJson(dataBeanArrayList.get(parentPosition).getServiceModelList().get(childPosition).getStringArrayList());

            calender();

        }


        // Toast.makeText(ShowServiceCalendarActivity.this, selectedDataString, Toast.LENGTH_SHORT).show();
        //Toast.makeText(ShowServiceCalendarActivity.this, parentPosition + " " + childPosition, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("SELECT".equals(tag_FromWhere)) {

            nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(ShowServiceCalendarActivity.this)));
            nameValuePairList.add(new BasicNameValuePair("personnel_id", personal_id));
            nameValuePairList.add(new BasicNameValuePair("service_data", selectedDataString));
            nameValuePairList.add(new BasicNameValuePair("appointment_id", appointmentId));
            nameValuePairList.add(new BasicNameValuePair("choose_appointment_date", appointmentSelectedData));
            nameValuePairList.add(new BasicNameValuePair("calendar_slottimein", checkInString));
            nameValuePairList.add(new BasicNameValuePair("calendar_slottimeout", checkOutString));

            new Service_Integration_Duplicate(ShowServiceCalendarActivity.this, nameValuePairList, "business/Scheduler/save_edit_appointment", this, "UPDATE_TIME_SLOT", true).execute();

        }
        if ("timeslot".equals(tag_FromWhere)) {
            String selectedSlotString = (String) object;
            String[] parts = selectedSlotString.split("-");
            checkInString = parts[0];
            checkOutString = parts[1];

        }
        if ("back".equals(tag_FromWhere)) {
            ShowServiceCalendarActivity.this.finish();
        }
        if ("Contact".equals(tag_FromWhere)) {
            nameValuePairList = new ArrayList<>();

            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(ShowServiceCalendarActivity.this)));
            nameValuePairList.add(new BasicNameValuePair("customer_id", clientId));
            new Service_Integration_Duplicate(ShowServiceCalendarActivity.this, nameValuePairList, "business/Scheduler/get_customer_data", this, "CONTACTRESULT", true).execute();

        }
        if ("Checkout".equals(tag_FromWhere)) {
            nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(ShowServiceCalendarActivity.this)));
            nameValuePairList.add(new BasicNameValuePair("appointment_id", appointmentId));
            nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
            new Service_Integration_Duplicate(ShowServiceCalendarActivity.this, nameValuePairList, "business/pos_setup/pos_schedule_launch_appointments", this, "CHECKOUTRESULT", true).execute();

        }
    }
}
