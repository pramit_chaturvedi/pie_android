package com.mydevit.pieservicess.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityChangePasswordBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class ChangePasswordActivity extends AppCompatActivity implements ServiceReponseInterface_Duplicate, ClickListener {

    ActivityChangePasswordBinding activityChangePasswordBinding;
    List<NameValuePair> nameValuePairList;
    String currentPasswordString;
    String newPasswordString;
    String confirmPasswordString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityChangePasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        activityChangePasswordBinding.setClickListener(this);
        getSupportActionBar().hide();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void onSuccess(String result, String type) {
        if ("CHANGE_PASSWORD".equals(type)) {
            String str = "Password has been changed successfully!";
            Intent intent = new Intent(this, SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            ((Activity) this).overridePendingTransition(0, 0);
        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("back".equals(tag_FromWhere)) {
            this.finish();
        }
        if ("Update Password".equals(tag_FromWhere)) {
            currentPasswordString = activityChangePasswordBinding.currentPassword.getText().toString();
            newPasswordString = activityChangePasswordBinding.newPassword.getText().toString();
            confirmPasswordString = activityChangePasswordBinding.confirmPassword.getText().toString();
            if (TextUtils.isEmpty(currentPasswordString)) {
                AlphaHolder.customToast(this, "Please enter current password");
            } else if (TextUtils.isEmpty(newPasswordString)) {
                AlphaHolder.customToast(this, "Please enter new password");
            } else if (TextUtils.isEmpty(newPasswordString)) {
                AlphaHolder.customToast(this, "Please enter confirm password");
            } else {
                nameValuePairList = new ArrayList<>();
                nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(ChangePasswordActivity.this)));
                nameValuePairList.add(new BasicNameValuePair("current_password", currentPasswordString));
                nameValuePairList.add(new BasicNameValuePair("new_password", newPasswordString));
                if (SharedPreferences_Util.getRoleId(this).equalsIgnoreCase("1"))
                    new Service_Integration_Duplicate(this, nameValuePairList, "auth_api/business_change_pwd", this, "CHANGE_PASSWORD", true).execute();
                else
                    new Service_Integration_Duplicate(this, nameValuePairList, "auth_api/personnel_change_pwd", this, "CHANGE_PASSWORD", true).execute();
            }

        }
    }
}
