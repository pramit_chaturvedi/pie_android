package com.mydevit.pieservicess.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.utils.AlphaHolder;

import org.apache.http.NameValuePair;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import sun.bob.mcalendarview.MCalendarViewEvent;
import sun.bob.mcalendarview.MarkStyleEvent;
import sun.bob.mcalendarview.listeners.OnDateClickListenerEvent;
import sun.bob.mcalendarview.vo.DateDataEvent;

public class ConfigureCalenderActivity extends AppCompatActivity implements View.OnClickListener {

    // Define the variable of CalendarView type
    // and TextView type;
    //  CalendarView logoutDialog;
    //   TextView date_view;

    MCalendarViewEvent calendarView;
    String selectedDate = "";
    List<NameValuePair> nameValuePairList;
    String staticDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender_configure);
        getSupportActionBar().hide();

        calendarView = findViewById(R.id.calenderAAA);
        nameValuePairList = new ArrayList<>();

        initiateCateOnCalendar(AlphaHolder.calendarList, calendarView, "mark");

        Log.e("size", "" + AlphaHolder.calendarList.size());
        calendarView.setOnDateClickListener(new OnDateClickListenerEvent() {
            @Override
            public void onDateClick(View view, DateDataEvent date) {
                //Toast.makeText(ConfigureCalenderActivity.this, date.getDay() + " " + date.getMonth() + " " + date.getYear(), Toast.LENGTH_SHORT).show();
                staticDate = date.getDay() + "-" + date.getMonth() + "-" + date.getYear();
                AlphaHolder.selectedDate = parseDateToddMMyyyy(staticDate);

                initiateCateOnCalendar(AlphaHolder.calendarList, calendarView, "unMark");
                AlphaHolder.calendarList = new ArrayList<>();
                AlphaHolder.calendarList = getDay(date.getDayString(), date.getMonthString(), "" + date.getYear());
                ConfigureCalenderActivity.this.finish();
                // initiateCateOnCalendar(AlphaHolder.calendarList, calendarView);
            }
        });
        if ("".equals(AlphaHolder.selectedDate)) {
            String date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
            int dayInt = Integer.parseInt(date.substring(0, 2));
            int monthInt = Integer.parseInt(date.substring(3, 5));
            int yearInt = Integer.parseInt(date.substring(6, 10));
            AlphaHolder.calendarList = getDay("" + dayInt, "" + monthInt, "" + yearInt);

        } else {

        }
    }
    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "d-M-yyyy";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
    private void initiateCateOnCalendar(List<String> calendarList, MCalendarViewEvent mCalendarViewEvent, String type) {
        ArrayList<DateDataEvent> dates = new ArrayList<>();
        if (calendarList.size() > 0) {
            for (int j = 0; j < calendarList.size(); j++) {
                String dateValue = calendarList.get(j);

                int dayInt = Integer.parseInt(dateValue.substring(0, 2));
                int monthInt = Integer.parseInt(dateValue.substring(3, 5));
                int yearInt = Integer.parseInt(dateValue.substring(6, 10));
                // dates.add(new DateDataEvent(yearInt, monthInt, dayInt));

                if ("mark".equals(type)) {
                    calendarView.markDate(yearInt, monthInt, dayInt, MarkStyleEvent.DEFAULT);//mark multiple dates with this code.
                    Log.e("DATEISS", "" + dayInt + " " + monthInt + " " + yearInt);
                } else {
                    calendarView.unMarkDate(yearInt, monthInt, dayInt, MarkStyleEvent.DEFAULT);//mark multiple dates with this code.
                    Log.e("DATEISS", "" + dayInt + " " + monthInt + " " + yearInt);
                }
            }
        }

        /*      Log.d("marked dates:-", "" + calendarView.getMarkedDates());//get all marked dates.*/
    }

    private List<String> getDay(String day, String month, String year) {
        List<String> selectedDateNew = new ArrayList<>();
        String string;

        Calendar cal = Calendar.getInstance();
        if ("".equals(day)) {
            string = " 08/11/2019 15:15:50";
        } else {
            string = day + "/" + month + "/" + year + " 15:15:50";
        }
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
        try {
            Date date = format.parse(string);
            cal.setTime(date);//Set specific Date if you want to
        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (int i = Calendar.SUNDAY; i <= Calendar.SATURDAY; i++) {
            cal.set(Calendar.DAY_OF_WEEK, i);
            //  System.out.println(cal.getTime());

            SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
            System.out.println(cal.getTime());

            String formatted = format1.format(cal.getTime());
            System.out.println(formatted);

            //System.out.println();
            selectedDateNew.add(format1.format(cal.getTime()));
            Log.e("DATEISS", format1.format(cal.getTime()));


            //Returns Date
        }
        return selectedDateNew;
    }

    @Override
    public void onClick(View view) {

    }
}
