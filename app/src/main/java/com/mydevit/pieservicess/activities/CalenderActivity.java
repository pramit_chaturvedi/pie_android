package com.mydevit.pieservicess.activities;

import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.recyclerviewadapters.TimeSlot_RecyclerViewAdapter;
import com.mydevit.pieservicess.utils.CustomCalendarView;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalenderActivity extends AppCompatActivity implements CustomCalendarView.RobotoCalendarListener, ClickListener {

    @BindView(R.id.slotRecyclerViewId)
    RecyclerView slotRecyclerView;

    ArrayList<Boolean> dataArrayList;
    TimeSlot_RecyclerViewAdapter timeSlot_recyclerViewAdapter;

    ///logoutDialog
    private CustomCalendarView robotoCalendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        ButterKnife.bind(this);
        getSupportActionBar().hide();
        InitialteCalender();
        initialization();
    }
    private void initialization() {
        dataArrayList = new ArrayList();
        dataArrayList.add(true);
        for (int j = 0; j < 20; j++) {
            dataArrayList.add(false);
        }
      /*  slotRecyclerView.setHasFixedSize(true);
        slotRecyclerView.setLayoutManager(new GridLayoutManager(CalenderActivity.this, 2));
        timeSlot_recyclerViewAdapter = new TimeSlot_RecyclerViewAdapter(CalenderActivity.this, dataArrayList, this);
        slotRecyclerView.setAdapter(timeSlot_recyclerViewAdapter);*/
    }
    private void InitialteCalender() {
        robotoCalendarView = findViewById(R.id.robotoCalendarPicker);
        Button markDayButton = findViewById(R.id.markDayButton);
        Button clearSelectedDayButton = findViewById(R.id.clearSelectedDayButton);

        markDayButton.setOnClickListener(view -> {
            Calendar calendar = Calendar.getInstance();
            Random random = new Random(System.currentTimeMillis());
            int style = random.nextInt(2);
            int daySelected = random.nextInt(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.DAY_OF_MONTH, daySelected);

            switch (style) {
                case 0:
                    robotoCalendarView.markCircleImage1(calendar.getTime());
                    break;
                case 1:
                    robotoCalendarView.markCircleImage2(calendar.getTime());
                    break;
                default:
                    break;
            }
        });
        clearSelectedDayButton.setOnClickListener(v -> robotoCalendarView.clearSelectedDay());
        // Set listener, in this case, the same activity
        robotoCalendarView.setRobotoCalendarListener(this);

        robotoCalendarView.setShortWeekDays(false);

        robotoCalendarView.showDateTitle(true);

        robotoCalendarView.setDate(new Date());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
      /*  if (requestCode == ADD_NOTE && resultCode == RESULT_OK) {
            MyEventDay myEventDay = data.getParcelableExtra(RESULT);
            mCalendarView.setDate(myEventDay.getCalendar());
            mEventDays.add(myEventDay);
            mCalendarView.setEvents(mEventDays);
        }*/
    }

    @Override
    public void onDayClick(String date, ArrayList<String> stringArrayList) {

    }

    @Override
    public void onDayLongClick(Date date, ArrayList<String> stringArrayList) {

    }

    @Override
    public void onRightButtonClick() {
        // Toast.makeText(context, "onRightButtonClick!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLeftButtonClick() {
        // Toast.makeText(context, "onLeftButtonClick!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("TIMESLOT".equals(tag_FromWhere)) {

        }
    }
}
