package com.mydevit.pieservicess.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityConfirmPaymentBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.ConfirmBillingRow_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.ConfirmOrder_CreditCard_ServiceModel;
import com.mydevit.pieservicess.service.ConfirmPayment_ServiceModel;
import com.mydevit.pieservicess.service.Payment_submit_model;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class ConfirmPaymentActivity extends AppCompatActivity implements ClickListener, ServiceReponseInterface_Duplicate {
    ConfirmBillingRow_RecyclerViewAdapter confirmBillingRow_recyclerViewAdapter;
    ActivityConfirmPaymentBinding activityConfirmPaymentBinding;

    String responseData, customer_receipt_mode;
    Payment_submit_model payment_submit_model;

    List<NameValuePair> nameValuePairList;
    String location_id, personnel_id;

    ConfirmOrder_CreditCard_ServiceModel confirmOrder_creditCard_serviceModel;

    double tipAmount;
    double totalPriceFloat;
    double afterTipAmount;
    String tipAmountFromService, cartId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityConfirmPaymentBinding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_payment);
        getSupportActionBar().hide();

        getDataFrom();
        dataManipulation();
        radioButtonClickHandler();
        onTextChnagedListner();
    }

    private void onTextChnagedListner() {
        activityConfirmPaymentBinding.tipAmoutEditTextId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String enteredData = charSequence.toString();
                if (!"".equals(enteredData)) {
                    payment_submit_model.getData().getCart_detail().setTip_amount(getDataAfterTwoPoint(Double.parseDouble(enteredData)));
                    double enteredAmount = Double.parseDouble(enteredData);
                    double totalPrice = Double.parseDouble(payment_submit_model.getData().getCart_detail().getTotal());

                    totalPriceFloat = enteredAmount + totalPrice;
                    activityConfirmPaymentBinding.setTotalAmount("" + totalPriceFloat);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public String getDataAfterTwoPoint(double value) {
        return new DecimalFormat("##.##").format(value);
    }

    private void radioButtonClickHandler() {
        activityConfirmPaymentBinding.tipRadioGroupId.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                View radioButton = activityConfirmPaymentBinding.tipRadioGroupId.findViewById(checkedId);
                int index = activityConfirmPaymentBinding.tipRadioGroupId.indexOfChild(radioButton);

                String totalPrice = payment_submit_model.getData().getCart_detail().getTotal();
                totalPriceFloat = Double.parseDouble(totalPrice);

                switch (index) {
                    case 0: // first button

                        tipAmount = getPercentOfTotal(10, totalPriceFloat);
                        payment_submit_model.getData().getCart_detail().setTip_amount(getDataAfterTwoPoint(tipAmount));

                        afterTipAmount = tipAmount + totalPriceFloat;
                        // payment_submit_model.getData().getCart_detail().setTotal("" + afterTipAmount);
                        activityConfirmPaymentBinding.setTotalAmount("" + afterTipAmount);
                        activityConfirmPaymentBinding.setConfirmPaymentData(payment_submit_model);

                        break;
                    case 1: // secondbutton

                        tipAmount = getPercentOfTotal(15, totalPriceFloat);

                        payment_submit_model.getData().getCart_detail().setTip_amount(getDataAfterTwoPoint(tipAmount));

                        afterTipAmount = tipAmount + totalPriceFloat;

                        // payment_submit_model.getData().getCart_detail().setTotal("" + afterTipAmount);
                        activityConfirmPaymentBinding.setTotalAmount("" + afterTipAmount);
                        activityConfirmPaymentBinding.setConfirmPaymentData(payment_submit_model);


                        //Toast.makeText(getApplicationContext(), "Selected button number " + index, Toast.LENGTH_LONG).show();
                        break;
                    case 2: // secondbutton

                        tipAmount = getPercentOfTotal(20, totalPriceFloat);
                        payment_submit_model.getData().getCart_detail().setTip_amount(getDataAfterTwoPoint(tipAmount));
                        afterTipAmount = tipAmount + totalPriceFloat;

                        //  payment_submit_model.getData().getCart_detail().setTotal("" + afterTipAmount);
                        activityConfirmPaymentBinding.setTotalAmount("" + afterTipAmount);
                        activityConfirmPaymentBinding.setConfirmPaymentData(payment_submit_model);

                        //Toast.makeText(getApplicationContext(), "Selected button number " + index, Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });


        activityConfirmPaymentBinding.receiptRadiobuttonId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    customer_receipt_mode = "email";
                else
                    customer_receipt_mode = "";
            }
        });

    }

    private double getPercentOfTotal(int howManyPercent, double totalPrice) {
        double res = (totalPrice / 100.0f) * howManyPercent;
        return res;
    }

    private void dataManipulation() {
        confirmBillingRow_recyclerViewAdapter = new ConfirmBillingRow_RecyclerViewAdapter(ConfirmPaymentActivity.this, payment_submit_model.getData().getCart_item_detail(), this);
        activityConfirmPaymentBinding.setSetAdapter(confirmBillingRow_recyclerViewAdapter);
        activityConfirmPaymentBinding.setConfirmPaymentData(payment_submit_model);
        activityConfirmPaymentBinding.setTotalAmount(payment_submit_model.getData().getCart_detail().getTotal());
        activityConfirmPaymentBinding.setClickListener(this);
        activityConfirmPaymentBinding.executePendingBindings();
    }

    private void getDataFrom() {
        if (getIntent().getExtras() != null) {
            responseData = getIntent().getExtras().getString("confirmData");
            location_id = getIntent().getExtras().getString("location_id");
            personnel_id = getIntent().getExtras().getString("personnel_id");
            payment_submit_model = new Gson().fromJson(responseData, Payment_submit_model.class);
        }
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("Confirm Payment".equals(tag_FromWhere)) {
            nameValuePairList = new ArrayList<>();

            String paymentType = payment_submit_model.getData().getPayment_mode().getPayment_mode_id();
            if (paymentType.equals("2")) {
                nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(ConfirmPaymentActivity.this)));
                nameValuePairList.add(new BasicNameValuePair("cart_id", payment_submit_model.getData().getCart_detail().getCart_id()));
                nameValuePairList.add(new BasicNameValuePair("payment_type", payment_submit_model.getData().getPayment_mode().getPayment_mode_id()));
                nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
                nameValuePairList.add(new BasicNameValuePair("personnel_id", personnel_id));
                nameValuePairList.add(new BasicNameValuePair("tip_amount", payment_submit_model.getData().getCart_detail().getTip_amount()));
                nameValuePairList.add(new BasicNameValuePair("customer_receipt_mode", customer_receipt_mode));
                nameValuePairList.add(new BasicNameValuePair("role_id", SharedPreferences_Util.getRoleId(ConfirmPaymentActivity.this)));
                nameValuePairList.add(new BasicNameValuePair("total_amount", activityConfirmPaymentBinding.alAmountTexTviewId.getText().toString()));
                new Service_Integration_Duplicate(ConfirmPaymentActivity.this, nameValuePairList, "business/pos_setup/pos_credit_card_pay", this, "CONFIRM_ORDER_CREDIT_CARD", true).execute();

            } else {
                nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(ConfirmPaymentActivity.this)));
                nameValuePairList.add(new BasicNameValuePair("cart_id", payment_submit_model.getData().getCart_detail().getCart_id()));
                nameValuePairList.add(new BasicNameValuePair("payment_type", payment_submit_model.getData().getPayment_mode().getPayment_mode_id()));
                nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
                nameValuePairList.add(new BasicNameValuePair("personnel_id", personnel_id));
                nameValuePairList.add(new BasicNameValuePair("tip_amount", payment_submit_model.getData().getCart_detail().getTip_amount()));
                nameValuePairList.add(new BasicNameValuePair("customer_receipt_mode", customer_receipt_mode));
                new Service_Integration_Duplicate(ConfirmPaymentActivity.this, nameValuePairList, "business/pos_setup/checkout_confirm_payment", this, "CONFIRM_ORDER", true).execute();

            }


        }
        if ("back".equals(tag_FromWhere)) {
            ConfirmPaymentActivity.this.finish();
        }
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("CONFIRM_ORDER".equals(type)) {
            ConfirmPayment_ServiceModel confirmPayment_serviceModel = new Gson().fromJson(result, ConfirmPayment_ServiceModel.class);
            if ("Payment Successfull".equals(confirmPayment_serviceModel.getMessage())) {

                Toast.makeText(ConfirmPaymentActivity.this, confirmPayment_serviceModel.getMessage(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ConfirmPaymentActivity.this, PaymentSuccessActivity.class);
                intent.putExtra("amount", confirmPayment_serviceModel.getData().getAmount());
                intent.putExtra("order_id", confirmPayment_serviceModel.getData().getOrder_id());
                intent.putExtra("transaction_id", confirmPayment_serviceModel.getData().getTransaction_id());
                startActivity(intent);
                ConfirmPaymentActivity.this.finish();

                /*BillActivity billActivity = new BillActivity();
                billActivity.finish();
                AppointmentsActivity appointmentsActivity = new AppointmentsActivity();
                appointmentsActivity.finish();
                ConfirmPaymentActivity.this.finish();*/
            }
        }
        if ("CONFIRM_ORDER_CREDIT_CARD".equals(type)) {
            confirmOrder_creditCard_serviceModel = new Gson().fromJson(result, ConfirmOrder_CreditCard_ServiceModel.class);
            if (confirmOrder_creditCard_serviceModel.getData() != null) {
                if (confirmOrder_creditCard_serviceModel.getData().getCard_screen_data() != null) {

                    tipAmountFromService = confirmOrder_creditCard_serviceModel.getData().getCard_screen_data().getTip_amount();
                    cartId = confirmOrder_creditCard_serviceModel.getData().getCard_screen_data().getCart_id();
                    Intent intent = new Intent(ConfirmPaymentActivity.this, CardDetailActivity.class);
                    intent.putExtra("tipamount", tipAmount);
                    intent.putExtra("cartid", cartId);
                    intent.putExtra("payment_type", confirmOrder_creditCard_serviceModel.getData().getCard_screen_data().getPay_type());
                    intent.putExtra("location_id", location_id);
                    startActivity(intent);


                }
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }
}
