package com.mydevit.pieservicess.activities;

import android.os.Bundle;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityForgotPasswordConfirmationBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.model.Forgot_Password_Model;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ForgotPassword_ConfirmationActivity extends AppCompatActivity implements ClickListener, ServiceReponseInterface_Duplicate {

    Forgot_Password_Model forgot_password_model;
    ActivityForgotPasswordConfirmationBinding binding;
    List<NameValuePair> nameValuePairList;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();

        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password__confirmation);
        binding.setClickListener(this);
        forgot_password_model = new Forgot_Password_Model();
        binding.setMainDataObject(forgot_password_model);
        getDataFromIntent();
    }

    private void getDataFromIntent() {
        email = getIntent().getExtras().getString("email");
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("Submit".equals(tag_FromWhere)) {
            if (TextUtils.isEmpty(forgot_password_model.getCodeGorFromEmail())) {
                AlphaHolder.customToast(ForgotPassword_ConfirmationActivity.this, getResources().getString(R.string.verificationcodecannotbeempty));
            } else if (TextUtils.isEmpty(forgot_password_model.getPassword())) {
                AlphaHolder.customToast(ForgotPassword_ConfirmationActivity.this, getResources().getString(R.string.passwordcantbeempty));
            } else if (TextUtils.isEmpty(forgot_password_model.getConfirmedPassword())) {
                AlphaHolder.customToast(ForgotPassword_ConfirmationActivity.this, getResources().getString(R.string.enterconfirmpassword));
            } else if (!forgot_password_model.getPassword().equals(forgot_password_model.getConfirmedPassword())) {
                AlphaHolder.customToast(ForgotPassword_ConfirmationActivity.this, getResources().getString(R.string.passwordandcanfirmcantbsame));
            } else {
                nameValuePairList = new ArrayList<>();
                nameValuePairList.add(new BasicNameValuePair("email", email));
                nameValuePairList.add(new BasicNameValuePair("reset_code", forgot_password_model.getCodeGorFromEmail()));
                nameValuePairList.add(new BasicNameValuePair("new_password", forgot_password_model.getPassword()));

                new Service_Integration_Duplicate(ForgotPassword_ConfirmationActivity.this, nameValuePairList, "auth_api/business_check_resetcode", this, "RESET_PASSWORD", true).execute();
            }
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onSuccess(String result, String type) {
        if ("RESET_PASSWORD".equals(type)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String message = jsonObject.getString("message");
                if ("Password Updated Successfully".equals(message)) {
                    ForgotPassword_ConfirmationActivity.this.finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }
}
