package com.mydevit.pieservicess.activities;

import android.icu.util.Calendar;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivitySchedularCalenderBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.ScheduleTimeSlot_Parent_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.ConfigureAvailabiltyModel;
import com.mydevit.pieservicess.service.SaveSlotModel;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SchedularCalenderActivity extends AppCompatActivity implements ClickListenerPC, ClickListener, ServiceReponseInterface_Duplicate {
    ActivitySchedularCalenderBinding binding;
    ArrayList<String> timeSlotList;
    ScheduleTimeSlot_Parent_RecyclerViewAdapter scheduleTimeSlot_Parent_recyclerViewAdapter;
    List<NameValuePair> basicNameValuePairArrayList;
    String personal_id, location_id;
    String weekNo;
    String personal_today_date;
    List<String> daysName;
    private Parcelable recyclerViewSchedular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_schedular_calender);
        binding.setOnClick(this);
        updateDayName();
    }

    private void updateDayName() {
        daysName = new ArrayList<>();

        daysName.add("Mon");
        daysName.add("Tue");
        daysName.add("Wed");
        daysName.add("Thu");
        daysName.add("Fri");
        daysName.add("Sat");
        daysName.add("Sun");

        getDataFrom();
    }

    private void getDataFrom() {
        weekNo = getIntent().getExtras().getString("week_no");
        location_id = getIntent().getExtras().getString("location_id");
        personal_id = getIntent().getExtras().getString("personal_id");
        timeSlotList = getIntent().getStringArrayListExtra("timeSlotList");
        personal_today_date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        ((SimpleItemAnimator) binding.mainRecyclerViewId.getItemAnimator()).setSupportsChangeAnimations(false);

        if (AlphaHolder.personnelTimingsList.size() == 0) {
            manageDataInBlnk();
        } else if (!isViewAbleToVisible()) {

            ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean.DayTimingsBean dayTimingsBean = new ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean.DayTimingsBean();
            dayTimingsBean.setOpen_time("");
            dayTimingsBean.setClose_time("");
            for (int i = 0; i < AlphaHolder.personnelTimingsList.size(); i++) {
                AlphaHolder.personnelTimingsList.get(i).getDay_timings().add(dayTimingsBean);

                if (i == AlphaHolder.personnelTimingsList.size() - 1) {
                    scheduleTimeSlot_Parent_recyclerViewAdapter = new ScheduleTimeSlot_Parent_RecyclerViewAdapter(SchedularCalenderActivity.this, AlphaHolder.personnelTimingsList, this, timeSlotList);
                    binding.setMainData(scheduleTimeSlot_Parent_recyclerViewAdapter);
                    binding.executePendingBindings();
                }
            }
        } else {
            scheduleTimeSlot_Parent_recyclerViewAdapter = new ScheduleTimeSlot_Parent_RecyclerViewAdapter(SchedularCalenderActivity.this, AlphaHolder.personnelTimingsList, this, timeSlotList);
            binding.setMainData(scheduleTimeSlot_Parent_recyclerViewAdapter);
            binding.executePendingBindings();
        }


    }

    private boolean isViewAbleToVisible() {
        for (int j = 0; j < AlphaHolder.personnelTimingsList.size(); j++) {
            if (AlphaHolder.personnelTimingsList.get(j).getDay_timings().size() > 0) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    private void manageDataInBlnk() {
        AlphaHolder.personnelTimingsList = new ArrayList<>();

        for (int i = 0; i < daysName.size(); i++) {
            ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean personnelTimingsBean = new ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean();

            int dayNum = i + 1;
            personnelTimingsBean.setDay_name(daysName.get(i));
            personnelTimingsBean.setIs_off("Y");
            personnelTimingsBean.setDay_num(String.valueOf(dayNum));
            personnelTimingsBean.setIsSelected("N");
            personnelTimingsBean.setPersonnel_schedule_id("");

            List<ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean.DayTimingsBean> dayTimingsBeans = new ArrayList<>();
            ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean.DayTimingsBean dayTimingsBean = new ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean.DayTimingsBean();
            dayTimingsBeans.add(dayTimingsBean);

            personnelTimingsBean.setDay_timings(dayTimingsBeans);

            AlphaHolder.personnelTimingsList.add(personnelTimingsBean);

            if (i == daysName.size() - 1) {
                scheduleTimeSlot_Parent_recyclerViewAdapter = new ScheduleTimeSlot_Parent_RecyclerViewAdapter(SchedularCalenderActivity.this, AlphaHolder.personnelTimingsList, this, timeSlotList);
                binding.setMainData(scheduleTimeSlot_Parent_recyclerViewAdapter);
                binding.executePendingBindings();

            }
        }
    }

    @Override
    public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
        if ("addview".equals(tag_FromWhere)) {
            //AlphaHolder.customToast(SchedularCalenderActivity.this, "ADD" + parentPosition + "  " + childPosition);
            if (childPosition > 0) {
                recyclerViewSchedular = binding.mainRecyclerViewId.getLayoutManager().onSaveInstanceState();
                AlphaHolder.personnelTimingsList.get(parentPosition).getDay_timings().remove(childPosition);
                binding.mainRecyclerViewId.getItemAnimator().endAnimations();
                binding.getMainData().notifyDataSetChanged();
                binding.mainRecyclerViewId.getLayoutManager().onRestoreInstanceState(recyclerViewSchedular);

            } else {
                if ("Y".equals(AlphaHolder.personnelTimingsList.get(parentPosition).getIs_off())) {

                } else {
                    ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean.DayTimingsBean dayTimingsBean = new ConfigureAvailabiltyModel.DataBean.PersonnelBean.PersonnelTimingsBean.DayTimingsBean();
                    dayTimingsBean.setClose_time("");
                    dayTimingsBean.setOpen_time("");
                    AlphaHolder.personnelTimingsList.get(parentPosition).getDay_timings().add(dayTimingsBean);
                    binding.mainRecyclerViewId.getItemAnimator().endAnimations();
                    binding.getMainData().notifyDataSetChanged();
                }
            }
        }
        if ("scheduledayclicked".equals(tag_FromWhere)) {
            for (int i = 0; i < AlphaHolder.personnelTimingsList.size(); i++) {
                if (i == parentPosition) {
                    String value = AlphaHolder.personnelTimingsList.get(i).getIs_off();
                    if ("Y".equals(value))
                        AlphaHolder.personnelTimingsList.get(i).setIs_off("N");
                    else
                        AlphaHolder.personnelTimingsList.get(i).setIs_off("Y");
                }
            }
        }
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("back".equals(tag_FromWhere)) {
            SchedularCalenderActivity.this.finish();

        }
        if ("Save".equals(tag_FromWhere)) {

            String dataToSend = new Gson().toJson(AlphaHolder.personnelTimingsList);
            Log.e("DATATOSEND", dataToSend);

            basicNameValuePairArrayList = new ArrayList<>();
            basicNameValuePairArrayList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(SchedularCalenderActivity.this)));
            basicNameValuePairArrayList.add(new BasicNameValuePair("personnel_location_id", location_id));
            basicNameValuePairArrayList.add(new BasicNameValuePair("week_start_date", getStartEndOFWeek(Integer.parseInt(weekNo), Calendar.getInstance().get(Calendar.YEAR))));
            basicNameValuePairArrayList.add(new BasicNameValuePair("personnel_today_date", personal_today_date));
            basicNameValuePairArrayList.add(new BasicNameValuePair("data", "[{\"personnel_id\":\"" + personal_id + "\",\"personnel_timings\":" + dataToSend + "}]"));
            basicNameValuePairArrayList.add(new BasicNameValuePair("personnel_week_num", weekNo));
            new Service_Integration_Duplicate(SchedularCalenderActivity.this, basicNameValuePairArrayList, "business/Scheduler/new_save_personnel_day_timings", this, "SAVE_SLOT_DATA", true).execute();

            //AlphaHolder.customToast(SchedularCalenderActivity.this, "SAVE");

        }
    }

    String getStartEndOFWeek(int enterWeek, int enterYear) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        calendar.set(Calendar.YEAR, enterYear);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // PST`
        Date startDate = calendar.getTime();
        String startDateInStr = formatter.format(startDate);
        System.out.println("...date..." + startDateInStr);


        return startDateInStr;
    }

    String getEndOFWeek(int enterWeek, int enterYear) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        calendar.set(Calendar.YEAR, enterYear);

        SimpleDateFormat formatter = new SimpleDateFormat("ddMMM yyyy"); // PST`
        calendar.add(Calendar.DATE, 6);
        Date enddate = calendar.getTime();
        String endDaString = formatter.format(enddate);
        System.out.println("...date..." + endDaString);


        return endDaString;
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("SAVE_SLOT_DATA".equals(type)) {
            SaveSlotModel saveSlotModel = new Gson().fromJson(result, SaveSlotModel.class);
            if (saveSlotModel != null) {
                if ("Personnel Appointment Saved Successfully".equals(saveSlotModel.getMessage())) {
                    SchedularCalenderActivity.this.finish();
                }
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
