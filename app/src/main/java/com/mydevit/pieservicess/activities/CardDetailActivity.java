package com.mydevit.pieservicess.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityCardDetailBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.model.CardDetail_Model;
import com.mydevit.pieservicess.service.CardDetail_ServiceModel;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;
import com.paysafe.Environment;
import com.paysafe.PaysafeApiClient;
import com.paysafe.customervault.SingleUseToken;
import com.paysafe.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CardDetailActivity extends AppCompatActivity implements ClickListenerPC, ServiceReponseInterface_Duplicate {

    ActivityCardDetailBinding cardDetailBinding;
    CardDetail_Model cardDetail_model;
    String cartId, tipAmount, location_id, payment_type;
    List<NameValuePair> nameValuePairList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        cardDetailBinding = DataBindingUtil.setContentView(this, R.layout.activity_card_detail);
        cardDetail_model = new CardDetail_Model();
        cardDetailBinding.setCardDetailData(cardDetail_model);
        cardDetailBinding.setClickListener(this);
        getIntentData();
    }

    private void getIntentData() {
        if (getIntent().getExtras() != null) {
            cartId = getIntent().getExtras().getString("cartid");
            tipAmount = getIntent().getExtras().getString("tipamount");
            location_id = getIntent().getExtras().getString("location_id");
            payment_type = getIntent().getExtras().getString("payment_type");
        }
    }

    @Override
    public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
        if ("submitpaymenttag".equals(tag_FromWhere)) {
            if (TextUtils.isEmpty(cardDetail_model.getName())) {
                AlphaHolder.customToast(CardDetailActivity.this, getResources().getString(R.string.selectcountry));
            } else if (TextUtils.isEmpty(cardDetail_model.getCardNo())) {
                AlphaHolder.customToast(CardDetailActivity.this, getResources().getString(R.string.selectcountry));
            } else if (TextUtils.isEmpty(cardDetail_model.getCvvString())) {
                AlphaHolder.customToast(CardDetailActivity.this, getResources().getString(R.string.selectcountry));
            } else if (TextUtils.isEmpty(cardDetail_model.getExpiryMonth())) {
                AlphaHolder.customToast(CardDetailActivity.this, getResources().getString(R.string.selectcountry));
            } else if (TextUtils.isEmpty(cardDetail_model.getExpiryYear())) {
                AlphaHolder.customToast(CardDetailActivity.this, getResources().getString(R.string.selectcountry));
            } else if (TextUtils.isEmpty(cardDetail_model.getMailid())) {
                AlphaHolder.customToast(CardDetailActivity.this, getResources().getString(R.string.selectcountry));
            } else {
                SingleUseToken singleUseToken = singleUseTokenRequest();
                if (singleUseToken != null) {
                    sendTokenToServer(singleUseToken.getPaymentToken());
                }
            }
        }
    }


    private SingleUseToken singleUseTokenRequest() {
        PaysafeApiClient client = new PaysafeApiClient("OT-438580", "B-qa2-0-5e28b73e-0-302c021468f81617a15697f620f0897cdbcc3debd65ea60602146cbd07adf56ab77ac0b1023da979a70cf8316c68",
                Environment.TEST, "1001651920");


        try {
            SingleUseToken sObjResponse;
            // Check if Month field of Card Expiry

            // Make API call for single use token
            sObjResponse = client.customerVaultService().createSingleUseToken(
                    SingleUseToken.builder()
                            .card()
                            .holderName(cardDetail_model.getName())
                            .cardNum(cardDetail_model.getCardNo())
                            .cardExpiry()
                            .month(Integer.valueOf(cardDetail_model.getExpiryMonth()))
                            .year(Integer.valueOf(cardDetail_model.getExpiryYear()))
                            .done()
                            .billingAddress()
                            .done()
                            .done()
                            .build());

            Log.d("Token", sObjResponse.getPaymentToken());

            return sObjResponse;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    } // end of singleUseTokenRequest()


    private void sendTokenToServer(String token) {
        nameValuePairList = new ArrayList<>();

        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(CardDetailActivity.this)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        nameValuePairList.add(new BasicNameValuePair("cart_id", cartId));
        nameValuePairList.add(new BasicNameValuePair("payment_type", payment_type));
        nameValuePairList.add(new BasicNameValuePair("tip_amount", tipAmount));
        nameValuePairList.add(new BasicNameValuePair("name", cardDetail_model.getName()));
        nameValuePairList.add(new BasicNameValuePair("email", cardDetail_model.getMailid()));
        nameValuePairList.add(new BasicNameValuePair("card_num", cardDetail_model.getCardNo()));
        nameValuePairList.add(new BasicNameValuePair("cvv", cardDetail_model.getCvvString()));
        nameValuePairList.add(new BasicNameValuePair("exp_month", cardDetail_model.getExpiryMonth()));
        nameValuePairList.add(new BasicNameValuePair("exp_year", cardDetail_model.getExpiryYear()));
        nameValuePairList.add(new BasicNameValuePair("paysafe_token", token));

        new Service_Integration_Duplicate(CardDetailActivity.this, nameValuePairList, "business/pos_setup/checkout_confirm_payment", this, "SUBMIT_PAYMENT_DETAIL", true).execute();

    }

    @Override
    public void onSuccess(String result, String type) {
        if ("SUBMIT_PAYMENT_DETAIL".equals(type)) {
            CardDetail_ServiceModel cardDetail_serviceModel = new Gson().fromJson(result, CardDetail_ServiceModel.class);
            if (cardDetail_serviceModel != null) {
                AlphaHolder.customToast(CardDetailActivity.this, cardDetail_serviceModel.getMessage());
                Intent intent = new Intent(CardDetailActivity.this, PaymentSuccessActivity.class);
                intent.putExtra("amount", cardDetail_serviceModel.getData().getAmount());
                intent.putExtra("order_id", cardDetail_serviceModel.getData().getOrder_id());
                intent.putExtra("transaction_id", cardDetail_serviceModel.getData().getTransaction_id());
                startActivity(intent);
                CardDetailActivity.this.finish();
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
