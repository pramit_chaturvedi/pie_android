package com.mydevit.pieservicess.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityForgotBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ForgotActivity extends AppCompatActivity implements ClickListener, ServiceReponseInterface_Duplicate {

    ActivityForgotBinding activityForgotBinding;
    List<NameValuePair> nameValuePairList;
    String emailString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        activityForgotBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot);
        activityForgotBinding.setClickListener(this);
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("sendtoclick".equals(tag_FromWhere)) {
            emailString = activityForgotBinding.emailAddressEditTextId.getText().toString();
            if (TextUtils.isEmpty(emailString)) {
                AlphaHolder.customToast(ForgotActivity.this, getResources().getString(R.string.emailmustrequire));
            } else if (!AlphaHolder.isEmailValid(emailString)) {
                AlphaHolder.customToast(ForgotActivity.this, getResources().getString(R.string.emailmustvalid));
            } else {
                nameValuePairList = new ArrayList<>();
                nameValuePairList.add(new BasicNameValuePair("email", emailString));
                new Service_Integration_Duplicate(ForgotActivity.this, nameValuePairList, "auth_api/business_forgot_password", this, "FORGOT_PASSWORD", true).execute();
            }

        }
        if ("back".equals(tag_FromWhere)) {
            ForgotActivity.this.finish();
        }
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("FORGOT_PASSWORD".equals(type)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String message = jsonObject.getString("message");
                if ("Reset Code Successfully Send.Please check your email".equals(message)) {

                    Intent intent = new Intent(ForgotActivity.this, ForgotPassword_ConfirmationActivity.class);
                    intent.putExtra("email", emailString);
                    startActivity(intent);
                    overridePendingTransition(0, 0);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
