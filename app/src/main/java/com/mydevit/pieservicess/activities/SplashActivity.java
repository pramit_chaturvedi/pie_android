package com.mydevit.pieservicess.activities;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.mydevit.pieservicess.Firebase.Config;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.navigationDrawer.SlidingActivity;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;
import com.paysafe.Environment;
import com.paysafe.PaysafeApiClient;
import com.paysafe.customervault.SingleUseToken;
import com.paysafe.utils.Utils;
import com.pubnub.api.PubNub;

import java.io.IOException;

public class SplashActivity extends AppCompatActivity {

    public static PubNub pubnub;
    SharedPreferences_Util sharedPreferences_util;
    private Handler mWaitHandler = new Handler();
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        initialization();

        /*mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                  //  txtMessage.setText(message);
                }
            }
        };

        displayFirebaseRegId();*/
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        // Log.e("Firebase reg id: " , regId);


    }

    @Override
    protected void onResume() {
        super.onResume();

       /* // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());*/
    }

    @Override
    protected void onPause() {
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void initialization() {
        //   String token = FirebaseInstanceId.getInstance().getToken();
        //   Log.e("token",token);
        sharedPreferences_util = new SharedPreferences_Util(SplashActivity.this);
        mWaitHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (null != sharedPreferences_util.getLoginModel()) {

                        Intent intent = new Intent(SplashActivity.this, SlidingActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        SplashActivity.this.finish();

                    } else {
                        Intent intent = new Intent(SplashActivity.this, SignInActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0, 0);
                        SplashActivity.this.finish();

                    }
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
            }
        }, 2000);  // Give a 5 seconds delay.


    }


}
