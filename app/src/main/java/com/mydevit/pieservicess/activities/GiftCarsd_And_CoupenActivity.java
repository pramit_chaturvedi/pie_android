package com.mydevit.pieservicess.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityGiftCarsdAndCoupenBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GiftCarsd_And_CoupenActivity extends AppCompatActivity implements ClickListener, AdapterView.OnItemSelectedListener, ServiceReponseInterface_Duplicate {

    ActivityGiftCarsdAndCoupenBinding activityGiftCarsdAndCoupenBinding;
    List<NameValuePair> nameValuePairList;
    List<String> discountStarusList, discountCategoryList;

    String discountCategoryString, discountValueString, discountLimitString, discountCodeString, discountType, discountStstusString, location_Id, discount_offer_id;
    String from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        activityGiftCarsdAndCoupenBinding = DataBindingUtil.setContentView(this, R.layout.activity_gift_carsd__and__coupen);
        activityGiftCarsdAndCoupenBinding.setClickListener(this);


        spinnerClickData();
        getDiscountCategoryData();
        getIntentData();

    }

    private void getIntentData() {
        if (getIntent().getExtras() != null) {
            from = getIntent().getExtras().getString("from");
            location_Id = getIntent().getExtras().getString("location_id");

            if ("editdata".equals(from)) {

                discountCategoryString = getIntent().getExtras().getString("discount_category");
                discountValueString = getIntent().getExtras().getString("discount_value");
                discountLimitString = getIntent().getExtras().getString("discount_limit");
                discountCodeString = getIntent().getExtras().getString("discount_code");
                discountType = getIntent().getExtras().getString("discount_type");
                discountStstusString = getIntent().getExtras().getString("discount_status");
                discount_offer_id = getIntent().getExtras().getString("discount_offer_id");

                activityGiftCarsdAndCoupenBinding.discountValueEditTextId.setText(discountValueString);
                activityGiftCarsdAndCoupenBinding.discountlimitEditTextId.setText(discountLimitString);
                activityGiftCarsdAndCoupenBinding.discountCodeEditTextId.setText(discountCodeString);

                if ("fixed".equals(discountType) || "Fixed".equals(discountType))
                    activityGiftCarsdAndCoupenBinding.fixedTypeRBId.setChecked(true);
                else
                    activityGiftCarsdAndCoupenBinding.percentTypeRBId.setChecked(true);


                if ("Y".equals(discountStstusString))
                    activityGiftCarsdAndCoupenBinding.discountStatusSpinnerId.setSelection(0);
                else
                    activityGiftCarsdAndCoupenBinding.discountStatusSpinnerId.setSelection(1);


            } else {

            }
        }
    }

    private void spinnerClickData() {
        activityGiftCarsdAndCoupenBinding.discountCategorySpinnerId.setOnItemSelectedListener(this);
        activityGiftCarsdAndCoupenBinding.discountStatusSpinnerId.setOnItemSelectedListener(this);
    }

    private void getDiscountCategoryData() {
        discountStarusList = new ArrayList<>();
        discountCategoryList = new ArrayList<>();

        //now all data is static
        discountCategoryList.add("Coupen");

        discountStarusList.add("Active");
        discountStarusList.add("In Active");

        ArrayAdapter arrayAdapter = new ArrayAdapter(GiftCarsd_And_CoupenActivity.this, android.R.layout.simple_list_item_1, discountCategoryList);
        activityGiftCarsdAndCoupenBinding.setDiscountcategory(arrayAdapter);

        ArrayAdapter arrayAdapterTwo = new ArrayAdapter(GiftCarsd_And_CoupenActivity.this, android.R.layout.simple_list_item_1, discountStarusList);
        activityGiftCarsdAndCoupenBinding.setDiscountstatus(arrayAdapterTwo);

        activityGiftCarsdAndCoupenBinding.executePendingBindings();
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        String discountStatusTemp = "";
        if ("addgift".equals(tag_FromWhere)) {

            if (activityGiftCarsdAndCoupenBinding.percentTypeRBId.isChecked())
                discountType = "percentage";
            else
                discountType = "fixed";

            if ("Active".equals(discountStstusString))
                discountStatusTemp = "Y";
            else
                discountStatusTemp = "I";


            discountValueString = activityGiftCarsdAndCoupenBinding.discountValueEditTextId.getText().toString();
            discountLimitString = activityGiftCarsdAndCoupenBinding.discountlimitEditTextId.getText().toString();
            discountCodeString = activityGiftCarsdAndCoupenBinding.discountCodeEditTextId.getText().toString();

            if ("editdata".equals(from)) {

                nameValuePairList = new ArrayList<>();
                nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(GiftCarsd_And_CoupenActivity.this)));
                nameValuePairList.add(new BasicNameValuePair("discount_offer_id", discount_offer_id));
                nameValuePairList.add(new BasicNameValuePair("location", location_Id));
                nameValuePairList.add(new BasicNameValuePair("discount_category", discountCategoryString));
                nameValuePairList.add(new BasicNameValuePair("discount_code", discountCodeString));

                nameValuePairList.add(new BasicNameValuePair("discount_type", discountType));
                nameValuePairList.add(new BasicNameValuePair("discount_value", discountValueString));
                nameValuePairList.add(new BasicNameValuePair("discount_limit", discountLimitString));
                nameValuePairList.add(new BasicNameValuePair("discount_status", discountStatusTemp));

                new Service_Integration_Duplicate(GiftCarsd_And_CoupenActivity.this, nameValuePairList, "business/pos_setup/edit_discount_offer", this, "ADD_DISCOUNT_OFFER", true).execute();


            } else {
                nameValuePairList = new ArrayList<>();
                nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(GiftCarsd_And_CoupenActivity.this)));
                nameValuePairList.add(new BasicNameValuePair("location", location_Id));
                nameValuePairList.add(new BasicNameValuePair("discount_category", discountCategoryString));
                nameValuePairList.add(new BasicNameValuePair("discount_code", discountCodeString));
                nameValuePairList.add(new BasicNameValuePair("discount_type", discountType));
                nameValuePairList.add(new BasicNameValuePair("discount_value", discountValueString));
                nameValuePairList.add(new BasicNameValuePair("discount_limit", discountLimitString));
                nameValuePairList.add(new BasicNameValuePair("discount_status", discountStatusTemp));
                new Service_Integration_Duplicate(GiftCarsd_And_CoupenActivity.this, nameValuePairList, "business/pos_setup/add_discount_offer", this, "ADD_DISCOUNT_OFFER", true).execute();

            }


        }
        if ("Cancel".equals(tag_FromWhere)) {
            GiftCarsd_And_CoupenActivity.this.finish();
        }
        if ("backclicked".equals(tag_FromWhere)) {
            GiftCarsd_And_CoupenActivity.this.finish();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        if (activityGiftCarsdAndCoupenBinding.discountStatusSpinnerId == adapterView) {
            discountStstusString = adapterView.getItemAtPosition(position).toString();
        }
        if (activityGiftCarsdAndCoupenBinding.discountCategorySpinnerId == adapterView) {
            discountCategoryString = adapterView.getItemAtPosition(position).toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onSuccess(String result, String type) {
        if ("ADD_DISCOUNT_OFFER".equals(type)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                String message = jsonObject.getString("message");
                if ("Discount coupon created Successfully".equals(message)) {
                    GiftCarsd_And_CoupenActivity.this.finish();
                } else {
                    AlphaHolder.customToast(GiftCarsd_And_CoupenActivity.this, message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }
}
