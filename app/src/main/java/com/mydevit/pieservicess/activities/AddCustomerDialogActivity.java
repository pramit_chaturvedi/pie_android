package com.mydevit.pieservicess.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityAddDialogCustomerBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.model.AddCustomerModel;
import com.mydevit.pieservicess.service.AddCustomer_DialogModel;
import com.mydevit.pieservicess.service.customer_management_model;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;
import com.ybs.countrypicker.CountryDataModel;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class AddCustomerDialogActivity extends AppCompatActivity implements ClickListenerPC, ServiceReponseInterface_Duplicate {

    public static customer_management_model.DataBean dataBean;
    ActivityAddDialogCustomerBinding activityBusinessDialogCustomerBinding;
    List<NameValuePair> nameValuePairList;
    AddCustomerModel addCustomerModel;
    CountryPicker picker;
    CountryDataModel countryDataModel;
    String cartId;

    /* @BindingAdapter("bind:manipulatedialogcustomerAdd")
     public static void manipulatecustomer(RadioGroup radioGroup, String status) {
         if (status.equals("0") || status.equals("")) {
             ((RadioButton) radioGroup.getChildAt(1)).setChecked(true);
         } else {
             ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
         }
         radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
             @Override
             public void onCheckedChanged(RadioGroup radioGroup, int i) {
                 RadioButton radioButton = radioGroup.findViewById(i);
                 String value = radioButton.getText().toString();

                 if ("Active".equals(value)) {
                     dataBean.setStatus("1");
                 } else {
                     dataBean.setStatus("0");

                 }
             }
         });
     }
 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        activityBusinessDialogCustomerBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_dialog_customer);
        selectCompanyCodeInitialization();
        getIntentData();

        addCustomerModel = new AddCustomerModel();
        countryDataModel = new CountryDataModel();
        countryDataModel.setDialCode("+1");
        activityBusinessDialogCustomerBinding.setMainDataObject(addCustomerModel);
        activityBusinessDialogCustomerBinding.setClickListener(this);
    }

    private void getIntentData() {
        cartId = getIntent().getExtras().getString("cartid");
    }

    private void selectCompanyCodeInitialization() {
        picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {

                countryDataModel = new CountryDataModel();
                countryDataModel.setCode(code);
                countryDataModel.setDialCode(dialCode);
                countryDataModel.setFlagDrawableResId(flagDrawableResID);
                countryDataModel.setName(name);
                activityBusinessDialogCustomerBinding.setCountryCodeData(countryDataModel);

                activityBusinessDialogCustomerBinding.countryCodeTextViewId.setText(dialCode);

                //  Bitmap icon = BitmapFactory.decodeResource(getResources(), flagDrawableResID);

                Drawable myIcon = getResources().getDrawable(flagDrawableResID);
                activityBusinessDialogCustomerBinding.countryImageViewId.setBackground(myIcon);
                picker.dismiss();
             /*   EditText countryCode = (EditText) findViewById(R.id.countryCode);
                EditText countryName = (EditText) findViewById(R.id.countryName);
                EditText countryDialCode = (EditText) findViewById(R.id.countryDialCode);
                ImageView countryIcon = (ImageView) findViewById(R.id.countryIcon);

                countryCode.setText(code);
                countryName.setText(name);
                countryDialCode.setText(dialCode);
                countryIcon.setImageResource(flagDrawableResID);

                picker.dismiss();*/

            }
        });
    }

    public void openPicker(View view) {
        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
    }

    @Override
    public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
        if ("savetag".equals(tag_FromWhere)) {
            nameValuePairList = new ArrayList<>();
            if (TextUtils.isEmpty(addCustomerModel.getFirstName())) {
                AlphaHolder.customToast(AddCustomerDialogActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(addCustomerModel.getLastName())) {
                AlphaHolder.customToast(AddCustomerDialogActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(addCustomerModel.getPhoneNo())) {
                AlphaHolder.customToast(AddCustomerDialogActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(addCustomerModel.getEmailString())) {
                AlphaHolder.customToast(AddCustomerDialogActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            }   /*else if (TextUtils.isEmpty(dataBean.getAddress())) {
                AlphaHolder.customToast(AddCustomerDialogActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(dataBean.getCity())) {
                AlphaHolder.customToast(AddCustomerDialogActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(dataBean.getState())) {
                AlphaHolder.customToast(AddCustomerDialogActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(dataBean.getZipcode())) {
                AlphaHolder.customToast(AddCustomerDialogActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(dataBean.getCountry())) {
                AlphaHolder.customToast(AddCustomerActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(dataBean.getLatitude())) {
                AlphaHolder.customToast(AddCustomerActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(dataBean.getLongitude())) {
                AlphaHolder.customToast(AddCustomerActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(dataBean.getInstagram_user())) {
                AlphaHolder.customToast(AddCustomerActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(dataBean.getTwitter_user())) {
                AlphaHolder.customToast(AddCustomerActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(dataBean.getFacebook_user())) {
                AlphaHolder.customToast(AddCustomerActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            }  else if (TextUtils.isEmpty(dataBean.getPassword())) {
                AlphaHolder.customToast(AddCustomerDialogActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            } else if (TextUtils.isEmpty(dataBean.getState())) {
                AlphaHolder.customToast(AddCustomerDialogActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
            }*/ else {
                String selectedCountryNumber = activityBusinessDialogCustomerBinding.countryCodeTextViewId.getText().toString();
                nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(AddCustomerDialogActivity.this)));
                nameValuePairList.add(new BasicNameValuePair("first_name", addCustomerModel.getFirstName()));
                nameValuePairList.add(new BasicNameValuePair("last_name", addCustomerModel.getLastName()));
                nameValuePairList.add(new BasicNameValuePair("phone", addCustomerModel.getPhoneNo()));
                nameValuePairList.add(new BasicNameValuePair("phone_country_code", selectedCountryNumber));
                nameValuePairList.add(new BasicNameValuePair("email", addCustomerModel.getEmailString()));
                nameValuePairList.add(new BasicNameValuePair("cart_id", cartId));

                new Service_Integration_Duplicate(AddCustomerDialogActivity.this, nameValuePairList, "business/pos_setup/save_customer", this, "ADD_DIALOG_CUSTOMER_DATA", true).execute();

            }

        }
        /*if ("Appointments".equals(tag_FromWhere)) {
            Intent intent = new Intent(AddCustomerActivity.this, ClientAppointmentsActivity.class);
            intent.putExtra("customer_id", dataBean.getCustomer_id());
            startActivity(intent);

        }*/
        if ("back".equals(tag_FromWhere)) {
            AddCustomerDialogActivity.this.finish();
        }
    }

    public String valueIfNotNull(String value) {
        if (null == value || "".equals(value)) {
            AlphaHolder.customToast(AddCustomerDialogActivity.this, getResources().getString(R.string.datafieldcannotbeempty));
        } else {
            return value;
        }
        return "";
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onSuccess(String result, String type) {
        if ("ADD_DIALOG_CUSTOMER_DATA".equals(type)) {
            AddCustomer_DialogModel addCustomer_dialogModel = new Gson().fromJson(result, AddCustomer_DialogModel.class);
            if ("Customer Added Successfully".equals(addCustomer_dialogModel.getMessage())) {
                AddCustomerDialogActivity.this.finish();
            }
            /*EditCustomer_DataModel editCustomer_dataModel = new Gson().fromJson(result, EditCustomer_DataModel.class);
            if (editCustomer_dataModel != null && editCustomer_dataModel.getMessage().equals("Customer Added Successfully")) {
                AddCustomerDialogActivity.this.finish();

               *//* dataBean = new customer_management_model.DataBean();
                dataBean.setCustomer_id("");
                dataBean.setFirst_name(editCustomer_dataModel.getData().getFirst_name());
                dataBean.setLast_name(editCustomer_dataModel.getData().getLast_name());
                dataBean.setPhone(editCustomer_dataModel.getData().getPhone());
                dataBean.setMob_verified("");
                dataBean.setEmail(editCustomer_dataModel.getData().getEmail());
                dataBean.setEmail_verified(editCustomer_dataModel.getData().getEmail_verified());
                dataBean.setAddress(editCustomer_dataModel.getData().getAddress());
                dataBean.setLatitude("");
                dataBean.setLongitude("");
                dataBean.setCity(editCustomer_dataModel.getData().getCity());
                dataBean.setState(editCustomer_dataModel.getData().getState());
                dataBean.setCountry("");
                dataBean.setZipcode(editCustomer_dataModel.getData().getZipcode());
                dataBean.setInstagram_user(editCustomer_dataModel.getData().getInstagram_user());
                dataBean.setTwitter_user(editCustomer_dataModel.getData().getTwitter_user());
                dataBean.setFacebook_user(editCustomer_dataModel.getData().getFacebook_user());
                dataBean.setStatus(editCustomer_dataModel.getData().getStatus());
                activityBusinessDialogCustomerBinding.setMainDataObject(dataBean);*//*

            }*/

        }
    }

    @Override
    public void onFailed(String result) {

    }
}
