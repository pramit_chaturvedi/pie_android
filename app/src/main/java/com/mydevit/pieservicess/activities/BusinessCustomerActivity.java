package com.mydevit.pieservicess.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityBusinessCustomerBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListenerPC;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.service.EditCustomer_DataModel;
import com.mydevit.pieservicess.service.customer_management_model;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class BusinessCustomerActivity extends AppCompatActivity implements ClickListenerPC, ServiceReponseInterface_Duplicate {

    public static customer_management_model.DataBean dataBean;
    ActivityBusinessCustomerBinding activityBusinessCustomerBinding;
    List<NameValuePair> nameValuePairList;

    @BindingAdapter("bind:manipulatecustomer")
    public static void manipulatecustomer(RadioGroup radioGroup, String status) {
        if (status.equals("0")) {
            ((RadioButton) radioGroup.getChildAt(1)).setChecked(true);
        } else {
            ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
        }
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton radioButton = radioGroup.findViewById(i);
                String value = radioButton.getText().toString();
                if ("Active".equals(value)) {
                    dataBean.setStatus("1");
                } else {
                    dataBean.setStatus("0");
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        activityBusinessCustomerBinding = DataBindingUtil.setContentView(this, R.layout.activity_business_customer);
        activityBusinessCustomerBinding.setClickListener(this);

        getDataFromIntent();
    }

    private void getDataFromIntent() {
        Object o = getIntent().getSerializableExtra("usetomerDataObject");
        dataBean = (customer_management_model.DataBean) o;
        activityBusinessCustomerBinding.setClickListener(this);
        activityBusinessCustomerBinding.setMainDataObject(dataBean);
    }

    @Override
    public void onClickPC(int parentPosition, int childPosition, Object object, String tag_FromWhere) {
        if ("Save".equals(tag_FromWhere)) {

            nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(BusinessCustomerActivity.this)));
            nameValuePairList.add(new BasicNameValuePair("first_name", dataBean.getFirst_name()));
            nameValuePairList.add(new BasicNameValuePair("last_name", dataBean.getLast_name()));
            nameValuePairList.add(new BasicNameValuePair("phone", dataBean.getPhone()));
            nameValuePairList.add(new BasicNameValuePair("email", dataBean.getEmail()));
            nameValuePairList.add(new BasicNameValuePair("address", dataBean.getAddress()));
            nameValuePairList.add(new BasicNameValuePair("city", dataBean.getCity()));
            nameValuePairList.add(new BasicNameValuePair("state", dataBean.getState()));
            nameValuePairList.add(new BasicNameValuePair("zip_code", dataBean.getZipcode()));
            nameValuePairList.add(new BasicNameValuePair("country", dataBean.getCountry()));
            nameValuePairList.add(new BasicNameValuePair("insta_username", dataBean.getInstagram_user()));
            nameValuePairList.add(new BasicNameValuePair("twitter_username", dataBean.getTwitter_user()));
            nameValuePairList.add(new BasicNameValuePair("facebook_username", dataBean.getFacebook_user()));
            nameValuePairList.add(new BasicNameValuePair("status", dataBean.getStatus()));
            nameValuePairList.add(new BasicNameValuePair("email_verify", dataBean.getEmail_verified()));
            nameValuePairList.add(new BasicNameValuePair("phone_verify", dataBean.getMob_verified()));
            nameValuePairList.add(new BasicNameValuePair("customer_id", dataBean.getCustomer_id()));

            new Service_Integration_Duplicate(BusinessCustomerActivity.this, nameValuePairList, "business/Customer/edit_customer", this, "EDIT_CUSTOMER_DATA", true).execute();

        }
        if ("Appointments".equals(tag_FromWhere)) {
            Intent intent = new Intent(BusinessCustomerActivity.this, ClientAppointmentsActivity.class);
            intent.putExtra("customer_id", dataBean.getCustomer_id());
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
        if ("back".equals(tag_FromWhere)) {
            BusinessCustomerActivity.this.finish();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onSuccess(String result, String type) {
        if ("EDIT_CUSTOMER_DATA".equals(type)) {
            EditCustomer_DataModel editCustomer_dataModel = new Gson().fromJson(result, EditCustomer_DataModel.class);
            if (editCustomer_dataModel != null) {

                dataBean = new customer_management_model.DataBean();
                dataBean.setCustomer_id("");
                dataBean.setFirst_name(editCustomer_dataModel.getData().getFirst_name());
                dataBean.setLast_name(editCustomer_dataModel.getData().getLast_name());
                dataBean.setPhone(editCustomer_dataModel.getData().getPhone());
                dataBean.setMob_verified("");
                dataBean.setEmail(editCustomer_dataModel.getData().getEmail());
                dataBean.setEmail_verified(editCustomer_dataModel.getData().getEmail_verified());
                dataBean.setAddress(editCustomer_dataModel.getData().getAddress());
                dataBean.setLatitude("");
                dataBean.setLongitude("");
                dataBean.setCity(editCustomer_dataModel.getData().getCity());
                dataBean.setState(editCustomer_dataModel.getData().getState());
                dataBean.setCountry("");
                dataBean.setZipcode(editCustomer_dataModel.getData().getZipcode());
                dataBean.setInstagram_user(editCustomer_dataModel.getData().getInstagram_user());
                dataBean.setTwitter_user(editCustomer_dataModel.getData().getTwitter_user());
                dataBean.setFacebook_user(editCustomer_dataModel.getData().getFacebook_user());
                dataBean.setStatus(editCustomer_dataModel.getData().getStatus());

                activityBusinessCustomerBinding.setMainDataObject(dataBean);

            }


        }
    }

    @Override
    public void onFailed(String result) {

    }
}
