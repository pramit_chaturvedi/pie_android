package com.mydevit.pieservicess.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityClientAppointmentsBinding;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.recyclerviewadapters.ClientsAppointments_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.ClientAppointment_ServiceModel;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class ClientAppointmentsActivity extends AppCompatActivity implements ServiceReponseInterface_Duplicate, ClickListener {

    List<NameValuePair> nameValuePairList;
    String customerId;
    List<ClientAppointment_ServiceModel.DataBean> dataBeanList;
    ClientsAppointments_RecyclerViewAdapter clientsAppointments_recyclerViewAdapter;
    ActivityClientAppointmentsBinding activityClientAppointmentsBinding;
    ClientAppointment_ServiceModel clientAppointment_serviceModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityClientAppointmentsBinding = DataBindingUtil.setContentView(this, R.layout.activity_client_appointments);
        activityClientAppointmentsBinding.setClickListener(this);
        getSupportActionBar().hide();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getDataFromService();
    }

    private void getDataFromService() {
        dataBeanList = new ArrayList<>();
        customerId = getIntent().getExtras().getString("customer_id");
        nameValuePairList = new ArrayList<>();

        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(ClientAppointmentsActivity.this)));
        nameValuePairList.add(new BasicNameValuePair("customer_id", customerId));
        new Service_Integration_Duplicate(ClientAppointmentsActivity.this, nameValuePairList, "business/customer/Customer_appointment_history", this, "CLIENT_APPOITMENTS_SERVICE", true).execute();
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("CLIENT_APPOITMENTS_SERVICE".equals(type)) {
            clientAppointment_serviceModel = new Gson().fromJson(result, ClientAppointment_ServiceModel.class);
            if (clientAppointment_serviceModel != null && clientAppointment_serviceModel.getData() != null) {
                dataBeanList.addAll(clientAppointment_serviceModel.getData());
                clientsAppointments_recyclerViewAdapter = new ClientsAppointments_RecyclerViewAdapter(ClientAppointmentsActivity.this, dataBeanList, this);
                activityClientAppointmentsBinding.setMainData(clientsAppointments_recyclerViewAdapter);
            }
        }
    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("back".equals(tag_FromWhere)) {
            ClientAppointmentsActivity.this.finish();
        }
        if ("parentclicked".equals(tag_FromWhere)) {
            Intent intent = new Intent(ClientAppointmentsActivity.this, CustomerAppointmentDetailActivity.class);
            intent.putExtra("customerId", customerId);
            intent.putExtra("appointmentNo", clientAppointment_serviceModel.getData().get(position).getAppointment_number());
            startActivity(intent);
            overridePendingTransition(0, 0);
        }
    }
}
