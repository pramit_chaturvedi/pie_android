package com.mydevit.pieservicess.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.mydevit.pieservicess.BR;
import com.mydevit.pieservicess.R;
import com.mydevit.pieservicess.databinding.ActivityBillBinding;
import com.mydevit.pieservicess.databinding.SelectPaymentTypeBinding;
import com.mydevit.pieservicess.databinding_model.Bill_Model;
import com.mydevit.pieservicess.listenerInterface.Cart_Interface;
import com.mydevit.pieservicess.listenerInterface.ClickListener;
import com.mydevit.pieservicess.listenerInterface.ServiceReponseInterface_Duplicate;
import com.mydevit.pieservicess.model.Cart_Update;
import com.mydevit.pieservicess.model.SelectedAppointment_Model;
import com.mydevit.pieservicess.recyclerviewadapters.BillingRow_RecyclerViewAdapter;
import com.mydevit.pieservicess.recyclerviewadapters.PaymentMode_RecyclerViewAdapter;
import com.mydevit.pieservicess.service.BilllNow_Model;
import com.mydevit.pieservicess.service.CartDelete_ServiceModel;
import com.mydevit.pieservicess.service.Data_cart_model;
import com.mydevit.pieservicess.service.Payment_submit_model;
import com.mydevit.pieservicess.utils.AlphaHolder;
import com.mydevit.pieservicess.utils.Service_Integration_Duplicate;
import com.mydevit.pieservicess.utils.ServicesUtilities.SharedPreferences_Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BillActivity extends AppCompatActivity implements ClickListener, ServiceReponseInterface_Duplicate, Cart_Interface {

    ActivityBillBinding activityBillBinding;
    AlertDialog alertDialog;

    List<NameValuePair> nameValuePairList;
    String location_id, personal_id;
    Payment_submit_model payment_submit_model;

    BillingRow_RecyclerViewAdapter billingRow_recyclerViewAdapter;

    Data_cart_model cart_model;
    List<Data_cart_model.DataBean.CartItemDetailBean> cartItemDetailBeanList;
    int clickedPosition;
    Bill_Model bill_model;
    ServiceReponseInterface_Duplicate serviceReponseInterface_duplicate;
    BilllNow_Model billlNow_model;

    PaymentMode_RecyclerViewAdapter paymentMode_recyclerViewAdapter;
    List<SelectedAppointment_Model> selectedAppointment_List;

    String paymentModeId = null;
    SelectPaymentTypeBinding selectPaymentTypeBinding;
    String cart_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBillBinding = DataBindingUtil.setContentView(this, R.layout.activity_bill);

        bill_model = ViewModelProviders.of(this).get(Bill_Model.class);
        activityBillBinding.setGetData(bill_model);
        activityBillBinding.setLifecycleOwner(this);
        serviceReponseInterface_duplicate = this;
        selectedAppointment_List = new ArrayList<>();
        activityBillBinding.setListner(this);

        getSupportActionBar().hide();
        getData();
        Initialize();
        getDataListner();
    }

    private void getData() {
        cartItemDetailBeanList = new ArrayList<>();
        if (getIntent().getExtras() != null) {
            location_id = getIntent().getExtras().getString("location_id");
            personal_id = getIntent().getExtras().getString("personal_id");
            cart_id = getIntent().getExtras().getString("card_id");
        }
    }

    private void Initialize() {
        nameValuePairList = new ArrayList<>();
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(BillActivity.this)));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
        nameValuePairList.add(new BasicNameValuePair("personnel_id", personal_id));
        new Service_Integration_Duplicate(BillActivity.this, nameValuePairList, "business/pos_setup/pos_checkout_view", this, "POS_CHECKOUT_VIEW", true).execute();

    }

    public void getDataListner() {
        bill_model.getPromoCode().observe(this, new Observer<Bill_Model.Bill>() {
            @Override
            public void onChanged(Bill_Model.Bill bill) {

                //Toast.makeText(BillActivity.this, bill.getPromoCode(), Toast.LENGTH_SHORT).show();
                nameValuePairList = new ArrayList<>();
                nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(BillActivity.this)));
                nameValuePairList.add(new BasicNameValuePair("coupon_code", bill.getPromoCode()));
                nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
                nameValuePairList.add(new BasicNameValuePair("cart_id", cart_model.getData().getCart_detail().getCart_id()));

                new Service_Integration_Duplicate(BillActivity.this, nameValuePairList, "business/pos_setup/pos_apply_coupon", serviceReponseInterface_duplicate, "PROMO_CODE", true).execute();
                //Toast.makeText(BillActivity.this, "APPLY", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showDialogPayment() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BillActivity.this, R.style.CustomAlertDialog);
        ViewGroup viewGroup = ((Activity) BillActivity.this).findViewById(android.R.id.content);
        selectPaymentTypeBinding = DataBindingUtil.inflate(LayoutInflater.from(BillActivity.this), R.layout.select_payment_type, viewGroup, false);
        builder.setView(selectPaymentTypeBinding.getRoot());

        if (billlNow_model.getData().size() > 0) {
            billlNow_model.getData().get(0).setIsChecked("Y");

        }
        paymentModeId = billlNow_model.getData().get(0).getPayment_mode_id();
        paymentMode_recyclerViewAdapter = new PaymentMode_RecyclerViewAdapter(BillActivity.this, billlNow_model.getData(), this::onClick);
        selectPaymentTypeBinding.setPaymentAdapter(paymentMode_recyclerViewAdapter);
        selectPaymentTypeBinding.setClickListener(this);
        selectPaymentTypeBinding.executePendingBindings();

        alertDialog = builder.create();
        alertDialog.show();


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());

        // Set the alert dialog window width and height
        // Set alert dialog width equal to screen width 90%
        // int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 90%
        // int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.9f);

        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

        // Apply the newly created layout parameters to the alert dialog window
        alertDialog.getWindow().setAttributes(layoutParams);
    }

    @Override
    public void onClick(int position, Object object, String tag_FromWhere) {
        if ("DELETE_ITEM".equals(tag_FromWhere)) {
            this.clickedPosition = position;

            nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("cart_id", cart_id));
            nameValuePairList.add(new BasicNameValuePair("cart_item_id", cartItemDetailBeanList.get(position).getCart_item_id()));
            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(BillActivity.this)));

            new Service_Integration_Duplicate(BillActivity.this, nameValuePairList, "business/pos_setup/pos_cart_delete", this, "DELETE_ITEM", true).execute();


        }
        if ("apply".equals(tag_FromWhere)) {
            String promoCode = activityBillBinding.promoEditTextViewId.getText().toString();
            if ("".equals(promoCode)) {
                AlphaHolder.customToast(BillActivity.this, getResources().getString(R.string.enterpromocodefirst));
            } else {
                bill_model.applyClicked();
            }
        }
        if ("billnow".equals(tag_FromWhere)) {
            nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(BillActivity.this)));
            nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
            new Service_Integration_Duplicate(BillActivity.this, nameValuePairList, "business/pos_setup/pos_loc_paymode", this, "billnow", true).execute();

        }
        if ("paymentmodel".equals(tag_FromWhere)) {
            for (int i = 0; i < billlNow_model.getData().size(); i++) {
                if (i == position) {
                    paymentModeId = billlNow_model.getData().get(i).getPayment_mode_id();
                    billlNow_model.getData().get(i).setIsChecked("Y");
                } else {
                    billlNow_model.getData().get(i).setIsChecked("N");
                }
                if (i == billlNow_model.getData().size() - 1)
                    selectPaymentTypeBinding.getPaymentAdapter().notifyDataSetChanged();
            }
        }
        if ("close".equals(tag_FromWhere)) {
            alertDialog.dismiss();
        }
        if ("submitcart".equals(tag_FromWhere)) {
            nameValuePairList = new ArrayList<>();
            nameValuePairList.add(new BasicNameValuePair("cart_id", cart_model.getData().getCart_detail().getCart_id()));
            nameValuePairList.add(new BasicNameValuePair("payment_type", paymentModeId));
            nameValuePairList.add(new BasicNameValuePair("location_id", location_id));
            nameValuePairList.add(new BasicNameValuePair("personnel_id", personal_id));
            nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(BillActivity.this)));
            new Service_Integration_Duplicate(BillActivity.this, nameValuePairList, "business/pos_setup/checkout_confirm_order", this, "SUBMIT_CART", true).execute();

        }
        if ("paymenttypetag".equals(tag_FromWhere)) {
            for (int k = 0; k < billlNow_model.getData().size(); k++) {
                if (k == position) {
                    billlNow_model.getData().get(k).setIsChecked("Y");
                    paymentModeId = billlNow_model.getData().get(k).getPayment_mode_id();
                } else {
                    billlNow_model.getData().get(k).setIsChecked("N");
                }
                if (k == billlNow_model.getData().size() - 1) {
                    selectPaymentTypeBinding.getPaymentAdapter().notifyDataSetChanged();
                }

            }
        }
        if ("back".equals(tag_FromWhere)) {
            BillActivity.this.finish();
        }
    }

    @Override
    public void onSuccess(String result, String type) {
        if ("POS_CHECKOUT_VIEW".equals(type)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                if ("Cart is empty".equals(jsonObject.getString("message"))) {

                } else {
                    cart_model = new Gson().fromJson(result, Data_cart_model.class);
                    if (cart_model != null) {
                        activityBillBinding.setVariable(BR.data, cart_model);
                        cartItemDetailBeanList.addAll(cart_model.getData().getCart_item_detail());
                        billingRow_recyclerViewAdapter = new BillingRow_RecyclerViewAdapter(BillActivity.this, cartItemDetailBeanList, this, this);
                        activityBillBinding.setDataAdapter(billingRow_recyclerViewAdapter);
                        activityBillBinding.executePendingBindings();


                        activityBillBinding.setDiscount(cart_model.getData().getCart_detail().getDiscount_amount());
                        activityBillBinding.setTax(cart_model.getData().getCart_detail().getTax_amount());
                        activityBillBinding.setNetamount(cart_model.getData().getCart_detail().getSub_total());
                        activityBillBinding.setTotal(cart_model.getData().getCart_detail().getTotal());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        if ("inc".equals(type)) {
            Cart_Update cart_update = new Gson().fromJson(result, Cart_Update.class);
            updateManipulations(cart_update);
        }
        if ("dec".equals(type)) {
            Cart_Update cart_update = new Gson().fromJson(result, Cart_Update.class);
            updateManipulations(cart_update);
        }
        if ("DELETE_ITEM".equals(type)) {
            CartDelete_ServiceModel cartDelete_serviceModel = new Gson().fromJson(result, CartDelete_ServiceModel.class);
            if (cartDelete_serviceModel.getMessage().equals("Cart Item deleted successfully")) {
                cartItemDetailBeanList.remove(clickedPosition);
                activityBillBinding.getDataAdapter().notifyItemRemoved(clickedPosition);
                activityBillBinding.getDataAdapter().notifyItemRangeChanged(clickedPosition, cartItemDetailBeanList.size());

                activityBillBinding.setDiscount(cartDelete_serviceModel.getData().getDiscount_amount());
                activityBillBinding.setTax(cartDelete_serviceModel.getData().getTax_amount());
                activityBillBinding.setNetamount(cartDelete_serviceModel.getData().getSub_total());

              /*  float tax = Float.parseFloat(cartDelete_serviceModel.getData().getTax_amount());
                float total = Float.parseFloat(cartDelete_serviceModel.getData().get());
                float grandTotal = tax + total;*/

                activityBillBinding.setTotal(cartDelete_serviceModel.getData().getTotal());

            }
            if ("0.00".equals(cartDelete_serviceModel.getData().getTotal())) {
                BillActivity.this.finish();
            }


        }
        if ("PROMO_CODE".equals(type)) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                jsonObject = jsonObject.getJSONObject("data");
                if (jsonObject.has("message")) {
                    if ("Coupon code not valid".equals(jsonObject.getString("message"))) {
                        AlphaHolder.customToast(BillActivity.this, jsonObject.getString("message"));
                    } else {
                        AlphaHolder.customToast(BillActivity.this, jsonObject.getString("message"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if ("billnow".equals(type)) {
            billlNow_model = new Gson().fromJson(result, BilllNow_Model.class);
            showDialogPayment();
        }
        if ("SUBMIT_CART".equals(type)) {
            payment_submit_model = new Gson().fromJson(result, Payment_submit_model.class);
            if ("Cart Data Show Successfully".equals(payment_submit_model.getMessage())) {
                Intent intent = new Intent(BillActivity.this, ConfirmPaymentActivity.class);
                intent.putExtra("confirmData", result);
                intent.putExtra("location_id", location_id);
                intent.putExtra("personnel_id", personal_id);
                startActivity(intent);
                overridePendingTransition(0, 0);
                alertDialog.dismiss();
                BillActivity.this.finish();
            }
        }
    }

    private void updateManipulations(Cart_Update cart_update) {
        cartItemDetailBeanList.get(clickedPosition).setQuantity(cart_update.getData().getQuantity());
        cart_model.getData().getCart_detail().setDiscount_amount(cart_update.getData().getDiscount_amount());
        cart_model.getData().getCart_detail().setTax_amount(cart_update.getData().getTax_amount());
        cart_model.getData().getCart_detail().setSub_total(cart_update.getData().getSub_total());
        cart_model.getData().getCart_detail().setTotal(cart_update.getData().getTotal());

        activityBillBinding.setVariable(BR.data, cart_model);
        activityBillBinding.getDataAdapter().notifyItemChanged(clickedPosition);
        activityBillBinding.executePendingBindings();

        activityBillBinding.setDiscount(cart_update.getData().getDiscount_amount());
        activityBillBinding.setTax(cart_update.getData().getTax_amount());
        activityBillBinding.setNetamount(cart_update.getData().getSub_total());

        // float tax = Float.parseFloat(cart_update.getData().getTax_amount());
        // float total = Float.parseFloat(cart_update.getData().getTotal());
        //float grandTotal = tax + total;

        activityBillBinding.setTotal(cart_update.getData().getTotal());

    }

    @Override
    public void onFailed(String result) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void plusClicked(int position, int quantity, String itemPrice) {
        Log.e("QUANTITY", "" + quantity);
        clickedPosition = position;
        nameValuePairList = new ArrayList<>();
        updateCert(position, "inc");

    }

    @Override
    public void minusClicked(int position, int quantity, String itemPrice) {
        //Log.e("QUANTITY",""+quantity);
        if (quantity > 1) {
            clickedPosition = position;
            nameValuePairList = new ArrayList<>();
            updateCert(position, "dec");
        }
    }

    public void updateCert(int position, String type) {
        nameValuePairList.add(new BasicNameValuePair("token", SharedPreferences_Util.getToken(BillActivity.this)));
        nameValuePairList.add(new BasicNameValuePair("cart_id", cart_model.getData().getCart_detail().getCart_id()));
        nameValuePairList.add(new BasicNameValuePair("cart_item_id", cart_model.getData().getCart_item_detail().get(position).getCart_item_id()));
        nameValuePairList.add(new BasicNameValuePair("cart_action", type));
        nameValuePairList.add(new BasicNameValuePair("cart_quantity", cart_model.getData().getCart_item_detail().get(position).getItem_qty()));
        nameValuePairList.add(new BasicNameValuePair("item_id", cart_model.getData().getCart_item_detail().get(position).getItem_id()));
        nameValuePairList.add(new BasicNameValuePair("item_type", cart_model.getData().getCart_item_detail().get(position).getItem_type()));
        nameValuePairList.add(new BasicNameValuePair("itempriceid", cart_model.getData().getCart_item_detail().get(position).getItem_price_id()));
        new Service_Integration_Duplicate(BillActivity.this, nameValuePairList, "business/pos_setup/pos_cart_calculations", this, type, true).execute();

    }


}
