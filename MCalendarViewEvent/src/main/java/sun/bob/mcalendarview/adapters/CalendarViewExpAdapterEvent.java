package sun.bob.mcalendarview.adapters;

import android.content.Context;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import sun.bob.mcalendarview.views.ExpCalendarViewEvent;
import sun.bob.mcalendarview.views.MonthExpFragmentEvent;
import sun.bob.mcalendarview.vo.DateDataEvent;

/**
 * Created by 明明大美女 on 2015/12/8.
 */
public class CalendarViewExpAdapterEvent extends FragmentStatePagerAdapter {

    private DateDataEvent date;

    private int dateCellId;
    private int markCellId;
    private boolean hasTitle = true;

    private Context context;
    private int mCurrentPosition = -1;

    public CalendarViewExpAdapterEvent(FragmentManager fm) {
        super(fm);
    }

    public CalendarViewExpAdapterEvent setDate(DateDataEvent date){
        this.date = date;
        return this;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public CalendarViewExpAdapterEvent setDateCellId(int dateCellRes){
        this.dateCellId =  dateCellRes;
        return this;
    }


    public CalendarViewExpAdapterEvent setMarkCellId(int markCellId){
        this.markCellId = markCellId;
        return this;
    }


    @Override
    public Fragment getItem(int position) {
        MonthExpFragmentEvent fragment = new MonthExpFragmentEvent();
        fragment.setData(position, dateCellId, markCellId);
        return fragment;
    }

    @Override
    public int getCount() {
        return 1000;
    }

    public CalendarViewExpAdapterEvent setTitle(boolean hasTitle){
        this.hasTitle = hasTitle;
        return this;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        ((ExpCalendarViewEvent) container).measureCurrentView(position);
    }

    /**
     * 重写该方法，为了刷新页面
     * @param object
     * @return
     */
    @Override
    public int getItemPosition(Object object) {
        if (object.getClass().getName().equals(MonthExpFragmentEvent.class.getName())) {
            return POSITION_NONE;
        }
        return super.getItemPosition(object);
    }

}
