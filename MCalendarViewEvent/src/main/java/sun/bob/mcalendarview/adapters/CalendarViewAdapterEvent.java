package sun.bob.mcalendarview.adapters;

import android.content.Context;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import sun.bob.mcalendarview.fragments.MonthFragmentEvent;
import sun.bob.mcalendarview.MCalendarViewEvent;
import sun.bob.mcalendarview.utils.CalendarUtilEvent;
import sun.bob.mcalendarview.vo.DateDataEvent;
import sun.bob.mcalendarview.vo.MonthDataEvent;

/**
 * Created by bob.sun on 15/8/27.
 */
public class CalendarViewAdapterEvent extends FragmentStatePagerAdapter {

    private DateDataEvent date;

    private int dateCellId;
    private int markCellId;
    private boolean hasTitle = true;

    private Context context;
    private int mCurrentPosition = -1;

    public CalendarViewAdapterEvent(FragmentManager fm) {
        super(fm);
    }

    public CalendarViewAdapterEvent setDate(DateDataEvent date){
        this.date = date;
        return this;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public CalendarViewAdapterEvent setDateCellId(int dateCellRes){
        this.dateCellId =  dateCellRes;
        return this;
    }


    public CalendarViewAdapterEvent setMarkCellId(int markCellId){
        this.markCellId = markCellId;
        return this;
    }


    @Override
    public Fragment getItem(int position) {
        int year = CalendarUtilEvent.position2Year(position);
        int month = CalendarUtilEvent.position2Month(position);

        MonthFragmentEvent fragment = new MonthFragmentEvent();
        fragment.setTitle(hasTitle);
        MonthDataEvent monthDataEvent = new MonthDataEvent(new DateDataEvent(year, month, month / 2), hasTitle);
        fragment.setData(monthDataEvent, dateCellId, markCellId);
        return fragment;
    }
    @Override
    public int getCount() {
        return 1000;
    }

    public CalendarViewAdapterEvent setTitle(boolean hasTitle){
        this.hasTitle = hasTitle;
        return this;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        ((MCalendarViewEvent) container).measureCurrentView(position);
    }
}
