package sun.bob.mcalendarview.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import sun.bob.mcalendarview.MarkStyleEvent;
import sun.bob.mcalendarview.listeners.OnDateClickListenerEvent;
import sun.bob.mcalendarview.utils.CurrentCalendarEvent;
import sun.bob.mcalendarview.views.BaseCellViewEvent;
import sun.bob.mcalendarview.views.BaseMarkViewEvent;
import sun.bob.mcalendarview.views.DefaultCellViewEvent;
import sun.bob.mcalendarview.views.DefaultMarkViewEvent;
import sun.bob.mcalendarview.vo.DayDataEvent;
import sun.bob.mcalendarview.vo.MarkedDatesEvent;

/**
 * Created by bob.sun on 15/8/27.
 */
public class CalendarAdapterEvent extends ArrayAdapter implements Observer {
    private ArrayList data;
    private int cellView = -1;
    private int markView = -1;
    public CalendarAdapterEvent(Context context, int resource, ArrayList data) {
        super(context, resource);
        this.data = data;
        MarkedDatesEvent.getInstance().addObserver(this);
    }

    public CalendarAdapterEvent setCellViews(int cellView, int markView){
        this.cellView = cellView;
        this.markView = markView;
        return this;
    }


    public View getView(int position, View convertView, ViewGroup viewGroup){
        View ret = null;
        DayDataEvent dayDataEvent = (DayDataEvent) data.get(position);
        MarkStyleEvent style = MarkedDatesEvent.getInstance().check(dayDataEvent.getDate());
        boolean marked = style != null;
        if (marked){
            dayDataEvent.getDate().setMarkStyleEvent(style);
            if (markView > 0){
                BaseMarkViewEvent baseMarkView = (BaseMarkViewEvent) View.inflate(getContext(), markView, null);
                baseMarkView.setDisplayText(dayDataEvent);
                ret = baseMarkView;
            } else {
                ret = new DefaultMarkViewEvent(getContext());
                ((DefaultMarkViewEvent) ret).setDisplayText(dayDataEvent);
            }
        } else {
            if (cellView > 0) {
                BaseCellViewEvent baseCellViewEvent = (BaseCellViewEvent) View.inflate(getContext(), cellView, null);
                baseCellViewEvent.setDisplayText(dayDataEvent);
                ret = baseCellViewEvent;
            } else {
                ret = new DefaultCellViewEvent(getContext());
                ((DefaultCellViewEvent) ret).setDisplayText(dayDataEvent);
            }
        }
        ((BaseCellViewEvent) ret).setDate(dayDataEvent.getDate());
        if (OnDateClickListenerEvent.instance != null) {
                ((BaseCellViewEvent) ret).setOnDateClickListener(OnDateClickListenerEvent.instance);
        }
        if (dayDataEvent.getDate().equals(CurrentCalendarEvent.getCurrentDateData())){
            ret.setBackground(MarkStyleEvent.todayBackground);
        }
        return ret;
    }

    @Override
    public int getCount(){
        return data.size();
    }

    @Override
    public void update(Observable observable, Object data) {
        this.notifyDataSetChanged();
    }
}
