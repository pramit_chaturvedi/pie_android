package sun.bob.mcalendarview.utils;

import java.util.Calendar;

import sun.bob.mcalendarview.vo.DateDataEvent;

/**
 * Created by bob.sun on 15/8/27.
 */
public class CurrentCalendarEvent {
    public static DateDataEvent getCurrentDateData(){
        Calendar calendar = Calendar.getInstance();
        return new DateDataEvent(calendar.get(calendar.YEAR), calendar.get(calendar.MONTH) + 1, calendar.get(calendar.DAY_OF_MONTH));
    }

}
