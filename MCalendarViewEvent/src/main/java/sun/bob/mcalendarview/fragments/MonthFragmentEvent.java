package sun.bob.mcalendarview.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.text.DateFormatSymbols;

import sun.bob.mcalendarview.R;
import sun.bob.mcalendarview.adapters.CalendarAdapterEvent;
import sun.bob.mcalendarview.views.MonthViewEvent;
import sun.bob.mcalendarview.vo.MonthDataEvent;

/**
 * Created by bob.sun on 15/8/27.
 */
public class MonthFragmentEvent extends Fragment {
    private MonthDataEvent monthDataEvent;
    private int cellView = -1;
    private int markView = -1;
    private boolean hasTitle = true;

    public void setData(MonthDataEvent monthDataEvent, int cellView, int markView) {
        this.monthDataEvent = monthDataEvent;
        this.cellView = cellView;
        this.markView = markView;
    }

    public void setTitle(boolean hasTitle) {
        this.hasTitle = hasTitle;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        LinearLayout ret = new LinearLayout(getContext());
        ret.setOrientation(LinearLayout.VERTICAL);
        ret.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ret.setGravity(Gravity.CENTER);
        if ((monthDataEvent != null) && (monthDataEvent.getDate() != null)) {
            if (hasTitle) {

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, 40);

                TextView textView = new TextView(getContext());
                textView.setText(getMonth(monthDataEvent.getDate().getMonth())+" "+ monthDataEvent.getDate().getYear());
                textView.setGravity(Gravity.CENTER_HORIZONTAL);
                textView.setTextColor(getResources().getColor(R.color.colorPrimary));
                textView.setTextSize(22);
                textView.setLayoutParams(params);
                textView.setTypeface(null, Typeface.BOLD);

                ret.addView(textView);

            }
            MonthViewEvent monthViewEvent = new MonthViewEvent(getContext());
            monthViewEvent.setAdapter(new CalendarAdapterEvent(getContext(), 1, monthDataEvent.getData()).setCellViews(cellView, markView));
            ret.addView(monthViewEvent);
        }
        return ret;
    }
    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }
}
