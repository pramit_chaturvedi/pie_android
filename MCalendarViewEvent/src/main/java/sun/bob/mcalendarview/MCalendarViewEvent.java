package sun.bob.mcalendarview;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.view.View;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import sun.bob.mcalendarview.adapters.CalendarViewAdapterEvent;
import sun.bob.mcalendarview.listeners.OnDateClickListenerEvent;
import sun.bob.mcalendarview.listeners.OnMonthChangeListenerEvent;
import sun.bob.mcalendarview.utils.CalendarUtilEvent;
import sun.bob.mcalendarview.utils.CurrentCalendarEvent;
import sun.bob.mcalendarview.vo.DateDataEvent;
import sun.bob.mcalendarview.vo.MarkedDatesEvent;

/**
 * Created by bob.sun on 15/8/27.
 */
public class MCalendarViewEvent extends ViewPager {
    private int dateCellViewResId = -1;
    private View dateCellView = null;
    private int markedStyle = -1;
    private int markedCellResId = -1;
    private View markedCellView = null;
    private boolean hasTitle = true;

    private boolean initted = false;

    private DateDataEvent currentDate;
    private CalendarViewAdapterEvent adapter;

    private int width, height;
    private int currentIndex;

    public MCalendarViewEvent(Context context) {
        super(context);
        if (context instanceof FragmentActivity) {
            init((FragmentActivity) context);
        }
    }

    public MCalendarViewEvent(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (context instanceof FragmentActivity) {
            init((FragmentActivity) context);
        }
    }

    public void init(FragmentActivity activity) {
        if (initted) {
            return;
        }
        initted = true;
        if (currentDate == null) {
            currentDate = CurrentCalendarEvent.getCurrentDateData();
        }
        // TODO: 15/8/28 Will this cause trouble when achieved?
        if (this.getId() == View.NO_ID) {
            this.setId(R.id.calendarViewPager);
        }
        adapter = new CalendarViewAdapterEvent(activity.getSupportFragmentManager()).setDate(currentDate);
        this.setAdapter(adapter);
        this.setCurrentItem(500);
//        addBackground();
        float density = getContext().getResources().getSystem().getDisplayMetrics().density;
        CellConfigEvent.cellHeight = getContext().getResources().getSystem().getDisplayMetrics().widthPixels / 7 / density;
        CellConfigEvent.cellWidth = getContext().getResources().getSystem().getDisplayMetrics().widthPixels / 7 / density;
    }

    private void addBackground() {
        ShapeDrawable drawable = new ShapeDrawable(new RectShape());
        drawable.getPaint().setColor(Color.LTGRAY);
        drawable.getPaint().setStyle(Paint.Style.STROKE);
        this.setBackground(drawable);
    }

    //// TODO: 15/8/28 May cause trouble when invoked after inited
    public MCalendarViewEvent travelTo(DateDataEvent dateDataEvent) {
        this.currentDate = dateDataEvent;
        CalendarUtilEvent.date = dateDataEvent;
        this.initted = false;
        init((FragmentActivity) getContext());
        return this;
    }

    public MCalendarViewEvent markDate(int year, int month, int day, int markStyleInt) {
        MarkStyleEvent markStyleEvent = new MarkStyleEvent();
        markStyleEvent.setStyle(markStyleInt);
        MarkedDatesEvent.getInstance().add(new DateDataEvent(year, month, day).setMarkStyleEvent(markStyleEvent));
        return this;
    }

    public MCalendarViewEvent unMarkDate(int year, int month, int day, int markStyleInt) {
        MarkStyleEvent markStyleEvent = new MarkStyleEvent();
        markStyleEvent.setStyle(markStyleInt);
        MarkedDatesEvent.getInstance().remove(new DateDataEvent(year, month, day).setMarkStyleEvent(markStyleEvent));
        return this;
    }

    public MCalendarViewEvent markDate(DateDataEvent date) {
        MarkedDatesEvent.getInstance().add(date);
        return this;
    }

    public MCalendarViewEvent unMarkDate(DateDataEvent date) {
        MarkedDatesEvent.getInstance().remove(date);
        return this;
    }

    public MarkedDatesEvent getMarkedDates() {
        return MarkedDatesEvent.getInstance();
    }

    public MCalendarViewEvent setDateCell(int resId) {
        adapter.setDateCellId(resId);
        return this;
    }

    public MCalendarViewEvent setMarkedStyle(int style, int color) {
        MarkStyleEvent.current = style;
        MarkStyleEvent.defaultColor = color;
        return this;
    }

    public MCalendarViewEvent setMarkedStyle(int style) {
        // MarkStyleEvent.current = style;
        return this;
    }

    public MCalendarViewEvent setMarkedCell(int resId) {
        adapter.setMarkCellId(resId);
        return this;
    }

    public MCalendarViewEvent setOnMonthChangeListener(OnMonthChangeListenerEvent listener) {
        this.addOnPageChangeListener(listener);
        return this;
    }

    public MCalendarViewEvent setOnDateClickListener(OnDateClickListenerEvent onDateClickListenerEvent) {
        OnDateClickListenerEvent.instance = onDateClickListenerEvent;
        return this;
    }

    public MCalendarViewEvent hasTitle(boolean hasTitle) {
        this.hasTitle = hasTitle;
        adapter.setTitle(hasTitle);
        return this;
    }

    @Override
    protected void onMeasure(int measureWidthSpec, int measureHeightSpec) {
        width = measureWidth(measureWidthSpec);
        height = measureHeight(measureHeightSpec);
        measureHeightSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        super.onMeasure(measureWidthSpec, measureHeightSpec);
    }

    private int measureWidth(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        int result = 0;
        if (specMode == MeasureSpec.AT_MOST) {
            float destiney = getContext().getResources().getSystem().getDisplayMetrics().density;
            result = (int) (CellConfigEvent.cellWidth * 7 * destiney);
        } else if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = (int) CellConfigEvent.cellHeight;
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        int result = 0;
        if (specMode == MeasureSpec.AT_MOST) {
            int columns = CalendarUtilEvent.getWeekCount(currentIndex);
            columns = hasTitle ? columns + 1 : columns;
            float density = getContext().getResources().getSystem().getDisplayMetrics().density;
            result = (int) (CellConfigEvent.cellHeight * columns * density);
        } else if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = (int) CellConfigEvent.cellHeight;
        }
        return result;
    }


    public void measureCurrentView(int currentIndex) {
        this.currentIndex = currentIndex;
        requestLayout();
    }
}
