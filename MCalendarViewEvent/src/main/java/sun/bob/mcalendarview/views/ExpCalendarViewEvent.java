package sun.bob.mcalendarview.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import android.util.Log;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import sun.bob.mcalendarview.CellConfigEvent;
import java.util.Calendar;
import sun.bob.mcalendarview.MarkStyleEvent;
import sun.bob.mcalendarview.R;
import sun.bob.mcalendarview.adapters.CalendarViewExpAdapterEvent;
import sun.bob.mcalendarview.listeners.OnDateClickListenerEvent;
import sun.bob.mcalendarview.listeners.OnMonthChangeListenerEvent;
import sun.bob.mcalendarview.listeners.OnMonthScrollListenerEvent;
import sun.bob.mcalendarview.utils.CurrentCalendarEvent;
import sun.bob.mcalendarview.vo.DateDataEvent;
import sun.bob.mcalendarview.vo.MarkedDatesEvent;


/**
 * Created by 明明大美女 on 2015/12/8.
 */
public class ExpCalendarViewEvent extends ViewPager {
    private int dateCellViewResId = -1;
    private View dateCellView = null;
    private int markedStyle = -1;
    private int markedCellResId = -1;
    private View markedCellView = null;
    private boolean hasTitle = true;

    private boolean initted = false;

    private DateDataEvent currentDate;
    private CalendarViewExpAdapterEvent adapter;

    private int width, height;
    private int currentIndex;

    public ExpCalendarViewEvent(Context context) {
        super(context);
        if (context instanceof FragmentActivity) {
            init((FragmentActivity) context);
        }
    }

    public ExpCalendarViewEvent(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (context instanceof FragmentActivity) {
            init((FragmentActivity) context);
        }
    }

    public void init(FragmentActivity activity) {
        if (initted) {
            return;
        }
        initted = true;
        if (currentDate == null) {
            currentDate = CurrentCalendarEvent.getCurrentDateData();
        }
        // TODO: 15/8/28 Will this cause trouble when achieved?
        if (this.getId() == View.NO_ID) {
            this.setId(R.id.calendarViewPager);
        }
        adapter = new CalendarViewExpAdapterEvent(activity.getSupportFragmentManager()).setDate(currentDate);
        this.setAdapter(adapter);
        this.setCurrentItem(500);
//        addBackground();
        float density = getContext().getResources().getSystem().getDisplayMetrics().density;
        CellConfigEvent.cellHeight = getContext().getResources().getSystem().getDisplayMetrics().widthPixels / 7 / density;
        CellConfigEvent.cellWidth = getContext().getResources().getSystem().getDisplayMetrics().widthPixels / 7 / density;

//        this.addOnPageChangeListener(new );
    }

    //// TODO: 15/8/28 May cause trouble when invoked after inited
    public ExpCalendarViewEvent travelTo(DateDataEvent dateDataEvent) {
        // 获得当前页面的年月（position=500）
        Calendar calendar = Calendar.getInstance();
        int thisYear = calendar.get(Calendar.YEAR);
        int thisMonth = calendar.get(Calendar.MONTH);
         int realPosition = 500 + (dateDataEvent.getYear() - thisYear) * 12 + (dateDataEvent.getMonth() - thisMonth - 1);
         if (realPosition > 1000 || realPosition < 0)
             throw new RuntimeException("Please travelto a right date: today-500~today~today+500");

         // 来个步进滑动？因为一次滑个几百页，界面有时候不刷新（蛋疼）
         for (int i = getCurrentItem(); i < realPosition; i=i+50) {
             setCurrentItem(i);
             Log.i("step", " "+i);
         }
         for (int i = getCurrentItem(); i > realPosition; i=i-50) {
             setCurrentItem(i);
             Log.i("step", " "+i);
         }
         setCurrentItem(realPosition);
         // 标记
         MarkedDatesEvent.getInstance().add(dateDataEvent);
          return this;
    }
    public void expand() {
        adapter.notifyDataSetChanged();
    }

    public void shrink() {
        adapter.notifyDataSetChanged();
    }


    public ExpCalendarViewEvent markDate(int year, int month, int day) {
        MarkedDatesEvent.getInstance().add(new DateDataEvent(year, month, day));
        return this;
    }

    public ExpCalendarViewEvent unMarkDate(int year, int month, int day) {
        MarkedDatesEvent.getInstance().remove(new DateDataEvent(year, month, day));
        return this;
    }

    public ExpCalendarViewEvent markDate(DateDataEvent date) {
        MarkedDatesEvent.getInstance().add(date);
        return this;
    }

    public ExpCalendarViewEvent unMarkDate(DateDataEvent date) {
        MarkedDatesEvent.getInstance().remove(date);
        return this;
    }

    public MarkedDatesEvent getMarkedDates() {
        return MarkedDatesEvent.getInstance();
    }

    public ExpCalendarViewEvent setDateCell(int resId) {
        adapter.setDateCellId(resId);
        return this;
    }

    public ExpCalendarViewEvent setMarkedStyle(int style, int color) {
        MarkStyleEvent.current = style;
        MarkStyleEvent.defaultColor = color;
        return this;
    }

    public ExpCalendarViewEvent setMarkedStyle(int style) {
        MarkStyleEvent.current = style;
        return this;
    }

    public ExpCalendarViewEvent setMarkedCell(int resId) {
        adapter.setMarkCellId(resId);
        return this;
    }

    public ExpCalendarViewEvent setOnMonthChangeListener(OnMonthChangeListenerEvent listener) {
        this.addOnPageChangeListener(listener);
        return this;
    }

    public ExpCalendarViewEvent setOnMonthScrollListener(OnMonthScrollListenerEvent listener) {
        this.addOnPageChangeListener(listener);
        return this;
    }

    public ExpCalendarViewEvent setOnDateClickListener(OnDateClickListenerEvent onDateClickListenerEvent) {
        OnDateClickListenerEvent.instance = onDateClickListenerEvent;
        return this;
    }

    public ExpCalendarViewEvent hasTitle(boolean hasTitle) {
        this.hasTitle = hasTitle;
        adapter.setTitle(hasTitle);
        return this;
    }

    @Override
    protected void onMeasure(int measureWidthSpec, int measureHeightSpec) {
        width = measureWidth(measureWidthSpec);
        height = measureHeight(measureHeightSpec);
        measureHeightSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        super.onMeasure(measureWidthSpec, measureHeightSpec);
    }

    private int measureWidth(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        int result = 0;
        if (specMode == MeasureSpec.AT_MOST) {
            float destiney = getContext().getResources().getSystem().getDisplayMetrics().density;
            result = (int) (CellConfigEvent.cellWidth * 7 * destiney);
        } else if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = (int) CellConfigEvent.cellHeight;
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        float density = getContext().getResources().getSystem().getDisplayMetrics().density;
        if (specMode == MeasureSpec.AT_MOST) {
            if (CellConfigEvent.ifMonth)
                return (int) (CellConfigEvent.cellHeight * 6 * density);
            else
                return (int) (CellConfigEvent.cellHeight * density);
        } else if (specMode == MeasureSpec.EXACTLY) {
            return specSize;
        } else {
            return (int) CellConfigEvent.cellHeight;
        }
    }


    public void measureCurrentView(int currentIndex) {
        this.currentIndex = currentIndex;
        requestLayout();
    }
}

