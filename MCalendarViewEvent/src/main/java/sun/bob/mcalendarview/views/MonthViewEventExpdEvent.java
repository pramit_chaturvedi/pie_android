package sun.bob.mcalendarview.views;

import android.content.Context;
import android.util.AttributeSet;

import sun.bob.mcalendarview.adapters.CalendarExpAdapterEvent;
import sun.bob.mcalendarview.vo.MonthWeekDataEvent;

/**
 * Created by Bigflower on 2015/12/8.
 */
public class MonthViewEventExpdEvent extends MonthViewEvent {
    private MonthWeekDataEvent monthWeekDataEvent;
    private CalendarExpAdapterEvent adapter;

    public MonthViewEventExpdEvent(Context context) {
        super(context);
    }

    public MonthViewEventExpdEvent(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void initMonthAdapter(int pagePosition, int cellView, int markView) {
        getMonthWeekData(pagePosition);
        adapter = new CalendarExpAdapterEvent(getContext(), 1, monthWeekDataEvent.getData()).setCellViews(cellView, markView);
        this.setAdapter(adapter);
    }

    private void getMonthWeekData(int position) {
        monthWeekDataEvent = new MonthWeekDataEvent(position);
    }


}
