package sun.bob.mcalendarview.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

import sun.bob.mcalendarview.adapters.CalendarAdapterEvent;
import sun.bob.mcalendarview.vo.MonthDataEvent;

/**
 * Created by bob.sun on 15/8/27.
 */
public class MonthViewEvent extends GridView {
    private MonthDataEvent monthDataEvent;
    private CalendarAdapterEvent adapter;

    public MonthViewEvent(Context context) {
        super(context);
        this.setNumColumns(7);
    }

    public MonthViewEvent(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setNumColumns(7);
    }

    /**
     * @deprecated
     * @param monthDataEvent
     * @return
     */
    public MonthViewEvent displayMonth(MonthDataEvent monthDataEvent){
        adapter = new CalendarAdapterEvent(getContext(), 1, monthDataEvent.getData());
        return this;
    }

}
