package sun.bob.mcalendarview.views;

import android.content.Context;
import android.util.AttributeSet;

import sun.bob.mcalendarview.listeners.OnDateClickListenerEvent;
import sun.bob.mcalendarview.vo.DateDataEvent;

/**
 * Created by bob.sun on 15/8/28.
 */
public abstract class BaseMarkViewEvent extends BaseCellViewEvent {
    private OnDateClickListenerEvent clickListener;
    private DateDataEvent date;

    public BaseMarkViewEvent(Context context) {
        super(context);
    }

    public BaseMarkViewEvent(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

}
