package sun.bob.mcalendarview.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import sun.bob.mcalendarview.CellConfigEvent;
import sun.bob.mcalendarview.listeners.OnDateClickListenerEvent;
import sun.bob.mcalendarview.vo.DateDataEvent;
import sun.bob.mcalendarview.vo.DayDataEvent;

/**
 * Created by bob.sun on 15/8/28.
 */
public abstract class BaseCellViewEvent extends LinearLayout {
    private OnDateClickListenerEvent clickListener;
    private DateDataEvent date;
    int width, height;
    float density;
    float defaultCellSize;

    public BaseCellViewEvent(Context context) {
        super(context);
        density = getContext().getResources().getSystem().getDisplayMetrics().density;
    }

    public BaseCellViewEvent(Context context, AttributeSet attrs) {
        super(context, attrs);
        density = getContext().getResources().getSystem().getDisplayMetrics().density;
    }

    public BaseCellViewEvent setDate(DateDataEvent date){
        this.date = date;
        return this;
    }

    public BaseCellViewEvent setOnDateClickListener(OnDateClickListenerEvent clickListener){
        this.clickListener = clickListener;
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseCellViewEvent.this.clickListener != null) {
                    BaseCellViewEvent.this.clickListener.onDateClick(BaseCellViewEvent.this, date);
                }
            }
        });
        return this;
    }

    public BaseCellViewEvent removeOnDateClickListener(){
        this.clickListener = null;
        return this;
    }
    public OnDateClickListenerEvent getOnDateClickListener(){
        return this.clickListener;
    }

    @Override
    protected void onMeasure(int measureWidthSpec,int measureHeightSpec){
        width = measureWidth(measureWidthSpec);
        height = measureHeight(measureHeightSpec);
        measureHeightSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        measureWidthSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        super.onMeasure(measureWidthSpec, measureHeightSpec);
    }
    private int measureWidth(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        int result = 0;
        if (specMode == MeasureSpec.AT_MOST) {
            result = (int) (CellConfigEvent.cellWidth * density);
        } else if (specMode == MeasureSpec.EXACTLY) {
            result = (int) (specSize * density);
        } else {
            result = (int) CellConfigEvent.cellHeight;
        }
        return result;
    }
    private int measureHeight(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        int result = 0;
        if (specMode == MeasureSpec.AT_MOST) {
            result = (int) (CellConfigEvent.cellHeight * density);
        } else if (specMode == MeasureSpec.EXACTLY) {
            result = (int) (specSize * density);
        } else {
            result = (int) CellConfigEvent.cellHeight;
        }
        return result;
    }
    public abstract void setDisplayText(DayDataEvent day);
}
