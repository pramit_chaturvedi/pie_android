package sun.bob.mcalendarview.vo;

import java.util.ArrayList;
import java.util.Observable;

import sun.bob.mcalendarview.MarkStyleEvent;

/**
 * Created by bob.sun on 15/8/28.
 */
public class MarkedDatesEvent extends Observable {
    private static MarkedDatesEvent staticInstance;
    private ArrayList<DateDataEvent> data;

    public MarkedDatesEvent(){
        super();
        data = new ArrayList<>();
    }

    public static MarkedDatesEvent getInstance(){
        if (staticInstance == null)
            staticInstance = new MarkedDatesEvent();
        return staticInstance;
    }

    public MarkStyleEvent check(DateDataEvent date){
        int index = data.indexOf(date);
        if (index == -1) {
            return null;
        }
        return data.get(index).getMarkStyleEvent();
    }

    public boolean remove(DateDataEvent date){
        this.setChanged();
        this.notifyObservers();
        return data.remove(date);

    }

    public MarkedDatesEvent add(DateDataEvent dateDataEvent){
        data.add(dateDataEvent);
        this.setChanged();
        this.notifyObservers();
        return this;
    }


    public ArrayList<DateDataEvent> getAll(){
        return data;
    }

    public MarkedDatesEvent removeAdd(){
        data.clear();
        this.setChanged();
        this.notifyObservers();
        return this;
    }
}
