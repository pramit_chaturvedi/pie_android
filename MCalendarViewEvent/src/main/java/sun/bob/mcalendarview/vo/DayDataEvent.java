package sun.bob.mcalendarview.vo;

/**
 * Created by bob.sun on 15/8/27.
 */
public class DayDataEvent {

    private DateDataEvent date;
    private int textColor;
    private int textSize;

    public DayDataEvent(DateDataEvent date){
        this.date = date;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    public String getText(){
        return "" + date.getDay();
    }

    public DateDataEvent getDate(){
        return date;
    }

}
