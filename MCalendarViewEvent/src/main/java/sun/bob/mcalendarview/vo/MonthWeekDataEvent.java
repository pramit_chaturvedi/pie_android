package sun.bob.mcalendarview.vo;

import android.graphics.Color;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;

import sun.bob.mcalendarview.CellConfigEvent;


/**
 * Created by Bigflower on 2015/12/8.
 */
public class MonthWeekDataEvent {
    private DateDataEvent pointDate;
    private Calendar calendar;

    private int realPosition;
    private int weekIndex, preNumber, afterNumber;

    private ArrayList<DayDataEvent> monthContent;
    private ArrayList<DayDataEvent> weekContent;

    /**
     * 绝对位置
     *
     * @param position
     */
    public MonthWeekDataEvent(int position) {
        realPosition = position;
        calendar = Calendar.getInstance();
        DateDataEvent today = new DateDataEvent(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        if (CellConfigEvent.m2wPointDate == null) {
            CellConfigEvent.m2wPointDate = today;
        }
        if (CellConfigEvent.w2mPointDate == null) {
            CellConfigEvent.w2mPointDate = today;
        }
        if (CellConfigEvent.weekAnchorPointDate == null) {
            CellConfigEvent.weekAnchorPointDate = today;
        }

        if (CellConfigEvent.ifMonth) {
            getPointDate();
            initMonthArray();
        } else {
            initWeekArray();
        }
    }

    private void getPointDate() {
        // 获得收缩后的那个point
        calendar.set(CellConfigEvent.w2mPointDate.getYear(), CellConfigEvent.w2mPointDate.getMonth() - 1, CellConfigEvent.w2mPointDate.getDay());
        // 获得周的相对滑动的页面差
        int distance = CellConfigEvent.Week2MonthPos - CellConfigEvent.Month2WeekPos;
        calendar.add(Calendar.DATE, distance * 7);
        Log.i("我是Month", "是我变了吗？" + CellConfigEvent.w2mPointDate.toString());
        Log.i("getPointDate", " 基准日期：" + (calendar.get(Calendar.MONTH) + 1) + "月" + calendar.get(Calendar.DAY_OF_MONTH));
        // 判断是否中间页
        if (realPosition == CellConfigEvent.middlePosition) {
            CellConfigEvent.m2wPointDate = new DateDataEvent(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
            Log.v("我是Month", "哈哈当前页面锚点" + CellConfigEvent.m2wPointDate.toString());
        } else {
            calendar.add(Calendar.MONTH, realPosition - CellConfigEvent.Week2MonthPos);
        }
        calendar.set(Calendar.DATE, 1);
        Log.d("getPointDate", " 最终滚动后：" + (calendar.get(Calendar.MONTH) + 1) + "月" + calendar.get(Calendar.DAY_OF_MONTH));

    }

    /**
     * 这个表是确定的6行，所以共有42个数字
     */
    private void initMonthParams() {
        weekIndex = calendar.get(Calendar.DAY_OF_WEEK);
        //我擦 败家的11月有问题，获得星期不对，拟合一下 TODO
//        if (calendar.get(Calendar.MONTH) == 11)
//            weekIndex--;
        preNumber = weekIndex - 1;
        afterNumber = 42 - calendar.getActualMaximum(Calendar.DAY_OF_MONTH) - preNumber;
//        Log.e("initMonthParams", " weekIndex:" + weekIndex);
//        Log.e("initMonthParams", " preNumber:" + preNumber);
//        Log.e("initMonthParams", " afterNumber:" + afterNumber);
    }

    private void initMonthArray() {
        DayDataEvent addDate;
        monthContent = new ArrayList<DayDataEvent>();

        initMonthParams();

        // 本月前面的 上个月的灰色的日期
        calendar.add(Calendar.MONTH, -1);
        int lastMonthDayNumber = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int preDay = lastMonthDayNumber - preNumber + 1; preDay < lastMonthDayNumber + 1; preDay++) {
            addDate = new DayDataEvent(new DateDataEvent(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, preDay));
            addDate.setTextColor(Color.LTGRAY);
            monthContent.add(addDate);
        }

        // 本月的 日期
        calendar.add(Calendar.MONTH, 1);
        int thisMonthDayNumber = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int day = 1; day < thisMonthDayNumber + 1; day++) {
            addDate = new DayDataEvent(new DateDataEvent(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, day));
            addDate.setTextColor(Color.BLACK);
            monthContent.add(addDate);
        }

        // 本月的后面 下个月的灰色的日期
        afterNumber = afterNumber + 1;
        calendar.add(Calendar.MONTH, 1);
        for (int afterDay = 1; afterDay < afterNumber; afterDay++) {
            addDate = new DayDataEvent(new DateDataEvent(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, afterDay));
            addDate.setTextColor(Color.LTGRAY);
            monthContent.add(addDate);
        }
        calendar.add(Calendar.MONTH, -1);
    }

    private void thisMonthArray() {

    }

    private void otherMonthArray() {

    }

    /**
     * Week2MonthPos 与 Month2WeekPos 是关键。
     */
    private void initWeekArray() {
        weekContent = new ArrayList<DayDataEvent>();

        // 下面是：获得上次记录的位置，根据页数位移，判断该页面的锚点（该页是几月份）
        calendar.set(CellConfigEvent.m2wPointDate.getYear(), CellConfigEvent.m2wPointDate.getMonth() - 1, CellConfigEvent.m2wPointDate.getDay());
        if (CellConfigEvent.Week2MonthPos != CellConfigEvent.Month2WeekPos) {
            // 中间页面与今天的相对页数差
            int distance = CellConfigEvent.Month2WeekPos - CellConfigEvent.Week2MonthPos;
            // 滚到当前页
            calendar.add(Calendar.MONTH, distance);
        }
        // 如果是今天的月份，则锚点的日期为今天； 如果不是今天的月份，则锚点的日期为1号
        calendar.set(Calendar.DAY_OF_MONTH, getAnchorDayOfMonth(CellConfigEvent.weekAnchorPointDate));
///////////////////////////////////////////////////////////////////////////////////////////
        // 下面是：获得该页的锚点后，判断三页显示的内容，中间和两边页显示不同
        if (realPosition == CellConfigEvent.Month2WeekPos) {
            ;
        } else {
            calendar.add(Calendar.DATE, (realPosition - CellConfigEvent.Month2WeekPos) * 7);
        }

        // 记录中间页的pointDate
        if (realPosition == CellConfigEvent.middlePosition) {
            CellConfigEvent.w2mPointDate = new DateDataEvent(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
            Log.v("我是Week", " 哈哈当前页面锚点：" + CellConfigEvent.w2mPointDate.toString());
        }

        // 添加数据
        DayDataEvent addDate;
        weekIndex = calendar.get(Calendar.DAY_OF_WEEK);
        calendar.add(Calendar.DATE, -weekIndex + 1);
        for (int i = 0; i < 7; i++) {
            addDate = new DayDataEvent(new DateDataEvent(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH)));
            weekContent.add(addDate);
            calendar.add(Calendar.DATE, 1);
        }
    }

    private int getAnchorDayOfMonth(DateDataEvent date) {
        int thisMonth = Calendar.getInstance().get(Calendar.MONTH);
        int month = date.getMonth() - 1;
        int selectedMonth = calendar.get(Calendar.MONTH);
        if (selectedMonth == month && calendar.get(Calendar.YEAR) == date.getYear()) {
            return date.getDay();
        }

        if (selectedMonth == thisMonth && month != thisMonth) {
            Calendar calendar = Calendar.getInstance();
            return calendar.get(Calendar.DAY_OF_MONTH);
        }

        return 1;
    }

    public ArrayList getData() {
        if (CellConfigEvent.ifMonth)
            return monthContent;
        else
            return weekContent;
    }

}
