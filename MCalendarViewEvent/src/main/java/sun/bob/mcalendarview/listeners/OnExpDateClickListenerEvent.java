package sun.bob.mcalendarview.listeners;

import android.view.View;

import sun.bob.mcalendarview.utils.CurrentCalendarEvent;
import sun.bob.mcalendarview.views.DefaultCellViewEvent;
import sun.bob.mcalendarview.vo.DateDataEvent;

/**
 * Created by Bigflower on 2015/12/10.
 * <p>
 * 分别要对上次的和这次的处理
 * 而今日和其他日也有区别 所以有两个判断
 * 1.对上次的点击判断
 * 2.对这次的点击判断
 */
public class OnExpDateClickListenerEvent extends OnDateClickListenerEvent {

    private View lastClickedView;
    private DateDataEvent lastClickedDate = CurrentCalendarEvent.getCurrentDateData();

    @Override
    public void onDateClick(View view, DateDataEvent date) {

        if(view instanceof DefaultCellViewEvent) {

            // 判断上次的点击
            if (lastClickedView != null) {
                // 节约！
                if (lastClickedView == view)
                    return;
                if (lastClickedDate.equals(CurrentCalendarEvent.getCurrentDateData())) {
                    ((DefaultCellViewEvent) lastClickedView).setDateToday();
                } else {
                    ((DefaultCellViewEvent) lastClickedView).setDateNormal();
                }
            }
            // 判断这次的点击
            ((DefaultCellViewEvent) view).setDateChoose();
            lastClickedView = view;
            lastClickedDate = date;
        }


    }

}
