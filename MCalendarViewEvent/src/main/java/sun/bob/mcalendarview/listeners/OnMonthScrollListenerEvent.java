package sun.bob.mcalendarview.listeners;


import androidx.viewpager.widget.ViewPager;

import sun.bob.mcalendarview.CellConfigEvent;
import sun.bob.mcalendarview.utils.ExpCalendarUtilEvent;
import sun.bob.mcalendarview.vo.DateDataEvent;

/**
 * Created by Bigflower on 2015/12/8.
 *
 * add a onMonthScroll . the aim is for cool effect
 */
public abstract class OnMonthScrollListenerEvent implements ViewPager.OnPageChangeListener {
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        onMonthScroll(positionOffset);
    }

    @Override
    public void onPageSelected(int position) {
        CellConfigEvent.middlePosition = position;
        DateDataEvent date;
        if (CellConfigEvent.ifMonth)
            date = ExpCalendarUtilEvent.position2Month(position);
        else
            date = ExpCalendarUtilEvent.position2Week(position);
        onMonthChange(date.getYear(), date.getMonth());
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public abstract void onMonthChange(int year, int month);

    public abstract void onMonthScroll(float positionOffset);
}
