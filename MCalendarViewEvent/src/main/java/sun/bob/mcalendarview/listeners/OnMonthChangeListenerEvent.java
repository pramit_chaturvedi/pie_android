package sun.bob.mcalendarview.listeners;

import androidx.viewpager.widget.ViewPager;

import sun.bob.mcalendarview.utils.CalendarUtilEvent;

/**
 * Created by bob.sun on 15/8/28.
 */
public abstract class OnMonthChangeListenerEvent implements ViewPager.OnPageChangeListener {
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//        Log.wtf("OnPageScrolled:", ""+position);
    }

    @Override
    public void onPageSelected(int position) {
//        Log.wtf("OnPageSelected:", ""+position);
        onMonthChange(CalendarUtilEvent.position2Year(position), CalendarUtilEvent.position2Month(position));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public abstract void onMonthChange(int year, int month);
}
