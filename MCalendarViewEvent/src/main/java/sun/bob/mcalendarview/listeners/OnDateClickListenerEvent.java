package sun.bob.mcalendarview.listeners;

import android.view.View;

import sun.bob.mcalendarview.vo.DateDataEvent;

/**
 * Created by bob.sun on 15/8/28.
 */
public abstract class OnDateClickListenerEvent {
    public static OnDateClickListenerEvent instance;

    public abstract void onDateClick(View view, DateDataEvent date);
}
